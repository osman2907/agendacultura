<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\SmnPersonas;
use app\models\SiamMedios;
use app\models\SmnCorreos;
use app\models\SmnPersonasCorreos;
use app\models\SmnMediosCorreos;
use app\models\SmnSolicitantes;
use app\models\SmnSolicitantesCorreos;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class SmnCorreosController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actionListarCorreos() {
        $idPersona = $_POST['id_persona'];
        $model = SmnPersonas::findOne($idPersona);
        $personasCorreos = $model->smnPersonasCorreos;
        return $this->renderPartial('listar-correos', compact('personasCorreos'));
    }

    public function actionAgregarCorreos() {
        $model = new SmnCorreos;
        if (isset($_POST['SmnCorreos'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idPersona = $_POST['SmnPersonas']['id'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de correos y asignación de correos a la persona registrada            
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelPersonaCorreo = new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona = $idPersona;
                    $modelPersonaCorreo->id_correo = $model->id;
                    $modelPersonaCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
            exit();
        }
        if (isset($_POST['usuario_medio'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idPersona = $_POST['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de correos y asignación de correos a la persona registrada            
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelPersonaCorreo = new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona = $idPersona;
                    $modelPersonaCorreo->id_correo = $model->id;
                    $modelPersonaCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
                return $modelPersonaCorreo;
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
            exit();
        }
        $idPersona = $_POST['id_persona'];
        $modelPersona = SmnPersonas::findOne($idPersona);
        return $this->renderPartial('agregar-correos', compact('model', 'modelPersona'));
    }

    public function actionAgregarCorreosMippci() {
        $model = new SmnCorreos;
        if (isset($_POST['SmnCorreos'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idPersona = $_POST['SmnCorreos']['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelPersonaCorreo = new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona = $idPersona;
                    $modelPersonaCorreo->id_correo = $model->id;
                    $modelPersonaCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
                return true;
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
        }

        $idPersona = $_POST['id_persona'];
        return $this->renderPartial('agregar-correos-mippci', compact('model', 'idPersona'));
    }

    public function actionValidarCorreos() {
        $correo = Yii::$app->request->post()['valor'];
        $model = new SmnCorreos;
        $model->correo = $correo;
        if (!$model->validate()) {
            print_r($model->errors['correo'][0]);
        }
    }

    public function actionEliminarPersonasCorreos() {
        $idPersona = $_POST['id_persona'];
        $idCorreo = $_POST['id_correo'];
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $modelPersonaCorreo = SmnPersonasCorreos::find()->where(['id_persona' => $idPersona, 'id_correo' => $idCorreo])->one();
            $modelCorreo = SmnCorreos::findOne($idCorreo);
            $modelPersonaCorreo->delete();
            $modelCorreo->delete();
            $transaction->commit();
        } catch (ErrorException $e) {
            $transaction->rollback();
        }
    }

    public function actionEliminarSolicitantesCorreos(){
        $idSolicitante = $_POST['id_solicitante'];
        $idCorreo = $_POST['id_correo'];
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $modelSolicitanteCorreo = SmnSolicitantesCorreos::find()->where(['id_solicitante' => $idSolicitante,'id_correo' => $idCorreo])->one();
            $modelCorreo = SmnCorreos::findOne($idCorreo);
            $modelSolicitanteCorreo->delete();
            $modelCorreo->delete();
            $transaction->commit();
        }catch (ErrorException $e){
            $transaction->rollback();
        }
    }

    public function actionListarCorreosSolicitantes() {
        $idSolicitante = $_POST['id_solicitante'];
        $model = SmnSolicitantes::findOne($idSolicitante);
        $solicitantesCorreos = $model->smnSolicitantesCorreos;
        return $this->renderPartial('listar-correos-solicitantes', compact('solicitantesCorreos'));
    }

    public function actionListarCorreosMedios() {
        $idMedio = $_POST['id_medio'];
        $model = SmnMediosCorreos::find()->where(['id_medio' => $idMedio])->all();
        $mediosCorreos = $model;
        return $this->renderPartial('listar-correos-medios', compact('mediosCorreos'));
    }

    public function actionMostrarCorreosMedios() {
        $idMedio = $_POST['id_medio'];
        $model = SmnMediosCorreos::find()->where(['id_medio' => $idMedio])->all();
        $mediosCorreos = $model;
        return $this->renderPartial('mostrar-correos-medios', compact('mediosCorreos'));
    }

    public function actionAgregarCorreosSolicitantes() {
        $model = new SmnCorreos;
        if (isset($_POST['SmnCorreos'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idSolicitante = $_POST['SmnSolicitantes']['id'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de correos y asignación de correos a la persona registrada            
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelSolicitanteCorreo = new SmnSolicitantesCorreos;
                    $modelSolicitanteCorreo->id_solicitante = $idSolicitante;
                    $modelSolicitanteCorreo->id_correo = $model->id;
                    $modelSolicitanteCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
            exit();
        }
        $idSolicitante = $_POST['id_solicitante'];
        $modelSolicitante = SmnSolicitantes::findOne($idSolicitante);
        return $this->renderPartial('agregar-correos-solicitantes', compact('model', 'modelSolicitante'));
    }

    public function actionListarCorreosPersonas() {
        $idPersona = $_POST['id_persona'];
        $model = SmnPersonasCorreos::find()->where(['id_persona' => $idPersona])->all();
        $personasCorreos = $model;
        return $this->renderPartial('listar-correos-personas', compact('personasCorreos'));
    }

    public function actionAgregarCorreosPersonas() {
        $idPersona = $_POST['id_persona'];
        $model = new SmnCorreos;
        if (isset($_POST['SmnCorreos'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idPersona = $_POST['SmnCorreos']['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelPersonaCorreo = new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona = $idPersona;
                    $modelPersonaCorreo->id_correo = $model->id;
                    $modelPersonaCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
                return true;
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
        }
        return $this->renderPartial('agregar-correos-personas', compact('model', 'idPersona'));
    }

    public function actionMostrarCorreosPersonas() {
        $idPersona = $_POST['id_persona'];
        $model = SmnPersonasCorreos::find()->where(['id_persona' => $idPersona])->all();
        $personasCorreos = $model;
        return $this->renderPartial('mostrar-correos-personas', compact('personasCorreos'));
    }

    public function actionAgregarCorreosMedios() {
        $model = new SmnCorreos;
        if (isset($_POST['SmnCorreos'])) {
            $correo = $_POST['SmnCorreos']['correo'];
            $idmedio = $_POST['id_medio'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de correos y asignación de correos a la persona registrada            
                $model->correo = $correo;
                if ($model->validate()) {
                    $model->save();
                    $modelPersonaCorreo = new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona = $idPersona;
                    $modelPersonaCorreo->id_correo = $model->id;
                    $modelPersonaCorreo->save();
                    $modelMediosCorreo = new SmnMediosCorreos;
                    $modelMediosCorreo->id_medio = $idmedio;
                    $modelMediosCorreo->id_correo = $model->id;
                    $modelMediosCorreo->save();
                } else {
                    throw new ErrorException('Error Registrando Correo' . $model->correo);
                }
                $transaction->commit();
            } catch (ErrorException $e) {
                $transaction->rollback();
            }
            exit();
        }
        $idMedio = $_POST['id_medio'];
        $modelMedios = SmnMediosCorreos::findOne($idMedio);
        return $this->renderPartial('agregar-correos-medios', compact('model', 'modelMedios', 'idMedio'));
    }

    public function actionEliminarCorreoMedio() {
        $idMedio = $_POST['id_medio'];
        $idCorreo = $_POST['id_correo'];
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $modelMedioCorreo = SmnMediosCorreos::find()->where(['id_medio' => $idMedio, 'id_correo' => $idCorreo])->one();
            $modelCorreo = SmnCorreos::findOne($idCorreo);
            $modelMedioCorreo->delete();
            $modelCorreo->delete();
            $transaction->commit();
        } catch (ErrorException $e) {
            $transaction->rollback();
        }
    }


    public function actionNuevosCorreos(){
        return $this->renderPartial('nuevos-correos');
    }

}
