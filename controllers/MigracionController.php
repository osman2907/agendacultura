<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\Ldap;

class MigracionController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actionMigrar(){
    	$ldap = new Ldap();
    	$ruta= 'pensionados/pensionadosJubilados3.csv';
    	$fila = 1;
    	if (($gestor = fopen($ruta, "r")) !== FALSE) {
		    while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE){
		        $numero = count($datos);
		        $fila++;
		        $info=[];

		        /*for ($c=0; $c < $numero; $c++) {
		            echo $datos[$c] . "<br />\n";
		        }*/
		        $cedula=trim($datos[1]);
		        $apellido=trim($datos[2]);
		        $nombre=trim($datos[3]);
		        $tipo=trim($datos[4]);
		        $correo=trim($datos[5]);

		        $resp=$ldap->buscar('pager', $cedula);

		        if($resp['count'] == 1){
		        	//VALIDA SI EXISTE EN EL LDAP
		        	//print_r($resp);
		        	$dn = explode(",", $resp[0]['dn']);
		        	if ($dn[1] != 'ou=personal_jubilado'){
		        		//SI EXISTE EN EL LDAP Y ESTA UN NODO DIFERENTE A personal_jubilado LO MUEVE,
		        		//Y AH SU VEZ LE ACTUALIZA EL CORREO A GMAIL.
		        		$newParent =Yii::$app->params['ldap']['dnJubilados'];
		        		$newRdn = $dn[0];
						
						if(!$ldap->renombrar($resp[0]['dn'],$newRdn,$newParent)){
							echo "Error al mover el usuario de nodo $cedula";
							die;
						}
		        		
		        		$info["mail"][0]=$correo;

		        		if(!$ldap->modificar($newRdn.','.$newParent,$info)){
		        			echo "Error al al cambiar el correo al usuario que se movioo $cedula";
							die;	
		        		}
		        		
		        	}else{
		        		//YA EXISTE EN EL NODO personal_jubilado Y SE LE ACTUALIZA EL CORREO A GMAIL. 
		        		$info["mail"][0]=$correo;
						if(!$ldap->modificar($resp[0]['dn'],$info)){
		        			echo "Error al cambiar el correo al usuario $cedula";
							die;
		        		}
		        	}
		        }elseif($resp['count'] > 1){
		        	echo "Error, existe en varios nodos $cedula"; die;
		        }else{
		        	//SI NO EXISTE EN EL LDAP 
		       		if(!$ldap->registrarJubilado($datos)){
		        		echo "Error registrando al usuario $cedula"; die;
		       		}
		        }

		    //echo $cedula.'&nbsp;'.$apellido.'&nbsp;'.$nombre.'&nbsp;'.$tipo.'&nbsp;'.$correo.'<br><br><br>';   
		    }
		    echo "EXITOSO...";
		    fclose($gestor);
		}
    }


    public function actionListarNodo(){
    	$ldap = new Ldap();
    	$ruta= 'pensionados/personalLDAP.csv';
    	$fila = 1;
    	if (($gestor = fopen($ruta, "r")) !== FALSE){
		    while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE){
		        $numero = count($datos);
		        $prefijo=substr($datos[0],0,3);
		        if($prefijo == 'uid'){
		        	$dn=$datos[0];
		        	$id=explode(",",$dn)[0];
		        	$uid=explode("=",$id)[1];
		        	$resp=$ldap->buscar('uid', $uid);

		        	if(isset($resp[0]['givenname'][0])){
			        	if(isset($resp[0]['pager'][0])){
				        	/*echo "<pre>";
				        	print_r($resp);
				        	echo "</pre>";*/
				        	echo $resp[0]['pager'][0].",".$resp[0]['givenname'][0].",".$resp[0]['sn'][0].",".$resp[0]['mail'][0]."<br>";
				        	//echo "<br><br><br>";
			        	}
			        }
		        }
		        $fila++;
		    }
		}
    }
}

		        //$menciones=str_replace(".", "", $menciones);
		        //$menciones=str_replace(",", "", $menciones);
/*
antes del recorrido
1- conexion
2- metodo vinculacion(conex,dn,contraseña)

durante el recorrido
1-






*/