<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
use app\models\SmnUser;

class SimonController extends Controller{
	public $idVenezuela=1;
	public $idDesTiposMediosSolicitados=127;

	public function llenarSelect($datos){
	    echo "<option value=''>Seleccione</option>";
	    foreach ($datos as $key => $value){
	        echo "<option value=$key>$value</option>";
	    }
	}

	public function llenarGrid($datos){
	    foreach ($datos as $key => $value){
	        echo "<option value=$key>$value</option>";
	    }
	}

	public function cambiarFormatoFecha($fecha){
        if(strpos($fecha,"/")){ //Si está en formato dd/mm/yyyy lo convierte a yyyy-mm-dd
            $fecha=explode("/",$fecha);
            $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
        }else{ //Si está en formato yyyy-mm-dd lo convierte a dd/mm/yyyy
            $fecha=explode("-",$fecha);
            $fecha=$fecha[2]."/".$fecha[1]."/".$fecha[0];
        }
        return $fecha;
    }

    public static function getUsuario($id){
    	$modelUser=new SmnUser;
    	return $modelUser->getUsuarioConectado($id);
    }

}
