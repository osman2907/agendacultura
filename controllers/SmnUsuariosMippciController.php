<?php
namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\SmnUserSearch;
use app\models\SmnUser;
use app\models\SmnPersonas;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\models\Correos;
use app\models\SmnOir;


//use app\models\SmnPersonasSearch;

//use app\models\Ldap;
//use app\models\SiamMedios;

class SmnUsuariosMippciController extends SimonController
{
    /*public function actionUser()
    {
        return $this->render('user');
    }*/

    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }
    
    public function actionIndex(){
        
        $searchModel = new SmnUserSearch();
        $dataProvider = $searchModel->searchMippci(Yii::$app->request->queryParams);

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate(){
        $model = new SmnUser();
        $model->setScenario('registrarUsuariosMippci');

        if (Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if(isset($_POST['SmnUser'])){
            /*echo "<pre>";
            print_r($_POST);
            echo "</pre>";
            exit();*/
            $respuesta=$model->registrarUsuariosMippci($_POST);
            if($respuesta){
                Yii::$app->session->setFlash('success', 'Usuario Mippci registrado con éxito');
                return $this->redirect(['view', 'id' => $respuesta->id]);
            }
        }
        
        $listOirs=SmnOir::listOirs();
        $modelAuthItem=new AuthItem;
        $listRoles=$modelAuthItem->rolesPorGrupo('mippci');
        return $this->render('create',compact('model','listRoles','listOirs'));
    }
    
    public function actionValidarUsuarioMippci(){
        $cedula=$_POST['cedula'];
        $condicion= ['cedula'=> $cedula];
        $persona = SmnPersonas::find()->where($condicion)->all();
        
        if(count($persona) == 0) return false;
        
        $condicion=['id_persona'=>$persona[0]->id];
        $usuario=SmnUser::find()->where($condicion)->all();

        if(count($usuario) == 0) return false;
        
        $modelAuthItem=new AuthItem;
        $roles=$modelAuthItem->rolesPorGrupo('mippci');
        $roles="'".implode("','",$roles)."'";

        $userId=$usuario[0]->id;
        $condicion="item_name IN($roles) AND user_id=$userId";
        $asignaciones=AuthAssignment::find()->where($condicion)->all();
        
        if(count($asignaciones) > 0) return true;
        
        return false;
    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionUpdate($id){  
        $model = $this->findModel($id);
        $model->setScenario('actualizaUsuariosMippci');

        if (Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if(isset($_POST['SmnUser'])){
            $respuesta=$model->actualizarUsuariosMippci($_POST);
            if($respuesta){
                Yii::$app->session->setFlash('success', 'Usuario Mippci registrado con éxito');
                return $this->redirect(['view', 'id' => $respuesta->id]);
            }
        }
        
        $modelAuthItem=new AuthItem;
        $listOirs=SmnOir::listOirs();
        $listRoles=$modelAuthItem->rolesPorGrupo('mippci');
        $model->id_oir=isset($model->usuarioOir)?$model->usuarioOir->id_oir:'';

        return $this->render('update',compact('model','listRoles','listOirs'));
    }
    
    protected function findModel($id)
    {
        if(($model = SmnUser::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDelete($id)
    {   
        $model = $this->findModel($id);
        
            $borroRoles = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $id]);
            
                Yii::$app->session->setFlash('success', 'Usuario Eliminado con éxito');
                return $this->redirect(['index']);

    }
    
    public function actionRegenerarContrasena(){   
        $data = SmnUser::regenerarContrasena($_POST);
        if($data){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
