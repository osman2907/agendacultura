<?php

namespace app\controllers;

use Yii;
use app\models\SmnSolicitantes;
use app\models\SmnSolicitantesSearch;
use app\models\SmnEstructuraGobierno;
use app\models\SmnDireccion;
use app\models\SmnCorreos;
use app\models\SmnTelefonos;
use app\models\SmnPersonas;
use app\models\SmnSolicitantesTelefonos;
use app\models\SmnSolicitantesCorreo;
use app\models\SmnSolicitanteEstructuraGobierno;
use app\models\SmnSolicitantesContactos;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


/**
 * SmnSolicitantesController implements the CRUD actions for SmnSolicitantes model.
 */
class SmnSolicitantesController extends SimonController
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnSolicitantes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmnSolicitantesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnSolicitantes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view',[
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SmnSolicitantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SmnSolicitantes();
        $telefonos = new SmnTelefonos();
        $correos = new SmnCorreos();
        $direccion = new SmnDireccion();

        if(Yii::$app->request->post()){
            $model=$model->registrar($_POST,$model,$direccion);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create',compact('model','telefonos','correos','direccion'));
    }

    /**
     * Updates an existing SmnSolicitantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $telefonos = new SmnTelefonos();
        $correos = new SmnCorreos();

        $direccion = SmnDireccion::find()->where(['id'=>$model->id_direccion])->one();
        if($direccion==NULL){
            $direccion=new SmnDireccion();
        }

        if(Yii::$app->request->post()){
            $model=$model->modificar($_POST,$model,$direccion);
            Yii::$app->session->setFlash('success', 'Ente Adscrito modificado con éxito');
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('update',compact('model','telefonos','correos','direccion'));

    }

    /**
     * Deletes an existing SmnSolicitantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){

        $this->findModel($id)->delete();
        return $this->redirect(['index']);

    }

    /**
     * Finds the SmnSolicitantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnSolicitantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmnSolicitantes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListvicepresidencia(){ 
        $idEstructuraGobierno=Yii::$app->request->post()['idestructuragobierno'];
        $estructuraGobierno=SmnEstructuraGobierno::findOne($idEstructuraGobierno);
        $listVicepresidencias=SmnSolicitantes::listvicepresidencias();
        return $this->renderPartial("viewlistvicepresidencias",compact('estructuraGobierno','listVicepresidencias'));
    }

    public function actionAgregarContacto(){
        $cedula= $_POST['cedula'];
        $solicitanteId=$_POST['solicitante_id'];
        $model=SmnPersonas::buscarPersona($cedula);
        if(!empty($solicitanteId)){
            $modelSolicitanteContacto=new SmnSolicitantesContactos;
            $modelSolicitanteContacto->id_solicitante=$solicitanteId;
            $modelSolicitanteContacto->id_persona=$model->id;
            $modelSolicitanteContacto->save();
        }
        return $this->renderPartial("_filaContacto",compact('model'));
    }

    public function actionRemoverContacto(){
        $idPersona=$_POST['id_persona'];
        $idSolicitante=$_POST['id_solicitante'];

        if(!empty($idSolicitante)){
            $model = SmnSolicitantesContactos::find()->where(['id_solicitante'=>$idSolicitante,'id_persona'=>$idPersona])->one();
            $model->delete();
        }
    }

    public function actionValidarAsociarEstructura(){
        $solicitanteestructuragobierno=SmnSolicitanteEstructuraGobierno::find()->where(
                ['id_solicitante'=>$_POST['id_solicitante'],'id_estructura_gobierno'=>$_POST['id_estructura_gobierno']])
            ->all();
        return count($solicitanteestructuragobierno);
    }

    public function actionAsociarEstructura(){
        $solicitanteestructuragobierno=new SmnSolicitanteEstructuraGobierno;
        $solicitanteestructuragobierno->id_solicitante=$_POST['id_solicitante'];
        $solicitanteestructuragobierno->id_estructura_gobierno=$_POST['id_estructura_gobierno'];
        $solicitanteestructuragobierno->id_vicepresidencia=$_POST['id_vicepresidencia'];
        $solicitanteestructuragobierno->save();

        return $this->renderPartial("_asociarEstructura",compact('solicitanteestructuragobierno'));
    }

    public function actionRemoverAsociarEstructura(){
        $solicitanteestructuragobierno=SmnSolicitanteEstructuraGobierno::find()->where(
                ['id_solicitante'=>$_POST['id_solicitante'],'id_estructura_gobierno'=>$_POST['id_estructura_gobierno']])
            ->one();
        $solicitanteestructuragobierno->delete();
    }


    public function actionValidarRegistroSolicitante(){
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
    }

}
