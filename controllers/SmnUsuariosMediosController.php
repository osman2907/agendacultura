<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\SmnUserSearch;
use app\models\SmnUser;
use app\models\Ldap;
use app\models\SiamMedios;
use app\models\AuthItem;
use app\models\Correos;
use app\models\SmnPersonas;
use app\models\SmnTelefonos;
use app\models\SmnCorreos;
use app\models\SmnUsuariosMedios;
use app\models\AuthAssignment;

class SmnUsuariosMediosController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actionIndex(){
    	$searchModel = new SmnUserSearch();
        $dataProvider = $searchModel->searchMedios(Yii::$app->request->queryParams);

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->id_medio = $model->usuarioMedio->id_medio;

        $personas = new SmnPersonas();
        $telefonos = new SmnTelefonos();
        $correos = new SmnCorreos();
        $listMedios=SiamMedios::listMediosConvocatorias();
        $listRoles=AuthItem::rolesPorGrupo('medios');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(isset($_POST['SmnUser']) and !isset($_POST['roles']))
        Yii::$app->session->setFlash('warning', 'El usuario debe tener al menos un (1) Rol asignado.');
        

        if(isset($_POST['roles']) and isset($_POST['SmnUser']['id_medio'])){
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                echo "<pre>";
                print_r($model->status);
                echo "</pre>";
                if($_POST['roles']){
                    $roles=$_POST['roles'];
                    $borrado = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $model->id]);
                    if(!$this->AsignarRolesMedios($roles, $model->id))
                        throw new ErrorException('Error Asignando los roles al Usuario'); 
                }else{
                    throw new ErrorException('Error Actualizando Usuario.');
                }

                $usuario =SmnUsuariosMedios::findOne(['id_usuario'=>$_POST['SmnUser']['id']]);
                $usuario->id_medio = $_POST['SmnUser']['id_medio'];

                if(!$usuario->update())
                   throw new ErrorException('Error Actualizando Usuario Medio.'); 


                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Usuario Medio Actualizado con éxito');
                return $this->redirect(['view', 'id' => $model->id]);
            }catch(ErrorException $e){
                $transaction->rollback();
            }
        } 
        return $this->render('update',compact('model','telefonos','correos','listRoles','listMedios'));
    }

    public function AsignarRolesMedios($roles, $iduser){
        foreach ($roles as $rol=>$valor){
            $valor = Yii::$app->db->createCommand()->insert(AuthAssignment::tableName(),
                [
                    'item_name' => $rol,
                    'user_id' => $iduser,
                    'created_at' => strtotime(date("Y-m-d"))
                ])->execute();
            
            if(!$valor) return FALSE;
        }
        
        return TRUE;
    }

    public function actionDelete($id){
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        
        $borroRoles = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $id]);
        $UsuariosSolicitantes= SmnUsuariosMedios::deleteAll("id_usuario=:id_usuario", [":id_usuario" => $id]);
        
            Yii::$app->session->setFlash('success', 'Usuario Eliminado con éxito');
            return $this->redirect(['index']);
    }

    public function actionCreate(){
        $model = new SmnUser();
        $model->setScenario('registrarUsuario');

        if (Yii::$app->request->isAjax){ //Validación Ajax para el formulario.
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(isset($_POST['SmnUser'])){ //Si recibimos datos del formulario procedemos a registrar.

            $respuesta = SmnUser::registrarUsuariosMedios($_POST); //Método para registro ubicado en SmnUser.
            if($respuesta){
                        
                $modeloUser = $respuesta;
               
                /*if(!empty($modeloUser->smnUsuarioCorreos)){
                    $this->layout='correos';
                    $correo=new Correos();
                    $correos = array();
                    foreach ($modeloUser->smnUsuarioCorreos as $objeto){
                        array_push($correos, $objeto->correo->correo);
                    }
                    $cuerpo=$this->render("correoRegistroMedios",compact('modeloUser', 'clave'),true);
                    $correo->enviarCorreo("Registro de Usuario",$cuerpo,$correos);    
                }*/
                
                Yii::$app->session->setFlash('success', 'Usuario de Comunicaciones registrado con éxito.');
                return $this->redirect(['view', 'id' => $modeloUser->id]);
            } else {
                    Yii::$app->session->setFlash('danger', 'No se pudo registrar al usuario.');
            }
        }

        $listMedios=SiamMedios::listMediosConvocatorias();
        $modelAuthItem=new AuthItem;
        $listRoles=$modelAuthItem->rolesPorGrupo('medios');
        return $this->render('create',compact('model','listMedios','listRoles'));
    }

    public function actionView($id)
    {
        return $this->render('view',['model' => $this->findModel($id)]);
    }
    
    protected function findModel($id){
        if(($model = SmnUser::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

    public function actionModificarLdap(){
        $ldap=new Ldap();
        $entrada=$ldap->buscar("uid","jfuentes");
        $dn=$entrada[0]['dn'];
        $newRdn="uid=".$entrada[0]['uid'][0];
        $newParent=Yii::$app->params['ldap']['dnSistemas'];

        $ldap->renombrar($dn,$newRdn,$newParent);

        /*echo "<pre>";
        print_r($newRdn);
        echo "</pre>";*/
    }

    public function actionPruebaCorreo(){
        $this->layout='correos';
        $correo=new Correos();
        //$destinatarios=['osman2907@gmail.com' => 'Osman Pérez','guilferlopez@gmail.com'=>'Guilfer López'];
        $destinatarios=['osman2907@gmail.com','guilferlopez@gmail.com'];
        //$destinatarios=['sistemas@mippci.gob.ve' => 'Coordinación de Sistemas'];
        $cuerpo=$this->render("correoRegistro",[],true);
        //return $this->render("correoRegistro",[],true);
        echo "<pre>";
        print_r($correo->enviarCorreo("Registro de Usuario",$cuerpo,$destinatarios));
        echo "</pre>";
    }

}