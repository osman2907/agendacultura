<?php

namespace app\controllers;
use Yii;

use app\models\SmnSolicitantes;
use app\models\SmnSolicitantesContactos;
use app\models\SmnPersonas;
use app\models\SmnConvocatoriasPersonas;
use app\models\SmnTelefonos;
use app\models\SmnCorreos;
use app\models\SmnPersonasTelefonos;
use app\models\SmnPersonasCorreos;

use yii\web\Response;

use app\models\Model;

use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


class SmnSolicitantesContactosController extends SimonController
{
	public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }
	
    public function actionIndex()
    {

    	$modelSolicitantes = new SmnSolicitantesContactos();
    	$listSolicitantes = SmnSolicitantes::listSolicitantes();

        return $this->render('index', [
        	'modelSolicitantes' => $modelSolicitantes,
        	'listSolicitantes' => $listSolicitantes
        	]);
    }

    public function actionContactos () {

    	$idContacto = $_POST['idSolicitante'];

    	$modelPersonas = new SmnSolicitantesContactos();
    	$listContactos = SmnSolicitantesContactos::listContactos($idContacto);

    	return $this->renderPartial('contactos',[
    		'modelPersonas' => $modelPersonas,
    		'listContactos' => $listContactos
    		]);

    }

    public function actionDatacontactos() {

    	$idPersona = $_POST['idPersona'];

        $modelPhone = new SmnTelefonos();
        $modelEmail = new SmnCorreos();
    	$modelDataPersonas = new SmnPersonas();
        $modelConvocatoriasPersonas = new SmnConvocatoriasPersonas();
    	$dataContacto = SmnPersonas::listData($idPersona);
        $convocatoriaPersona = SmnConvocatoriasPersonas::dataConvocatorias($idPersona);
        $countConvocatoria = SmnConvocatoriasPersonas::dataCountConvocatorias($idPersona);

    	return $this->renderPartial('datacontactos',[
    		'modelDataPersonas' => $modelDataPersonas,
            'modelConvocatoriasPersonas' => $modelConvocatoriasPersonas,
            'modelPhone' => $modelPhone,
            'modelEmail' => $modelEmail,
    		'dataContacto' => $dataContacto,
            'convocatoriaPersona' => $convocatoriaPersona,
            'countConvocatoria' => $countConvocatoria
    		]);

    }

    public function actionModificar_persona() {

        $data = $_POST;

        $id = $data['SmnPersonas']['id'];
        $model = SmnPersonas::findOne($id);
        $model->setScenario('modificarPersona');
        $model->load($data);
        $model->save();
        $message_sucessful = \Yii::$app->getSession()->setFlash('exitoso', 'Sus datos han sido actualizados con exito.');
        $message_erro = \Yii::$app->getSession()->setFlash('error', 'Existe algun error por favor intente nuevamente.');

        if ($model->save()):
            return $this->renderPartial('message', [
                'message' => $message_sucessful
                ]);
        else:
            return $this->renderPartial('message', [
                'message' => $message_erro
                ]);
        endif;

    }

}
