<?php

namespace app\controllers;

use Yii;
use app\models\SmnMediosTrabajadores;
use app\models\SmnMediosTrabajadoresSearch;
use app\models\SmnPersonas;
use app\models\SmnUser;
use app\models\SiamMedios;
use app\models\MciDescripcion;
use app\models\MciEstado;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Json;

/**
 * SmnMediosTrabajadoresController implements the CRUD actions for SmnMediosTrabajadores model.
 */
class SmnMediosTrabajadoresController extends SimonController
{

    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new SmnMediosTrabajadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnMediosTrabajadores model.
     * @param integer $id_medio
     * @param integer $id_persona
     * @return mixed
     */
    public function actionView($id_medio, $id_persona)
    {
        return $this->render('view',[
            'model' => $this->findModel($id_medio, $id_persona),
        ]);
    }

    /**
     * Creates a new SmnMediosTrabajadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idPersona=null){
        $model = new SmnMediosTrabajadores();
        $model->setScenario('registrarTrabajador');
        $modelPersona=new SmnPersonas;
        $modelPersona->setScenario("validarCedula");

        if(Yii::$app->request->isAjax){
            if(isset($_POST['SmnPersonas'])){
                $modelPersona->load(Yii::$app->request->post());
                $validaciones=ActiveForm::validate($modelPersona);
                return Json::encode($validaciones);
            }
        }

        //Se ejecuta cuando se recibe sólo la cédula para validar si la persona existe.
        if(isset($_POST['SmnPersonas']['cedula'])){
            $cedula=$_POST['SmnPersonas']['cedula'];
            $modelPersona=SmnPersonas::find()->where(['cedula'=>$cedula])->all();
            $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
            $modelUser=new SmnUser;
            $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

            if($tipoUsuario == 'MEDIOS'){
                $listMediosConvocatorias[$usuario->usuarioMedio->id_medio]=$usuario->usuarioMedio->idMedio->identificacion;
                $model->id_medio=$usuario->usuarioMedio->id_medio;
            }else{
                $listMediosConvocatorias=SiamMedios::listMediosConvocatorias();
            }

            $listCargos=MciDescripcion::listOpciones(Yii::$app->params['listas']['cargosMediosTrabajadores']);
            $listEstados=MciEstado::listEstados();
            if(count($modelPersona) > 0){ //valida que la persona exista
                if(count($modelPersona[0]->smnMediosTrabajadores) > 0){ //Valida que la persona pertenezca a un medio.
                    $identificacion=$modelPersona[0]->smnMediosTrabajadores[0]->idMedio->identificacion;
                    $modelPersona=$modelPersona[0];

                    switch ($tipoUsuario){
                        case 'COMUNICACIONES':
                            Yii::$app->session->setFlash('danger', "Este trabajador ya pertenece a la entidad $identificacion");
                        break;

                        case 'MIPPCI':
                            Yii::$app->session->setFlash('danger', "Este trabajador ya pertenece a la entidad $identificacion si desea cambiarlo puede buscarlo en la bandeja de autoridades y modificarle la entidad.");
                        break;
                    }
                }else{
                    //Muestra la pantalla de registro de personal y carga los datos de la persona ya existente.
                    $modelPersona=$modelPersona[0];
                    return $this->render("create",compact('model','modelPersona','listMediosConvocatorias','listCargos','listEstados'));
                }
            }else{
                //Muestra la pantalla de registro de personal y la persona no existe en la BD.
                $modelPersona=new SmnPersonas;
                $modelPersona->cedula=$cedula;
                return $this->render("create",compact('model','modelPersona','listMediosConvocatorias','listCargos','listEstados'));
            }
        }


        //Se ejecuta cuando ya vienen todos los datos del trabajador desde el formulario
        if(isset($_POST['SmnMediosTrabajadores'])){
            $registro=$model->registrarTrabajador($_POST,$_FILES,$model); //Método ubicado en /models/SmnMediosTrabajadores.php
            if($registro){ //si es diferente de falso registró exitosamente.
                Yii::$app->session->setFlash('success', 'Autoridad registrada con éxito');
                return $this->redirect(['index']);
            }else{ // Si el registro es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al registrar autoridad");
            }
        }

        return $this->render('validar-cedula', compact('modelPersona'));
    }

    /**
     * Updates an existing SmnMediosTrabajadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_medio
     * @param integer $id_persona
     * @return mixed
     */
    public function actionUpdate($id_medio, $id_persona)
    {
        $model = $this->findModel($id_medio, $id_persona);
        $model->setScenario('registrarTrabajador');
        $modelPersona=$model->idPersona;

        if(isset($_POST['SmnMediosTrabajadores'])){
            $modificacion=$model->modificarTrabajador($_POST,$_FILES,$model); //Método ubicado en /models/SmnMediosTrabajadores.php
            if($modificacion){ //si es diferente de falso modificó exitosamente.
                Yii::$app->session->setFlash('success', 'Trabajador modificado con éxito');
                return $this->redirect(['view', 'id_medio' => $model->id_medio, 'id_persona' => $model->id_persona]);
            }else{ // Si la modificacion es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al modificar trabajador");
            }
            exit();
        }

        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        if($tipoUsuario == 'MEDIOS'){
            $listMediosConvocatorias[$usuario->usuarioMedio->id_medio]=$usuario->usuarioMedio->idMedio->identificacion;
        }else{
            $listMediosConvocatorias=SiamMedios::listMediosConvocatorias();
        }

        $listCargos=MciDescripcion::listOpciones(Yii::$app->params['listas']['cargosMediosTrabajadores']);
        $listEstados=MciEstado::listEstados();
        $model->perimetro=$model->getListPerimetro();
        if(!empty($model->fecha_nacimiento)){
            $model->fecha_nacimiento=$this->cambiarFormatoFecha($model->fecha_nacimiento);
        }
        return $this->render('update',compact('model','modelPersona','listMediosConvocatorias','listCargos','listEstados','listPerimetro'));
    }

    /**
     * Deletes an existing SmnMediosTrabajadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_medio
     * @param integer $id_persona
     * @return mixed
     */
    public function actionDelete($id_medio, $id_persona)
    {
        $this->findModel($id_medio, $id_persona)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnMediosTrabajadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_medio
     * @param integer $id_persona
     * @return SmnMediosTrabajadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_medio, $id_persona)
    {
        if (($model = SmnMediosTrabajadores::findOne(['id_medio' => $id_medio, 'id_persona' => $id_persona])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
