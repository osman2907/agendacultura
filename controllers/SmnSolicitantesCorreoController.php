<?php

namespace app\controllers;

use Yii;
use app\models\SmnSolicitantesCorreo;
use app\models\SmnSolicitantesCorreoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmnSolicitantesCorreoController implements the CRUD actions for SmnSolicitantesCorreo model.
 */
class SmnSolicitantesCorreoController extends Controller
{
    
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnSolicitantesCorreo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmnSolicitantesCorreoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnSolicitantesCorreo model.
     * @param integer $id_solicitante
     * @param integer $id_correo
     * @return mixed
     */
    public function actionView($id_solicitante, $id_correo)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_solicitante, $id_correo),
        ]);
    }

    /**
     * Creates a new SmnSolicitantesCorreo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SmnSolicitantesCorreo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_solicitante' => $model->id_solicitante, 'id_correo' => $model->id_correo]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SmnSolicitantesCorreo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_solicitante
     * @param integer $id_correo
     * @return mixed
     */
    public function actionUpdate($id_solicitante, $id_correo)
    {
        $model = $this->findModel($id_solicitante, $id_correo);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_solicitante' => $model->id_solicitante, 'id_correo' => $model->id_correo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SmnSolicitantesCorreo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_solicitante
     * @param integer $id_correo
     * @return mixed
     */
    public function actionDelete($id_solicitante, $id_correo)
    {
        $this->findModel($id_solicitante, $id_correo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnSolicitantesCorreo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_solicitante
     * @param integer $id_correo
     * @return SmnSolicitantesCorreo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_solicitante, $id_correo)
    {
        if (($model = SmnSolicitantesCorreo::findOne(['id_solicitante' => $id_solicitante, 'id_correo' => $id_correo])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
