<?php

namespace app\controllers;

use Yii;
use app\models\SiamMedios;
use app\models\SmnPersonas;
use app\models\SmnMediosContactos;
use app\models\SiamMediosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MciEstado;
use app\models\MciMunicipio;
use app\models\MciParroquia;
use app\models\MciCiudad;
use app\models\SmnDireccion;
use app\models\SmnTelefonos;

/**
 * MediosMediosController implements the CRUD actions for MediosMedios model.
 */
class SiamMediosController extends Controller
{
    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all MediosMedios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiamMediosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MediosMedios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MediosMedios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new MediosMedios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_medio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    public function actionCreate(){
        $model = new SmnPersonas();
        $model->activo=1;
        $direccion = new SmnDireccion();
        $correos = new SmnCorreos();
        $telefonos = new SmnTelefonos();
    
        $listEstados=MciEstado::listEstados();
        $listCiudades=[];
        $listMunicipios=[];
        $listParroquias=[];
    }
    
    /**
     * Updates an existing MediosMedios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_medio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MediosMedios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MediosMedios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MediosMedios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiamMedios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*public function actionPersonaContacto(){
        $model=new SmnCorreos;
        if(isset($_POST['SmnCorreos'])){
            $correo=$_POST['SmnCorreos']['correo'];
            $idPersona=$_POST['SmnPersonas']['id'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de correos y asignación de correos a la persona registrada            
                $model->correo=$correo;
                if($model->validate()){
                    $model->save();
                    $modelPersonaCorreo=new SmnPersonasCorreos;
                    $modelPersonaCorreo->id_persona=$idPersona;
                    $modelPersonaCorreo->id_correo=$model->id;
                    $modelPersonaCorreo->save();
                }else{
                    throw new ErrorException('Error Registrando Correo'.$model->correo);
                }

                $transaction->commit();
            }catch(ErrorException $e){
                $transaction->rollback();
            }
            exit();
        }
    }*/

    public function actionPersonaContacto(){
        if(isset($_POST['SmnPersonas'])){
            $medioId=$_POST['id_medio'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registrar una dirección por persona
                $direccion->load($_POST);
                if($direccion->validate()){
                    $direccion->save();
                }else{
                    throw new ErrorException('Error Registrando Dirección');
                }
                //registrar los datos de la persona asignandole la dirección previamente registrada
                $model->load($_POST);
                $model->id_direccion=$direccion->id;
                if($model->validate()){
                    $model->save();
                }else{
                    throw new ErrorException('Error Registrando los datos de la persona');   
                }
                //registro de correos y asignación de correos a la persona registrada
                foreach ($_POST['SmnCorreos']['correo'] as $itemCorreo){
                    $modelCorreo=new SmnCorreos();
                    $modelCorreo->correo=$itemCorreo;
                    if($modelCorreo->validate()){
                        $modelCorreo->save();
                        $modelPersonaCorreo=new SmnPersonasCorreos;
                        $modelPersonaCorreo->id_persona=$model->id;
                        $modelPersonaCorreo->id_correo=$modelCorreo->id;
                        $modelPersonaCorreo->save();
                    }else{
                        throw new ErrorException('Error Registrando Correo'.$modelCorreo->correo);
                    }
                }
                //registro de teléfonos y asignación de teléfonos a la persona registrada
                foreach ($_POST['SmnTelefonos']['telefono'] as $itemTelefono){
                    $modelTelefono=new SmnTelefonos();
                    $modelTelefono->telefono=$itemTelefono;
                    if($modelTelefono->validate()){
                        $modelTelefono->save();
                        $modelPersonaTelefono=new SmnPersonasTelefonos;
                        $modelPersonaTelefono->id_persona=$model->id;
                        $modelPersonaTelefono->id_telefono=$modelTelefono->id;
                        $modelPersonaTelefono->save();
                    }else{
                        throw new ErrorException('Error Registrando Teléfono'.$modelTelefono->telefono);
                    }
                }
                $modelMedioContacto=new SmnMediosContactos;
                $modelMedioContacto->id_medio=$medioId;
                $modelMedioContacto->id_persona=$model->id;
                $modelMedioContacto->save();
                $modelContacto = SmnPersonas::findOne($model->id);
                $transaction->commit();
                return $this->renderPartial("_filaContacto",compact('model'));
                //return $this->redirect(['view', 'id' => $model->id]);
            }catch(ErrorException $e){
                $transaction->rollback();
            }
        }
    }

    public function actionAsociarContacto(){
        $PersonaId= $_POST['id_persona'];
        $medioId=$_POST['id_medio'];
        $modelMedioContacto=new SmnMediosContactos;
        $modelMedioContacto->id_medio=$medioId;
        $modelMedioContacto->id_persona=$PersonaId;
        $modelMedioContacto->save();
        $model = SmnPersonas::findOne($PersonaId);  
        return $this->renderPartial("_filaContacto",compact('model'));
    }

    public function actionValidarContacto(){
        $idMedio=$_POST['id_medio'];
        $idPersona=$_POST['id_persona'];

        $model=SmnMediosContactos::find()->where(['id_medio'=>$idMedio,'id_persona'=>$idPersona])->all();
        return count($model)>=1?true:false;
    }

    public function actionRemoverContacto(){
        $PersonaId= $_POST['id_persona'];
        $medioId=$_POST['id_medio'];

        if(!empty($medioId)){
            $model = SmnMediosContactos::find()->where(['id_medio'=>$medioId,'id_persona'=>$PersonaId])->one();
            $model->delete();
        }
    }
}

