<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\SmnPersonas;
use app\models\SiamMedios;
use app\models\SmnTelefonos;
use app\models\SmnPersonasTelefonos;
use app\models\SmnMediosTelefonos;
use app\models\SmnSolicitantes;
use app\models\SmnSolicitantesTelefonos;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class SmnTelefonosController extends \yii\web\Controller
{
    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actionListarTelefonos(){
        $idPersona=$_POST['id_persona'];
        $model = SmnPersonas::findOne($idPersona);
        $personasTelefonos=$model->smnPersonasTelefonos;
        return $this->renderPartial('listar-telefonos',compact('personasTelefonos'));
    }

    public function actionListarTelefonosMedios(){
        $idMedio=$_POST['id_medio'];
        $model = SmnMediosTelefonos::find()->where(['id_medio'=>$idMedio])->all();
        $MediosTelefonos=$model;
        return $this->renderPartial('listar-telefonos-medios',compact('MediosTelefonos'));
    }

    public function actionMostrarTelefonosMedios(){
        $idMedio=$_POST['id_medio'];
        $model = SmnMediosTelefonos::find()->where(['id_medio'=>$idMedio])->all();
        $MediosTelefonos=$model;
        return $this->renderPartial('mostrar-telefonos-medios',compact('MediosTelefonos'));
    }
    
    public function actionMostrarTelefonosSolicitantes(){
        $idSolicitante=$_POST['id_solicitante'];
        $model = SmnMediosTelefonos::find()->where(['id_solicitante'=>$idSolicitante])->all();
        $SolicitantesTelefonos=$model;
        return $this->renderPartial('mostrar-telefonos-solicitantes',compact('SolicitantesTelefonos'));
    }

    public function actionBuscarTelefono(){
        $id=$_POST['id'];
        $modelTelefono= SmnPersonasTelefonos::find()->where(['id_persona'=>$id])->one();

    }

    public function actionAgregarTelefonosMippci(){
        if(isset($_POST['id_persona'])){
            $idPersona = $_POST['id_persona'];
        }else{
            $idPersona = $_POST['SmnTelefonos']['id_persona'];
        }
        $model=new SmnTelefonos;
        
        if(isset($_POST['SmnTelefonos'])){
            $telefono=$_POST['SmnTelefonos']['telefono'];
            $idPersona=$_POST['SmnTelefonos']['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();
                    $modelPersonaTelefono = new smnPersonasTelefonos;
                    $modelPersonaTelefono->id_persona = $idPersona;
                    $modelPersonaTelefono->id_telefono = $model->id;
                    $modelPersonaTelefono->save();
                }else{
                    throw new ErrorException('Error Registrando Teléfono'.$model->telefono);
                }

                $transaction->commit();
                return true;
                //$this->redirect(array(Url::previous()));
            }catch(ErrorException $e){
                $transaction->rollback();
            }
        }
        return $this->renderPartial('agregar-telefonos-mippci',compact('model','idPersona'));
    }
    
    public function actionAgregarTelefonos(){
    	$model=new SmnTelefonos;
    	if(isset($_POST['SmnTelefonos'])){
    		$telefono=$_POST['SmnTelefonos']['telefono'];
    		$idPersona=$_POST['SmnPersonas']['id'];
    		$connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de telefonos y asignación de telefonos a la persona registrada            
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();
                    $modelPersonaTelefono=new smnPersonasTelefonos;
                    $modelPersonaTelefono->id_persona=$idPersona;
                    $modelPersonaTelefono->id_telefono=$model->id;
                    $modelPersonaTelefono->save();
                }else{
                    throw new ErrorException('Error Registrando Telefono'.$model->telefono);
                }

                $transaction->commit();
            }catch(ErrorException $e){
                $transaction->rollback();

            }
            exit();
    	}

        if(isset($_POST['usuario_medio'])){
            $correo=$_POST['SmnTelefonos']['telefono'];
            $idPersona=$_POST['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{
                //registro de correos y asignación de correos a la persona registrada            
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();
                    $modelPersonaTelefono=new smnPersonasTelefonos;
                    $modelPersonaTelefono->id_persona=$idPersona;
                    $modelPersonaTelefono->id_telefono=$model->id;
                    $modelPersonaTelefono->save();
                    die();
                }else{
                    throw new ErrorException('Error Registrando Correo'.$model->correo);
                }

                $transaction->commit();
                return $modelPersonaTelefono;
            }catch(ErrorException $e){
                $transaction->rollback();
            }
            exit();
        }

    	$idPersona=$_POST['id_persona'];
    	$modelPersona=SmnPersonas::findOne($idPersona);

    	return $this->renderPartial('agregar-telefonos',compact('model','modelPersona'));
    }

	public function actionValidarTelefonos(){
    	$telefono=Yii::$app->request->post()['valor'];
        $model=new SmnTelefonos;
      	$model->telefono=$telefono;
       	if(!$model->validate()){
            print_r($model->errors['telefono'][0]);
        }
    }

    public function actionEliminarPersonasTelefonos(){
        $idPersona=$_POST['id_persona'];
        $idTelefono=$_POST['id_telefono'];
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {           
            $modelPersonaTelefono=SmnPersonasTelefonos::find()->where(['id_persona'=>$idPersona,'id_telefono'=>$idTelefono])->one();
            $modelTelefono=SmnTelefonos::findOne($idTelefono);
            $modelPersonaTelefono->delete();
            $modelTelefono->delete();
            $transaction->commit();
        }catch(ErrorException $e){
            $transaction->rollback();
        }
    }


    public function actionEliminarSolicitantesTelefonos(){
        $idSolicitante=$_POST['id_solicitante'];
        $idTelefono=$_POST['id_telefono'];
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $modelSolicitanteTelefono=SmnSolicitantesTelefonos::find()->where(['id_solicitante'=>$idSolicitante,'id_telefono'=>$idTelefono])->one();
            $modelTelefono=SmnTelefonos::findOne($idTelefono);
            $modelSolicitanteTelefono->delete();
            $modelTelefono->delete();
            $transaction->commit();
        }catch(ErrorException $e){
            $transaction->rollback();
        }
    }


    public function actionListarTelefonosSolicitantes(){
        $idSolicitante=$_POST['id_solicitante'];
        $model = SmnSolicitantes::findOne($idSolicitante);
        $solicitantesTelefonos=$model->smnSolicitantesTelefonos;
        return $this->renderPartial('listar-telefonos-solicitantes',compact('solicitantesTelefonos'));
    }

    public function actionAgregarTelefonosSolicitantes(){
        $model=new SmnTelefonos;
        if(isset($_POST['SmnTelefonos'])){
            $telefono=$_POST['SmnTelefonos']['telefono'];
            $idSolicitante=$_POST['SmnSolicitantes']['id'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de telefonos y asignación de telefonos a la persona registrada            
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();
                    $modelSolicitanteTelefono=new smnSolicitantesTelefonos;
                    $modelSolicitanteTelefono->id_solicitante=$idSolicitante;
                    $modelSolicitanteTelefono->id_telefono=$model->id;
                    $modelSolicitanteTelefono->save();
                }else{
                    throw new ErrorException('Error Registrando Telefono'.$model->telefono);
                }

                $transaction->commit();
            }catch(ErrorException $e){
                $transaction->rollback();

            }
            exit();
        }

        $idSolicitante=$_POST['id_solicitante'];
        $modelSolicitante=SmnSolicitantes::findOne($idSolicitante);

        return $this->renderPartial('agregar-telefonos-solicitantes',compact('model','modelSolicitante'));
    }
    
        public function actionListarTelefonosPersonas(){
        $idPersona=$_POST['id_persona'];
        $model = SmnPersonas::findOne($idPersona);
        $personasTelefonos=$model->smnPersonasTelefonos;
        return $this->renderPartial('listar-telefonos-personas',compact('personasTelefonos'));
    }


    public function actionAgregarTelefonosPersonas(){
        $idPersona = $_POST['id_persona'];
        $model=new SmnTelefonos;
        
        if(isset($_POST['SmnTelefonos'])){
            $telefono=$_POST['SmnTelefonos']['telefono'];
            $idPersona=$_POST['SmnTelefonos']['id_persona'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();
                    $modelPersonaTelefono = new smnPersonasTelefonos;
                    $modelPersonaTelefono->id_persona = $idPersona;
                    $modelPersonaTelefono->id_telefono = $model->id;
                    $modelPersonaTelefono->save();
                }else{
                    throw new ErrorException('Error Registrando Teléfono'.$model->telefono);
                }

                $transaction->commit();
                return true;
                //$this->redirect(array(Url::previous()));
            }catch(ErrorException $e){
                $transaction->rollback();
            }
        }
        return $this->renderPartial('agregar-telefonos-personas',compact('model','idPersona'));
    }

    public function actionAgregarTelefonosMedios(){
        $model=new SmnTelefonos;
        if(isset($_POST['SmnTelefonos'])){
            $telefono=$_POST['SiamMedios']['id_telefonos'];
            $idmedio=$_POST['SiamMedios']['id_medio'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                //registro de telefonos y asignación de telefonos a la persona registrada            
                $model->telefono=$telefono;
                if($model->validate()){
                    $model->save();

                    $modelPersonaTelefono=new smnPersonasTelefonos;
                    $modelPersonaTelefono->id_persona=$idPersona;
                    $modelPersonaTelefono->id_telefono=$model->id;
                    $modelPersonaTelefono->save();
                    $modelMediosTelefono=new SmnMediosTelefonos;
                    $modelMediosTelefono->id_medio=$idmedio;
                    $modelMediosTelefono->id_telefono=$model->id;
                    $modelMediosTelefono->save();

                }else{
                    throw new ErrorException('Error Registrando Telefono'.$model->telefono);
                }

                $transaction->commit();
            }catch(ErrorException $e){
                $transaction->rollback();

            }
            exit();
        }

        //$idPersona=$_POST['id_persona'];
        //$modelPersona=SmnPersonas::findOne($idPersona);

        //return $this->renderPartial('agregar-telefonos-personas',compact('model','modelPersona'));

        $idMedio=$_POST['id_medio'];
        $modelMedios=SiamMedios::findOne($idMedio);
    
        return $this->renderPartial('agregar-telefonos-medios',compact('model','modelMedios'));
    }

    public function actionEliminarTelefonoMedio(){
        $idMedio=$_POST['id_medio'];
        $idTelefono=$_POST['id_telefono'];

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $modelMediosTelefono=SmnMediosTelefonos::find()->where(['id_medio'=>$idMedio,'id_telefono'=>$idTelefono])->one();
            $modelCorreo=SmnTelefonos::findOne($idTelefono);
            $modelMediosTelefono->delete();
            $modelCorreo->delete();
            $transaction->commit();
        }catch(ErrorException $e){
            $transaction->rollback();
        }

    }


    public function actionNuevosTelefonos(){
        return $this->renderPartial('nuevos-telefonos');
    }
}
