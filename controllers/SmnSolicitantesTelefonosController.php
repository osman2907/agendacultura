<?php

namespace app\controllers;

use Yii;
use app\models\SmnSolicitantesTelefonos;
use app\models\SmnSolicitantesTelefonosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmnSolicitantesTelefonosController implements the CRUD actions for SmnSolicitantesTelefonos model.
 */
class SmnSolicitantesTelefonosController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnSolicitantesTelefonos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmnSolicitantesTelefonosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnSolicitantesTelefonos model.
     * @param integer $id_solicitante
     * @param integer $id_telefono
     * @return mixed
     */
    public function actionView($id_solicitante, $id_telefono)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_solicitante, $id_telefono),
        ]);
    }

    /**
     * Creates a new SmnSolicitantesTelefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SmnSolicitantesTelefonos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_solicitante' => $model->id_solicitante, 'id_telefono' => $model->id_telefono]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SmnSolicitantesTelefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_solicitante
     * @param integer $id_telefono
     * @return mixed
     */
    public function actionUpdate($id_solicitante, $id_telefono)
    {
        $model = $this->findModel($id_solicitante, $id_telefono);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_solicitante' => $model->id_solicitante, 'id_telefono' => $model->id_telefono]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SmnSolicitantesTelefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_solicitante
     * @param integer $id_telefono
     * @return mixed
     */
    public function actionDelete($id_solicitante, $id_telefono)
    {
        $this->findModel($id_solicitante, $id_telefono)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnSolicitantesTelefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_solicitante
     * @param integer $id_telefono
     * @return SmnSolicitantesTelefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_solicitante, $id_telefono)
    {
        if (($model = SmnSolicitantesTelefonos::findOne(['id_solicitante' => $id_solicitante, 'id_telefono' => $id_telefono])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
