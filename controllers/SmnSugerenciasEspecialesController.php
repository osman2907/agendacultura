<?php

namespace app\controllers;

use Yii;
use app\models\SmnSugerenciasEspeciales;
use app\models\SmnSugerenciasEspecialesSearch;
use app\models\MciDescripcion;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmnSugerenciasEspecialesController implements the CRUD actions for SmnSugerenciasEspeciales model.
 */
class SmnSugerenciasEspecialesController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnSugerenciasEspeciales models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmnSugerenciasEspecialesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnSugerenciasEspeciales model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SmnSugerenciasEspeciales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SmnSugerenciasEspeciales();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            $listSectores = MciDescripcion::listSectores();

            return $this->render('create', [
                'model' => $model,
                'listSectores' => $listSectores
            ]);
        }
    }

    /**
     * Updates an existing SmnSugerenciasEspeciales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            $listSectores = MciDescripcion::listSectores();

            return $this->render('update', [
                'model' => $model,
                'listSectores' => $listSectores
            ]);
        }
    }

    /**
     * Deletes an existing SmnSugerenciasEspeciales model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnSugerenciasEspeciales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnSugerenciasEspeciales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmnSugerenciasEspeciales::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
