<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
//use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RecuperarPassword;
use app\models\SmnUser;
use app\models\Ldap;
use webvimark\modules\UserManagement\models\forms\LoginForm;

class SiteController extends SimonController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()){
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionRecuperarpassword()
    { 
        $model = new RecuperarPassword();
        if ($model->load(Yii::$app->request->post()) && $model->recuperarPassword(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('recuperarPasswordSubmitted');

            return $this->refresh();
        }
        return $this->render('recuperarPassword', [
            'model' => $model,
        ]);
    }

    public function actionCambiarContrasena(){
        $modelUser=new SmnUser;
        $usuario=$modelUser->getUsuarioConectado(Yii::$app->user->id);
        if (Yii::$app->user->isGuest || $usuario->superadmin){
            return $this->goHome();
        }

        if(isset($_POST['usuario'])){
            $usuario=$_POST['usuario']['username'];
            $contrasenaNueva=$_POST['usuario']['contrasena_nueva'];
            $modelUser=new SmnUser;
            
            if($modelUser->cambiarContrasena($usuario,$contrasenaNueva)){
                Yii::$app->session->setFlash('success', 'Contraseña modificada exitosamente');
            }else{
                Yii::$app->session->setFlash('success', 'Error modificando contraseña, intente nuevamente');
            }
        }

        $modelUsuario=$this->getUsuario(Yii::$app->user->id);
        return $this->render('cambiar-contrasena',compact('modelUsuario'));
    }

    public function actionValidarContrasena(){
        $modelLogin=new LoginForm;
        $usuario=$_POST['usuario']['username'];
        $contrasenaActual=$_POST['usuario']['contrasena_actual'];
        $modelLogin->username=$usuario;
        $modelLogin->password=$contrasenaActual;
        return $modelLogin->login();
    }

    public function actionValidarSesion(){
        if(!Yii::$app->user->isGuest){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
