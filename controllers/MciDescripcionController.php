<?php

namespace app\controllers;

use Yii;
use app\models\MciDescripcion;
use app\models\MciDescripcionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MciDescripcionController implements the CRUD actions for MciDescripcion model.
 */
class MciDescripcionController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all MciDescripcion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MciDescripcionSearch();
        $searchModel->id_padre=0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MciDescripcion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionListarhijos($id)
    {
        //$hijos=MciDescripcion::find()->where(['id_padre' => $id])->orderBy('id_descripcion')->all();
        $searchModel = new MciDescripcionSearch();
        $searchModel->id_padre=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=5;
         return $this->render('listarHijos', [
            'model'=>$this->findModel($id),
            'searchModel' => $searchModel,'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Creates a new MciDescripcion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MciDescripcion();
        if ($model->load(Yii::$app->request->post()))
        {
            $model->id_padre=0;
            $model->cant_hijos=0;
            if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id_descripcion]);
            } else {
            return $this->render('create', [
                'model' => $model,
            ]);    
            }   
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionCrearhijos($id)
    {
        $model = new MciDescripcion();

        if ($model->load(Yii::$app->request->post()))
        {   $model->id_padre=$id;
            $model->cant_hijos=0;
            if ($model->save()) {
            return $this->redirect(['listarhijos', 'id' => $model->id_padre]);
            } else {
            return $this->render('create', [
                'model' => $model,
            ]);    
            }   
        } else {
            $model->id_padre=$id;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing MciDescripcion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_descripcion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MciDescripcion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MciDescripcion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MciDescripcion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MciDescripcion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
