<?php

namespace app\controllers;

use Yii;
use app\models\SmnOir;
use app\models\SmnOirSearch;
use app\models\MciEstado;
use app\models\MciCiudad;
use app\models\MciMunicipio;
use app\models\MciParroquia;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmnOirController implements the CRUD actions for SmnOir model.
 */
class SmnOirController extends Controller
{

    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SmnOirSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnOir model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SmnOir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new SmnOir();

        if (isset($_POST['SmnOir'])){
            $registrar=$model->registrar($_POST);
            if($registrar){ //Si registrar retorna distinto de false mostramos el registro creado
                return $this->redirect(['view', 'id' => $registrar->id]);
            }
        }

        $listEstados=MciEstado::listEstados();
        return $this->render('create',compact('model','listEstados'));
    }

    /**
     * Updates an existing SmnOir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(isset($_POST['SmnOir'])){
            $modificar=$model->modificar($_POST,$model);
            if($modificar){ //Si modificar retorna distinto de false mostramos el registro creado
                Yii::$app->session->setFlash("success","Oficina de Información Regional modificada con éxito");
                return $this->redirect(['view', 'id' => $modificar->id]);
            }
            Yii::$app->session->setFlash("danger","Error al modificar OIR");
        }
        
        $model->setZonasInfluencia();
        $model->id_estado=$model->idDireccion->id_estado;
        $model->id_ciudad=$model->idDireccion->id_ciudad;
        $model->id_municipio=$model->idDireccion->id_municipio;
        $model->id_parroquia=$model->idDireccion->id_parroquia;
        $model->direccion=$model->idDireccion->direccion;
        $listEstados=MciEstado::listEstados();
        $listCiudades=MciCiudad::listCiudades($model->idDireccion->id_estado);
        $listMunicipios=MciMunicipio::listMunicipios($model->idDireccion->id_estado);
        $listParroquias=MciParroquia::listParroquias($model->idDireccion->id_municipio);
        return $this->render('update',compact('model','listEstados','listCiudades','listMunicipios','listParroquias'));
    }

    /**
     * Deletes an existing SmnOir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnOir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnOir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmnOir::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
