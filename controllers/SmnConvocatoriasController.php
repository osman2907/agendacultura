<?php
namespace app\controllers;

use Yii;
use app\models\MciPais;
use app\models\MciEstado;
use app\models\MciCiudad;
use app\models\MciMunicipio;
use app\models\SiamMedios;
use app\models\SmnConvocatorias;
use app\models\SmnConvocatoriasTrabajadores;
use app\models\SmnMediosConvocatorias;
use app\models\SmnMediosTrabajadores;
use app\models\SmnConvocatoriasSearch;
use app\models\SmnConvocatoriasPersonas;
use app\models\SmnArchivos;
use app\models\SmnObservaciones;
use app\models\SmnSolicitantes;
use app\models\SmnPersonas;
use app\models\SmnEstructuraGobierno;
use app\models\MciDescripcion;
use app\models\SmnUser;
use app\models\SmnSolicitantesContactos;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use yii\db\Query;


class SmnConvocatoriasController extends SimonController{
    
    public function behaviors(){
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnConvocatorias models.
     * @return mixed
     */
    public function actionIndex($id=null){
        $searchModel = new SmnConvocatoriasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(isset($_GET)){
            $searchModel->load($_GET);
        }

        $listMediosConvocatorias=SiamMedios::listMediosConvocatorias();
        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $listTiposDifusion=MciDescripcion::listOpciones(Yii::$app->params['listas']['tiposDifusion']);
        $listSolicitantes=SmnSolicitantes::listSolicitantes();
        $listEstatus=MciDescripcion::listOpciones(Yii::$app->params['listas']['estatus']);
        $listEstados=MciEstado::listEstados();
        $listPrioridad=MciDescripcion::listOpciones(Yii::$app->params['listas']['prioridad']);
        $listSector=MciDescripcion::listOpciones(Yii::$app->params['listas']['sector']);

        return $this->render('index',compact('searchModel','dataProvider','listMediosConvocatorias','listTiposMediosSolicitados','listTiposDifusion','listSolicitantes','listEstatus','listEstados','listPrioridad','listSector'));
    }

    /**
     * Displays a single SmnConvocatorias model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $model=$this->findModel($id);
        $model->fecha=$this->cambiarFormatoFecha($model->fecha);
        return $this->renderPartial('view',compact('model','listTiposMediosSolicitados'));
    }

    /**
     * Creates a new SmnConvocatorias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new SmnConvocatorias();
        $model->setScenario('registrar');
        $modelUser=new SmnUser;

        //Validación Ajax. Se ejecuta solo cuando el form es enableAjaxValidation=true
        if(Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            $validaciones=ActiveForm::validate($model);
            if($model->id_pais != Yii::$app->params['constantes']['idVenezuela']){//Cuando el país es venzuela no se pide estado, municipio ni ciudad.
                unset($validaciones['smnconvocatorias-id_estado']);
                unset($validaciones['smnconvocatorias-id_ciudad']);
                unset($validaciones['smnconvocatorias-id_municipio']);
            }
            return Json::encode($validaciones);
        }
        //Fin Validación Ajax

        if(Yii::$app->request->post('SmnConvocatorias')){
            $model->id_estatus=Yii::$app->params['convocatorias']['idPorConfirmar']; //Busca el valor en /config/params.php
            $_POST['SmnConvocatorias']['tipo_creador']=$modelUser->getTipoUsuario(); //Hay que condicionar dependiendo de quien la crea.

            $registro=$model->registrarConvocatoria($_POST,$_FILES,$model); //Método ubicado en /models/SmnConvocatorias.php
            if($registro){ //si es diferente de falso registró exitosamente.
                Yii::$app->session->setFlash('success', 'Convocatoria registrada con éxito');
                return $this->redirect(['index', 'id' => $model->id]);
            }else{ // Si el registro es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al registrar convocatoria");
            }
        }

        if(empty($model->id_pais)){
            $model->id_pais=Yii::$app->params['constantes']['idVenezuela'];
        }

        $userId=Yii::$app->user->id;
        $modelUser=SmnUser::findOne($userId);
        $tipoUsuario=$modelUser->getTipoUsuario();

        if($tipoUsuario == 'ENTE ADSCRITO'){
            $idSolicitante=$modelUser->idUsuarioSolicitante->id_solicitante;
            $listSolicitantes=SmnSolicitantes::listSolicitantes($idSolicitante);
        }else{
            $listSolicitantes=SmnSolicitantes::listSolicitantes();
        }

        $listCoconvocantes=SmnSolicitantes::listSolicitantes();
        $listestructuragobierno=SmnEstructuraGobierno::listEstructuraGobiernos();
        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $listTendencias=$model->getTendencias();
        $listPaises=MciPais::listPaises();
        $listEstados=MciEstado::listEstados();
        $listCiudades=[];
        $listMunicipios=[];

        return $this->render('create',compact('model','listestructuragobierno','listPaises','listEstados','listCiudades','listMunicipios','listSolicitantes','listTendencias','listTiposMediosSolicitados','listCoconvocantes'));
    }

    /**
     * Updates an existing SmnConvocatorias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->setScenario('modificar');

        $userId=Yii::$app->user->id;
        $modelUser=SmnUser::findOne($userId);
        $tipoUsuario=$modelUser->getTipoUsuario();

        if($tipoUsuario == 'ENTIDAD SOLICITANTE'){
            $idSolicitante=$modelUser->idUsuarioSolicitante->id_solicitante;
            if($idSolicitante != $model->id_convocante){
                throw new ForbiddenHttpException('Operación no permitida. Sólo podrá modificar convocatorias creadas por su ente de adscripción');
            }
            $listSolicitantes=SmnSolicitantes::listSolicitantes($idSolicitante);
        }else{
            $listSolicitantes=SmnSolicitantes::listSolicitantes();
        }


        if(Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            $validaciones=ActiveForm::validate($model);
            if($model->id_pais != Yii::$app->params['constantes']['idVenezuela']){//Cuando el país es venezuela no se pide estado, municipio ni ciudad.
                unset($validaciones['smnconvocatorias-id_estado']);
                unset($validaciones['smnconvocatorias-id_ciudad']);
                unset($validaciones['smnconvocatorias-id_municipio']);
            }
            return Json::encode($validaciones);
        }

        if(isset($_POST['SmnConvocatorias'])){
            $model->load(Yii::$app->request->post());
            $registro=$model->modificarConvocatoria($_POST,$model);
            if($registro){ //si es diferente de falso registró exitosamente.
                Yii::$app->session->setFlash('success', 'Convocatoria modificada con éxito');
                return $this->redirect(['index', 'id' => $model->id]);
            }else{ // Si el registro es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al modificar convocatoria");
            }            
        }

        $listCoconvocantes=SmnSolicitantes::listSolicitantes();
        $listestructuragobierno=SmnEstructuraGobierno::listEstructuraGobiernos();
        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $listTendencias=$model->getTendencias();
        
        $model->asignarValores(); //Ḿétodo para asignar valores de otros modelos.
        $listPaises=MciPais::listPaises();
        $listEstados=MciEstado::listEstados();
        $listCiudades=MciCiudad::listCiudades($model->id_estado);
        $listMunicipios=MciMunicipio::listMunicipios($model->id_estado);
        $listTiposDifusion=MciDescripcion::listOpciones(Yii::$app->params['listas']['tiposDifusion']);

        $personas=$model->solicitantesContactos();
        $personas=ArrayHelper::map($personas, 'id_persona', 'contacto');
        $personas=$model->contactosNoElegidos($personas);

        $model->fecha=$this->cambiarFormatoFecha($model->fecha);

        return $this->render('update', compact('model','listestructuragobierno','listPaises','listEstados','listCiudades','listMunicipios','listSolicitantes','listTendencias','listTiposMediosSolicitados','listTiposDifusion','personas','listCoconvocantes'));
    }

    /**
     * Deletes an existing SmnConvocatorias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionConfirmar($id = null){
        $model = $this->findModel($id);
        $model->setScenario('confirmar');

        //Validación del formulario con AJAX
        if(Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            $validaciones=ActiveForm::validate($model);
            return Json::encode($validaciones);
        }

        //Si el formulario envía información procedemos a guardar los datos de confirmación
        if(isset($_POST['SmnConvocatorias'])){
            $confirmacion=$model->confirmarConvocatoria($_POST,$model);
             if($confirmacion){ //si es diferente de falso confirmó exitosamente.
                Yii::$app->session->setFlash('success', 'Convocatoria confirmada con éxito');
                return $this->redirect(['index', 'id' => $model->id]);
            }else{ // Si la confirmación es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al confirmar convocatoria");
            }
        }

        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $listSenal=MciDescripcion::listOpciones(Yii::$app->params['listas']['senal']);
        $listPrioridad=MciDescripcion::listOpciones(Yii::$app->params['listas']['prioridad']);
        $listSector=MciDescripcion::listOpciones(Yii::$app->params['listas']['sector']);
        $listEstatus=MciDescripcion::listOpciones(Yii::$app->params['listas']['estatus']);
        $listestructuragobierno=SmnEstructuraGobierno::listEstructuraGobiernos();
        unset($listEstatus[Yii::$app->params['convocatorias']['idRealizadas']]);

        $model->fecha=$this->cambiarFormatoFecha($model->fecha);
        return $this->render('confirmar',compact('model','listTiposMediosSolicitados','listSenal','listPrioridad','listSector','listEstatus','listestructuragobierno'));
    }


    public function actionConfirmarMedios($id = null){
        $model = $this->findModel($id);
        $model->id_estado=$model->idDireccion->id_estado;
        $usuario=SimonController::getUsuario(Yii::$app->user->id);
        $idMedio=$usuario->usuarioMedio->id_medio;
        $model->id_medio=$idMedio;
        $modelMedConv=SmnMediosConvocatorias::find()->where(['id_convocatoria'=>$id,'id_medio'=>$idMedio])->one();
        $modelMedConv->setScenario('confirmarAsistencia');

        //Validación del formulario con AJAX
        if(Yii::$app->request->isAjax){
            $modelMedConv->load(Yii::$app->request->post());
            $validaciones=ActiveForm::validate($modelMedConv);
            return Json::encode($validaciones);
        }

        if(isset($_POST['SmnMediosConvocatorias'])){
            $confirmacion=$model->confirmarAsistenciaMedio($_POST,$modelMedConv);
            if($confirmacion){ //si es diferente de falso confirmó exitosamente.
                Yii::$app->session->setFlash('success', 'Confirmación realizada con éxito');
                return $this->redirect(['index']);
            }else{ // Si la confirmación es falso hubo un error y vuelve a mostrar el formulario.
                Yii::$app->session->setFlash("danger","Error al confirmar asistencia");
            }
        }

        if($modelMedConv->asistencia == NULL){
            $modelMedConv->asistencia=0;
        }
        $mediosTrabajadores=$model->listadoTrabajadores();
        $trabajadoresElegidos=$model->trabajadoresElegidos();
        $trabajadoresElegidos=ArrayHelper::map($trabajadoresElegidos, 'id_trabajador', 'id_trabajador');
        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $model->fecha=$this->cambiarFormatoFecha($model->fecha);
        $listSenalAsignada=MciDescripcion::listOpciones(Yii::$app->params['listas']['senalAsignada']);
        
        return $this->render("confirmar-medios",compact('model','modelMedConv','listTiposMediosSolicitados','listSenalAsignada','mediosTrabajadores','trabajadoresElegidos'));
    }


    public function actionConvocar($id=null){
        $model=$this->findModel($id);
        $modelMedConv=new SmnMediosConvocatorias;

        if($model->id_estatus != Yii::$app->params['convocatorias']['idConfirmadas']){
            throw new ForbiddenHttpException('Operación no permitida. Para convocar medios el estatus debe ser CONFIRMADA');
        }

        //Validación del formulario con AJAX
        if(Yii::$app->request->isAjax){
            $modelMedConv->load(Yii::$app->request->post());
            $validaciones=ActiveForm::validate($modelMedConv);
            return Json::encode($validaciones);
        }

        if(isset($_POST['SmnMediosConvocatorias'])){
            $convocatoria=$model->convocarMedio($_POST,$modelMedConv);
            if($convocatoria){
                Yii::$app->session->setFlash('success',"Medio ".$convocatoria->idMedio->identificacion." convocado con éxito");
                return $this->redirect(['convocar', 'id' => $model->id]);
            }else{
                Yii::$app->session->setFlash("danger","Error al convocar medio");
            }
        }

        $listTiposMediosSolicitados=MciDescripcion::listTiposMediosSolicitados($this->idDesTiposMediosSolicitados);
        $listMediosConvocatorias=SiamMedios::listMediosConvocatorias();
        $listMediosConvocatorias=$model->mediosNoConvocados($listMediosConvocatorias);
        $listSenalAsignada=MciDescripcion::listOpciones(Yii::$app->params['listas']['senalAsignada']);
        $mediosConvocados=SmnMediosConvocatorias::find()->where(['id_convocatoria'=>$id])->all();
        $model->fecha=$this->cambiarFormatoFecha($model->fecha);
        return $this->render('convocar',compact('model','listTiposMediosSolicitados','modelMedConv','listMediosConvocatorias','listSenalAsignada','mediosConvocados'));
    }

    public function actionRemoverConvocatoria($idConvocatoria,$idMedio){
        $model=$this->findModel($idConvocatoria);
        $eliminacion=$model->removerConvocatoria($idConvocatoria,$idMedio);

        if($eliminacion){
            Yii::$app->session->setFlash('success',"Convocatoria removida con éxito");
        }else{
            Yii::$app->session->setFlash("danger","Error al remover convocatoria");
        }

        return $this->redirect(['convocar', 'id' => $model->id]);
    }

    /**
     * Finds the SmnConvocatorias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnConvocatorias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = SmnConvocatorias::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEncabezado(){
        return $this->renderPartial("_encabezado");
    }


    //Función llamada cuando cambia el combo de convocantes en _form-registro.php acción registrar
    public function actionPersonasConvocatoriasAnteriores(){
        $idConvocante=$_POST['id_convocante'];
        $modelConvocatorias=new SmnConvocatorias;
        $modelConvocatorias->id_convocante=$idConvocante;
        $personas=$modelConvocatorias->solicitantesContactos();
        $personas=ArrayHelper::map($personas, 'id_persona', 'contacto');
        return $this->renderPartial('personas-convocatorias-anteriores',compact('personas'));
    }


    //Función llamada cuando se elimina una persona de contacto en _form-registro.php acción modificar
    public function actionActualizarPersonasConvocantes(){
        $idConvocante=$_POST['id_convocante'];
        $idConvocatoria=$_POST['id_convocatoria'];

        $model = $this->findModel($idConvocatoria);
        $personas=$model->solicitantesContactos();
        $personas=ArrayHelper::map($personas, 'id_persona', 'contacto');
        $personas=$model->contactosNoElegidos($personas);

        foreach ($personas as $idPersona => $detallePersona){
            echo "<option value='$idPersona'>$detallePersona</option>";
        }
    }


    //Función llamada cuando se elimina un trabajador asistente confirmar-medios.php acción confirmar-medios
    public function actionActualizarTrabajadores(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idMedio=$_POST['id_medio'];

        $model = $this->findModel($idConvocatoria);
        $model->id=$idConvocatoria;
        $model->id_medio=$idMedio;
        $model->id_estado=$model->idDireccion->id_estado;

        $mediosTrabajadores=$model->listadoTrabajadores();
        $trabajadoresElegidos=$model->trabajadoresElegidos();
        $trabajadoresElegidos=ArrayHelper::map($trabajadoresElegidos, 'id_trabajador', 'id_trabajador');

        foreach ($mediosTrabajadores as $medioTrabajador){
            $idPersona=$medioTrabajador->idPersona->id;
            $datosTrabajador=$medioTrabajador->datosTrabajador;
            $idTipoTrabajador=$medioTrabajador->id_tipo_trabajador;
            if(!in_array($idPersona,$trabajadoresElegidos)){
                echo "<option value='$idPersona' data-id_tipo_trabajador='$idTipoTrabajador'>$datosTrabajador</option>";
            }
        }
    }


    //Método llamado desde _form.php por eventos accionados en contacto-nuevo-cedula.php
    public function actionContactoNuevo(){
        $accion=$_POST['accion'];

        switch ($accion){
            case 'cedula':
                return $this->renderPartial('contacto-nuevo-cedula');
            break;
            
            case 'validar_cedula':
                $cedula=$_POST['cedula'];
                //retorna un arreglo con la función asArray()
                $modelPersona=SmnPersonas::find()->where(['cedula'=>$cedula])->asArray()->all();
                
                if(count($modelPersona) > 0){
                    echo json_encode($modelPersona[0]);
                }else{
                    echo false;
                }
            break;
        }
    }


    //Método llamado desde _form.php por eventos accionados en formulario-observaciones.php
    public function actionObservacionNueva(){
        $accion=$_POST['accion'];
        $model=new SmnObservaciones;
        $modelUser=new SmnUser;

        switch ($accion){
            case 'formulario':
                return $this->renderPartial('formulario-observaciones',compact('model'));
            break;
            
            case 'registrar':
                $modelObservacion=new SmnObservaciones;
                $modelObservacion->observacion=$_POST['observacion'];
                $modelObservacion->id_tabla=$_POST['id_convocatoria'];
                $modelObservacion->tipo_tabla="smn_convocatorias";
                $modelObservacion->tipo_creador=$modelUser->getTipoUsuario(); //Esta línea hay que condicionarla...
                $modelObservacion->save();

                $retorno=[
                    'id_observacion'=>$modelObservacion->id,
                    'id_tabla'=>$modelObservacion->id_tabla,
                    'tipo_tabla'=>$modelObservacion->tipo_tabla,
                    'tipo_creador'=>$modelObservacion->tipo_creador,
                    'observacion'=>$modelObservacion->observacion
                ];

                echo json_encode($retorno);
            break;
        }
    }


    public function actionRemoverObservacion(){
        $idObservacion=$_POST['id_observacion'];
        $modelObservacion=SmnObservaciones::find()->where(['id'=>$idObservacion])->one();
        return $modelObservacion->delete();
    }


    //Método para almacenar contactos en la acción update de Convocatorias...
    public function actionGuardarPersonaContacto(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idPersona=$_POST['id_persona'];

        $modelConvocatoria=SmnConvocatorias::find()->where(['id'=>$idConvocatoria])->one();

        $convocatoriasPersonas=SmnConvocatoriasPersonas::find()->where(['id_convocatoria'=>$idConvocatoria,'id_persona'=>$idPersona])->asArray()->all();

        $guardado=false;
        if(count($convocatoriasPersonas) == 0){
            $modelConvPers=new SmnConvocatoriasPersonas;
            $modelConvPers->id_convocatoria=$idConvocatoria;
            $modelConvPers->id_persona=$idPersona;
            $guardado=$modelConvPers->save();

            $modelSolCont=SmnSolicitantesContactos::find()->where(
                ['id_solicitante'=>$modelConvocatoria->id_convocante,'id_persona'=>$idPersona])->all();
            if(count($modelSolCont) == 0){
                $modelSolCont=new SmnSolicitantesContactos;
                $modelSolCont->id_solicitante=$modelConvocatoria->id_convocante;
                $modelSolCont->id_persona=$idPersona;
                $modelSolCont->save();
            }
        }
        return $guardado;
    }

    public function actionGuardarConvocatoriaTrabajador(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idPersona=$_POST['id_persona'];
        $idTipoTrabajador=$_POST['id_tipo_trabajador'];
        $idMedio=$_POST['id_medio'];

        $modelConvTrab=new SmnConvocatoriasTrabajadores;
        $modelConvTrab->id_convocatoria=$idConvocatoria;
        $modelConvTrab->id_trabajador=$idPersona;
        $modelConvTrab->id_tipo_trabajador=$idTipoTrabajador;
        $modelConvTrab->id_medio=$idMedio;

        return $modelConvTrab->save();
    }

    public function actionConsultarConvocatoriaTrabajador(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idPersona=$_POST['id_persona'];
        $idMedio=$_POST['id_medio'];

        $condiciones=['id_convocatoria'=>$idConvocatoria,'id_trabajador'=>$idPersona,'id_medio'=>$idMedio];
        $convocatoriaTrabajador=SmnConvocatoriasTrabajadores::find()->where($condiciones)->one();

        return $this->renderPartial("_consultar-convocatoria-trabajador",compact('convocatoriaTrabajador'));
    }


    //Método para remover contactos en la acción update de Convocatorias...
    public function actionRemoverPersonaContacto(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idPersona=$_POST['id_persona'];

        $connection = Yii::$app->getDb();
        $tablaConvPers=SmnConvocatoriasPersonas::tableName();
        $condicion="id_convocatoria=$idConvocatoria AND id_persona=$idPersona";
        $connection ->createCommand()->delete($tablaConvPers,$condicion)->execute();
        return true;

    }

    //Método para remover trabajadores en la acción confirmar-medios de Convocatorias...
    public function actionRemoverConvocatoriaTrabajador(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idPersona=$_POST['id_persona'];
        $idMedio=$_POST['id_medio'];

        $connection = Yii::$app->getDb();
        $tablaConvTrab=SmnConvocatoriasTrabajadores::tableName();
        $condicion="id_convocatoria=$idConvocatoria AND id_trabajador=$idPersona AND id_medio=$idMedio";
        return $connection->createCommand()->delete($tablaConvTrab,$condicion)->execute();
    }


    public function actionEliminarArchivo(){
        $idArch=$_POST['id_convocatoria_archivo'];
        $modelArch=SmnArchivos::find()->where(['id'=>$idArch])->one();
        $ruta=Yii::$app->params['convocatorias']['rutaArchivos'].$modelArch->archivo;
        $modelArch->delete();
        return (unlink($ruta));
    }


    public function actionSubirArchivo(){

        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
        
        $idConvocatoria=$_POST['SmnConvocatorias']['id_convocatoria'];
        $modelArch=SmnArchivos::find()->where(['id_tabla'=>$idConvocatoria])->all();
        $numero=$this->archivoMayor($modelArch);
        $numero++;
        
        $dir_subida = Yii::$app->params['convocatorias']['rutaArchivos'];
        $extension=$this->obtenerExtension($_FILES[0]['name']);
        $nombre=$idConvocatoria."-".$numero; //Cambia el nombre del archivo para que no se repitan en el servidor.
        $fichero_subido = $dir_subida.basename($nombre.".".$extension);
        $valor=$_FILES[0]['tmp_name'];
        move_uploaded_file($valor,$fichero_subido); //sube el archivo desde el temporal para el servidor.
        
        $modelArchivo=new SmnArchivos;
        $modelArchivo->id_tabla=$idConvocatoria;
        $modelArchivo->archivo=$nombre.".".$extension;
        $modelArchivo->tipo_tabla="smn_convocatorias";
        $modelArchivo->tipo_creador=$tipoUsuario;
        $modelArchivo->nombre=$_FILES[0]['name'];
        $modelArchivo->save();

        $retorno=[
            'id_convocatoria_archivo'=>$modelArchivo->id,
            'id_convocatoria'=>$modelArchivo->id_tabla,
            'ruta'=>Yii::$app->params['convocatorias']['rutaArchivos'], 'archivo'=>$nombre,
            'extension'=>$extension
        ];
        return json_encode($retorno);
    }

    public function actionValidarFormulario(){
        if(!isset($_POST['SmnConvocatorias']['medios_solicitados'])){
            return "Por favor seleccione al menos 1 tipo de medio a solicitar";
        }

        if(empty($_POST['SmnConvocatorias']['id'])){ //Acción Registrar
            $model = new SmnConvocatorias();             
            $model->setScenario('registrar');
            $_POST['SmnConvocatorias']['id_direccion']=0;
            $_POST['SmnConvocatorias']['id_estatus']=0;
        }else{ //Acción Modificar
            $model = SmnConvocatorias::findOne($_POST['SmnConvocatorias']['id']);
            $model->setScenario('modificar');
        }

        $model->load($_POST);
        $model->validate();
        $errores=$model->errors;
        if(count($errores) > 0){
            $itemError=current($errores);
            return $itemError[0];
        }
        return false;
    }

    public function actionEliminarContactos(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $tablaConvPers=SmnConvocatoriasPersonas::tableName();
        $condicion="id_convocatoria=$idConvocatoria";
        $connection = Yii::$app->getDb();
        return $connection->createCommand()->delete($tablaConvPers,$condicion)->execute();
    }

    public function actionMedioConvocado(){
        $idConvocatoria=$_POST['id_convocatoria'];
        $idMedio=$_POST['id_medio'];

        $condicion=['id_convocatoria'=>$idConvocatoria,'id_medio'=>$idMedio];
        $modelMedConv=SmnMediosConvocatorias::find()->where($condicion)->one();

        $modelConvTrab=SmnConvocatoriasTrabajadores::find()->where($condicion)->all();

        return $this->renderPartial('medio-convocado',compact('modelMedConv','modelConvTrab'));
    }


    public function actionNotificaciones(){
        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        $cantidad=0;
        $bandera=false;
        $estatus="";
        $url="";
        $html="";
        switch ($tipoUsuario){

            case 'COMUNICACIONES':
                //Notificaciones 
                $idPorConfirmar=Yii::$app->params['convocatorias']['idPorConfirmar'];
                $condicion=[
                    'id_estatus'=>$idPorConfirmar,
                    'fecha'=>date("Y-m-d")
                ];
                $modelConvocatorias=SmnConvocatorias::find()->where($condicion)->all();
                $cantidad=count($modelConvocatorias);

                if($cantidad > 0){
                    $bandera=true;
                    $estatus="por confirmar";
                    $url="/smn-convocatorias/index?SmnConvocatoriasSearch[id_estatus]=$idPorConfirmar";
                    $html.="<li class='justificar'><a href='$url'>Existe(n) <b>$cantidad</b> convocatoria(s) para hoy <b>".date('d/m/Y')."</b> con estatus de <b>$estatus</b></a></li>";
                }

                $idConfirmadas=Yii::$app->params['convocatorias']['idConfirmadas'];
                $condicion=[
                    'id_estatus'=>$idConfirmadas,
                    'fecha'=>date("Y-m-d"),
                    '(select count(id_convocatoria) FROM "SIMON".smn_medios_convocatorias WHERE id_convocatoria=id)'=>0
                ];

                $query=new Query;
                $select="id,descripcion";
                $query->select($select)->from(SmnConvocatorias::tableName())->where($condicion);
                $filas = $query->all();
                $cantidad=count($filas);

                if($cantidad > 0){
                    $bandera=true;
                    $estatus="confirmadas sin autoridades asignadas";
                    $url="/smn-convocatorias/index?SmnConvocatoriasSearch[confirmadas]=1";
                    $html.="<li class='justificar'><a href='$url'>Existe(n) <b>$cantidad</b> convocatoria(s) para hoy <b>".date('d/m/Y')."</b> con estatus de <b>$estatus</b></a></li>";
                }



                $idConfirmadas=Yii::$app->params['convocatorias']['idConfirmadas'];
                $idMedio=$usuario->usuarioMedio->id_medio;
                
                $condicion=[
                    'id_estatus'=>$idConfirmadas,
                    'fecha'=>date("Y-m-d"),
                    "(select count(id_convocatoria) FROM \"SIMON\".smn_medios_convocatorias WHERE id_medio=$idMedio AND id_convocatoria=id AND asistencia is null)"=>1
                ];

                $query=new Query;
                $select="id,descripcion,(select count(id_convocatoria) FROM \"SIMON\".smn_medios_convocatorias WHERE id_convocatoria=id AND id_medio=$idMedio AND asistencia is null)";
                $query->select($select)->from(SmnConvocatorias::tableName())->where($condicion);
                $filas = $query->all();

                $cantidad=count($filas);
                if($cantidad > 0){
                    $bandera=true;
                    $estatus="confirmadas que requieren la confirmación de asistencia de las autoridades";
                    $url="/smn-convocatorias/index?SmnConvocatoriasSearch[asistencia]=1";
                    $html.="<li class='justificar'><a href='$url'>Existe(n) <b>$cantidad</b> convocatoria(s) para hoy <b>".date('d/m/Y')."</b> con estatus de <b>$estatus</b></a></li>";
                }

            break;

        }

        if($bandera){
            return $html;
        }else{
            return "<li class='vacio'><a href='#'>No existen notificaciones pendientes.</a></li>";
        }

    }

    public function actionHorasCriticas(){
        $fecha=$this->cambiarFormatoFecha($_POST['fecha']);
        $hora=$_POST['hora'];
        $idEstado=$_POST['id_estado'];
        if(!empty($idEstado)){
            $modelEstado=MciEstado::findOne($idEstado);
            $modelConvocatorias=SmnConvocatorias::find()->where(['fecha'=>$fecha,'hora'=>$hora,])->all();
            $grupos=(new yii\db\Query())->from('SIMON.smn_convocatorias c')
                ->select(['hora','count(hora) AS cantidad'])
                ->leftJoin('SIMON.smn_direccion dir',"dir.id=c.id_direccion")
                ->where(['fecha'=>$fecha,'dir.id_estado'=>$idEstado])
                ->groupBy(['hora'])->orderBy(['hora'=>SORT_ASC])->all();
        }else{
            $modelEstado='';
            $modelConvocatorias=SmnConvocatorias::find()->where(['fecha'=>$fecha,'hora'=>$hora])->all();
            $grupos=(new yii\db\Query())->from('SIMON.smn_convocatorias')->select(['hora','count(hora) AS cantidad'])->where(['fecha'=>$fecha])->groupBy(['hora'])->orderBy(['hora'=>SORT_ASC])->all();
        }

        $horasGrafico=[];
        $cantidadGrafico=[];
        foreach ($grupos as $grupo){
            $horasGrafico[]=$grupo['hora'];
            $cantidadGrafico[]=$grupo['cantidad'];
        }

        return $this->renderPartial("horas-criticas",compact('fecha','modelEstado','hora','modelConvocatorias','horasGrafico','cantidadGrafico'));
    }


    private function archivoMayor($arreglo){
        $mayor=0;
        foreach ($arreglo as $archivo){
            $nombreArchivo=explode(".",$archivo->archivo);
            $nombre=explode("-",$nombreArchivo[0]);
            $numero=$nombre[1];
            if($numero > $mayor){
                $mayor=$numero;
            }
        }
        return $mayor;
    }


    private function obtenerExtension($archivo){
        $partes=explode(".",$archivo);
        $extension=$partes[count($partes)-1];
        return $extension;
    }

}
