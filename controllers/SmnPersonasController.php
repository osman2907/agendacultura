<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SmnPersonas;
use app\models\SmnPersonasSearch;
use app\models\SmnTelefonos;
use app\models\SmnCorreos;
use app\models\SmnDireccion;
use app\models\MciEstado;
use app\models\MciMunicipio;
use app\models\MciParroquia;
use app\models\MciCiudad;
use app\models\SmnPersonasTelefonos;
use app\models\SmnPersonasCorreos;
use app\models\SmnSolicitantesContactos;
use app\models\Ldap;
use app\models\SmnUser;
use app\models\AuthItem;
use app\models\AuthAssignment;

use yii\web\Response;
use app\models\Model;

use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SmnPersonasController implements the CRUD actions for SmnPersonas model.
 */
class SmnPersonasController extends SimonController
{

    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnPersonas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmnPersonasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

    

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmnPersonas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SmnPersonas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionBuscarPersonaMedio(){
        return $this->renderPartial('buscarPersonaMedio',compact('model','cedula'));
    }

    public function actionCrearContactoMedio(){
        $model = new SmnPersonas();
        $direccion = new SmnDireccion();
        
        if(isset($_POST['SmnPersonas'])){
            $model=$model->registrarPersonas($_POST,$model,$direccion);

            $retorno=json_encode([
                'cedula'=>$model->cedula,
                'id'=>$model->id,
                'nombre'=>strtoupper($model->nombre),
                'apellido'=>strtoupper($model->apellido)
            ]);
            return $retorno;
            exit();
        }

        $cedula=$_POST['cedula'];
        $idSolicitante=isset($_POST['id_solicitante'])?$_POST['id_solicitante']:'';
        $model->setScenario('crearContactoMedio');
        $model->cedula=$cedula;
        $model->activo=1;
        $correos = new SmnCorreos();
        $telefonos = new SmnTelefonos();
    
        $listEstados=MciEstado::listEstados();
        $listCiudades=[];
        $listMunicipios=[];
        $listParroquias=[];

        return $this->renderPartial('crearContactoMedio',compact('model','correos','telefonos','direccion','listEstados','listMunicipios','listParroquias','listCiudades','idSolicitante'));
    }

    public function actionCreate(){
        $model = new SmnPersonas();
        $model->activo=1;
        $direccion = new SmnDireccion();
        $correos = new SmnCorreos();
        $telefonos = new SmnTelefonos();
    
        $listEstados=MciEstado::listEstados();
        $listCiudades=[];
        $listMunicipios=[];
        $listParroquias=[];

        //Validación Ajax. Se ejecuta solo cuando el form es enableAjaxValidation=true
        if (Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            $direccion->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model,$direccion);
        }
        //Fin Validación Ajax

        //Registrar Personas y todos sus modelos foráneos(Dirección, Teléfonos,Correos)
        if(isset($_POST['SmnPersonas'])){
            $model->registrarPersonas($_POST,$model,$direccion);
            return $this->redirect(['view', 'id' => $model->id]);
        }
        //Fin registrar personas

        return $this->render('create',compact('model','correos','telefonos','direccion','listEstados','listMunicipios','listParroquias','listCiudades'));
        
    }

    public function actionListmunicipios(){
        $idEstado=Yii::$app->request->post()['id'];
        $listMunicipios=MciMunicipio::listMunicipios($idEstado);
        $this->llenarSelect($listMunicipios);
    }

    public function actionListparroquias(){
        $idMunicipio=Yii::$app->request->post()['id'];
        $listParroquias=MciParroquia::listParroquias($idMunicipio);
        $this->llenarSelect($listParroquias);
    }

    public function actionListciudades(){
        $idEstado=Yii::$app->request->post()['id'];
        $listCiudades=MciCiudad::listCiudades($idEstado);
        $this->llenarSelect($listCiudades);
    }


    /**
     * Updates an existing SmnPersonas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate2($id)
    {  
        $model = $this->findModel($id);
        ///traemos la direccion de esta persona
        $direccion = SmnDireccion::find()->where(['id'=>$model->id_direccion])->one();
        if($direccion==NULL){
            $direccion=new SmnDireccion();
        }

        $listEstados=MciEstado::listEstados();

        if(isset($_POST['SmnPersonas'])){

        }
        $idEstado=($direccion == NULL)?NULL:$direccion->id_estado;
        $listCiudades=MciCiudad::listCiudades($idEstado);
        $listMunicipios=MciMunicipio::listMunicipios($idEstado);

        $idMunicipio=($direccion == NULL)?NULL:$direccion->id_municipio;
        $listParroquias=MciParroquia::listParroquias($idMunicipio); 
        
        return $this->render('update',compact('model','telefonos','correos','direccion',
            'listEstados','listMunicipios','listParroquias','listCiudades'));
        
    }


    public function actionUpdate($id)
    {  
        //busca en la tabla personas
        $model = $this->findModel($id);
        $model->setScenario('modificarPersona');

        $direccion = SmnDireccion::find()->where(['id'=>$model->id_direccion])->one();
        if($direccion==NULL){
            $direccion=new SmnDireccion();
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if(count(ActiveForm::validate($model)) > 0){
                return ActiveForm::validate($model);
            }

            $direccion->load(Yii::$app->request->post());
            if(count(ActiveForm::validate($direccion)) > 0){
                return ActiveForm::validate($direccion);
            }
            return [];
        }

        if(isset($_POST['SmnPersonas'])){
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $direccion->load(Yii::$app->request->post());
                $direccion->save();

                if($direccion->validate()){
                    $model->load(Yii::$app->request->post());
                    $model->id_direccion=$direccion->id;
                    $model->save();
                }else{
                    throw new ErrorException('Error Registrando Persona');
                }

                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }catch(ErrorException $e){
                $transaction->rollback();
                echo "<pre>";
                print_r($e);
                echo "</pre>";
            }
            exit();

        }
        $listEstados=MciEstado::listEstados();
        
        $condicion=($direccion != NULL)?['id_estado'=>$direccion->id_estado]:[];
        $ciudad=MciCiudad::find()->where($condicion)->all();
        $listCiudades=ArrayHelper::map($ciudad,'id_ciudad','nom_ciudad');
        
        $listMunicipios=MciMunicipio::listMunicipios($direccion->id_estado);
        
        $condicion=($direccion != NULL)?['id_municipio'=>$direccion->id_municipio]:[];
        $parroquias=MciParroquia::find()->where($condicion)->all();
        $listParroquias=ArrayHelper::map($parroquias,'id_parroquia','nom_parroquia');

        if($model->load(Yii::$app->request->post()) && 
            //$telefonos->load(Yii::$app->request->post()) && 
            $direccion->load(Yii::$app->request->post())){ 
            //$correos->load(Yii::$app->request->post())) {
                if($model->validate())
                {
                    
                    $model->save();
                    $direccion->save();
                    //$telefonos->save();
                    //$correos->save();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
        }
            
            
    return $this->render('update',compact('model','telefonos','correos','direccion',
        'listEstados','listMunicipios','listParroquias','listCiudades'));
        
    }

    /**
     * Deletes an existing SmnPersonas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmnPersonas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnPersonas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(($model = SmnPersonas::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidarTelefonos(){
        $telefono=Yii::$app->request->post()['valor'];
        $model=new SmnTelefonos;
        $model->telefono=$telefono;
        if(!$model->validate()){
            print_r($model->errors['telefono'][0]);
        }
    }

    public function actionValidarCedula(){
        $cedula= $_POST['cedula'];
        $condicion= ['cedula'=> $cedula];
        $model = SmnPersonas::find($cedula)->where($condicion)->all();
        echo count($model);
        //print_r($model);
    }

    public function actionMostrarPersona(){
        $cedula= $_POST['cedula'];
        $condicion= ['cedula'=> $cedula];
        $model = SmnPersonas::find($cedula)->where($condicion)->one();
        return $this->renderPartial('mostrarPersona',compact('model'));
    }

    public function actionValidarContacto(){
        //print_r($_POST);
        $modelPersona=new SmnPersonas;
        $modelPersona->setScenario('crearContactoMedio');
        $modelPersona->load(Yii::$app->request->post());
        $modelPersona->validate();
        foreach ($modelPersona->errors as $error){
            return $error[0];
        }

        $modelDireccion=new SmnDireccion;
        $modelDireccion->load(Yii::$app->request->post());
        $modelDireccion->validate();
        foreach ($modelDireccion->errors as $error){
            return $error[0];
        }
    }
   
    public function actionBuscarPersona(){
        $cedula= $_POST['cedula'];
        $persona=SmnPersonas::buscarPersona($cedula);
 
        if($persona){
            $persona = ArrayHelper::toArray($persona,[
                'app\models\SmnPersonas' => [
                    'id',
                    'cedula',
                    'nombre',
                    'apellido',
                    /*'length' => function ($persona) {
                        return strlen($persona->nombre);
                    },*/
                ],
            ]);
            return json_encode($persona);
        }else{
            return false;
        }
        
    }

    public function actionBuscarPersona2(){
        $idPersona= $_POST['id_persona'];
        $persona=SmnPersonas::buscarPersona2($idPersona);
 
        if($persona){
            $persona = ArrayHelper::toArray($persona,[
                'app\models\SmnPersonas' => [
                    'id',
                    'cedula',
                    'nombre',
                    'apellido',
                    /*'length' => function ($persona) {
                        return strlen($persona->nombre);
                    },*/
                ],
            ]);
            return json_encode($persona);
        }else{
            return false;
        }
        
    }

    public function actionValidarUsuarioMedio(){
        $cedula=$_POST['cedula'];
        $condicion= ['cedula'=> $cedula];
        $persona = SmnPersonas::find()->where($condicion)->all();
        if(count($persona) == 0){
            return false;
        }

        $condicion=['id_persona'=>$persona[0]->id];
        $usuario=SmnUser::find()->where($condicion)->all();

        if(count($usuario) == 0){
            return false;
        }

        $modelAuthItem=new AuthItem;
        $roles=$modelAuthItem->rolesPorGrupo('medios');
        $roles="'".implode("','",$roles)."'";

        $userId=$usuario[0]->id;
        $condicion="item_name IN($roles) AND user_id=$userId";
        $asignaciones=AuthAssignment::find()->where($condicion)->all();

        if(count($asignaciones) > 0){
            return true;
        }

        return false;
    }
    
     public function actionValidarUsuarioSolicitante(){
        $cedula=$_POST['cedula'];
        $condicion= ['cedula'=> $cedula];
        $persona = SmnPersonas::find()->where($condicion)->all();
        //print_r($persona);die;
        if(count($persona) == 0){
            return false;
        }

        $condicion=['id_persona'=>$persona[0]->id];
        $usuario=SmnUser::find()->where($condicion)->all();
        //print_r($usuario);die;
        if(count($usuario) == 0){
            return false;
        }

        $modelAuthItem=new AuthItem;
        $roles=$modelAuthItem->rolesPorGrupo('solicitantes');
        $roles="'".implode("','",$roles)."'";
        //print_r('3');die;
        
        $userId=$usuario[0]->id;
        $condicion="item_name IN($roles) AND user_id=$userId";
        $asignaciones=AuthAssignment::find()->where($condicion)->all();
        //print_r('4');die;
        if(count($asignaciones) > 0){
            return true;
        }
        
        return false;
    }

    public function actionBuscarCedulaLdap(){
        $cedula= $_POST['cedula'];
        $modelLdap=new Ldap;
        $usuarios=$modelLdap->buscar('pager',$cedula);
        print_r(json_encode($usuarios));
        //print_r($usuarios);
    }
    
    public function actionCedulaPersonasId()
    {
        $id = $_POST['id_persona'];
        if(($model = SmnPersonas::findOne($id)) !== null){
           return json_encode($model->cedula); 
        }else{
            return FALSE;
        }
    }
}
