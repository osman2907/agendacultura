<?php

namespace app\controllers;

use Yii;
use app\models\SmnUser;
use app\models\SmnUserSearch;
use app\models\SmnPersonas;
use app\models\SmnCorreos;
use app\models\SmnTelefonos;
use app\models\SmnPersonasCorreos;
use app\models\SmnPersonasTelefonos;
use app\models\SmnSolicitantesContactos;
use app\models\SmnSolicitantes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use app\models\AuthItem;
use app\models\Ldap;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\base\ErrorException;
use app\models\Correos;
use app\models\AuthAssignment;
use app\models\SmnUsuariosSolicitantes;


/**
 * SmnUsuariosSolicitantesController implements the CRUD actions for SmnUser model.
 */
class SmnUsuariosSolicitantesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
            return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SmnUser models.
     * @return mixed
     */
    public function actionIndex()
    {   
        $searchModel = new SmnUserSearch();
        $dataProvider = $searchModel->searchSolicitantes(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new SmnUser();
        $model->setScenario('UsuariosSolicitantes');
        $modelCorreos=new SmnCorreos;
              
        if (Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if(isset($_POST['SmnUser'])){
            $modeloUser=$model->registrarUsuariosSolicitantes($_POST);
            if($modeloUser){
                
                if(isset($respuesta['clave'])){
                    $clave = $respuesta['clave'];     
                }else $clave = 'noAplica';

                
                /*if(!empty($modeloUser->smnUsuarioCorreos)){
                    $this->layout='correos';
                    $correo=new Correos();
                    $correos = array();
                    foreach ($modeloUser->smnUsuarioCorreos as $objeto) {
                        array_push($correos, $objeto->correo->correo);
                    }
                    $cuerpo=$this->render("correoRegistroSolicitantes",compact('modeloUser', 'clave'),true);
                    $correo->enviarCorreo("Registro de Usuario",$cuerpo,$correos);    
                }*/
                
                Yii::$app->session->setFlash('success', 'Usuario Solicitante registrado con éxito.');
                return $this->redirect(['view', 'id' => $modeloUser->id]);
            }  else {
                    Yii::$app->session->setFlash('danger', 'No se pudo registrar al usuario.');
            }
        }

        $listSolicitantes=SmnSolicitantes::listSolicitantes();
        $modelAuthItem=new AuthItem;
        $listRoles=$modelAuthItem->rolesPorGrupo('solicitantes');
        return $this->render('create',compact('model','listSolicitantes','listRoles'));
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Updates an existing SmnUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('actualizarUsuariosSolicitantes');
        //var_dump($model);die;
        if (Yii::$app->request->isAjax){
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if(isset($_POST['SmnUser'])){
            $respuesta=$model->actualizarUsuariosSolicitantes($_POST);
            if($respuesta){
                Yii::$app->session->setFlash('success', 'Usuario Solicitante registrado con éxito');
                return $this->redirect(['view', 'id' => $respuesta->id]);
            }
        }
            $listSolicitantes=SmnSolicitantes::listSolicitantes();
            $modelAuthItem=new AuthItem;
            $listRoles=$modelAuthItem->rolesPorGrupo('solicitantes');
           return $this->render('update',compact('model','listRoles','listSolicitantes','SmnUsuariosSolicitantes'));
        
    }

    /**
     * Deletes an existing SmnUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $borroRoles = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $id]);
        $UsuariosSolicitantes= SmnUsuariosSolicitantes::deleteAll("id_usuario=:id_usuario", [":id_usuario" => $id]);
        
        Yii::$app->session->setFlash('success', 'Usuario Eliminado con éxito');
        return $this->redirect(['index']);
    }


    public function actionValidarFormulario(){
        if(!isset($_POST['roles'])){
            return "Por favor seleccione al menos un rol";
        }
        return false;
    }


    /**
     * Finds the SmnUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmnUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmnUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionBuscarUsuario(){
        $idPersona=$_POST['id_persona'];
        $modelUsuario=SmnUser::find()->where(['id_persona'=>$idPersona])->all();
        
        if(count($modelUsuario) > 0){
            $usuario=['id_usuario'=>$modelUsuario[0]['id'],'usuario'=>$modelUsuario[0]['username']];
            echo json_encode($usuario);
        }else{
            echo false;
        }
    }
    
}
