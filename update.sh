#!/bin/bash
# -*- ENCODING: UTF-8 -*-

echo "Creando carpeta runtime..."
mkdir runtime

echo "Asignando permisoligía..."
chmod -R 777 runtime

echo "Creando carpeta vendor..."
mkdir vendor

echo "Creando carpeta web/assets..."
mkdir web/assets

echo "Asignando permisología..."
chmod -R 777 web/assets

echo "Creando carpeta web/files/..."
mkdir web/files/

echo "Creando carpeta web/files/convocatorias..."
mkdir web/files/convocatorias

echo "Creando carpeta web/files/convocatorias/archivos..."
mkdir web/files/convocatorias/archivos

echo "Asignando permisología web/files/convocatorias/archivos..."
chmod -R 777 web/files/convocatorias/archivos

echo "Creando carpeta web/files/fotos_personas..."
mkdir web/files/fotos_personas

echo "Asignando permisología web/files/fotos_personas..."
chmod -R 777 web/files/fotos_personas

echo "Fin de la Ejecución"
