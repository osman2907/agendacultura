<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=pautas',
    'username' => 'postgres',
    'password' => 'postgres',
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    /*'schemaMap'=>[
        'pgsql'=>[
            'class'=>'yii\db\pgsql\Schema',
            'defaultSchema'=>'SIMON'
        ]
    ],*/
];
