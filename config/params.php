<?php

return [
    'adminEmail' => 'admin@example.com',
    'ldap'=>[
    	//Funciona como dn base para hacer busquedas con ldap_search()
    	'base'=>'dc=mippci,dc=gob,dc=ve',
    	//Funciona como login para la funcion ldap_bin()
        'rdn'=>'cn=admin,dc=mippci,dc=gob,dc=ve',
        //Funciona como contraseña para autenticar al LDAP.
        'password'=>'123456',
    	//Funciona como dn base para registrar con la función ldap_add()
    	'dnSistemas'=>'ou=sistemas_aplicaciones,ou=dg_tecnologia,ou=Users,dc=mippci,dc=gob,dc=ve',
        //Funciona como dn base para registrar con la función ldap_add() al personal JUBILADO
        'dnJubilados'=>'ou=personal_jubilado,ou=Users,dc=mippci,dc=gob,dc=ve',
    	//Funciona para clasificar las personas en el LDAP(MIPPCI, OIR, SIBCI, ETC)
    	'gidNumber'=>'10001',
        //Almacena la ruta donde se encuentran los egresados del MIPPCI.
        'dnEgresos'=>'ou=EGRESOS,dc=mippci,dc=gob,dc=ve'
    ],

    'email'=>[
        'fromAddress'=>'simonmippci@mippci.gob.ve',
        'fromName'=>'SISTEMA INTEGRADO DE MEDIOS OFICIALES NACIONALES',
        'pie'=>'Ministerio del Poder Popular para la comunicación e Información'
    ],

    'convocatorias'=>[
        'rutaArchivos'=>'files/convocatorias/archivos/',
        'idPorConfirmar'=>41,
        'idConfirmadas'=>40,
        'idRealizadas'=>45,
        'mediosConvocatorias'=>[1]
    ],

    'medios'=>[
        'rutaFotosTrabajadores'=>'files/fotos_personas/',
    ],

    'direccionStandard'=>[
        'direccion'=>'Caracas',
        'id_estado'=>1,
        'id_ciudad'=>1,
        'id_municipio'=>1,
        'id_parroquia'=>0,
        'id_pais'=>1
    ],

    'constantes'=>[
        'idVenezuela'=>1
    ],

    'listas'=>[
        'tiposDifusion'=>4,
        'senal'=>5,
        'prioridad'=>1, //Nivel de Vocería
        'sector'=>2,
        'estatus'=>3, //Estatus de las convocatorias
        'senalAsignada'=>128, //senal cuando vamos a convocar cada medio
        'cargosMediosTrabajadores'=>6 //Cargos de los trabajadores de los medios.
    ]
];
