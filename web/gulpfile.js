/**
	@author: MIPPCI
	@version: 1.0
 */

var gulp	= require('gulp');
var order	= require('gulp-order');
var sass	= require('gulp-ruby-sass');
var concat	= require('gulp-concat');
var csso	= require('gulp-csso');
var prefixer	= require('gulp-autoprefixer');
var sourcemaps	= require('gulp-sourcemaps');

gulp.task('compileSass',function(){

	return sass('sass/**/*.scss',{sourcemap: true, noCache:true})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
			includeContent: false,
			sourceRoot: 'source'
		}))
		.pipe(gulp.dest('css/sources/'));

});

gulp.task('concatsStyles',function(){

	return gulp.src('css/sources/**/*.css')
		.pipe(order([
			"default.css",
			"header.css",
			"sidebar.css",
			"forms.css",
			"*.css",
			]))
		.pipe(sourcemaps.init())
		.pipe(concat('app.css'))
		.pipe(prefixer({
			browsers: ['last 10 versions'],
			cascade: false
		}))
		.pipe(csso())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./css'));

});

gulp.task('default',function(){

	gulp.watch('sass/**/*.scss',['compileSass']);
	gulp.watch('css/**/*.css',['concatsStyles']);

});
