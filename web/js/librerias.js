consultarPHP=function(url,datos,tipo,async){
    var retorno;
    var objeto=$.ajax({
        async: false,
        type: "POST",
        url: "/site/validar-sesion",
        dataType: 'html',
        success: function(respuesta){
            retorno=respuesta;
            if(!respuesta){
                bootbox.alert("La sesión a expirado, por favor autenticarse nuevamente",function(){
                    location.reload();
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            retorno="error";
        }
    });

    if(!retorno){
        return false;
    }

    var objeto=$.ajax({
        async: async,
        type: "POST",
        url: url,
        dataType: tipo,
        data:datos,
        success: function(respuesta){
            retorno=respuesta;
        },
        error: function (xhr, ajaxOptions, thrownError) {
	        retorno="error";
      	}
    });
    
    return retorno;
};


$(document).on('keyup','.solo-numero',function (){
    this.value = (this.value + '').replace(/[^0-9]/g,'');
});

$(document).on('keyup','.minuscula',function (){
    this.value = this.value.toLowerCase();
});

$(document).on("click","#cerrar-sesion",function(){
    url=$(this).attr('href');
    bootbox.confirm("¿Seguro que desea cerrar sesión?",function(respuesta){
        if(respuesta){
            document.location=url;
        }
    })
    return false;
});