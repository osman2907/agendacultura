/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    
    $("#formularioUsuarioMippci").hide();
    $("#limpiarUsuarioMippci").hide();
    
    $(document).on("click", "#limpiarUsuarioMippci", function () {
        window.location.replace('/user/create');
        //limpiar();
    });
    
    $(document).on("click", "#verificarUsuarioMippci", function () {
        limpiar();
        cedula = $("#cedula").val().trim();
        if (cedula == '') {
            bootbox.alert("Por favor ingrese la cédula del usuario a verificar");
            return false;
        }
        data = {cedula: cedula},
        personaLdap = consultarPHP('/smn-personas/buscar-cedula-ldap', data, 'json', false);

        
        if(personaLdap.count == 0){
            bootbox.alert("La persona que desea registrar no se encuentra en el directorio activo. Debe comunicarse con la Coordinación de Sistemas.");
            return false;
        }
        
        if (personaLdap.count > 1) {
            bootbox.alert("La persona que desea registrar se encuentra 2 o mas veces en el directorio activo. Debe comunicarse con la Coordinación de Sistemas.");
            return false;
        }
        
        data = {cedula: cedula},
        usuarioMippci = consultarPHP('/smn-usuarios-mippci/validar-usuario-mippci', data, 'html', false);
        if (usuarioMippci) {
            bootbox.alert("La persona que desea registrar ya es un usuario mippci.");
            return false;
        }
        
        data = {cedula: cedula},
        usuarioMedio = consultarPHP('/smn-personas/validar-usuario-medio', data, 'html', false);
        if (usuarioMedio) {
            bootbox.alert("La persona que desea registrar ya es un usuario medio.");
            return false;
        }
        
        data = {cedula: cedula},
        usuarioSolicitante = consultarPHP('/smn-personas/validar-usuario-solicitante', data, 'html', false);
        if (usuarioSolicitante) {
            bootbox.alert("La persona que desea registrar ya es un usuario solicitante.");
            return false;
        }
        
        if(personaLdap.count == 1){
            var dn = personaLdap[0].dn;
            if (dn.indexOf('ou=EGRESOS')!=-1) {
                bootbox.alert("La persona que desea registrar se encuentra en egresos en el directorio activo. Debe comunicarse con la Coordinación de Sistemas.");
                return false;
            }
            
            if (dn.indexOf('ou=sistemas_aplicaciones')!=-1) {
                bootbox.alert("La persona que desea registrar se encuentra en el directorio activo como usuario externo. Debe comunicarse con la Coordinación de Sistemas.");
                return false;
            }
            
            bootbox.alert("Se asignará un usuario automáticamente", function () {
                usuario = personaLdap[0]['uid'][0];
                $("#smnuser-username").val(usuario);
                $("#smnuser-username").attr('readonly', 'readonly');
            });
            
            data = {cedula: cedula},
            datosPersona = consultarPHP('/smn-personas/buscar-persona', data, 'json', false);
            if (datosPersona.id) {
                $("#tablanuevotc").empty();
                $("#tablanuevotc").hide();
                $("#smnuser-id_persona").val(datosPersona.id);
                $("#smnuser-cedula").val(datosPersona.cedula);
                $("#smnuser-nombre").val(datosPersona.nombre);
                $("#smnuser-apellido").val(datosPersona.apellido);
                
                if ($("#listado-correos").length > 0)
                    listarCorreosMippci(datosPersona.id);
        
                if($("#listado-telefonos").length > 0)
                    listarTelefonosMippci(datosPersona.id);
                
            } else {
                bootbox.alert("El número de cédula no esta registrado, por favor ingrese el nombre y apellido de la persona", function () {
                    $("#tablatc").empty();
                    $("#tablatc").hide();
                    $("#smnuser-cedula").val(cedula);
                    $("#smnuser-nombre").attr('readonly', false);
                    $("#smnuser-apellido").attr('readonly', false);
                });
            }
            
            $("#formularioUsuarioMippci").show();
            $("#limpiarUsuarioMippci").show();
        }
    });

});
    
limpiar = function () {
    $("#smnuser-id_persona").val('');
    $("#smnuser-cedula").val('');
    $("#smnuser-nombre").val('');
    $("#smnuser-apellido").val('');
    $("#smnuser-nombre").attr('readonly', 'readonly');
    $("#smnuser-apellido").attr('readonly', 'readonly');
    $("#smnuser-username").val('');
    $("#smnuser-username").attr('readonly', false);
    
    $("#formularioUsuarioMippci").hide();
    $("#limpiarUsuarioMippci").hide();
}

function listarCorreosMippci(id){
    data={id_persona:id};
    respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
    $("#listado-correos").html(respuesta);
}

function listarTelefonosMippci(id){
    data={id_persona:id};
    respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
    $("#listado-telefonos").html(respuesta);
}

$(document).on("blur",".item-validar-mippci",function(){
    data={'valor':$(this).val()};
    accion=$(this).data('accion-validar-mippci');
    validacion=consultarPHP(accion,data,'html',false);
    if(validacion){
        $(this).parent().parent().addClass('has-error');
        $(this).parent().parent().find('.help-block').html(validacion);
    }else{
        $(this).parent().parent().removeClass('has-error');
        $(this).parent().parent().find('.help-block').html('');
    }
});

$(document).on("submit", "#formularioCorreosMippci", function () {
    errores = ejecutarValidacionItemsMippci();
    if (errores > 0) 
        return false;
        
    data=$(this).serialize();
    respuesta=consultarPHP('/smn-correos/agregar-correos-mippci',data,'html',false);
        bootbox.hideAll();
        if(respuesta == "error")
            bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
        else
            bootbox.alert('Correo electrónico registrado con éxito');

        listarCorreosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
    return false;
});

$(document).on("click",".agregar-item-mippci",function(){
    campoNuevo=$(this).parent().parent().parent().clone();
    $(campoNuevo).removeClass('has-error');
    $(campoNuevo).removeClass('has-success');
    $(campoNuevo).children('.help-block').html('');
    $(campoNuevo).children().children("input").val('');
    listadoItems=$(this).parents(".listado-items-mippci");
    listadoItems.append(campoNuevo);
    $(campoNuevo).children('.input-group').children('span').children().removeClass('agregar-item-mippci');
    $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
    $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
    $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item-mippci');
    $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
    $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
});

$(document).on("click",".eliminar-item-mippci",function(){
    $(this).parent().parent().parent().remove();
});

ejecutarValidacionItemsMippci = function () {
    cantidadItems = $(".item-validar-mippci").length;
    for (i = 0; i < cantidadItems; i++) {
        elementoHtml = $(".item-validar-mippci")[i];
        $(elementoHtml).focus();
        $(elementoHtml).blur();
    }
    errores = $(".has-error div .item-validar-mippci").length;
    console.log(errores);
    return errores;
}

$(document).on("submit", "#formularioTelefonosMippci", function () {
    errores = ejecutarValidacionItemsMippci();

    if(errores > 0){
        return false;
    }

    data = $(this).serialize();
    respuesta = consultarPHP('/smn-telefonos/agregar-telefonos-mippci', data, 'html', false);
    bootbox.hideAll();
    if (respuesta == "error")
        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
    else
        bootbox.alert('Teléfono registrado con éxito');

    listarTelefonosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
    return false;
});

$(document).on("click", "#RegenerarClave", function () {
    usuario = $(this).data('user');
    idpersona = $(this).data('idpersona');
    idusuario = $(this).data('userid');
    
    bootbox.confirm({
        message: "Esta seguro de reiniciar la contraseña del usuario '"+usuario+"'?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) { 
            if(result){
                data={id_persona:idpersona};
                cedula=consultarPHP('/smn-personas/cedula-personas-id',data,'html',false);
                if(cedula){
                    data={idusuario:idusuario};

                    respuesta=consultarPHP('/smn-usuarios-mippci/regenerar-contrasena',data,'html',false);
                    if(respuesta)
                        bootbox.alert('Contraseña reiniciada con éxito.');
                    else
                        bootbox.alert('Error al Regenerar la contraseña.');
                }else{
                    bootbox.alert('Error con la cédula de esta persona.');
                }
            }
        }
    });
});