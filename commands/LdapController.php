<?php

namespace app\commands;

use yii\console\Controller;

class LdapController extends Controller
{

	public function actionAdd($username) {
		$security = new \yii\base\Security();
		$new_user = new \webvimark\modules\UserManagement\models\User;

		/** 
			TODO: si es mysql .. hay que ponerle id = NULL 
		**/
		//$new_user->id = NULL;
		$new_user->username = $username;
		$new_user->password = md5($security->generateRandomString());
		$new_user->email = $username."@mippci.gob.ve";
		$new_user->email_confirmed = true;
		$new_user->ldap_user = true;
		$new_user->save();
		$this->stdout('Usuario '. $new_user->username.' agregado exitosamente!');		
	}

    public function actionIndex($message = null)
    {
    	if (is_null($message)) {
    		$this->stdout("Uso: yii ldap/add username");
    	}
        echo $message . "\n";
        return 0; 
    }
}