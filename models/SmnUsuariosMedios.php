<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_usuarios_medios".
 *
 * @property integer $id_usuario_medio
 * @property integer $id_medio
 * @property integer $id_usuario
 * @property boolean $estado
 *
 * @property SIAMSiamMedios $idMedio
 * @property SIMONUser $idUsuario
 */
class SmnUsuariosMedios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_usuarios_medios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_usuario'], 'integer'],
            [['estado'], 'boolean'],
            [['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id_medio']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => SmnUser::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_usuario_medio' => 'Id Usuario Medio',
            'id_medio' => 'Id Medio',
            'id_usuario' => 'Id Usuario',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(SmnUser::className(), ['id' => 'id_usuario']);
    }
}
