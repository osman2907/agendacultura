<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\controllers\SimonController;
use app\models\SmnConvocatorias;
use app\models\SmnUser;

/**
 * SmnConvocatoriasSearch represents the model behind the search form about `app\models\SmnConvocatorias`.
 */
class SmnConvocatoriasSearch extends SmnConvocatorias
{
    
    public function rules()
    {
        return [
            [['id', 'id_prioridad', 'id_sector', 'id_direccion', 'id_convocante', 'id_linea_discursiva', 'id_estatus', 'id_senal', 'id_sugerencia', 'id_coconvocante', 'senal_solicitada', 'id_estructura_gobierno', 'id_des_tendencia'], 'integer'],
            [['descripcion', 'fecha', 'vocero', 'vocero_rol', 'created_at', 'created_by', 'updated_at', 'updated_by', 'hora', 'medios_solicitados','id_medio','id_medio_solicitado','id_propagacion','id_estado','locacion','fecha_desde','fecha_hasta','confirmadas','asistencia'], 'safe'],
            [['agenda_comunicacional'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmnConvocatorias::find();
        $query->joinWith('idDireccion dir',true,'LEFT JOIN');

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 20),
        ]);

        if(!$this->validate()){
            return $dataProvider;
        }

        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        //Dependiendo del usuario conectado se filtran las convocatorias
        switch ($tipoUsuario){
            case 'COMUNICACIONES':
                $idMedio=$usuario->usuarioMedio->id_medio;
                //$this->id_medio=$idMedio;
            break;

            case 'ENTE ADSCRITO':
                $idConvocante=$usuario->idUsuarioSolicitante->id_solicitante;
                $query->andWhere(['or',['id_convocante'=>$idConvocante],['id_coconvocante'=>$idConvocante]]);
            break;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this->tableName().'.id' => $this->id,
            'fecha' => $this->fecha,
            'id_prioridad' => $this->id_prioridad,
            'id_sector' => $this->id_sector,
            'id_direccion' => $this->id_direccion,
            'id_convocante' => $this->id_convocante,
            'id_linea_discursiva' => $this->id_linea_discursiva,
            'id_estatus' => $this->id_estatus,
            'hora' => $this->hora,
            'id_senal' => $this->id_senal,
            'id_coconvocante' => $this->id_coconvocante,
            'senal_solicitada' => $this->senal_solicitada,
            'dir.id_estado' => $this->id_estado
        ]);

        $query->andFilterWhere(['ilike', 'descripcion', $this->descripcion])
            ->andFilterWhere(['ilike', 'vocero', $this->vocero])
            ->andFilterWhere(['ilike', 'vocero_rol', $this->vocero_rol])
            ->andFilterWhere(['ilike', 'medios_solicitados', $this->id_medio_solicitado]);

        if($this->id_medio){ //Filtra por los medios solicitados VTV, TVES, etc
            $query->andWhere("\"SIMON\".smn_convocatorias.id IN (SELECT id_convocatoria FROM \"SIMON\".smn_medios_convocatorias WHERE id_medio=$this->id_medio)");
        }

        if($this->id_propagacion){
            $query->andWhere("\"SIMON\".smn_convocatorias.id IN (SELECT id_convocatoria FROM \"SIMON\".smn_convocatoria_propagacion WHERE id_propagacion=$this->id_propagacion)");
        }

        if($this->locacion == 1){ //Convocatorias Nacionales
            $query->andFilterWhere(['dir.id_pais'=>Yii::$app->params['constantes']['idVenezuela']]);
        }

        if($this->locacion == 2){ //Convocatorias Nacionales
            $query->andFilterWhere(['<>','dir.id_pais',Yii::$app->params['constantes']['idVenezuela']]);
        }

        if(!empty($this->fecha_desde) && !empty($this->fecha_hasta)){
            $query->andWhere(['between','fecha',$this->fecha_desde,$this->fecha_hasta]);
        }else{
            $query->andWhere(['fecha'=>date("Y-m-d")]);
        }

        if(!empty($this->confirmadas)){
            $query->andWhere(['id_estatus'=>Yii::$app->params['convocatorias']['idConfirmadas'],'fecha'=>date("Y-m-d"),'(select count(id_convocatoria) FROM "SIMON".smn_medios_convocatorias WHERE id_convocatoria="SIMON".smn_convocatorias.id)'=>0]);   
        }

        if(!empty($this->asistencia)){
            $query->andWhere([
                'id_estatus'=>Yii::$app->params['convocatorias']['idConfirmadas'],
                'fecha'=>date("Y-m-d"),
                "(select count(id_convocatoria) FROM \"SIMON\".smn_medios_convocatorias WHERE id_medio=$idMedio AND id_convocatoria=\"SIMON\".smn_convocatorias.id AND asistencia is null)"=>1]);
        }

        $query->orderBy(["fecha"=>SORT_DESC]);

        return $dataProvider;
    }
}
