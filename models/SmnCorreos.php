<?php

namespace app\models;

use Yii;
use app\models\SmnUser;
/**
 * This is the model class for table "SIMON.smn_correos".
 *
 * @property integer $id
 * @property string $correo
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property SIMONSmnMediosCorreos[] $sIMONSmnMediosCorreos
 * @property SIMONSmnMedios[] $medios
 * @property SIMONSmnPersonasCorreos[] $sIMONSmnPersonasCorreos
 * @property SIMONSmnPersonas[] $personas
 * @property SIMONSmnSolicitantesCorreo[] $sIMONSmnSolicitantesCorreos
 * @property SIMONSmnSolicitantes[] $solicitantes
 */
class SmnCorreos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_correos';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['correo'], 'required'],
            [['correo'], 'required','on'=>'registrarUsuariosSolicitantes'],
            [['correo'], 'email'],
            [['correo', 'created_by', 'updated_by'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['correo'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'correo' => 'Correo Electrónico',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosCorreos()
    {
        return $this->hasMany(SIMONSmnMediosCorreos::className(), ['id_correo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedios()
    {
        return $this->hasMany(SIMONSmnMedios::className(), ['id' => 'id_medio'])->viaTable('smn_medios_correos', ['id_correo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnPersonasCorreos()
    {
        return $this->hasMany(SIMONSmnPersonasCorreos::className(), ['id_correo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(SIMONSmnPersonas::className(), ['id' => 'id_persona'])->viaTable('smn_personas_correos', ['id_correo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnSolicitantesCorreos()
    {
        return $this->hasMany(SIMONSmnSolicitantesCorreo::className(), ['id_correo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitantes()
    {
        return $this->hasMany(SIMONSmnSolicitantes::className(), ['id' => 'id_solicitante'])->viaTable('smn_solicitantes_correo', ['id_correo' => 'id']);
    }
    
    public static function buscarCorreo($id_correo){

        $condicion= ['id'=> $id_correo];
        $model = SmnCorreos::find()->where(['id_correo'=>$id_correo])->all();
        return $model;
    }
    
    public function getIdCorreos()
    {
        return $this->hasMany(SmnCorreos::className(), ['id' => 'id_correo'])->viaTable('SIMON.smn_personas_correos', ['id_correo' => 'id']);
    }
}
