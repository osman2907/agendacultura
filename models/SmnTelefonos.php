<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_telefonos".
 *
 * @property integer $id
 * @property string $telefono
 * @property string $tipo_telefono
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property SIMONSmnMediosTelefonos[] $sIMONSmnMediosTelefonos
 * @property SIMONSmnMedios[] $medios
 * @property SIMONSmnPersonasTelefonos[] $sIMONSmnPersonasTelefonos
 * @property SIMONSmnPersonas[] $personas
 * @property SIMONSmnSolicitantesTelefonos[] $sIMONSmnSolicitantesTelefonos
 * @property SIMONSmnSolicitantes[] $solicitantes
 */
class SmnTelefonos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_telefonos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['telefono'], 'required'],
            [['telefono'], 'integer'],
            [['telefono', 'tipo_telefono', 'created_by', 'updated_by'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['telefono'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefono' => 'Teléfono',
            'tipo_telefono' => 'Tipo Telefono',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public static function buscarTelefonoPersona($cedula){
        $condicion= ['cedula'=> $cedula];
        //$model = SmnPersonas::find($cedula)->where($condicion)->one();
        //$modelPersonaTelefono= SmnPersonasTelefonos::find()->where(['id_persona'=>$model->id])->one();
        //$modelTelefono= SmnTelefonos::find()->where(['id'=>$modelPersonaTelefono->id_telefono])->one();
        /*echo "<pre>";
        print_r($modelTelefono);
        echo "</pre>";*/
        return $modelTelefono;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosTelefonos()
    {
        return $this->hasMany(SmnMediosTelefonos::className(), ['id_telefono' => 'id_telefono']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiamMedios()
    {
        return $this->hasMany(SiamMedios::className(), ['id' => 'id_medio'])->viaTable('smn_medios_telefonos', ['id_telefono' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnPersonasTelefonos()
    {
        return $this->hasMany(SmnPersonasTelefonos::className(), ['id_telefono' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(SmnPersonas::className(), ['id' => 'id_persona'])->viaTable('smn_personas_telefonos', ['id_telefono' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnSolicitantesTelefonos()
    {
        return $this->hasMany(SmnSolicitantesTelefonos::className(), ['id_telefono' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitantes()
    {
        return $this->hasMany(SmnSolicitantes::className(), ['id' => 'id_solicitante'])->viaTable('smn_solicitantes_telefonos', ['id_telefono' => 'id']);
    }
}
