<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.mci_estado".
 *
 * @property string $nom_estado
 * @property integer $id_pais
 * @property integer $cod_postal
 * @property integer $id_estado
 */
class MciEstado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_estado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_estado', 'id_pais'], 'required'],
            [['id_pais', 'cod_postal'], 'integer'],
            [['nom_estado'], 'string', 'max' => 50],
            [['id_pais'], 'exist', 'skipOnError' => true, 'targetClass' => MciPais::className(), 'targetAttribute' => ['id_pais' => 'id_pais']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nom_estado' => 'Estado',
            'id_pais' => 'Pais',
            'cod_postal' => 'Cod Postal',
            'id_estado' => 'Id Estado',
        ];
    }


    public function listEstados(){
        $estado=MciEstado::find()->where(['id_pais'=>1])->orderBy(['nom_estado'=>SORT_ASC])->all();
        $listEstados=ArrayHelper::map($estado,'id_estado','nom_estado');
        return $listEstados;
    }
}
