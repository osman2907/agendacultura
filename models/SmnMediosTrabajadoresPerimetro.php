<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_medios_trabajadores_perimetro".
 *
 * @property integer $id_medio
 * @property integer $id_persona
 * @property integer $id_estado
 *
 * @property MciEstado $idEstado
 */
class SmnMediosTrabajadoresPerimetro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_medios_trabajadores_perimetro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_persona', 'id_estado'], 'required'],
            [['id_medio', 'id_persona', 'id_estado'], 'integer'],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_medio' => 'Id Medio',
            'id_persona' => 'Id Persona',
            'id_estado' => 'Id Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado()
    {
        return $this->hasOne(MciEstado::className(), ['id_estado' => 'id_estado']);
    }
}
