<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_linea_discursiva".
 *
 * @property integer $id
 * @property string $descripcion
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property SmnConvocatorias[] $smnConvocatorias
 * @property SmnEntrevistas[] $smnEntrevistas
 */
class SmnLineaDiscursiva extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_linea_discursiva';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion'], 'required'],
            [['descripcion', 'created_by', 'updated_by'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_linea_discursiva' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnEntrevistas()
    {
        return $this->hasMany(SmnEntrevistas::className(), ['id_linea_discursiva' => 'id']);
    }
}
