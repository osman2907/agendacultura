<?php

namespace app\models;

use Yii;
use app\models\SmnUser;
/**
 * This is the model class for table "SIMON.smn_personas_correos".
 *
 * @property integer $id_persona
 * @property integer $id_correo
 *
 * @property SIMONSmnCorreos $correo
 * @property SIMONSmnPersonas $persona
 */
class SmnPersonasCorreos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_personas_correos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_correo'], 'required'],
            [['id_persona', 'id_correo'], 'integer'],
            [['id_correo'], 'exist', 'skipOnError' => true, 'targetClass' => SmnCorreos::className(), 'targetAttribute' => ['id_correo' => 'id']],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_persona' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => 'Id Persona',
            'id_correo' => 'Id Correo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreo()
    {
        return $this->hasOne(SmnCorreos::className(), ['id' => 'id_correo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }
    
    public function getCorreosPersonas()
    {
        return $this->hasOne(SmnPersonasCorreos::className(), ['id_correo' => 'id']);
    }
}
