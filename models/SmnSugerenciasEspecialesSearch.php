<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmnSugerenciasEspeciales;

/**
 * SmnSugerenciasEspecialesSearch represents the model behind the search form about `app\models\SmnSugerenciasEspeciales`.
 */
class SmnSugerenciasEspecialesSearch extends SmnSugerenciasEspeciales
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_sector'], 'integer'],
            [['tema', 'acciones', 'fecha_inicio', 'created_at', 'created_by', 'updated_at', 'updated_by', 'fecha_fin', 'recomendaciones', 'responsables'], 'safe'],
            [['estatus'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmnSugerenciasEspeciales::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio' => $this->fecha_inicio,
            'id_sector' => $this->id_sector,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'fecha_fin' => $this->fecha_fin,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'tema', $this->tema])
            ->andFilterWhere(['like', 'acciones', $this->acciones])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'recomendaciones', $this->recomendaciones])
            ->andFilterWhere(['like', 'responsables', $this->responsables]);

        $query->orderBy('id desc');

        return $dataProvider;
    }
}
