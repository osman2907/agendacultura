<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_solicitantes_telefonos".
 *
 * @property integer $id_solicitante
 * @property integer $id_telefono
 *
 * @property SmnSolicitantes $idSolicitante
 * @property SmnTelefonos $idTelefono
 */
class SmnSolicitantesTelefonos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_solicitantes_telefonos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitante', 'id_telefono'], 'required'],
            [['id_solicitante', 'id_telefono'], 'integer'],
            [['id_solicitante'], 'exist', 'skipOnError' => true, 'targetClass' => SmnSolicitantes::className(), 'targetAttribute' => ['id_solicitante' => 'id']],
            [['id_telefono'], 'exist', 'skipOnError' => true, 'targetClass' => SmnTelefonos::className(), 'targetAttribute' => ['id_telefono' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_solicitante' => 'Id Solicitante',
            'id_telefono' => 'Id Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_solicitante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTelefono()
    {
        return $this->hasOne(SmnTelefonos::className(), ['id' => 'id_telefono']);
    }
}
