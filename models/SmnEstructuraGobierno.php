<?php

namespace app\models;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "SIMON.smn_estructura_gobierno".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $tipo
 *
 * @property SmnConvocatorias[] $smnConvocatorias
 * @property MciDescripcion $tipo0
 * @property SmnSolicitanteEstructuraGobierno[] $smnSolicitanteEstructuraGobiernos
 * @property SmnSolicitantes[] $idSolicitantes
 */
class SmnEstructuraGobierno extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_estructura_gobierno';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string'],
            [['tipo'], 'integer'],
            [['tipo'], 'exist', 'skipOnError' => true, 'targetClass' => MciDescripcion::className(), 'targetAttribute' => ['tipo' => 'id_descripcion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
        ];
    }

    public static function listEstructuraGobiernos()
    {
        $estructuragobierno=SmnEstructuraGobierno::find()->orderBy(['tipo'=>SORT_ASC,'nombre'=>SORT_ASC])->all();
        $listestructuragobierno=ArrayHelper::map($estructuragobierno,'id','nombre');
        return $listestructuragobierno;
                
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_estructura_gobierno' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo0()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnSolicitanteEstructuraGobiernos()
    {
        return $this->hasMany(SmnSolicitanteEstructuraGobierno::className(), ['id_estructura_gobierno' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitantes()
    {
        return $this->hasMany(SmnSolicitantes::className(), ['id' => 'id_solicitante'])->viaTable('smn_solicitante_estructura_gobierno', ['id_estructura_gobierno' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SmnEstructuraGobiernoQuery the active query used by this AR class.
     */
   
}
