<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmnPersonas;

/**
 * SIMONSmnPersonasSearch represents the model behind the search form about `app\models\SIMONSmnPersonas`.
 */
class SmnPersonasSearch extends SmnPersonas
{
    /**
     * @inheritdoc
     */
    public $direccion;

    
    public function rules()
    {
        return [
            [['id', 'cedula', 'id_direccion', 'id_tabla'], 'integer'],
            [['nombre', 'apellido', 'created_at', 'created_by', 'updated_at', 'updated_by', 'tipo','direccion'], 'safe'],
            [['activo'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmnPersonas::find();


        ///con esto añado las otras tablas que quiero filtrar
        //$query->joinWith(['direccion']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        /*
        $dataProvider->sort->attributes['direccion'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['smn_direccion.direccion' => SORT_ASC],
        'desc' => ['smn_direccion.direccion' => SORT_DESC],
        ];
        */
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cedula' => $this->cedula,
            'id_direccion' => $this->id_direccion,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activo' => $this->activo,
            'id_tabla' => $this->id_tabla,
        ]);

        $query->andFilterWhere(['ilike', 'nombre', $this->nombre])
            ->andFilterWhere(['ilike', 'apellido', $this->apellido])
            ->andFilterWhere(['ilike', 'created_by', $this->created_by])
            ->andFilterWhere(['ilike', 'updated_by', $this->updated_by])
            ->andFilterWhere(['ilike', 'tipo', $this->tipo]);
           // ->andFilterWhere(['like', 'smn_direccion.direccion', $this->direccion]);
            
        
        return $dataProvider;
    }
}
