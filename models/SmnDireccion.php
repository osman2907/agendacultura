<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_direccion".
 *
 * @property integer $id
 * @property string $direccion
 * @property integer $id_estado
 * @property integer $id_ciudad
 * @property integer $id_municipio
 * @property integer $id_parroquia
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $id_pais
 *
 * @property SIMONSmnConvocatorias[] $sIMONSmnConvocatorias
 * @property MciEstado $estado
 * @property MciMunicipio $municipio
 * @property MciParroquia $parroquia
 * @property SIMONSmnMedios[] $sIMONSmnMedios
 * @property SIMONSmnMediosActividades[] $sIMONSmnMediosActividades
 * @property SIMONSmnPersonas[] $sIMONSmnPersonas
 * @property SIMONSmnSolicitantes[] $sIMONSmnSolicitantes
 */
class SmnDireccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_direccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'id_ciudad', 'id_municipio', 'id_parroquia','direccion'], 'required'],
            [['direccion', 'created_by', 'updated_by'], 'string'],
            [['id_estado', 'id_ciudad', 'id_municipio', 'id_parroquia', 'id_pais'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
            [['id_municipio'], 'exist', 'skipOnError' => true, 'targetClass' => MciMunicipio::className(), 'targetAttribute' => ['id_municipio' => 'id_municipio']],
            [['id_parroquia'], 'exist', 'skipOnError' => true, 'targetClass' => MciParroquia::className(), 'targetAttribute' => ['id_parroquia' => 'id_parroquia']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'direccion' => 'Dirección',
            'id_estado' => 'Estado',
            'id_ciudad' => 'Ciudad',
            'id_municipio' => 'Municipio',
            'id_parroquia' => 'Parroquia',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_pais' => 'Id Pais',
        ];
    }

    public function getDireccionCompleta(){
        $direccion=$this->direccion.". ";
        $direccion.="Estado: ".$this->estado->nom_estado.". ";
        $direccion.="Ciudad: ".$this->ciudad->nom_ciudad.". ";
        $direccion.="Municipio: ".$this->municipio->nom_municipio.". ";
        return $direccion;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_direccion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPais()
    {
        return $this->hasOne(MciPais::className(), ['id_pais' => 'id_pais']);
    }

    public function getEstado()
    {
        return $this->hasOne(MciEstado::className(), ['id_estado' => 'id_estado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(MciCiudad::className(), ['id_ciudad' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipio()
    {
        return $this->hasOne(MciMunicipio::className(), ['id_municipio' => 'id_municipio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParroquia()
    {
        return $this->hasOne(MciParroquia::className(), ['id_parroquia' => 'id_parroquia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMedios()
    {
        return $this->hasMany(SmnMedios::className(), ['id_direccion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosActividades()
    {
        return $this->hasMany(SmnMediosActividades::className(), ['id_direccion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnPersonas()
    {
        return $this->hasMany(SmnPersonas::className(), ['id_direccion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnSolicitantes()
    {
        return $this->hasMany(SmnSolicitantes::className(), ['id_direccion' => 'id']);
    }
}
