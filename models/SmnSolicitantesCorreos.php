<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_solicitantes_correo".
 *
 * @property integer $id_solicitante
 * @property integer $id_correo
 *
 * @property SmnCorreos $idCorreo
 * @property SmnSolicitantes $idSolicitante
 */
class SmnSolicitantesCorreos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_solicitantes_correo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitante', 'id_correo'], 'required'],
            [['id_solicitante', 'id_correo'], 'integer'],
            [['id_correo'], 'exist', 'skipOnError' => true, 'targetClass' => SmnCorreos::className(), 'targetAttribute' => ['id_correo' => 'id']],
            [['id_solicitante'], 'exist', 'skipOnError' => true, 'targetClass' => SmnSolicitantes::className(), 'targetAttribute' => ['id_solicitante' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_solicitante' => 'Id Solicitante',
            'id_correo' => 'Id Correo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCorreo()
    {
        return $this->hasOne(SmnCorreos::className(), ['id' => 'id_correo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_solicitante']);
    }
}
