<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.mci_ciudad".
 *
 * @property integer $id_estado
 * @property string $nom_ciudad
 * @property integer $id_ciudad
 */
class MciCiudad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_ciudad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'nom_ciudad'], 'required'],
            [['id_estado'], 'integer'],
            [['nom_ciudad'], 'string', 'max' => 50],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id_estado' => 'Id Estado',
            'nom_ciudad' => 'Nom Ciudad',
            'id_ciudad' => 'Id Ciudad',
        ];
    }

    public function listCiudades($idEstado=NULL){
        $condicion=!is_null($idEstado)?['id_estado'=>$idEstado]:[];
        $ciudades=MciCiudad::find()->where($condicion)->all();
        $listCiudades=ArrayHelper::map($ciudades,'id_ciudad','nom_ciudad');
        return $listCiudades;
    }

}
