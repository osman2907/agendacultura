<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use app\controllers\SmnConvocatoriasController;
use app\controllers\SimonController;

use yii\web\NotFoundHttpException;
use yii\base\ErrorException;

class SmnConvocatorias extends \yii\db\ActiveRecord
{
    public $id_pais;
    public $id_estado;
    public $id_municipio;
    public $id_ciudad;
    public $direccion;
    public $archivo;
    public $observaciones;
    public $cedula_nueva;
    public $id_medio;
    public $id_medio_solicitado;
    public $id_propagacion;
    public $locacion;
    public $fecha_desde;
    public $fecha_hasta;
    public $confirmadas;
    public $asistencia;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_convocatorias';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['created_at', 'updated_at', 'hora','id_estado','senal_solicitada'], 'safe'],

            [['fecha','hora','descripcion','id_direccion', 'id_convocante','id_coconvocante', 'vocero', 'vocero_rol', 'id_estatus','id_pais','id_estado','id_ciudad','id_municipio','direccion'], 'required','on'=>'registrar'],

            [['fecha','hora','descripcion','id_direccion', 'id_convocante','id_coconvocante', 'vocero', 'vocero_rol', 'id_estatus','id_pais','id_estado','id_ciudad','id_municipio','direccion'], 'required','on'=>'modificar'],

            [['id_senal','id_prioridad','id_sector','id_estatus'], 'required','on'=>'confirmar'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'fecha' => 'Fecha de la Convocatoria',
            'id_prioridad' => 'Nivel de Vocería',
            'id_sector' => 'Sector',
            'id_direccion' => 'Id Direccion',
            'id_convocante' => 'Convocante',
            'id_linea_discursiva' => 'Id Linea Discursiva',
            'vocero' => 'Vocero',
            'vocero_rol' => 'Cargo del Vocero',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_estatus' => 'Estatus',
            'hora' => 'Hora',
            'id_senal' => 'Señal',
            'medios_solicitados' => 'Medios Solicitados',
            'id_sugerencia' => 'Id Sugerencia',
            'id_coconvocante' => 'Coconvocante',
            'senal_solicitada' => 'Señal Solicitada',
            'id_estructura_gobierno' => 'Revolución',
            'id_des_tendencia' => 'Tendencia',
            'agenda_comunicacional' => 'Agenda Comunicacional',
            'id_pais'=>'País',
            'id_estado'=>'Estado',
            'id_municipio'=>'Municipio',
            'id_ciudad'=>'Ciudad',
            'direccion'=>'Dirección',
            'fecha_desde'=>'Desde',
            'fecha_hasta'=>'Hasta'
        ];
    }

    public function getMediosConvocados(){
        $modelMedConv=SmnMediosConvocatorias::find()->where(['id_convocatoria'=>$this->id])->all();
        $html="<ul class='list-group'>";
        foreach ($modelMedConv as $medioConvocado){
            $html.="<li class='list-group-item f7 pad5'>".$medioConvocado->idMedio->identificacion."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getMediosAsistentes(){
        $modelMedConv=SmnMediosConvocatorias::find()->where(
            ['id_convocatoria'=>$this->id,'asistencia'=>'true']
        )->all();
        $html="<ul class='list-group'>";
        foreach ($modelMedConv as $medioConvocado){
            $idMedio=$medioConvocado->id_medio;
            $idConvocatoria=$medioConvocado->id_convocatoria;
            $html.="<li class='list-group-item f7 pad5 cursor convocado'>".
                "<div class='id-medio oculto'>$idMedio</div>".
                "<div class='id-convocatoria oculto'>$idConvocatoria</div><b>".
                $medioConvocado->idMedio->identificacion."</b> - ".
                $medioConvocado->idSenal->nom_descripcion."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getPropagacion(){
        $modelConvProp=SmnConvocatoriaPropagacion::find()->where(['id_convocatoria'=>$this->id])->all();
        $html="<ul class='list-group'>";
        foreach ($modelConvProp as $formaDifusion){
            $html.="<li class='list-group-item f7 pad5'>".$formaDifusion->idPropagacion->nom_descripcion."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getCobertura(){
        return "<b>hola mundo</b>";
    }

    public function getContactos(){
        $modelConvPers=SmnConvocatoriasPersonas::find()->where(['id_convocatoria'=>$this->id])->all();
        $html="<ul class='list-group'>";
        foreach ($modelConvPers as $personas){
            $html.="<li class='list-group-item f7 pad5'>".$personas->idPersona->datosBandeja."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getEstatus(){

        switch ($this->id_estatus){
            case Yii::$app->params['convocatorias']['idPorConfirmar']:
                return "<span class='label label-warning'>".$this->idEstatus->nom_descripcion."</span>";       
            break;

            case Yii::$app->params['convocatorias']['idConfirmadas']:
                return "<span class='label label-success'>".$this->idEstatus->nom_descripcion."</span>";
            break;
            
            default:
                return "<span class='label label-default'>".$this->idEstatus->nom_descripcion."</span>";
            break;
        }
    }

    public function getEstatusLetra(){
        switch ($this->id_estatus){
            case Yii::$app->params['convocatorias']['idPorConfirmar']:
                return "<div class='letra-amarilla negrita'>".$this->idEstatus->nom_descripcion."</div>";
            break;

            case Yii::$app->params['convocatorias']['idConfirmadas']:
                return "<div class='letra-verde negrita'>".$this->idEstatus->nom_descripcion."</div>";
            break;
            
            default:
                return "<div class='negrita'>".$this->idEstatus->nom_descripcion."</div>";
            break;
        }
    }

    public function getObtenerObservaciones(){
        $modelObservaciones=$this->idObservaciones;
        $html="<ul class='list-group'>";
        foreach ($modelObservaciones as $observacion){
            $html.="<li class='list-group-item f7 pad5'>";
                $html.="<b>".$observacion->tipo_creador."</b><br>";
                $html.=$observacion->observacion;
            $html.="</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getBotonesConvocatorias(){
        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
        $model=$this;
        return Yii::$app->controller->renderPartial('botones-convocatorias',compact('model','usuario','tipoUsuario'));
    }

    public static function getTendencias(){
        return ['0'=>'ALIADO','1'=>'OPOSITOR'];
    }

    public function solicitantesContactos(){
        $connection = Yii::$app->getDb();
        $sql="
            SELECT distinct(id_persona),p.*,CONCAT(p.cedula,' - ',p.apellido,' ',p.nombre) AS \"contacto\" FROM (
                SELECT id_persona FROM \"SIMON\".smn_solicitantes_contactos sc
                WHERE id_solicitante=$this->id_convocante
                /* Se comentó para que sólo aparecieran las personas asociadas al solicitante
                UNION ALL
                SELECT id_persona FROM \"SIMON\".smn_convocatorias_personas cp
                    INNER JOIN \"SIMON\".smn_convocatorias c ON cp.id_convocatoria=c.id
                WHERE id_convocante=$this->id_convocante*/
            ) \"cc\"
                INNER JOIN \"SIMON\".smn_personas p ON cc.id_persona=p.id
            WHERE activo=TRUE
            ORDER BY apellido";

        $contactos = $connection->createCommand($sql)->queryAll();
        return $contactos;
    }


    public function registrarConvocatoria($post,$files,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        try{
            if($post['SmnConvocatorias']['id_pais'] != Yii::$app->params['constantes']['idVenezuela']){
                $post['SmnConvocatorias']['id_estado']=0;
                $post['SmnConvocatorias']['id_municipio']=0;
                $post['SmnConvocatorias']['id_ciudad']=0;
            }
            //Crea el elemento SmnDirección en el post para asociarlo al objeto con el evento load.
            $post['SmnDireccion']=$post['SmnConvocatorias'];
            $modelDireccion=new SmnDireccion;
            $modelDireccion->load($post);
            //si el país es distinto a venezuela el estado y municipio es cero.
            $modelDireccion->id_parroquia=0;
            $modelDireccion->save();
            $model->load($post);
            $model->fecha=$this->cambiarFormatoFecha($model->fecha);
            $model->id_direccion=$modelDireccion->id;
            $model->medios_solicitados=implode(";",$post['SmnConvocatorias']['medios_solicitados']);//Guarda los tipos de medios solicitados separados por ";".


            if($model->save()){ //Si la convocatoria se guarda con éxito ejecuta lo siguiente.
                $personas=$post['SmnConvocatorias']['id_persona'];//Contiene las personas asociadas.
                foreach ($personas as $id_persona){ //Recorre las personas asociadas en el POST.
                    $modelConvPers=new SmnConvocatoriasPersonas;
                    $modelConvPers->id_convocatoria=$model->id;
                    $modelConvPers->id_persona=$id_persona;
                    $modelConvPers->save();

                    $modelSolCont=SmnSolicitantesContactos::find()->where(
                        ['id_solicitante'=>$model->id_convocante,'id_persona'=>$id_persona])->all();
                    if(count($modelSolCont) == 0){
                        $modelSolCont=new SmnSolicitantesContactos;
                        $modelSolCont->id_solicitante=$model->id_convocante;
                        $modelSolCont->id_persona=$id_persona;
                        $modelSolCont->save();
                    }
                }

                if(!empty($files['SmnConvocatorias']['name']['archivo'][0])){

                    foreach ($files['SmnConvocatorias']['tmp_name']['archivo'] as $clave => $valor){ //Recorre los archivos asociados.

                        $dir_subida = Yii::$app->params['convocatorias']['rutaArchivos'];
                        $extension=$this->obtenerExtension($files['SmnConvocatorias']['name']['archivo'][$clave]);
                        $nombre=$model->id."-".$clave; //Cambia el nombre del archivo para que no se repitan en el servidor.
                        $fichero_subido = $dir_subida.basename($nombre.".".$extension);
                        move_uploaded_file($valor,$fichero_subido); //sube el archivo desde el temporal para el servidor.

                        $modelArchivo=new SmnArchivos;
                        $modelArchivo->archivo=basename($nombre.".".$extension);
                        $modelArchivo->id_tabla=$model->id;
                        $modelArchivo->tipo_tabla="smn_convocatorias";
                        $modelArchivo->tipo_creador=$tipoUsuario;
                        $modelArchivo->nombre=$files['SmnConvocatorias']['name']['archivo'][$clave];
                        $modelArchivo->save();
                    }
                }


                //Para guardar las observaciones hechas por cada involucrado(MINCI, Medios o Solicitantes)...
                if(isset($post['SmnConvocatorias']['observaciones'])){
                    $post['SmnObservaciones']=$post['SmnConvocatorias'];
                    $post['SmnObservaciones']['observacion']=$post['SmnConvocatorias']['observaciones'];
                    $modelObservaciones=new SmnObservaciones;
                    $modelObservaciones->load($post);
                    $modelObservaciones->id_tabla=$model->id;
                    $modelObservaciones->tipo_tabla="smn_convocatorias";
                    $modelObservaciones->save();
                }
            }else{ //Cuando no registra la convocatoria por algún error.
                $transaction->rollback();
                return false;
            }
            
            $transaction->commit();
            return $model; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
            $transaction->rollback();
            return false;
        }
    }


    public function modificarConvocatoria($post,$model){
        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $model->medios_solicitados=implode(";",$post['SmnConvocatorias']['medios_solicitados']);//Guarda los tipos de medios 
            $model->load($post);
            $model->senal_solicitada=(!isset($post['SmnConvocatorias']['senal_solicitada']))?'':true;

            if($tipoUsuario != 'ENTE ADSCRITO'){
                $tablaConvProp=SmnConvocatoriaPropagacion::tableName();
                \Yii::$app->db->createCommand()->delete($tablaConvProp,['id_convocatoria' => $model->id])->execute();
            }else{
                $model->id_estatus=Yii::$app->params['convocatorias']['idPorConfirmar'];
            }
            
            if(isset($post['SmnConvocatorias']['formas_difusion'])){
                $formasDifusion=$post['SmnConvocatorias']['formas_difusion'];
                foreach ($formasDifusion as $formaDifusion){
                    $modelConvProp=new SmnConvocatoriaPropagacion;
                    $modelConvProp->id_convocatoria=$model->id;
                    $modelConvProp->id_propagacion=$formaDifusion;
                    $modelConvProp->save();
                }
            }

            $model->fecha=$this->cambiarFormatoFecha($model->fecha);
            if($model->save()){
                if($post['SmnConvocatorias']['id_pais'] != Yii::$app->params['constantes']['idVenezuela']){
                    $post['SmnConvocatorias']['id_estado']=0;
                    $post['SmnConvocatorias']['id_municipio']=0;
                    $post['SmnConvocatorias']['id_ciudad']=0;
                }
                //Crea el elemento SmnDirección en el post para asociarlo al objeto con el evento load.
                $post['SmnDireccion']=$post['SmnConvocatorias'];
                $modelDireccion=SmnDireccion::findOne($model->id_direccion);
                $modelDireccion->load($post);
                //si el país es distinto a venezuela el estado y municipio es cero.
                $modelDireccion->id_parroquia=0;
                if(!$modelDireccion->save()){
                    $transaction->rollback();
                    return false;
                }
            }else{
                $transaction->rollback();
                return false;
            }
            $transaction->commit();
            return $model; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }

    public function confirmarConvocatoria($post,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $model->load($post);
            if(!$model->save()){
                $transaction->rollback();
                return false;
            }
            $transaction->commit();
            return $model; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function confirmarAsistenciaMedio($post,$modelMedConv){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $modelMedConv->load($post);
            $modelMedConv->fecha_confirmado=date("Y-m-d H:i:s");
            if(empty($modelMedConv->asistencia)){
                $modelMedConv->id_senal="";
            }
            if(!$modelMedConv->save()){
                /*echo "<pre>";
                print_r($modelMedConv->errors);
                echo "</pre>";
                exit();*/
                throw new Exception('Error guardando en Medios Convocatorias');
                $transaction->rollback();
                return false;
            }

            //Agrega una nueva observación como medios...
            if(!empty($post['SmnMediosConvocatorias']['observaciones'])){
                $modelObs=new SmnObservaciones;
                $modelObs->observacion=$post['SmnMediosConvocatorias']['observaciones'];
                $modelObs->id_tabla=$modelMedConv->id_convocatoria;
                $modelObs->tipo_tabla="smn_convocatorias";
                $modelObs->tipo_creador="MEDIOS";
                if(!$modelObs->save()){
                    throw new Exception('Error guardando en Observaciones');
                    $transaction->rollback();
                    return false;
                }
            }

            //Si el medio no va a asistir elimina los trabajadores asociados a esta convocatoria.
            if(empty($modelMedConv->asistencia)){
                if(!$this->removerTrabajadoresConvocatorias()){
                    throw new Exception('Error eliminando trabajadores asociados');
                    $transaction->rollback();
                    return false;
                }
            }

            $transaction->commit();
            return $modelMedConv; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
            return false;
        }
    }

    public function removerTrabajadoresConvocatorias(){
        $condiciones=['id_convocatoria'=>$this->id,'id_medio'=>$this->id_medio];
        $trabajadores=SmnConvocatoriasTrabajadores::find()->where($condiciones)->all();

        if(count($trabajadores) > 0){
            $tablaConvTrab=SmnConvocatoriasTrabajadores::tableName();
            return \Yii::$app->db->createCommand()->delete($tablaConvTrab,$condiciones)->execute();
        }else{
            return TRUE;
        }
    }


    public function convocarMedio($post,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $model->load($post);

            if(!$model->validarRegistro()){
                $transaction->rollback();
                return false;
            }

            $model->fecha_asignado=date("Y-m-d H:i:s");
            if(!$model->save()){
                $transaction->rollback();
                return false;
            }
            $transaction->commit();
            return $model; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function removerConvocatoria($idConvocatoria,$idMedio){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $condiciones=['id_convocatoria'=>$idConvocatoria,'id_medio'=>$idMedio];
            $model=SmnMediosConvocatorias::find()->where($condiciones)->one();
            if(!$model->delete()){
                $transaction->rollback();
                return false;
            }
            $transaction->commit();
            return $model; //Retorna el objeto de convocatorias con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function obtenerExtension($archivo){
        $partes=explode(".",$archivo);
        $extension=$partes[count($partes)-1];
        return $extension;
    }

    public function cambiarFormatoFecha($fecha){
        if(strpos($fecha,"/")){ 
            $fecha=explode("/",$fecha);
            $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
        }else{
            $fecha=explode("-",$fecha);
            $fecha=$fecha[2]."/".$fecha[1]."/".$fecha[0];
        }
        return $fecha;
    }

    public function asignarValores(){
        /*Datos de Dirección*/
        $this->direccion=$this->idDireccion->direccion;
        $this->id_pais=$this->idDireccion->id_pais;
        if($this->id_pais == Yii::$app->params['constantes']['idVenezuela']){
            $this->id_estado=$this->idDireccion->id_estado;
        }
        /*Fin Datos de Dirección*/
    }

    public function contactosNoElegidos($personas){
        $elegidos=SmnConvocatoriasPersonas::find()->where(['id_convocatoria'=>$this->id])->all();

        foreach ($elegidos as $elegido){
            if(array_key_exists($elegido->id_persona,$personas)){
                unset($personas[$elegido->id_persona]);
            }
        }
        return $personas;
    }

    public function trabajadoresElegidos(){
        $condiciones=['id_convocatoria'=>$this->id,'id_medio'=>$this->id_medio];
        $elegidos=SmnConvocatoriasTrabajadores::find()->where($condiciones)->all();

        return $elegidos;
    }

    public function listadoTrabajadores(){
        $condicion="status=TRUE AND id_persona IN (SELECT id_persona FROM \"SIMON\".smn_medios_trabajadores_perimetro WHERE id_medio=$this->id_medio AND id_estado=$this->id_estado)";
        $mediosTrabajadores=SmnMediosTrabajadores::find()->where($condicion)->all();
        return $mediosTrabajadores;
    }

    public function mediosNoConvocados($lista){
        $convocados=SmnMediosConvocatorias::find()->where(['id_convocatoria'=>$this->id])->all();

        foreach ($convocados as $convocado){
            if(array_key_exists($convocado->id_medio,$lista)){
                unset($lista[$convocado->id_medio]);
            }
        }
        return $lista;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriaEquipos()
    {
        return $this->hasMany(SmnConvocatoriaEquipos::className(),['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEquipos()
    {
        return $this->hasMany(SmnEquipos::className(), ['id' => 'id_equipo'])->viaTable('smn_convocatoria_equipos', ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriaPropagacions()
    {
        return $this->hasMany(SmnConvocatoriaPropagacion::className(), ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPropagacions()
    {
        return $this->hasMany(MciDescripcion::className(), ['id_descripcion' => 'id_propagacion'])->viaTable('smn_convocatoria_propagacion', ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriasTrabajadores()
    {
        return $this->hasMany(SmnConvocatoriasTrabajadores::className(), ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTrabajadors()
    {
        return $this->hasMany(SmnPersonas::className(), ['id' => 'id_trabajador'])->viaTable('smn_convocatoria_trabajadores', ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrioridad()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_prioridad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSector()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_sector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstatus()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSenal()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_senal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDireccion()
    {
        return $this->hasOne(SmnDireccion::className(), ['id' => 'id_direccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstructuraGobierno()
    {
        return $this->hasOne(SmnEstructuraGobierno::className(), ['id' => 'id_estructura_gobierno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLineaDiscursiva()
    {
        return $this->hasOne(SmnLineaDiscursiva::className(), ['id' => 'id_linea_discursiva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_convocante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCoconvocante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_coconvocante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSugerencia()
    {
        return $this->hasOne(SmnSugerenciasEspeciales::className(), ['id' => 'id_sugerencia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenalSolicitada()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'senal_solicitada']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriasPersonas()
    {
        return $this->hasMany(SmnConvocatoriasPersonas::className(), ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosConvocatorias()
    {
        return $this->hasMany(SmnMediosConvocatorias::className(), ['id_convocatoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedios()
    {
        return $this->hasMany(SmnMedios::className(), ['id' => 'id_medio'])->viaTable('smn_medios_convocatorias', ['id_convocatoria' => 'id']);
    }

    public function getConvocatoriasArchivos()
    {
        return $this->hasMany(SmnArchivos::className(), ['id_tabla' => 'id']);
    }

    public function getIdObservaciones()
    {
        return $this->hasMany(SmnObservaciones::className(), ['id_tabla' => 'id']);
    }

    public function behaviors(){
        return [
            'timestamp'=>[
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

            'blameable'=>[
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value'=>Yii::$app->user->username
            ],
        ];
    }
}
