<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmnMediosTrabajadores;
use app\models\SmnMediosTrabajadoresPerimetro;
use app\controllers\SimonController;

/**
 * SmnMediosTrabajadoresSearch represents the model behind the search form about `app\models\SmnMediosTrabajadores`.
 */
class SmnMediosTrabajadoresSearch extends SmnMediosTrabajadores
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_persona', 'id_tipo_trabajador', 'id_estado'], 'integer'],
            [['cedula','nombre','apellido','pasaporte', 'fecha_emision', 'fecha_vencimiento', 'fecha_nacimiento', 'sistema_origen','perimetro'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmnMediosTrabajadores::find();
        $query->joinWith(['idPersona p']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

        //Dependiendo del usuario conectado se filtran las personas
        switch ($tipoUsuario){
            case 'MEDIOS':
                $idMedio=$usuario->usuarioMedio->id_medio;
                $query->andFilterWhere(["id_medio"=>$idMedio]);
                    //$q->where("smn_medios_convocatorias.id_medio=$idMedio");
            break;
        }

        if(isset($this->status) && $this->status === "0"){
            $query->andWhere(['IS','status',null]);
        }

        if(isset($this->status) && $this->status){
            $query->andFilterWhere(['status'=>$this->status]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'p.cedula' => $this->cedula,
            'id_medio' => $this->id_medio,
            'id_persona' => $this->id_persona,
            'id_tipo_trabajador' => $this->id_tipo_trabajador,
            'fecha_emision' => $this->fecha_emision,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'id_estado' => $this->id_estado,
        ]);

        $query->andFilterWhere(['like', 'pasaporte', $this->pasaporte]);
        $query->andFilterWhere(['like', 'sistema_origen', $this->sistema_origen]);
        $query->andFilterWhere(['ilike', 'p.nombre', $this->nombre]);
        $query->andFilterWhere(['ilike', 'p.apellido', $this->apellido]);

        if($this->perimetro){
            $query->andWhere("id_persona IN (SELECT id_persona FROM \"SIMON\".smn_medios_trabajadores_perimetro WHERE id_estado=$this->perimetro)");
        }

        $query->andWhere([
                'id_medio'=>Yii::$app->params['convocatorias']['mediosConvocatorias']]);

        $query->orderBy(['status'=>SORT_ASC,'p.nombre'=>SORT_ASC]);

        return $dataProvider;
    }
}
