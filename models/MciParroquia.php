<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.mci_parroquia".
 *
 * @property integer $id_municipio
 * @property string $nom_parroquia
 * @property integer $id_parroquia
 */
class MciParroquia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_parroquia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_municipio', 'nom_parroquia'], 'required'],
            [['id_municipio'], 'integer'],
            [['nom_parroquia'], 'string', 'max' => 100],
            [['id_municipio'], 'exist', 'skipOnError' => true, 'targetClass' => MciMunicipio::className(), 'targetAttribute' => ['id_municipio' => 'id_municipio']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_municipio' => 'Id Municipio',
            'nom_parroquia' => 'Nom Parroquia',
            'id_parroquia' => 'Id Parroquia',
        ];
    }

    public function listParroquias($idMunicipio=NULL){
        $condicion=!is_null($idMunicipio)?['id_municipio'=>$idMunicipio]:['id_municipio'=>null];
        $parroquias=MciParroquia::find()->where($condicion)->all();
        $listParroquias=ArrayHelper::map($parroquias,'id_parroquia','nom_parroquia');
        return $listParroquias;
    }

}
