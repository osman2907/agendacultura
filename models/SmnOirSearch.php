<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmnOir;

/**
 * SmnOirSearch represents the model behind the search form about `app\models\SmnOir`.
 */
class SmnOirSearch extends SmnOir
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_direccion'], 'integer'],
            [['descripcion', 'created_by', 'created_at', 'updated_by', 'updated_at','zona_influencia'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmnOir::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id_direccion' => $this->id_direccion,
        ]);

        $query->andFilterWhere(['ilike', 'descripcion', $this->descripcion])
            ->andFilterWhere(['ilike', 'created_by', $this->created_by])
            ->andFilterWhere(['ilike', 'updated_by', $this->updated_by]);

        if($this->zona_influencia){
            $query->andWhere("id IN (SELECT id_oir FROM \"SIMON\".smn_oir_estados WHERE id_estado=$this->zona_influencia)");
        }

        return $dataProvider;
    }
}
