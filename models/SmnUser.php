<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\models\Ldap;
use app\models\SmnCorreos;
use app\models\SmnTelefonos;
use app\models\SmnPersonas;
use app\models\SmnSolicitantes;
use app\models\SmnUsuariosSolicitantes;
use app\models\SmnPersonasCorreos;
use app\models\SmnPersonasTelefonos;
use app\models\SmnUsuariosOir;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\components\AuthHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property integer $status
 * @property integer $superadmin
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $registration_ip
  @property string $bind_to_ip
 * @property string $email
 * @property integer $email_confirmed
 * @property integer $ldap_user
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property UserVisitLog[] $userVisitLogs
 */
class SmnUser extends \yii\db\ActiveRecord {

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_BANNED = -1;

    public $cedula;
    public $nombre;
    public $apellido;
    public $id_medio;
    public $id_solicitante;
    public $password;
    public $password_conf;
    public $email;
    public $correo;
    public $telefono;
    public $id_oir;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'SIMON.user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [

            //medios 
            [['username', 'auth_key', 'id_medio', 'password_hash', 'nombre', 'apellido', 'created_at', 'updated_at', 'password', 'correo', 'telefono', 'password_conf'], 'required', 'on' => 'registrarUsuario'],
            [['username'], 'valGeneralSolicitanteCrear', 'on' => 'registrarUsuario'],
            [['username'], 'valGeneralMippciUpdate', 'on' => 'registrarUsuario'],
            //solicitantes
            [['username', 'auth_key', 'password_hash', 'created_at', 'updated_at', 'password', 'password_conf', 'id_solicitante'], 'required', 'on' => 'UsuariosSolicitantes'],
            [['username'], 'valGeneralSolicitanteCrear', 'on' => 'UsuariosSolicitantes'],
            [['cedula', 'nombre', 'apellido', 'username'], 'required', 'on' => 'UsuariosSolicitantes'],
            //mippci
            [['cedula', 'nombre', 'apellido', 'username'], 'required', 'on' => 'registrarUsuariosMippci'],
            [['username'], 'valGeneralMippciCrear', 'on' => 'registrarUsuariosMippci'],
            [['username'], 'valGeneralMippciTc', 'on' => 'registrarUsuariosMippci'],
            [['cedula', 'nombre', 'apellido', 'username'], 'required', 'on' => 'actualizaUsuariosMippci'],
            [['username'], 'valGeneralMippciUpdate', 'on' => 'actualizaUsuariosMippci'],
            [['username'], 'valGeneralMippciTc', 'on' => 'actualizaUsuariosMippci'],
            //general
            [['status', 'superadmin', 'created_at', 'updated_at', 'email_confirmed', 'ldap_user'], 'integer'],
            [['username', 'password_hash', 'confirmation_token', 'bind_to_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Usuario',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Estatus',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'registration_ip' => 'Registration Ip',
            'bind_to_ip' => 'Bind To Ip',
            'email' => 'Correo',
            'email_confirmed' => 'Email Confirmed',
            'ldap_user' => 'Ldap User',
            'cedula' => 'Cédula',
            'id_medio' => 'Entidad',
            'id_solicitante' => 'Ente Adscrito',
            'password' => "Contraseña",
            'password_conf' => "Confirme Contraseña",
            "id_oir"=>"Oficina de Información Regional"
        ];
    }

    public function valUsuarioUnico() {
        if (!empty($this->username)) {
            $modelLdap = new Ldap;
            $usuarios = $modelLdap->buscar('uid', $this->username);
            $smnUser = SmnUser::find()->where(['username' => $this->username])->all();
            if ($usuarios['count'] > 0 && count($smnUser) > 0) {
                $this->addError('username', 'Este usuario ya ha sido utilizado');
            }
        }
    }

    public function registrarUsuariosMedios($post){
        //Cuando la persona no está registrada en la BD.
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if (empty($post['SmnUser']['id_persona'])) {
                $modelPersona = new SmnPersonas;
                $modelPersona->setScenario('registrarUsuario');
                $post['SmnPersonas'] = $post['SmnUser'];
                $modelPersona->load($post);
                $modelPersona->save();
                if (!$modelPersona->save()) {
                    throw new ErrorException('Error Registrando Persona');
                    return false;
                }
                //registra los correos de las personas cuando no existen en la BD
                foreach ($post['SmnCorreos']['correo'] as $itemCorreo) {
                    $modelCorreo = new SmnCorreos();
                    $modelCorreo->correo = $itemCorreo;
                    if ($modelCorreo->validate()) {
                        $modelCorreo->save();
                        $modelPersonaCorreo = new SmnPersonasCorreos;
                        $modelPersonaCorreo->id_persona = $modelPersona->id;
                        $modelPersonaCorreo->id_correo = $modelCorreo->id;
                        $modelPersonaCorreo->save();
                    } else {
                        throw new ErrorException('Error Registrando Correo' . $modelCorreo->correo);
                    }
                }
                //registra los teléfonos de las personas cuando no existen en la BD
                foreach ($post['SmnTelefonos']['telefono'] as $itemTelefono) {
                    $modelTelefono = new SmnTelefonos();
                    $modelTelefono->telefono = $itemTelefono;
                    if ($modelTelefono->validate()) {
                        $modelTelefono->save();
                        $modelPersonaTelefono = new SmnPersonasTelefonos;
                        $modelPersonaTelefono->id_persona = $modelPersona->id;
                        $modelPersonaTelefono->id_telefono = $modelTelefono->id;
                        $modelPersonaTelefono->save();
                    } else {
                        throw new ErrorException('Error Registrando Teléfono' . $modelTelefono->telefono);
                    }
                }
                $id_persona = $modelPersona->id;
            }else
                $id_persona = $post['SmnUser']['id_persona'];

            $smnUsername = SmnUser::find()->where(['username' => $post['SmnUser']['username']])->one();
            $smnUsercedula = SmnUser::find()->where(['id_persona' => $id_persona])->one();

            if (is_object($smnUsername) || is_object($smnUsercedula)){
                if (is_object($smnUsername))
                    $modelUser = $smnUsername;
                else
                    $modelUser = $smnUsercedula;
            } else
                $modelUser = new SmnUser;
            
            if (!SmnUser::guardActualizaUser($modelUser, $post, $id_persona))
                throw new ErrorException('Error Registrando Usuario medios');
                
                $modelUsuariosMedios = new SmnUsuariosMedios;
                $modelUsuariosMedios->id_medio = $post['SmnUser']['id_medio'];
                $modelUsuariosMedios->id_usuario = $modelUser->id;
                $modelUsuariosMedios->estado = 1;
                $modelUsuariosMedios->save();
                
            if ($post['roles']) {
                $roles = $post['roles'];
                if (!SmnUser::asignarRoles($roles, $modelUser->id))
                    throw new ErrorException('Error Asignando los roles al Usuario Comunicaciones');
            }

            $transaction->commit(); 
            return $modelUser;

        } catch (ErrorException $e){
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
            $transaction->rollback();
            return false;
        }
    }

    public function registrarUsuariosSolicitantes($post) {
        //Cuando la persona no está registrada en la BD.
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if (empty($post['SmnUser']['id_persona'])) {
                $modelPersona = new SmnPersonas;
                $post['SmnPersonas'] = $post['SmnUser'];
                $modelPersona->load($post);
                if (!$modelPersona->save()) {
                    throw new ErrorException('Error Registrando Persona');
                }
                $id_persona = $modelPersona->id;

                //registro de correos y asignación de correos a los usuarios registrados
                foreach ($post['SmnCorreos']['correo'] as $itemCorreo) {
                    $modelCorreo = new SmnCorreos();
                    $modelCorreo->correo = $itemCorreo;
                    if ($modelCorreo->validate()) {
                        $modelCorreo->save();
                        $modelPersonaCorreo = new SmnPersonasCorreos;
                        $modelPersonaCorreo->id_persona = $modelPersona->id;
                        $modelPersonaCorreo->id_correo = $modelCorreo->id;
                        $modelPersonaCorreo->save();
                    } else {
                        throw new ErrorException('Error Registrando Correo' . $modelCorreo->correo);
                    }
                }
                //registro de teléfonos y asignación de teléfonos a los usuarios registrados
                foreach ($post['SmnTelefonos']['telefono'] as $itemTelefono) {
                    $modelTelefono = new SmnTelefonos();
                    $modelTelefono->telefono = $itemTelefono;
                    if ($modelTelefono->validate()) {
                        $modelTelefono->save();
                        $modelPersonaTelefono = new SmnPersonasTelefonos;
                        $modelPersonaTelefono->id_persona = $modelPersona->id;
                        $modelPersonaTelefono->id_telefono = $modelTelefono->id;
                        $modelPersonaTelefono->save();
                    } else {
                        throw new ErrorException('Error Registrando Teléfono' . $modelTelefono->telefono);
                    }
                }
            } else
                $id_persona = $post['SmnUser']['id_persona'];

            $smnUsername = SmnUser::find()->where(['username' => $post['SmnUser']['username']])->one();
            $smnUsercedula = SmnUser::find()->where(['id_persona' => $id_persona])->one();

            if (is_object($smnUsername) || is_object($smnUsercedula)) {
                if (is_object($smnUsername))
                    $modelUser = $smnUsername;
                else
                    $modelUser = $smnUsercedula;
            } else
                $modelUser = new SmnUser;

            
            if (!$this->guardActualizaUser($modelUser, $post, $id_persona))
                throw new ErrorException('Error Registrando Usuario');

            $modelUsuariosSolicitantes = new SmnUsuariosSolicitantes;
            $modelUsuariosSolicitantes->id_solicitante = $post['SmnUser']['id_solicitante'];
            $modelUsuariosSolicitantes->id_usuario = $modelUser->id;
            $modelUsuariosSolicitantes->estado = 1;
            $modelUsuariosSolicitantes->save();

            if ($post['roles']) {
                $roles = $post['roles'];
                if (!$this->asignarRoles($roles, $modelUser->id))
                    throw new ErrorException('Error Asignando los roles al Usuario');
            }

            $transaction->commit();
            return $modelUser;
            
        }catch (ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }

    public function actualizarUsuariosSolicitantes($post){

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $username = SmnUser::findOne($post['SmnUser']['id']);
            if (is_object($username)){
                $persona = SmnPersonas::findOne($username->id_persona);
                if (is_object($persona)) {
                    $post['SmnPersonas'] = $post['SmnUser'];
                    $persona->load($post);
                    if (!$persona->update()) {
                        throw new ErrorException('Error Registrando Persona');
                        return false;
                    }
                }

                $id = $post['SmnUser']['id'];
                $modelUsuariosSolicitantes = SmnUsuariosSolicitantes::find($id)->where(['id_usuario' => $id])->one();
                if (is_object($modelUsuariosSolicitantes)) {
                    $post['SmnUsuariosSolicitantes'] = $post['SmnUser'];
                    $modelUsuariosSolicitantes->load($post);
                    if (!$modelUsuariosSolicitantes->update()) {
                        throw new ErrorException('Error Registrando solicitante');
                        return false;
                    }
                }

                if (isset($post['SmnUser']['status']))
                    $estatus = 'si';
                else
                    $estatus = 'no';

                if (!$this->guardActualizaUser($username, $post, $username->id_persona, $estatus))
                    throw new ErrorException('Error actualizando Usuario');

                if ($post['roles']) {
                    $roles = $post['roles'];
                    $borrado = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $username->id]);
                    if (!$this->asignarRoles($roles, $username->id))
                        throw new ErrorException('Error Asignando los roles al Usuario');
                }
            }else {
                throw new ErrorException('Error no existe el id del usuario a registrar');
            }


            $transaction->commit();
            return $username;

        }catch (ErrorException $e){
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
            $transaction->rollback();
            return false;
        }
    }

    public function actualizarUsuariosMippci($post) {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            if(is_object($usuario = SmnUser::findOne($post['SmnUser']['id']))){
                if ($usuario->id_persona != $post['SmnUser']['id_persona']){
                    throw new ErrorException('Error en los datos, el id persona registrado no es el mismo que el que se intenta actualizar');
                    return false;
                }else{
                    if(is_object($persona = SmnPersonas::findOne($usuario->id_persona))){
                        $post['SmnPersonas'] = $post['SmnUser'];
                        $persona->load($post);
                        if (!$persona->update()){
                            throw new ErrorException('Error Registrando Persona');
                            return false;
                        }
                    }
                }

                if (isset($post['SmnUser']['status'])){
                    $estatus = 'si';
                }else{
                    $estatus = 'no';
                }

                if (!$this->guardActualizaUser($usuario, $post, $usuario->id_persona, $estatus))
                    throw new ErrorException('Error actualizando Usuario');

                if($post['roles']){
                    $roles = $post['roles'];
                    if(array_key_exists("oir_mippci",$roles)){
                        if(!empty($post['SmnUser']['id_oir'])){
                            $usuariosOir=SmnUsuariosOir::find()->where(['id_usuario'=>$usuario->id])->all();
                            if(count($usuariosOir) == 0){
                                $modelUsuOir=new SmnUsuariosOir;
                                $modelUsuOir->id_oir=$post['SmnUser']['id_oir'];
                                $modelUsuOir->id_usuario=$usuario->id;
                                if(!$modelUsuOir->save()){
                                    throw new ErrorException('Error registrando usuario OIR');
                                }
                            }else{
                                $modelUsuOir=$usuariosOir[0];
                                $modelUsuOir->id_oir=$post['SmnUser']['id_oir'];
                                if(!$modelUsuOir->save()){
                                    throw new ErrorException('Error registrando usuario OIR');
                                }
                            }
                        }else{
                            $tablaUsuOir=SmnUsuariosOir::tableName();
                            \Yii::$app->db->createCommand()->delete($tablaUsuOir,['id_usuario' => $usuario->id])->execute();
                        }
                    }else{
                        $tablaUsuOir=SmnUsuariosOir::tableName();
                            \Yii::$app->db->createCommand()->delete($tablaUsuOir,['id_usuario' => $usuario->id])->execute();
                    }
                    $borrado = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $usuario->id]);
                    if (!$this->asignarRoles($roles, $usuario->id)){
                        throw new ErrorException('Error Asignando los roles al Usuario');
                    }
                }
            }else{
                throw new ErrorException('Error no existe el id del usuario a registrar');
            }

            $transaction->commit();
            return $usuario;
        } catch (ErrorException $e) {
            $transaction->rollback();
            return false;
        }
    }

    public function registrarUsuariosMippci($post) {

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            if(empty($post['SmnUser']['id_persona'])){
                $modelPersona = new SmnPersonas;
                $modelPersona->setScenario('registrarUsuario');
                $post['SmnPersonas'] = $post['SmnUser'];
                $modelPersona->load($post);
                if (!$modelPersona->save()) {
                    throw new ErrorException('Error Registrando Persona');
                    return false;
                }
                $id_persona = $modelPersona->id;
            }else{
                $id_persona = $post['SmnUser']['id_persona'];
            }

            $smnUsername = SmnUser::find()->where(['username' => $post['SmnUser']['username']])->one();
            $smnUsercedula = SmnUser::find()->where(['id_persona' => $id_persona])->one();

            if (is_object($smnUsername) || is_object($smnUsercedula)){
                if (is_object($smnUsername))
                    $modelUser = $smnUsername;
                else
                    $modelUser = $smnUsercedula;
            }else{
                $modelUser = new SmnUser;
            }

            if (!$this->guardActualizaUser($modelUser, $post, $id_persona)){
                throw new ErrorException('Error Registrando Usuario');
            }


            if($post['roles']){
                $roles = $post['roles'];
                if(array_key_exists("oir_mippci",$roles)){
                    if(!empty($post['SmnUser']['id_oir'])){
                        $usuariosOir=SmnUsuariosOir::find()->where(['id_usuario'=>$modelUser->id])->all();
                        if(count($usuariosOir) == 0){
                            $modelUsuOir=new SmnUsuariosOir;
                            $modelUsuOir->id_oir=$post['SmnUser']['id_oir'];
                            $modelUsuOir->id_usuario=$modelUser->id;
                            if(!$modelUsuOir->save()){
                                throw new ErrorException('Error registrando usuario OIR');
                            }
                        }else{
                            $modelUsuOir=$usuariosOir[0];
                            $modelUsuOir->id_oir=$post['SmnUser']['id_oir'];
                            if(!$modelUsuOir->save()){
                                throw new ErrorException('Error registrando usuario OIR');
                            }
                        }
                    }else{
                        $tablaUsuOir=SmnUsuariosOir::tableName();
                        \Yii::$app->db->createCommand()->delete($tablaUsuOir,['id_usuario' => $modelUser->id])->execute();
                    }
                }else{
                    $tablaUsuOir=SmnUsuariosOir::tableName();
                        \Yii::$app->db->createCommand()->delete($tablaUsuOir,['id_usuario' => $modelUser->id])->execute();
                }
                $borrado = AuthAssignment::deleteAll("user_id=:user_id", [":user_id" => $modelUser->id]);
                if (!$this->asignarRoles($roles, $modelUser->id)){
                    throw new ErrorException('Error Asignando los roles al Usuario');
                }
            }
            
            if(isset($post['SmnCorreos']['correo'])){
                foreach ($post['SmnCorreos']['correo'] as $itemCorreo){
                    if($itemCorreo){
                        $modelCorreo = new SmnCorreos();
                        $modelCorreo->correo = $itemCorreo;
                        if($modelCorreo->validate()){
                            $modelCorreo->save();
                            $modelPersonaCorreo = new SmnPersonasCorreos;
                            $modelPersonaCorreo->id_persona = $id_persona;
                            $modelPersonaCorreo->id_correo = $modelCorreo->id;
                            $modelPersonaCorreo->save();
                        }else{
                            throw new ErrorException('Error Registrando Correo' . $modelCorreo->correo);
                        }
                    }
                }
            }
            
            if(isset($post['SmnTelefonos']['telefono'])){
                foreach ($post['SmnTelefonos']['telefono'] as $itemTelefono){
                    if ($itemTelefono) {
                        $modelTelefono = new SmnTelefonos();
                        $modelTelefono->telefono = $itemTelefono;
                        if ($modelTelefono->validate()) {
                            $modelTelefono->save();
                            $modelPersonaTelefono = new SmnPersonasTelefonos;
                            $modelPersonaTelefono->id_persona = $id_persona;
                            $modelPersonaTelefono->id_telefono = $modelTelefono->id;
                            $modelPersonaTelefono->save();
                        }else{
                            throw new ErrorException('Error Registrando Teléfono' . $modelTelefono->telefono);
                        }
                    }
                }
            }
            $transaction->commit();
            return $modelUser;

        }catch (ErrorException $e){
            print_r($e);
            $transaction->rollback();
            return false;
        }
    }

    public function asignarRoles($roles, $iduser) {
        foreach ($roles as $rol => $valor) {
            $valor = Yii::$app->db->createCommand()->insert(AuthAssignment::tableName(), [
                        'item_name' => $rol,
                        'user_id' => $iduser,
                        'created_at' => strtotime(date("Y-m-d"))
                    ])->execute();

            if (!$valor)
                return FALSE;
        }

        return TRUE;
    }

    public function guardActualizaUser($modelUser, $post, $id_persona, $estatus = NULL) {
        $claveGenerada="12345678";
        $modelUser->load($post);
        $modelUser->auth_key = "123456";
        $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($claveGenerada);
        $modelUser->created_at = strtotime(date("Y-m-d"));
        $modelUser->updated_at = strtotime(date("Y-m-d"));
        $modelUser->ldap_user = 0;
        if ($estatus == 'si') {
            $modelUser->status = 1;
        } elseif ($estatus == 'no') {
            $modelUser->status = 0;
        }
        $modelUser->id_persona = $id_persona;
        if (!$modelUser->save()) {
            return false;
        }
        return true;
    }

    public function validarUsuarioMippciRol($id) {
        $modelAuthItem = new AuthItem;
        $roles = $modelAuthItem->rolesPorGrupo('mippci');
        $roles = "'" . implode("','", $roles) . "'";
        $condicion = "item_name IN($roles) AND user_id=$id";
        $asignaciones = AuthAssignment::find()->where($condicion)->all();

        if (count($asignaciones) > 0)
            return true;

        return false;
    }

    public function valGeneralMippciTc() {
        if(isset($_POST['SmnUser']['id']) && $_POST['SmnUser']['id'] != ""){
            $user = SmnUser::findOne($_POST['SmnUser']['id']);
            if(is_object($user)){
            
                if(empty($user->smnUsuarioTelefonos)){
                    $this->addError('username', 'Es necesario asignar un teléfono al usuario');
                }
                
                if(empty($user->smnUsuarioCorreos)){
                    $this->addError('username', 'Es necesario asignar un correo al usuario');
                }
            }
        }elseif(isset($_POST['SmnUser']['id_persona']) && $_POST['SmnUser']['id_persona'] != ""){
            $user = SmnUser::find()->where(['id_persona' => $_POST['SmnUser']['id_persona']])->one();
            if(is_object($user)){
            
                if(empty($user->smnUsuarioTelefonos)){
                    $this->addError('username', 'Es necesario asignar un teléfono al usuario');
                }
                
                if(empty($user->smnUsuarioCorreos)){
                    $this->addError('username', 'Es necesario asignar un correo al usuario');
                }
            }
        }
    }
    
    public function valGeneralMippciUpdate() {
        if (empty($_POST['roles']) || $_POST['roles'] == NULL)
            $this->addError('username', 'Es necesario asignar un rol al usuario');
    }

    public function valGeneralMippciCrear() {
        $banderaRol = false;

        if (empty($_POST['roles']) || $_POST['roles'] == NULL)
            $banderaRol = TRUE;

        if ($_POST['SmnUser']['id_persona']) {
            $idpersona = $_POST['SmnUser']['id_persona'];
            $smnUser = SmnUser::find()->where(['id_persona' => $idpersona])->one();

            if (is_object($smnUser)) {
                if (!$this->validarUsuarioMippciRol($smnUser->id) && $banderaRol == TRUE)
                    $this->addError('username', 'Es necesario asignar un rol al usuario');
            }else {
                $smnUser = SmnUser::find()->where(['username' => $_POST['SmnUser']['username']])->one();
                if (is_object($smnUser))
                    $this->addError('username', 'Este usuario ya ha sido utilizado');
                else {
                    if ($banderaRol == TRUE)
                        $this->addError('username', 'Es necesario asignar un rol al usuario');
                }
            }
        }else {
            if ($banderaRol == TRUE)
                $this->addError('username', 'Es necesario asignar un rol al usuario');
            $smnUser = SmnUser::find()->where(['username' => $_POST['SmnUser']['username']])->one();
            if (is_object($smnUser))
                $this->addError('username', 'Este usuario ya ha sido utilizado');
        }
    }


    public function valGeneralSolicitanteCrear(){
        $cedula=$this->cedula;

        $modelPersonas=SmnPersonas::find()->where(['cedula'=>$cedula])->all();
        
        if(count($modelPersonas) > 0){
            $modelUser=SmnUser::find()->where(['id_persona'=>$modelPersonas[0]->id])->all();
            if(count($modelUser) > 0){
                if($modelUser[0]->username != $this->username){
                    $this->addError('username', "Usuario distinto al asignado anteriormente");
                }
            }else{
                $modelUser=SmnUser::find()->where(['username'=>$this->username])->all();
                if(count($modelUser) > 0){
                    $this->addError('username', "Usuario registrado previamente.");
                }
            }
        }else{
            $modelUser=SmnUser::find()->where(['username'=>$this->username])->all();
            if(count($modelUser) > 0){
                $this->addError('username', "Usuario registrado previamente.");
            }
        }
        
    }
    
    /*public function valGeneralSolicitanteCrear(){
        $banderaRol = false;
        $modelLdap = new ldap;

        if (empty($_POST['roles']) || $_POST['roles'] == NULL)
            $banderaRol = TRUE;
        
        //si existe en persona al registrar
        if ($_POST['SmnUser']['id_persona']) {
            $idpersona = $_POST['SmnUser']['id_persona'];
            $smnUser = SmnUser::find()->where(['id_persona' => $idpersona])->one();

            //si existe en la tabla user
            if (is_object($smnUser)) {

                if ($this->validarExtra($_POST['SmnUser']['username'], $idpersona))
                    $this->addError('username', 'Este usuario ya ha sido utilizado en el directorio activo por otra persona');

                if (!$this->validarRolSolicitante($smnUser->id) && $banderaRol == TRUE)
                    $this->addError('username', 'Es necesario asignar un rol al usuario');
            }else {//la busco en la tabla user por usernama ya que no esta por el id de persona
                $smnUser = SmnUser::find()->where(['username' => $_POST['SmnUser']['username']])->one();

                if (is_object($smnUser)) {
                    $this->addError('username', 'Este usuario ya ha sido utilizado');
                } else {//no esta en la tabla user como el id de persona y tampoco con el username
                    if ($this->validarExtra($_POST['SmnUser']['username'], $idpersona))
                        $this->addError('username', 'Este usuario ya ha sido utilizado en el directorio activo por otra persona');

                    if ($banderaRol == TRUE)
                        $this->addError('username', 'Es necesario asignar un rol al usuario');
                }
            }
        }else {
            $smnUser = SmnUser::find()->where(['username' => $_POST['SmnUser']['username']])->one();
            if (is_object($smnUser))
                $this->addError('username', 'Este usuario ya ha sido utilizado');

            if ($modelLdap->validarUsuario($_POST['SmnUser']['username'])) {
                if($_POST['SmnUser']['cedula']){
                    $usuario = $modelLdap->buscar('uid', $_POST['SmnUser']['username']);
                    $cedula = $usuario[0]['pager'][0];
                    
                    if(!($cedula == $_POST['SmnUser']['cedula']))
                        $this->addError('username', 'Este usuario ya ha sido utilizado en el directorio activo por otra persona');
                }
            }

            if ($banderaRol == TRUE)
                $this->addError('username', 'Es necesario asignar un rol al usuario');
        }
    }*/
    
    public function validarExtra($usename, $idpersona) {
        $modelLdap = new ldap;
        //valido si el username esta en el ldap, si no esta se puede registrar sin problemas
        if ($modelLdap->validarUsuario($usename)){ 

            //busco el usuario que tiene el username en el ldap
            $usuario = $modelLdap->buscar('uid', $usename);
            
            //si esta en el ldap verifico que sea el username de la persona que se esta intentando registrar
            $cedula = $usuario[0]['pager'][0];
            $condicion= ['cedula'=> $cedula];
            $modelPersona = SmnPersonas::find($cedula)->where($condicion)->one();

            //si es la misma persona pasa la validación, sino el username lo tiene otra persona
            if(is_object($modelPersona)){
                if(!($modelPersona->id == $idpersona))
                    return TRUE;
            }else return true;
            
        }else{
            return false;
        } 
    }

    public function validarRolSolicitante($id){
        $modelAuthItem = new AuthItem;
        $roles = $modelAuthItem->rolesPorGrupo('Solicitantes');
        $roles = "'" . implode("','", $roles) . "'";
        $condicion = "item_name IN($roles) AND user_id=$id";
        $asignaciones = AuthAssignment::find()->where($condicion)->all();

        if (count($asignaciones) > 0)
            return true;

        return false;
    }

    public static function getTipoUsuario(){
        $userId=Yii::$app->user->id;
        $connection = \Yii::$app->db;
        $sql="SELECT group_code FROM \"SIMON\".auth_item WHERE name IN(
                SELECT child FROM \"SIMON\".auth_item_child WHERE parent IN(
                    SELECT item_name FROM \"SIMON\".auth_assignment WHERE user_id=$userId
                )
            ) LIMIT 1";

        $model=$connection->createCommand($sql);
        $respuesta=$model->queryAll();

        if(!isset($respuesta[0]['group_code'])){
            return 'MIPPCI';
        }

        switch ($respuesta[0]['group_code']){
            case 'medios': return 'COMUNICACIONES'; break;
            case 'solicitantes': return 'ENTE ADSCRITO'; break;
            case 'mippci': return 'MIPPCI'; break;
            default: return 'NO ASOCIADO'; break;
        }
    }


    public function getOrigen(){
        $userId=Yii::$app->user->id;
        $tipoUsuario=$this->getTipoUsuario();
        $modelUser=SmnUser::findOne($userId);
        switch ($tipoUsuario){
            case 'COMUNICACIONES':
                $origen=$modelUser->usuarioMedio->idMedio->identificacion;
            break;

            case 'ENTE ADSCRITO':
                $origen=$modelUser->idUsuarioSolicitante->idSolicitante->nombre_abreviado;
            break;

            case 'MIPPCI':
                if($this->validarRol("articulacion")){
                    $origen="ARTICULACIÓN CON MEDIOS";
                }

                if($this->validarRol("comunicacion")){
                    $origen="COMUNICACIÓN DE GOBIERNO";
                }

                if($this->validarRol("comunicacion") && $this->validarRol("articulacion")){
                    $origen="COMUNICACIÓN DE GOBIERNO Y ARTICULACIÓN CON MEDIOS";
                }

                if(!isset($origen)){
                    $origen="Sin Perfil";
                }
            break;
            
            default:
                return false;
            break;
        }

        return $origen;
    }


    public function aleatorio($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++) {
            $key .= $pattern{mt_rand(0, $max)};
        }
        return $key;
    }

    public static function regenerarContrasena($post) {
        //$claveGenerada = smnUser::aleatorio(10);
        $claveGenerada="12345678";
        
        $user = SmnUser::findOne($post['idusuario']);
        $user->password_hash=Yii::$app->getSecurity()->generatePasswordHash($claveGenerada);
        $user->ldap_user=0;
        $actualizado=$user->save();
        if ($actualizado) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function getUsuarioConectado($id){
        return $this->find()->where(['id'=>$id])->one();
    }

    public function getNomAbreviado(){
        return $this->nombre . '  -  ' . $this->nombre_abreviado;
    }

    public function listSolicitantes(){
        $solicitantes = SmnSolicitantes::find()->all();
        $listSolicitantes = ArrayHelper::map($solicitantes, 'id', 'nomAbreviado');
        return $listSolicitantes;
    }

    public function obtenerRoles(){
        return Yii::$app->session->get(AuthHelper::SESSION_PREFIX_ROLES,[]);
    }

    public function validarRol($cadena){
        $roles=$this->obtenerRoles();
        foreach ($roles as $rol){
            if(stristr($rol,$cadena)){
                return true;
            }
        }
        return false;
    }


    public function validarAutenticacion($usuario,$contrasena){
        $contrasena=Yii::$app->getSecurity()->generatePasswordHash($contrasena);
        $modelUser=SmnUser::find()->where(['username'=>$usuario,'password_hash'=>$contrasena])->all();
        return count($modelUser) > 0?true:false;
    }


    public function cambiarContrasena($usuario,$contrasena){
        $modelUser=SmnUser::find()->where(['username'=>$usuario])->one();
        $modelUser->password_hash=Yii::$app->getSecurity()->generatePasswordHash($contrasena);
        return $modelUser->save();
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments() {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames() {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVisitLogs() {
        return $this->hasMany(UserVisitLog::className(), ['user_id' => 'id']);
    }

    public function getIdPersona() {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }

    public function getUsuarioOir() {
        return $this->hasOne(SmnUsuariosOir::className(), ['id_usuario' => 'id']);
    }

    public function getIdCorreos() {
        return $this->hasMany(SmnCorreos::className(), ['id' => 'id_correo'])->viaTable('smn_personas_correos', ['id_correo' => 'id']);
    }

    public function getSmnPersonasCorreos() {
        return $this->hasMany(SmnPersonasCorreos::className(), ['id_correo' => 'id']);
    }

    public function getSmnUsuarioCorreos() {
        return $this->hasMany(SmnPersonasCorreos::className(), ['id_persona' => 'id_persona']);
    }

    public function getSmnUsuarioTelefonos() {
        return $this->hasMany(SmnPersonasTelefonos::className(), ['id_persona' => 'id_persona']);
    }

    public function getSmnPersonasTelefonos() {
        return $this->hasMany(SmnPersonasTelefonos::className(), ['id_telefono' => 'id']);
    }

    public function getIdTelefonos() {
        return $this->hasMany(SmnTelefonos::className(), ['id' => 'id_telefono'])->viaTable('smn_personas_telefonos', ['id_telefono' => 'id']);
    }

    public function getUsuarioMedio() {
        return $this->hasOne(SmnUsuariosMedios::className(), ['id_usuario' => 'id']);
    }

    public function getIdUsuarioSolicitante() {
        return $this->hasOne(SmnUsuariosSolicitantes::className(), ['id_usuario' => 'id']);
    }

}