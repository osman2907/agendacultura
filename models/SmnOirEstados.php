<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_oir_estados".
 *
 * @property integer $id_oir_estado
 * @property integer $id_oir
 * @property integer $id_estado
 *
 * @property SIMONSmnOir $idOir
 * @property MciEstado $idEstado
 */
class SmnOirEstados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_oir_estados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_oir', 'id_estado'], 'integer'],
            [['id_oir'], 'exist', 'skipOnError' => true, 'targetClass' => SmnOir::className(), 'targetAttribute' => ['id_oir' => 'id']],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_oir_estado' => 'Id Oir Estado',
            'id_oir' => 'Id Oir',
            'id_estado' => 'Id Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOir()
    {
        return $this->hasOne(SIMONSmnOir::className(), ['id' => 'id_oir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado()
    {
        return $this->hasOne(MciEstado::className(), ['id_estado' => 'id_estado']);
    }
}
