<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MciDescripcion;

/**
 * MciDescripcionSearch represents the model behind the search form about `app\models\MciDescripcion`.
 */
class MciDescripcionSearch extends MciDescripcion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_padre', 'cant_hijos', 'id_descripcion'], 'integer'],
            [['nom_descripcion', 'username', 'fecha_registro'], 'safe'],
            [['status_registro'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MciDescripcion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_padre' => $this->id_padre,
            'cant_hijos' => $this->cant_hijos,
            'fecha_registro' => $this->fecha_registro,
            'status_registro' => $this->status_registro,
            'id_descripcion' => $this->id_descripcion,
        ]);

        $query->andFilterWhere(['ilike', 'nom_descripcion', $this->nom_descripcion])
            ->andFilterWhere(['ilike', 'username', $this->username]);

        return $dataProvider;
    }
}
