<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

use app\models\SmnTelefonos;
use app\models\SmnCorreos;
use app\models\SmnPersonasTelefonos;
use app\models\SmnPersonasCorreos;

use yii\web\NotFoundHttpException;
use yii\base\ErrorException;

/**
 * This is the model class for table "SIMON.smn_personas".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 * @property integer $id_direccion
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property boolean $activo
 * @property string $tipo
 * @property integer $id_tabla
 *
 * @property SIMONSistacPersonasEventos[] $SIMONSistacPersonasEventos
 * @property SIMONSistacEventos[] $eventos
 * @property SIMONSmnConvocatoriaTrabajadores[] $sIMONSmnConvocatoriaTrabajadores
 * @property SIMONSmnConvocatorias[] $convocatorias
 * @property SIMONSmnConvocatoriasPersonas[] $sIMONSmnConvocatoriasPersonas
 * @property SIMONSmnCuentasExternasMedios[] $sIMONSmnCuentasExternasMedios
 * @property SIMONSmnCuentasExternasSolicitantes[] $sIMONSmnCuentasExternasSolicitantes
 * @property SIMONSmnMediosActividadesTrabajadores[] $sIMONSmnMediosActividadesTrabajadores
 * @property SIMONSmnMediosActividades[] $actividads
 * @property SIMONSmnMediosContactos[] $sIMONSmnMediosContactos
 * @property SIMONSmnMedios[] $medios
 * @property SIMONSmnMediosTrabajadores[] $sIMONSmnMediosTrabajadores
 * @property SIMONSmnMedios[] $medios0
 * @property SIMONSmnDireccion $direccion
 * @property SIMONSmnPersonasCorreos[] $sIMONSmnPersonasCorreos
 * @property SIMONSmnCorreos[] $correos
 * @property SIMONSmnPersonasTelefonos[] $sIMONSmnPersonasTelefonos
 * @property SIMONSmnTelefonos[] $telefonos
 * @property SIMONSmnSolicitantesContactos[] $sIMONSmnSolicitantesContactos
 * @property SIMONSmnSolicitantes[] $solicitantes
 * @property SIMONSmnTemaVocero[] $sIMONSmnTemaVoceros
 * @property SIMONSmnProgramaTema[] $temas
 */
class SmnPersonas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_personas';
    }

    /**
     * @inheritdoc
     */
    //public $direccion;
    public function rules()
    {
        return [
            //[['nombre', 'apellido', 'cedula','id_direccion'], 'required'],
            [['nombre','apellido','cedula'], 'required','on'=>'modificarPersona'],
            [['nombre','apellido','cedula'], 'required','on'=>'crearContactoMedio'],
            [['nombre','apellido','cedula'], 'required','on'=>'registrarUsuario'],
            [['cedula'], 'required','on'=>'validarCedula'],
            [['nombre', 'apellido', 'created_by', 'updated_by'], 'string'],
            [['cedula', 'id_direccion', 'id_tabla'], 'integer'],
            [['cedula'],'unique','on'=>['modificarPersona','crearContactoMedio','registrarUsuario']],
            [['created_at', 'updated_at',], 'safe'],
            [['activo'], 'boolean'],
            [['tipo'], 'string', 'max' => 30],
            /*[['id_direccion'], 'exist', 'skipOnError' => true, 'targetClass' => SIMONSmnDireccion::className(), 'targetAttribute' => ['id_direccion' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    
    //ministerio de comercio
    
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombres',
            'apellido' => 'Apellidos',
            'cedula' => 'Cédula',
            'id_direccion' => 'Id Direccion',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'activo' => 'Activo',
            'tipo' => 'Tipo',
            'id_tabla' => 'Id Tabla',
        ];
    }

    public function listData($idPers) {
        $data = SmnPersonas::find()->where(['id'=>$idPers])->one();
        return $data;
    }

    public static function buscarPersona($cedula){
        $condicion= ['cedula'=> $cedula];
        $model = SmnPersonas::find($cedula)->where($condicion)->one();
        return $model;
    }

    public static function buscarPersona2($idPersona){
        $condicion= ['id'=> $idPersona];
        $model = SmnPersonas::find($idPersona)->where($condicion)->one();
        return $model;
    }

    public function registrarPersonas($post,$model,$direccion){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            //registrar una dirección por persona
            $direccion->load($post);
            if($direccion->validate()){
                $direccion->save();
            }else{
                throw new ErrorException('Error Registrando Dirección');
            }
            //registrar los datos de la persona asignandole la dirección previamente registrada
            $model->load($post);
            $model->id_direccion=$direccion->id;
            if($model->validate()){
                $model->save();
            }else{
                throw new ErrorException('Error Registrando los datos de la persona');   
            }

            //registro de correos y asignación de correos a la persona registrada
            if(count($post['SmnCorreos']['correo']) > 0){
                foreach ($post['SmnCorreos']['correo'] as $itemCorreo){
                    $modelCorreo=new SmnCorreos();
                    $modelCorreo->correo=$itemCorreo;
                    if($modelCorreo->validate()){
                        $modelCorreo->save();
                        $modelPersonaCorreo=new SmnPersonasCorreos;
                        $modelPersonaCorreo->id_persona=$model->id;
                        $modelPersonaCorreo->id_correo=$modelCorreo->id;
                        $modelPersonaCorreo->save();
                    }else{
                        throw new ErrorException('Error Registrando Correo'.$modelCorreo->correo);
                    }
                }
            }

            //registro de teléfonos y asignación de teléfonos a la persona registrada
            if(count($post['SmnTelefonos']['telefono']) > 0){
                foreach ($post['SmnTelefonos']['telefono'] as $itemTelefono){
                    $modelTelefono=new SmnTelefonos();
                    $modelTelefono->telefono=$itemTelefono;
                    if($modelTelefono->validate()){
                        $modelTelefono->save();
                        $modelPersonaTelefono=new SmnPersonasTelefonos;
                        $modelPersonaTelefono->id_persona=$model->id;
                        $modelPersonaTelefono->id_telefono=$modelTelefono->id;
                        $modelPersonaTelefono->save();
                    }else{
                        throw new ErrorException('Error Registrando Teléfono'.$modelTelefono->telefono);
                    }
                }
            }
            $transaction->commit();
            return $model;
        }catch(ErrorException $e){
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
            $transaction->rollback();
        }
    }

    public function getNombreCompleto(){
        return $this->nombre." ".$this->apellido;
    }

    public function getDatosBandeja(){
        $html=$this->nombre." ".$this->apellido;
        return $html;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSistacPersonasEventos()
    {
        return $this->hasMany(SistacPersonasEventos::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(SistacEventos::className(), ['id_evento' => 'id_evento'])->viaTable('sistac_personas_eventos', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriaTrabajadores()
    {
        return $this->hasMany(SmnConvocatoriaTrabajadores::className(), ['id_trabajador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id' => 'id_convocatoria'])->viaTable('smn_convocatoria_trabajadores', ['id_trabajador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatoriasPersonas()
    {
        return $this->hasMany(SmnConvocatoriasPersonas::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnCuentasExternasMedios()
    {
        return $this->hasMany(SmnCuentasExternasMedios::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnCuentasExternasSolicitantes()
    {
        return $this->hasMany(SIMONSmnCuentasExternasSolicitantes::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosActividadesTrabajadores()
    {
        return $this->hasMany(SmnMediosActividadesTrabajadores::className(), ['id_trabajador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividads()
    {
        return $this->hasMany(SmnMediosActividades::className(), ['id' => 'id_actividad'])->viaTable('smn_medios_actividades_trabajadores', ['id_trabajador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosContactos()
    {
        return $this->hasMany(SmnMediosContactos::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedios()
    {
        return $this->hasMany(SmnMedios::className(), ['id' => 'id_medio'])->viaTable('smn_medios_contactos', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosTrabajadores()
    {
        return $this->hasMany(SmnMediosTrabajadores::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedios0()
    {
        return $this->hasMany(SmnMedios::className(), ['id' => 'id_medio'])->viaTable('smn_medios_trabajadores', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccion()
    {
        return $this->hasOne(SmnDireccion::className(), ['id' => 'id_direccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnPersonasCorreos()
    {
        return $this->hasMany(SmnPersonasCorreos::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreos()
    {
        return $this->hasMany(SmnCorreos::className(), ['id' => 'id_correo'])->viaTable('"SIMON".smn_personas_correos', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnPersonasTelefonos()
    {
        return $this->hasMany(SmnPersonasTelefonos::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(SmnTelefonos::className(), ['id' => 'id_telefono'])->viaTable('smn_personas_telefonos', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnSolicitantesContactos()
    {
        return $this->hasMany(SmnSolicitantesContactos::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitantes()
    {
        return $this->hasMany(SmnSolicitantes::className(), ['id' => 'id_solicitante'])->viaTable('smn_solicitantes_contactos', ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnTemaVoceros()
    {
        return $this->hasMany(SmnTemaVocero::className(), ['id_vocero' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemas()
    {
        return $this->hasMany(SmnProgramaTema::className(), ['id_programa_tema' => 'id_tema'])->viaTable('smn_tema_vocero', ['id_vocero' => 'id']);
    }
}
