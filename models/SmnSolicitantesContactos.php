<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "SIMON.smn_solicitantes_contactos".
 *
 * @property integer $id_solicitante
 * @property integer $id_persona
 *
 * @property SmnPersonas $idPersona
 * @property SmnSolicitantes $idSolicitante
 */
class SmnSolicitantesContactos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_solicitantes_contactos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitante', 'id_persona'], 'required'],
            [['id_solicitante', 'id_persona'], 'integer'],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_persona' => 'id']],
            [['id_solicitante'], 'exist', 'skipOnError' => true, 'targetClass' => SmnSolicitantes::className(), 'targetAttribute' => ['id_solicitante' => 'id']],
        ];
    }

    public function getFullName() {
        return $this->idPersona->nombre.' '.$this->idPersona->apellido;
    }

    public function listContactos($contact) {
        $contactos=SmnSolicitantesContactos::find()->where(['id_solicitante'=>$contact])->all();
        $listContactos=ArrayHelper::map($contactos,'id_persona','fullName');
        return $listContactos;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_solicitante' => 'Solicitante',
            'id_persona' => 'Id Persona',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_solicitante']);
    }
}
