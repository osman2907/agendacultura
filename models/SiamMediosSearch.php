<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SiamMedios;

/**
 * MediosMediosSearch represents the model behind the search form about `app\models\MediosMedios`.
 */
class SiamMediosSearch extends SiamMedios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_estado', 'id_des_tendencia', 'id_des_estatus', 'id_des_tipo_medio', 'id_des_tipo_ambito', 'id_ciudad', 'id_municipio', 'id_parroquia', 'id_emisora', 'id_junta_directiva', 'id_telefonos', 'id_correos', 'id_des_problemas_tecnicos', 'id_des_interferencia', 'id_des_estatus_legal', 'id_des_origen_financiamiento', 'id_des_redes_comunitarias'], 'integer'],
            [['nom_concesionario', 'identificacion', 'created_at', 'created_by', 'updated_at', 'updated_by', 'direccion', 'ced_rif', 'fecha_creacion', 'nro_concesion', 'fecha_inicio_concesion', 'nro_habilitacion', 'fecha_inicio_habilitacion', 'url_twitter', 'url_facebook', 'url_instagram', 'url_youtube', 'otra_red_social', 'datos_tecnicos', 'datos_programacion', 'pagina_web', 'blog', 'fecha_inicio_transmision', 'otras_redes_comunitarias', 'fecha_verificacion_tendencia', 'tipo_programacion'], 'safe'],
            [['in_estacion', 'in_activo', 'actualizado', 'internacional', 'autorizado_conatel'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiamMedios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_medio' => $this->id_medio,
            'id_estado' => $this->id_estado,
            'id_des_tendencia' => $this->id_des_tendencia,
            'id_des_estatus' => $this->id_des_estatus,
            'id_des_tipo_medio' => $this->id_des_tipo_medio,
            'id_des_tipo_ambito' => $this->id_des_tipo_ambito,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id_ciudad' => $this->id_ciudad,
            'id_municipio' => $this->id_municipio,
            'id_parroquia' => $this->id_parroquia,
            'in_estacion' => $this->in_estacion,
            'id_emisora' => $this->id_emisora,
            'in_activo' => $this->in_activo,
            'fecha_creacion' => $this->fecha_creacion,
            'id_junta_directiva' => $this->id_junta_directiva,
            'fecha_inicio_concesion' => $this->fecha_inicio_concesion,
            'fecha_inicio_habilitacion' => $this->fecha_inicio_habilitacion,
            'id_telefonos' => $this->id_telefonos,
            'id_correos' => $this->id_correos,
            'id_des_problemas_tecnicos' => $this->id_des_problemas_tecnicos,
            'id_des_interferencia' => $this->id_des_interferencia,
            'id_des_estatus_legal' => $this->id_des_estatus_legal,
            'fecha_inicio_transmision' => $this->fecha_inicio_transmision,
            'id_des_origen_financiamiento' => $this->id_des_origen_financiamiento,
            'id_des_redes_comunitarias' => $this->id_des_redes_comunitarias,
            'fecha_verificacion_tendencia' => $this->fecha_verificacion_tendencia,
            'actualizado' => $this->actualizado,
            'internacional' => $this->internacional,
            'autorizado_conatel' => $this->autorizado_conatel,
        ]);

        $query->andFilterWhere(['like', 'nom_concesionario', $this->nom_concesionario])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'ced_rif', $this->ced_rif])
            ->andFilterWhere(['like', 'nro_concesion', $this->nro_concesion])
            ->andFilterWhere(['like', 'nro_habilitacion', $this->nro_habilitacion])
            ->andFilterWhere(['like', 'url_twitter', $this->url_twitter])
            ->andFilterWhere(['like', 'url_facebook', $this->url_facebook])
            ->andFilterWhere(['like', 'url_instagram', $this->url_instagram])
            ->andFilterWhere(['like', 'url_youtube', $this->url_youtube])
            ->andFilterWhere(['like', 'otra_red_social', $this->otra_red_social])
            ->andFilterWhere(['like', 'datos_tecnicos', $this->datos_tecnicos])
            ->andFilterWhere(['like', 'datos_programacion', $this->datos_programacion])
            ->andFilterWhere(['like', 'pagina_web', $this->pagina_web])
            ->andFilterWhere(['like', 'blog', $this->blog])
            ->andFilterWhere(['like', 'otras_redes_comunitarias', $this->otras_redes_comunitarias])
            ->andFilterWhere(['like', 'tipo_programacion', $this->tipo_programacion]);

        $query->andWhere([
                'id_medio'=>Yii::$app->params['convocatorias']['mediosConvocatorias']]);

        return $dataProvider;
    }
}
