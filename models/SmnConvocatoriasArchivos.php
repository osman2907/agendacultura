<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_convocatorias_archivos".
 *
 * @property integer $id_convocatoria_archivo
 * @property integer $id_convocatoria
 * @property string $ruta
 * @property string $archivo
 * @property string $extension
 *
 * @property SIMONSmnConvocatorias $idConvocatoria
 */
class SmnConvocatoriasArchivos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_convocatorias_archivos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_convocatoria', 'ruta', 'archivo', 'extension'], 'required'],
            [['id_convocatoria'], 'integer'],
            [['ruta', 'archivo', 'extension'], 'string'],
            [['id_convocatoria'], 'exist', 'skipOnError' => true, 'targetClass' => SmnConvocatorias::className(), 'targetAttribute' => ['id_convocatoria' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convocatoria_archivo' => 'Id Convocatoria Archivo',
            'id_convocatoria' => 'Id Convocatoria',
            'ruta' => 'Ruta',
            'archivo' => 'Archivo',
            'extension' => 'Extension',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria()
    {
        return $this->hasOne(SmnConvocatorias::className(), ['id' => 'id_convocatoria']);
    }
}
