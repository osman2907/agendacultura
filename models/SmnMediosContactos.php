<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_medios_contactos".
 *
 * @property integer $id_persona
 * @property integer $id_medio
 * @property boolean $activo
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $id_cargo
 *
 * @property SIAMSiamMedios $idMedio
 * @property SIMONSmnPersonas $idPersona
 * @property MciDescripcion $idCargo
 */
class SmnMediosContactos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_medios_contactos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_medio'], 'required'],
            [['id_persona', 'id_medio', 'id_cargo'], 'integer'],
            [['activo'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string'],
            
            [['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id_medio']],
            
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_persona' => 'id']],

            [['id_cargo'], 'exist', 'skipOnError' => true, 'targetClass' => MciDescripcion::className(), 'targetAttribute' => ['id_cargo' => 'id_descripcion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => 'Id Persona',
            'id_medio' => 'Id Medio',
            'activo' => 'Activo',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_cargo' => 'Id Cargo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SIAMSiamMedios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCargo()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_cargo']);
    }

    /**
     * @inheritdoc
     * @return SmnMediosContactosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SmnMediosContactosQuery(get_called_class());
    }
}
