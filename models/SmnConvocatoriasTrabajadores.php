<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_convocatoria_trabajadores".
 *
 * @property integer $id_convocatoria
 * @property integer $id_trabajador
 * @property integer $id_medio
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $id_tipo_trabajador
 *
 * @property SIMONMciDescripcion $idTipoTrabajador
 * @property SIMONSmnConvocatorias $idConvocatoria
 * @property SIMONSmnMedios $idMedio
 * @property SIMONSmnPersonas $idTrabajador
 */
class SmnConvocatoriasTrabajadores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_convocatoria_trabajadores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_convocatoria', 'id_trabajador', 'id_medio'], 'required'],
            [['id_convocatoria', 'id_trabajador', 'id_medio', 'id_tipo_trabajador'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string'],
            [['id_tipo_trabajador'], 'exist', 'skipOnError' => true, 'targetClass' => MciDescripcion::className(), 'targetAttribute' => ['id_tipo_trabajador' => 'id_descripcion']],
            [['id_convocatoria'], 'exist', 'skipOnError' => true, 'targetClass' => SmnConvocatorias::className(), 'targetAttribute' => ['id_convocatoria' => 'id']],
            [['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id_medio']],
            [['id_trabajador'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_trabajador' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convocatoria' => 'Id Convocatoria',
            'id_trabajador' => 'Id Trabajador',
            'id_medio' => 'Id Medio',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_tipo_trabajador' => 'Id Tipo Trabajador',
        ];
    }


    public function getFoto(){
        $condicion=['id_tabla'=>$this->id_trabajador,'tipo_tabla'=>'smn_personas'];
        $busqueda=SmnArchivos::find()->where($condicion)->all();
        if(count($busqueda) > 0){
            return $busqueda[0];
        }else{
            return false;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoTrabajador()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_tipo_trabajador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria()
    {
        return $this->hasOne(SmnConvocatorias::className(), ['id' => 'id_convocatoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTrabajador()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_trabajador']);
    }
}
