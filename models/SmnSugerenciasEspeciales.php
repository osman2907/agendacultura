<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_sugerencias_especiales".
 *
 * @property integer $id
 * @property string $tema
 * @property string $acciones
 * @property string $fecha_inicio
 * @property integer $id_sector
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $fecha_fin
 * @property string $recomendaciones
 * @property string $responsables
 * @property boolean $estatus
 *
 * @property SmnConvocatorias[] $smnConvocatorias
 * @property SmnMediosActividades[] $smnMediosActividades
 * @property SmnSubtema[] $smnSubtemas
 * @property MciDescripcion $idSector
 */
class SmnSugerenciasEspeciales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_sugerencias_especiales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tema', 'acciones', 'fecha_inicio', 'id_sector', 'fecha_fin'], 'required'],
            [['tema', 'acciones', 'created_by', 'updated_by', 'recomendaciones', 'responsables'], 'string'],
            [['fecha_inicio', 'created_at', 'updated_at', 'fecha_fin'], 'safe'],
            [['id_sector'], 'integer'],
            [['estatus'], 'boolean'],
            [['id_sector'], 'exist', 'skipOnError' => true, 'targetClass' => MciDescripcion::className(), 'targetAttribute' => ['id_sector' => 'id_descripcion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tema' => 'Tema',
            'acciones' => 'Acciones',

            'fecha_inicio' => 'Fecha inicio',
            'id_sector' => 'Sector',
            'fecha_fin' => 'Fecha fin',
            'recomendaciones' => 'Recomendaciones',
            'responsables' => 'Responsables',
            'estatus' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_sugerencia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosActividades()
    {
        return $this->hasMany(SmnMediosActividades::className(), ['id_tema' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnSubtemas()
    {
        return $this->hasMany(SmnSubtema::className(), ['id_tema' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSector()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_sector']);
    }
}
