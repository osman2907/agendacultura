<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;

use app\models\SmnCorreos;
use app\models\SmnTelefonos;
use app\models\SmnSolicitantesCorreos;
use app\models\SmnSolicitantesTelefonos;
use app\models\SmnSolicitantesContactos;
use app\models\SmnSolicitanteEstructuraGobierno;


class SmnSolicitantes extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'SIMON.smn_solicitantes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_direccion'], 'required'],
            [['nombre', 'nombre_abreviado', 'created_by', 'updated_by'], 'string'],
            [['id_direccion'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['tipo', 'activo', 'vicepresidencia'], 'boolean'],
            [['id_direccion'], 'exist', 'skipOnError' => true, 'targetClass' => SmnDireccion::className(), 'targetAttribute' => ['id_direccion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'nombre_abreviado' => 'Nombre Abreviado',
            'id_direccion' => 'Id Direccion',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'tipo' => 'Tipo',
            'activo' => 'Activo',
            'vicepresidencia' => 'Vicepresidencia',
        ];
    }

    public function getNomAbreviado(){
        return $this->nombre.'  -  '.$this->nombre_abreviado;
    }

    public function getNombreCompleto(){
        return $this->nombre_abreviado.'  -  '.$this->nombre;
    }

    public function listSolicitantes($idSolicitante = null){
        if($idSolicitante != null){
            $condicion=['activo'=>TRUE,'id'=>$idSolicitante];
        }else{
            $condicion=['activo'=>TRUE];
        }
        $solicitantes=SmnSolicitantes::find()->where($condicion)->orderBy(['nombre'=>SORT_ASC])->all();
        $listSolicitantes=ArrayHelper::map($solicitantes,'id','nomAbreviado');
        return $listSolicitantes;
    }


    public function registrar($post,$model,$direccion){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            //registrar una dirección por solicitante
            $direccion->load($post);
            $direccion->id_estado=0;
            $direccion->id_municipio=0;
            $direccion->id_parroquia=0;
            $direccion->id_ciudad=0;
            
            if($direccion->validate()){
                $direccion->save();
            }else{
                $direccion->validate();
                throw new ErrorException('Error Registrando Dirección');
            }
            
            //registrar los datos del solicitante asignandole la dirección previamente registrada
            $model->load($post);
            $model->id_direccion=$direccion->id;
            $model->tipo=TRUE;
            if($model->validate()){
                $model->save();
            }else{
                throw new ErrorException('Error Registrando los datos del solicitante');   
            }

            
            //registro de correos y asignación de correos al solicitante registrado
            foreach ($post['SmnCorreos']['correo'] as $itemCorreo){
                $modelCorreo=new SmnCorreos();
                $modelCorreo->correo=$itemCorreo;
                if($modelCorreo->validate()){
                    $modelCorreo->save();
                    $modelSolicitanteCorreo=new SmnSolicitantesCorreos;
                    $modelSolicitanteCorreo->id_solicitante=$model->id;
                    $modelSolicitanteCorreo->id_correo=$modelCorreo->id;
                    $modelSolicitanteCorreo->save();
                }else{
                    throw new ErrorException('Error Registrando Correo'.$modelCorreo->correo);
                }
            }
            //registro de teléfonos y asignación de teléfonos al solicitante registrado
            foreach ($post['SmnTelefonos']['telefono'] as $itemTelefono){
                $modelTelefono=new SmnTelefonos();
                $modelTelefono->telefono=$itemTelefono;
                if($modelTelefono->validate()){
                    $modelTelefono->save();
                    $modelSolicitanteTelefono=new SmnSolicitantesTelefonos;
                    $modelSolicitanteTelefono->id_solicitante=$model->id;
                    $modelSolicitanteTelefono->id_telefono=$modelTelefono->id;
                    $modelSolicitanteTelefono->save();
                }else{
                    throw new ErrorException('Error Registrando Teléfono'.$modelTelefono->telefono);
                }
            }

            //asignación de contactos al solicitante registrado
            if(isset($post['Contactos'])){
                foreach ($post['Contactos'] as $itemContacto){
                    $modelSolicitanteContacto=new SmnSolicitantesContactos;
                    $modelSolicitanteContacto->id_solicitante=$model->id;
                    $modelSolicitanteContacto->id_persona=$itemContacto['id_persona'];
                    $modelSolicitanteContacto->save();
                }
            }

            $transaction->commit();
            return $model;
        }catch(ErrorException $e){
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            $transaction->rollback();
        }
    }


    public function modificar($post,$model,$direccion){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            //registrar una dirección por solicitante
            $direccion->load($post);
            
            if($direccion->validate()){
                $direccion->save();
            }else{
                $direccion->validate();
                throw new ErrorException('Error Modificando Dirección');
            }
            
            //registrar los datos del solicitante asignandole la dirección previamente registrada
            $model->load($post);
            if($model->validate()){
                $model->save();
            }else{
                throw new ErrorException('Error Modificando los datos del solicitante');   
            }

            $transaction->commit();
            return $model;
            
        }catch(ErrorException $e){
            $transaction->rollback();
        }
    }
    

    public function getSmnConvocatorias()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_convocante' => 'id']);
    }



    public function getSmnConvocatorias0()
    {
        return $this->hasMany(SmnConvocatorias::className(), ['id_coconvocante' => 'id']);
    }

    

    public function getSmnCuentasExternasSolicitantes()
    {
        return $this->hasMany(SmnCuentasExternasSolicitantes::className(), ['id_solicitante' => 'id']);
    }

    

    public function getSmnEntrevistas()
    {
        return $this->hasMany(SmnEntrevistas::className(), ['id_convocante' => 'id']);
    }

    

    public function getSmnSolicitanteEstructuraGobiernos()
    {
        return $this->hasMany(SmnSolicitanteEstructuraGobierno::className(), ['id_solicitante' => 'id']);
    }

    

    public function getIdEstructuraGobiernos()
    {
        return $this->hasMany(SmnEstructuraGobierno::className(), ['id' => 'id_estructura_gobierno'])->viaTable('smn_solicitante_estructura_gobierno',['id_solicitante' => 'id']);
    }

    

    public function getIdDireccion()
    {
        return $this->hasOne(SmnDireccion::className(),['id' => 'id_direccion']);
    }

    

    public function getSmnSolicitantesContactos()
    {
        return $this->hasMany(SmnSolicitantesContactos::className(),['id_solicitante' => 'id']);
    }

    

    public function getIdPersonas()
    {
        return $this->hasMany(SmnPersonas::className(),['id' => 'id_persona'])->viaTable('smn_solicitantes_contactos', ['id_solicitante' => 'id']);
    }

    

    public function getSmnSolicitantesCorreos()
    {
        return $this->hasMany(SmnSolicitantesCorreos::className(),['id_solicitante' => 'id']);
    }

    

    public function getIdCorreos()
    {
        return $this->hasMany(SmnCorreos::className(), ['id' => 'id_correo'])->viaTable('smn_solicitantes_correo', ['id_solicitante' => 'id']);
    }

    

    public function getSmnSolicitantesTelefonos()
    {
        return $this->hasMany(SmnSolicitantesTelefonos::className(), ['id_solicitante' => 'id']);
    }

    

    public function getIdTelefonos()
    {
        return $this->hasMany(SmnTelefonos::className(), ['id' => 'id_telefono'])->viaTable('smn_solicitantes_telefonos', ['id_solicitante' => 'id']);
    }


    public function listVicepresidencias(){
        $vicepresidencias=SmnSolicitantes::find()->where(['vicepresidencia'=>TRUE])->all();
        $listVicepresidencias=ArrayHelper::map($vicepresidencias,'id','nombre');
        return $listVicepresidencias;
    }
}
