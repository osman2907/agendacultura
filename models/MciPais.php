<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.mci_pais".
 *
 * @property integer $id_pais
 * @property string $nom_pais
 * @property string $des_nacionalidad
 * @property integer $id_idioma
 */
class MciPais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pais', 'nom_pais'], 'required'],
            [['id_pais', 'id_idioma'], 'integer'],
            [['nom_pais', 'des_nacionalidad'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pais' => 'Id Pais',
            'nom_pais' => 'Nom Pais',
            'des_nacionalidad' => 'Des Nacionalidad',
            'id_idioma' => 'Id Idioma',
        ];
    }

    public static function listPaises()
    {
        $paises=MciPais::find()->orderBy(['id_pais'=>SORT_ASC,'nom_pais'=>SORT_ASC])->all();
        $listPaises=ArrayHelper::map($paises,'id_pais','nom_pais');
        return $listPaises;
                
    }
}
