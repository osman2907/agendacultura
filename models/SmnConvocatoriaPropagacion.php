<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_convocatoria_propagacion".
 *
 * @property integer $id_propagacion
 * @property integer $id_convocatoria
 *
 * @property SIMONMciDescripcion $idPropagacion
 * @property SIMONSmnConvocatorias $idConvocatoria
 */
class SmnConvocatoriaPropagacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_convocatoria_propagacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_propagacion', 'id_convocatoria'], 'required'],
            [['id_propagacion', 'id_convocatoria'], 'integer'],
            [['id_propagacion'], 'exist', 'skipOnError' => true, 'targetClass' => MciDescripcion::className(), 'targetAttribute' => ['id_propagacion' => 'id_descripcion']],
            [['id_convocatoria'], 'exist', 'skipOnError' => true, 'targetClass' => SmnConvocatorias::className(), 'targetAttribute' => ['id_convocatoria' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id_propagacion' => 'Id Propagacion',
            'id_convocatoria' => 'Id Convocatoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPropagacion(){
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_propagacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria(){
        return $this->hasOne(SIMONSmnConvocatorias::className(), ['id' => 'id_convocatoria']);
    }
}
