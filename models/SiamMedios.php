<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "SIAM.siam_medios".
 *
 * @property integer $id_medio
 * @property string $nom_concesionario
 * @property integer $id_estado
 * @property string $identificacion
 * @property integer $id_des_tendencia
 * @property integer $id_des_estatus
 * @property integer $id_des_tipo_medio
 * @property integer $id_des_tipo_ambito
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $id_ciudad
 * @property integer $id_municipio
 * @property integer $id_parroquia
 * @property string $direccion
 * @property boolean $in_estacion
 * @property integer $id_emisora
 * @property boolean $in_activo
 * @property string $ced_rif
 * @property string $fecha_creacion
 * @property integer $id_junta_directiva
 * @property string $nro_concesion
 * @property string $fecha_inicio_concesion
 * @property string $nro_habilitacion
 * @property string $fecha_inicio_habilitacion
 * @property integer $id_telefonos
 * @property integer $id_correos
 * @property integer $id_des_problemas_tecnicos
 * @property integer $id_des_interferencia
 * @property string $url_twitter
 * @property string $url_facebook
 * @property string $url_instagram
 * @property string $url_youtube
 * @property string $otra_red_social
 * @property string $datos_tecnicos
 * @property string $datos_programacion
 * @property string $pagina_web
 * @property string $blog
 * @property integer $id_des_estatus_legal
 * @property string $fecha_inicio_transmision
 * @property integer $id_des_origen_financiamiento
 * @property integer $id_des_redes_comunitarias
 * @property string $otras_redes_comunitarias
 * @property string $fecha_verificacion_tendencia
 * @property string $tipo_programacion
 * @property boolean $actualizado
 * @property boolean $internacional
 * @property boolean $autorizado_conatel
 *
 * @property SIAMSiamCorreos[] $sIAMSiamCorreos
 * @property SIAMSiamImpresos[] $sIAMSiamImpresos
 * @property SIAMSiamCorreos $idCorreos
 * @property SIAMSiamDescripcion $idDesTendencia
 * @property SIAMSiamDescripcion $idDesEstatus
 * @property SIAMSiamDescripcion $idDesTipoMedio
 * @property SIAMSiamDescripcion $idDesTipoAmbito
 * @property SIAMSiamDescripcion $idDesProblemasTecnicos
 * @property SIAMSiamDescripcion $idDesInterferencia
 * @property SIAMSiamDescripcion $idDesEstatusLegal
 * @property SIAMSiamDescripcion $idDesOrigenFinanciamiento
 * @property SIAMSiamDescripcion $idDesRedesComunitarias
 * @property SIAMSiamJuntaDirectiva $idJuntaDirectiva
 * @property SIAMSiamTelefonos $idTelefonos
 * @property MciEstado $idEstado
 * @property MciMunicipio $idMunicipio
 * @property MciParroquia $idParroquia
 * @property SIAMSiamMediosInternacionales[] $sIAMSiamMediosInternacionales
 * @property SIAMSiamPersonas[] $sIAMSiamPersonas
 * @property SIAMSiamPropietarios[] $sIAMSiamPropietarios
 * @property SIAMSiamTvEstaciones[] $sIAMSiamTvEstaciones
 * @property SIAMSiamTvOperadores[] $sIAMSiamTvOperadores
 * @property SIAMSiamZonas[] $sIAMSiamZonas
 * @property SIAMSiamZonasCobertura[] $sIAMSiamZonasCoberturas
 * @property SIMONSmnMediosContactos[] $sIMONSmnMediosContactos
 * @property SIMONSmnPersonas[] $idPersonas
 */
class SiamMedios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIAM.siam_medios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'id_des_tendencia', 'id_des_estatus', 'id_des_tipo_medio', 'id_des_tipo_ambito', 'id_ciudad', 'id_municipio', 'id_parroquia', 'id_emisora', 'id_junta_directiva', 'id_telefonos', 'id_correos', 'id_des_problemas_tecnicos', 'id_des_interferencia', 'id_des_estatus_legal', 'id_des_origen_financiamiento', 'id_des_redes_comunitarias'], 'integer'],
            [['identificacion', 'id_des_tipo_medio', 'id_des_tipo_ambito'], 'required'],
            [['created_at', 'updated_at', 'fecha_creacion', 'fecha_inicio_concesion', 'fecha_inicio_habilitacion', 'fecha_inicio_transmision', 'fecha_verificacion_tendencia'], 'safe'],
            /*[['in_estacion', 'in_activo', 'actualizado', 'internacional', 'autorizado_conatel'], 'boolean'],
            [['ced_rif', 'nro_concesion', 'nro_habilitacion', 'url_twitter', 'url_facebook', 'url_instagram', 'url_youtube', 'otra_red_social', 'datos_tecnicos', 'datos_programacion', 'pagina_web', 'blog', 'otras_redes_comunitarias'], 'string'],
            [['nom_concesionario', 'identificacion', 'tipo_programacion'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 300],*/
            /*[['id_correos'], 'exist', 'skipOnError' => true, 'targetClass' => SiamCorreos::className(), 'targetAttribute' => ['id_correos' => 'id_correos']],
            [['id_des_tendencia'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_tendencia' => 'id_descripcion']],
            [['id_des_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_estatus' => 'id_descripcion']],
            [['id_des_tipo_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_tipo_medio' => 'id_descripcion']],
            [['id_des_tipo_ambito'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_tipo_ambito' => 'id_descripcion']],
            [['id_des_problemas_tecnicos'], 'exist', 'skipOnError' => true, 'targetClass' => SIAMSiamDescripcion::className(), 'targetAttribute' => ['id_des_problemas_tecnicos' => 'id_descripcion']],
            [['id_des_interferencia'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_interferencia' => 'id_descripcion']],
            [['id_des_estatus_legal'], 'exist', 'skipOnError' => true, 'targetClass' => SiamDescripcion::className(), 'targetAttribute' => ['id_des_estatus_legal' => 'id_descripcion']],
            [['id_des_origen_financiamiento'], 'exist', 'skipOnError' => true, 'targetClass' => SIAMSiamDescripcion::className(), 'targetAttribute' => ['id_des_origen_financiamiento' => 'id_descripcion']],
            [['id_des_redes_comunitarias'], 'exist', 'skipOnError' => true, 'targetClass' => SIAMSiamDescripcion::className(), 'targetAttribute' => ['id_des_redes_comunitarias' => 'id_descripcion']],
            [['id_junta_directiva'], 'exist', 'skipOnError' => true, 'targetClass' => SIAMSiamJuntaDirectiva::className(), 'targetAttribute' => ['id_junta_directiva' => 'id_junta_directiva']],
            [['id_telefonos'], 'exist', 'skipOnError' => true, 'targetClass' => SiamTelefonos::className(), 'targetAttribute' => ['id_telefonos' => 'id_telefono']],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
            [['id_municipio'], 'exist', 'skipOnError' => true, 'targetClass' => MciMunicipio::className(), 'targetAttribute' => ['id_municipio' => 'id_municipio']],
            [['id_parroquia'], 'exist', 'skipOnError' => true, 'targetClass' => MciParroquia::className(), 'targetAttribute' => ['id_parroquia' => 'id_parroquia']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_medio' => 'Id Medio',
            'nom_concesionario' => 'Nom Concesionario',
            'id_estado' => 'Id Estado',
            'identificacion' => 'Identificacion',
            'id_des_tendencia' => 'Id Des Tendencia',
            'id_des_estatus' => 'Id Des Estatus',
            'id_des_tipo_medio' => 'Id Des Tipo Medio',
            'id_des_tipo_ambito' => 'Id Des Tipo Ambito',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_ciudad' => 'Id Ciudad',
            'id_municipio' => 'Id Municipio',
            'id_parroquia' => 'Id Parroquia',
            'direccion' => 'Direccion',
            'in_estacion' => 'In Estacion',
            'id_emisora' => 'Id Emisora',
            'in_activo' => 'In Activo',
            'ced_rif' => 'Ced Rif',
            'fecha_creacion' => 'Fecha Creacion',
            'id_junta_directiva' => 'Id Junta Directiva',
            'nro_concesion' => 'Nro Concesion',
            'fecha_inicio_concesion' => 'Fecha Inicio Concesion',
            'nro_habilitacion' => 'Nro Habilitacion',
            'fecha_inicio_habilitacion' => 'Fecha Inicio Habilitacion',
            'id_telefonos' => 'Id Telefonos',
            'id_correos' => 'Id Correos',
            'id_des_problemas_tecnicos' => 'Id Des Problemas Tecnicos',
            'id_des_interferencia' => 'Id Des Interferencia',
            'url_twitter' => 'Url Twitter',
            'url_facebook' => 'Url Facebook',
            'url_instagram' => 'Url Instagram',
            'url_youtube' => 'Url Youtube',
            'otra_red_social' => 'Otra Red Social',
            'datos_tecnicos' => 'Datos Tecnicos',
            'datos_programacion' => 'Datos Programacion',
            'pagina_web' => 'Pagina Web',
            'blog' => 'Blog',
            'id_des_estatus_legal' => 'Id Des Estatus Legal',
            'fecha_inicio_transmision' => 'Fecha Inicio Transmision',
            'id_des_origen_financiamiento' => 'Id Des Origen Financiamiento',
            'id_des_redes_comunitarias' => 'Id Des Redes Comunitarias',
            'otras_redes_comunitarias' => 'Otras Redes Comunitarias',
            'fecha_verificacion_tendencia' => 'Fecha Verificacion Tendencia',
            'tipo_programacion' => 'Tipo Programacion',
            'actualizado' => 'Actualizado',
            'internacional' => 'Internacional',
            'autorizado_conatel' => 'Autorizado Conatel',
        ];
    }

    public static function listMedios(){
        $medios=SiamMedios::find()->orderBy(['identificacion'=>SORT_ASC])->all();
        $listMedios=ArrayHelper::map($medios,'id_medio','identificacion');
        return $listMedios;
    }

    public function mediosConvocatorias(){
        $mediosConvocatorias=Yii::$app->params['convocatorias']['mediosConvocatorias'];
        $medios=SiamMedios::find()->where(['id_medio'=>$mediosConvocatorias])->orderBy(['identificacion'=>'SORT_ASC'])->all();
        return $medios;
    }

    public function listMediosConvocatorias(){
        $modelMedios=new SiamMedios;
        $medios=$modelMedios->mediosConvocatorias();
        $listMedios=ArrayHelper::map($medios,'id_medio','identificacion');
        return $listMedios;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamCorreos()
    {
        return $this->hasMany(SiamCorreos::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamImpresos()
    {
        return $this->hasMany(SiamImpresos::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCorreos()
    {
        return $this->hasOne(SiamCorreos::className(), ['id_correos' => 'id_correos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesTendencia()
    {
        return $this->hasOne(SiamDescripcion::className(), ['id_descripcion' => 'id_des_tendencia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesEstatus()
    {
        return $this->hasOne(SiamDescripcion::className(), ['id_descripcion' => 'id_des_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesTipoMedio()
    {
        return $this->hasOne(SiamDescripcion::className(), ['id_descripcion' => 'id_des_tipo_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesTipoAmbito()
    {
        return $this->hasOne(SiamDescripcion::className(), ['id_descripcion' => 'id_des_tipo_ambito']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesProblemasTecnicos()
    {
        return $this->hasOne(SiamDescripcion::className(), ['id_descripcion' => 'id_des_problemas_tecnicos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesInterferencia()
    {
        return $this->hasOne(SIAMSiamDescripcion::className(), ['id_descripcion' => 'id_des_interferencia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesEstatusLegal()
    {
        return $this->hasOne(SIAMSiamDescripcion::className(), ['id_descripcion' => 'id_des_estatus_legal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesOrigenFinanciamiento()
    {
        return $this->hasOne(SIAMSiamDescripcion::className(), ['id_descripcion' => 'id_des_origen_financiamiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDesRedesComunitarias()
    {
        return $this->hasOne(SIAMSiamDescripcion::className(), ['id_descripcion' => 'id_des_redes_comunitarias']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJuntaDirectiva()
    {
        return $this->hasOne(SIAMSiamJuntaDirectiva::className(), ['id_junta_directiva' => 'id_junta_directiva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTelefonos()
    {
        return $this->hasOne(SIAMSiamTelefonos::className(), ['id_telefono' => 'id_telefonos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado()
    {
        return $this->hasOne(MciEstado::className(), ['id_estado' => 'id_estado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMunicipio()
    {
        return $this->hasOne(MciMunicipio::className(), ['id_municipio' => 'id_municipio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParroquia()
    {
        return $this->hasOne(MciParroquia::className(), ['id_parroquia' => 'id_parroquia']);
    }

    public function getIdCiudad()
    {
        return $this->hasOne(MciCiudad::className(), ['id_ciudad' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamMediosInternacionales()
    {
        return $this->hasMany(SIAMSiamMediosInternacionales::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamPersonas()
    {
        return $this->hasMany(SIAMSiamPersonas::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamPropietarios()
    {
        return $this->hasMany(SIAMSiamPropietarios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamTvEstaciones()
    {
        return $this->hasMany(SIAMSiamTvEstaciones::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamTvOperadores()
    {
        return $this->hasMany(SIAMSiamTvOperadores::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamZonas()
    {
        return $this->hasMany(SIAMSiamZonas::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIAMSiamZonasCoberturas()
    {
        return $this->hasMany(SIAMSiamZonasCobertura::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmnMediosContactos()
    {
        return $this->hasMany(SmnMediosContactos::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersonas()
    {
        return $this->hasMany(SIMONSmnPersonas::className(), ['id' => 'id_persona'])->viaTable('smn_medios_contactos', ['id_medio' => 'id_medio']);
    }
}
