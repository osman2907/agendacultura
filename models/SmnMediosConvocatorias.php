<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_medios_convocatorias".
 *
 * @property integer $id_medio
 * @property integer $id_convocatoria
 * @property boolean $asignado
 * @property string $fecha_asignado
 * @property string $fecha_confirmado
 * @property boolean $asistencia
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $id_senal
 * @property integer $id_senal_asignada
 *
 * @property SIMONMciDescripcion $idSenalAsignada
 * @property SIMONSmnConvocatorias $idConvocatoria
 * @property SIMONSmnMedios $idMedio
 */
class SmnMediosConvocatorias extends \yii\db\ActiveRecord
{
    public $observaciones;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_medios_convocatorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_convocatoria', 'asistencia'], 'required','on'=>"confirmarAsistencia"],
            [['asistencia'],'valSenal','on'=>"confirmarAsistencia"],
            [['id_senal'],'valSenal','on'=>"confirmarAsistencia"],
            [['asistencia'],'valObservaciones','on'=>"confirmarAsistencia"],
            [['observaciones'],'valObservaciones','on'=>"confirmarAsistencia"],
            [['id_medio', 'id_convocatoria', 'id_senal', 'id_senal_asignada'], 'integer'],
            [['asignado', 'asistencia'], 'boolean'],
            [['id_senal','fecha_asignado', 'fecha_confirmado', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_medio' => 'Entidad',
            'id_convocatoria' => 'Id Convocatoria',
            'asignado' => 'Asignado',
            'fecha_asignado' => 'Fecha Asignado',
            'fecha_confirmado' => 'Fecha Confirmado',
            'asistencia' => 'Asistencia',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_senal' => 'Señal',
            'id_senal_asignada' => 'Señal Asignada',
        ];
    }

    public function validarRegistro(){
        $condiciones=['id_convocatoria'=>$this->id_convocatoria,'id_medio'=>$this->id_medio];
        $busqueda=SmnMediosConvocatorias::find()->where($condiciones)->all();
        return (count($busqueda) == 0)?true:false;
    }

    /*public function valSenal(){
        $this->addError('id_senal','probando');
    }*/

    public function valSenal($attribute, $params){
        if($this->asistencia && empty($this->id_senal)){
            $this->addError('id_senal',('Por favor seleccione la señal'));
        }
    }

    public function valObservaciones($attribute, $params){
        if(empty($this->asistencia) && empty($this->observaciones)){
            $this->addError('observaciones',('Por favor explique el motivo de ausencia'));
        }
    }

    public function getIdSenal(){
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_senal']);
    }

    public function getIdSenalAsignada()
    {
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_senal_asignada']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria()
    {
        return $this->hasOne(SmnConvocatorias::className(), ['id' => 'id_convocatoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }
}
