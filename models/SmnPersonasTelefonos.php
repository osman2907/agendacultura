<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_personas_telefonos".
 *
 * @property integer $id_persona
 * @property integer $id_telefono
 *
 * @property SIMONSmnPersonas $persona
 * @property SIMONSmnTelefonos $telefono
 */
class SmnPersonasTelefonos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_personas_telefonos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_telefono'], 'required'],
            [['id_persona', 'id_telefono'], 'integer'],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_persona' => 'id']],
            [['id_telefono'], 'exist', 'skipOnError' => true, 'targetClass' => SmnTelefonos::className(), 'targetAttribute' => ['id_telefono' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => 'Id Persona',
            'id_telefono' => 'Id Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefono()
    {
        return $this->hasOne(SmnTelefonos::className(), ['id' => 'id_telefono']);
    }
}
