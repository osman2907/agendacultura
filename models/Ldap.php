<?php
namespace app\models;

use Yii;
use app\models\SmnUser;


class Ldap{

	//Método que recibe el parámetro para filtrar(uid,pager,givenname, etc) y el valor para la búsqueda.
	//Retorna las entradas a través de un arreglo.
	public function buscar($parametro,$valor){
		$ds=$this->conectar();
		$r=$this->vinculacionAnonima($ds);
		$base=Yii::$app->params['ldap']['base'];
		$filtro="$parametro=$valor";

		//var_dump($ds,$base,$filtro); die;

		$busqueda=$this->busqueda($ds,$base,$filtro);
		$entradas=$this->obtenerEntradas($ds,$busqueda);
		$this->cerrarConexion($ds);
		return $entradas;
	}

	//Método que registra una persona en el servidor LDAP.
	public function registrar($post){
            
        $info=[];
	    $info["pager"][0] = $post['SmnUser']['cedula'];
        $info["cn"][0] = $post['SmnUser']['nombre']." ".$post['SmnUser']['apellido'];
	    $info["uid"][0] = $post['SmnUser']['username'];
	    
        if(isset($post['SmnCorreos']['correo'])){
            for($i=0; $i < count($post['SmnCorreos']['correo']); $i++){
                $info["mail"][$i] = $post['SmnCorreos']['correo'][$i];
            }
        }else{
            $persona = SmnPersonas::findOne($post['SmnUser']['id_persona']);
            for($i=0; $i < count($persona->correos); $i++){
                $info["mail"][$i] = $persona->correos[$i]->correo;
            }
        }
            
        $info["givenname"][0] = $post['SmnUser']['nombre'];
	    $info["sn"][0] = $post['SmnUser']['apellido'];
	    $info["gidnumber"][0] = Yii::$app->params['ldap']['gidNumber'];
	    //$info["uidnumber"][0] = $this->ultimoUidNumber()+1;//Se comentó porque consumía muchos recursos
	    $info["uidnumber"][0] = 99;
	    $info["userpassword"][0]=$this->encriptarSha1($post['SmnUser']['password']);
	    $info["homedirectory"][0] = "/home/".$post['SmnUser']['username'];
	    $info['objectclass'][0] = "inetOrgPerson";
	    $info['objectclass'][1] = "posixAccount";
	    $info['objectclass'][2] = "top";

		$ds=$this->conectar();
		$rdn=Yii::$app->params['ldap']['rdn'];
		$password=Yii::$app->params['ldap']['password'];
		$dnSistemas=Yii::$app->params['ldap']['dnSistemas'];
		$r=$this->vinculacion($ds,$rdn,$password);

	    // Agregar datos al directorio
	    $dnAgregar="uid=".$info['uid'][0].",".$dnSistemas;
	    $r=ldap_add($ds,$dnAgregar,$info);
	    $this->cerrarConexion($ds);
            return $r;
	}

	public function registrarJubilado($datos){

        $cedula=trim($datos[1]);
        $apellido=trim($datos[2]);
        $nombre=trim($datos[3]);
        $tipo=trim($datos[4]);
        $correo=trim($datos[5]);
            
        $info=[];
        $uid=str_replace(" ","",strtolower(substr($nombre,0,3).$apellido))."99";
	    $info["pager"][0] = $cedula;
        $info["cn"][0] = $nombre." ".$apellido;
	    $info["uid"][0] = $uid;
        $info["mail"][0] = $correo;
	    $info["givenname"][0] = $nombre;
	    $info["sn"][0] = $apellido;
	    $info["gidnumber"][0] = Yii::$app->params['ldap']['gidNumber'];
	    $info["uidnumber"][0] = 111111;
	    //$info["uidnumber"][0] = $this->ultimoUidNumber()+1;
	    $info["userpassword"][0]=$this->encriptarSha1('123456');
	    $info["homedirectory"][0] = "/home/".$uid;
	    $info['objectclass'][0] = "inetOrgPerson";
	    $info['objectclass'][1] = "posixAccount";
	    $info['objectclass'][2] = "top";

		$ds=$this->conectar();
		$rdn=Yii::$app->params['ldap']['rdn'];
		$password=Yii::$app->params['ldap']['password'];
		$dnJubilados=Yii::$app->params['ldap']['dnJubilados'];
		
		$r=$this->vinculacion($ds,$rdn,$password);

	    // Agregar datos al directorio
	    $dnAgregar="uid=".$info['uid'][0].",".$dnJubilados;
	    
	    //var_dump($ds,$dnAgregar,$info); die();
	    
	    $r=ldap_add($ds,$dnAgregar,$info);
	    $this->cerrarConexion($ds);
        return $r;
	}

	//Método que modifica una persona en el servidor LDAP.
	public function modificar($dn,$info){
		//Si se recibe el atributo contraseña lo encripta automáticamente
		if(isset($info['userpassword'])){
			$info["userpassword"][0]=$this->encriptarSha1($info["userpassword"][0]);
		}

		$ds=$this->conectar();
		$rdn=Yii::$app->params['ldap']['rdn'];
		$password=Yii::$app->params['ldap']['password'];
		//$dnSistemas=Yii::$app->params['ldap']['dnSistemas'];
		$r=$this->vinculacion($ds,$rdn,$password);
		$r=ldap_modify($ds,$dn,$info);
		$this->cerrarConexion($ds);
        return $r;
	}


	//Método que renombra el nodo de una persona en el servidor LDAP.
	//Ej: $dn='uid=jfuentes,ou=sistemas_aplicaciones,ou=dg_tecnologia,ou=Users,dc=mippci,dc=gob,dc=ve'
	//Ej: $newRdn='uid=jfuentes'
	//Ej: $newParent= 'ou=sistemas_aplicaciones,ou=dg_tecnologia,ou=Users,dc=mippci,dc=gob,dc=ve'
	public function renombrar($dn,$newRdn,$newParent){
		$ds=$this->conectar();
		$rdn=Yii::$app->params['ldap']['rdn'];
		$password=Yii::$app->params['ldap']['password'];
		
		$r=$this->vinculacion($ds,$rdn,$password);
		$r=ldap_rename($ds,$dn,$newRdn,$newParent,true);
		$this->cerrarConexion($ds);
        return $r;
	}

	//Método para establecer una conexión al servidor LDAP.
	private function conectar(){
		$servidor=Yii::$app->user->ldapServer[0];
		$ds=ldap_connect($servidor);
		return $ds;
	}


	//Método que recibe como parámetro una conexión LDAP para cerrarla.
	private function cerrarConexion($ds){
		return ldap_close($ds);
	}

	//Método para vincular con el servidor LDAP a través de una autenticación.
	private function vinculacion($ds,$dn,$password){
		ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		$r = @ldap_bind($ds,$dn,$password);
		return $r;
	}

	//Método para vincular de manera anónima con el servidor LDAP (Sin autenticación).
	private function vinculacionAnonima($ds){
		ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		$r=ldap_bind($ds);
		return $r;
	}


	//Método que realiza una búsqueda a través del parámetro enviado.
	//Retorna un objeto con las entradas. para convertirlas en arreglo se ejecuta ldap_get_entries.
	private function busqueda($ds,$base,$filtro){
		//ldap_set_option($ds, LDAP_OPT_SIZELIMIT, 5000);
		$busqueda=ldap_search($ds,$base,$filtro,[],0,0,0);
		return $busqueda;
	}

	//Método que convierte en arreglo las entradas de una búsqueda.
	private function obtenerEntradas($ds,$busqueda){
		$entradas = ldap_get_entries($ds, $busqueda);
		return $entradas;
	}

	//Método que realiza la encriptación en sha1 para que sea entendida por el LDAP.
	private function encriptarSha1($password){
		return "{SHA}".base64_encode(pack("H*",sha1($password)));
	}

	//Método que obtiene el último uid number registrado.
	private function ultimoUidNumber(){
		$usuarios=$this->buscar('uid','*');
        $ultimo=count($usuarios)-2;
        return $usuarios[$ultimo]['uidnumber'][0];
	}

	public function validarUsuario($usuario){
		$usuarios=$this->buscar('uid',$usuario);
		if($usuarios['count'] > 0){
			return true;
		}else{
			return false;
		}
	}

	public function validarAutenticacion($username,$password){
		$ds=$this->conectar();
		$entrada=$this->buscar("uid",$username);
		if($entrada['count'] != 1){
			return false;
		}
		$rdn=$entrada[0]['dn'];
		$r=$this->vinculacion($ds,$rdn,$password);
		return $r;
	}


	public function cambiarContrasena($username,$password){
		$ds=$this->conectar();
		$entrada=$this->buscar("uid",$username);
		if($entrada['count'] != 1){
			return false;
		}
		$rdn=$entrada[0]['dn'];
		$info=[];
        $info["userpassword"][0] = $password;
        return $this->modificar($rdn, $info);
	}

}



