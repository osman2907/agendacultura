<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_solicitante_estructura_gobierno".
 *
 * @property integer $id_solicitante
 * @property integer $id_estructura_gobierno
 * @property integer $id_vicepresidencia
 *
 * @property SmnEstructuraGobierno $idEstructuraGobierno
 * @property SmnSolicitantes $idSolicitante
 */
class SmnSolicitanteEstructuraGobierno extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_solicitante_estructura_gobierno';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitante', 'id_estructura_gobierno'], 'required'],
            [['id_solicitante', 'id_estructura_gobierno', 'id_vicepresidencia'], 'integer'],
            [['id_estructura_gobierno'], 'exist', 'skipOnError' => true, 'targetClass' => SmnEstructuraGobierno::className(), 'targetAttribute' => ['id_estructura_gobierno' => 'id']],
            [['id_solicitante'], 'exist', 'skipOnError' => true, 'targetClass' => SmnSolicitantes::className(), 'targetAttribute' => ['id_solicitante' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_solicitante' => 'Id Solicitante',
            'id_estructura_gobierno' => 'Id Estructura Gobierno',
            'id_vicepresidencia' => 'Id Vicepresidencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstructuraGobierno()
    {
        return $this->hasOne(SmnEstructuraGobierno::className(), ['id' => 'id_estructura_gobierno']);
    }

    public function getIdVicepresidencia()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_vicepresidencia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_solicitante']);
    }
}
