<?php

namespace app\models;

use Yii;
use app\controllers\SimonController;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "SIMON.smn_medios_trabajadores".
 *
 * @property integer $id_medio
 * @property integer $id_persona
 * @property integer $id_tipo_trabajador
 * @property string $pasaporte
 * @property string $fecha_emision
 * @property string $fecha_vencimiento
 * @property string $fecha_nacimiento
 * @property integer $id_estado
 * @property boolean $status
 * @property string $sistema_origen
 *
 * @property SIMONMciDescripcion $idTipoTrabajador
 * @property SIMONSmnPersonas $idPersona
 * @property MciEstado $idEstado
 */
class SmnMediosTrabajadores extends \yii\db\ActiveRecord
{
    public $cedula;
    public $nombre;
    public $apellido;
    public $foto;
    public $perimetro;
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'SIMON.smn_medios_trabajadores';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['cedula','nombre','apellido','id_medio', 'id_persona', 'id_tipo_trabajador', 'id_estado','perimetro'], 'required','on'=>'registrarTrabajador'],
            [['id_medio', 'id_persona', 'id_tipo_trabajador', 'id_estado'], 'integer'],
            [['pasaporte'], 'string'],
            [['fecha_emision', 'fecha_vencimiento', 'fecha_nacimiento'], 'safe'],
            [['status'], 'boolean'],
            [['sistema_origen'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'cedula' => 'Cédula',
            'nombre' => 'Nombres',
            'id_medio' => 'Entidad',
            'id_persona' => 'Id Persona',
            'id_tipo_trabajador' => 'Cargo',
            'pasaporte' => 'Número',
            'fecha_emision' => 'Fecha Emision',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'id_estado' => 'Asignado al Estado',
            'status' => 'Status',
            'sistema_origen' => 'Sistema Origen',
            'perimetro' => 'Perímetro de Cobertura'
        ];
    }

    public function getDatosTrabajador(){
        $cedula=$this->idPersona->cedula;
        $nombreCompleto=$this->idPersona->nombreCompleto;
        return strtoupper($cedula." - ".$nombreCompleto." - ".$this->idTipoTrabajador->nom_descripcion);
    }


    public function registrarTrabajador($post,$files,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
        try{

            //si la persona no existe se procede a registrarla...
            if(empty($post['SmnPersonas']['id'])){
                $modelPersona=new SmnPersonas;
                $modelDireccion=new SmnDireccion;
                $post['SmnDireccion']=Yii::$app->params['direccionStandard'];
                $post['SmnPersonas']=[
                    'cedula'=>$post['SmnMediosTrabajadores']['cedula'],
                    'nombre'=>$post['SmnMediosTrabajadores']['nombre'],
                    'apellido'=>$post['SmnMediosTrabajadores']['apellido'],
                ];
                if(empty($post['SmnCorreos']['correo'][0])){
                    unset($post['SmnCorreos']['correo'][0]);
                }
                if(empty($post['SmnTelefonos']['telefono'][0])){
                    unset($post['SmnTelefonos']['telefono'][0]);
                }

                $modelPersona=$modelPersona->registrarPersonas($post,$modelPersona,$modelDireccion);

                if($modelPersona){
                    $post['SmnPersonas']['id']=$modelPersona->id;
                }else{
                    $transaction->rollback();
                    return false;
                }
            }
            $idPersona=$post['SmnPersonas']['id'];
            $model->load($post);
            $model->id_persona=$idPersona;
            $model->status=true;
            $model->fecha_nacimiento=empty($model->fecha_nacimiento)?'':$this->cambiarFormatoFecha($model->fecha_nacimiento);
            $model->fecha_emision=empty($model->fecha_emision)?'':$this->cambiarFormatoFecha($model->fecha_emision);
            $model->fecha_vencimiento=empty($model->fecha_vencimiento)?'':$this->cambiarFormatoFecha($model->fecha_vencimiento);

            if($model->save()){
                if(isset($post['SmnMediosTrabajadores']['perimetro'])){
                    $perimetro=$post['SmnMediosTrabajadores']['perimetro'];
                    foreach ($perimetro as $valor){
                        $modelPerimetro=new SmnMediosTrabajadoresPerimetro;
                        $modelPerimetro->id_medio=$model->id_medio;
                        $modelPerimetro->id_persona=$model->id_persona;
                        $modelPerimetro->id_estado=$valor;
                        $modelPerimetro->save();
                    }
                }

                if(!empty($files['SmnMediosTrabajadores']['name']['foto'])){
                    $dir_subida = Yii::$app->params['medios']['rutaFotosTrabajadores'];
                    $extension=$this->obtenerExtension($files['SmnMediosTrabajadores']['name']['foto']);
                    $nombre=$model->id_persona; //Cambia el nombre del archivo para que no se repitan en el servidor.
                    $fichero_subido = $dir_subida.basename($nombre.".".$extension);
                    move_uploaded_file($files['SmnMediosTrabajadores']['tmp_name']['foto'],$fichero_subido); //sube el archivo desde el temporal para el servidor.
                    $modelArchivo=new SmnArchivos;
                    $modelArchivo->archivo=basename($nombre.".".$extension);
                    $modelArchivo->id_tabla=$model->id_persona;
                    $modelArchivo->tipo_tabla="smn_personas";
                    $modelArchivo->tipo_creador=$tipoUsuario;
                    $modelArchivo->nombre=$files['SmnMediosTrabajadores']['name']['foto'];
                    $modelArchivo->save();
                }
            }else{
                $transaction->rollback();
                return false;
            }

            $transaction->commit();
            return $model; //Retorna el objeto de SmnMediosTrabajadores con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function modificarTrabajador($post,$files,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
        $modelUser=new SmnUser;
        $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
        try{
            //Buscamos el registro de la persona para cambiar su nombre y apellido.
            $modelPersona=SmnPersonas::findOne($model->id_persona);
            $modelPersona->nombre=$post['SmnMediosTrabajadores']['nombre'];
            $modelPersona->apellido=$post['SmnMediosTrabajadores']['apellido'];

            if(!$modelPersona->save()){
                $transaction->rollback();
                return false;
            }

            if(!isset($post['SmnMediosTrabajadores']['status'])){
                $post['SmnMediosTrabajadores']['status']=0;
            }
            $model->load($post);
            $model->fecha_nacimiento=empty($model->fecha_nacimiento)?'':$this->cambiarFormatoFecha($model->fecha_nacimiento);
            $model->fecha_emision=empty($model->fecha_emision)?'':$this->cambiarFormatoFecha($model->fecha_emision);
            $model->fecha_vencimiento=empty($model->fecha_vencimiento)?'':$this->cambiarFormatoFecha($model->fecha_vencimiento);

            if($model->save()){
                $tablaPerimetro=SmnMediosTrabajadoresPerimetro::tableName();
                $condicion=['id_medio'=>$model->id_medio,'id_persona'=>$model->id_persona];
                \Yii::$app->db->createCommand()->delete($tablaPerimetro,$condicion)->execute();
                if(isset($post['SmnMediosTrabajadores']['perimetro'])){
                    $perimetro=$post['SmnMediosTrabajadores']['perimetro'];
                    foreach ($perimetro as $valor){
                        $modelPerimetro=new SmnMediosTrabajadoresPerimetro;
                        $modelPerimetro->id_medio=$model->id_medio;
                        $modelPerimetro->id_persona=$model->id_persona;
                        $modelPerimetro->id_estado=$valor;
                        $modelPerimetro->save();
                    }
                }

                if(!empty($files['SmnMediosTrabajadores']['name']['foto'])){
                    $tablaArchivos=SmnArchivos::tableName();
                    $condicion=['id_tabla'=>$model->id_persona,'tipo_tabla'=>'smn_personas'];
                    \Yii::$app->db->createCommand()->delete($tablaArchivos,$condicion)->execute();
                    $dir_subida = Yii::$app->params['medios']['rutaFotosTrabajadores'];
                    $extension=$this->obtenerExtension($files['SmnMediosTrabajadores']['name']['foto']);
                    $nombre=$model->id_persona; //Cambia el nombre del archivo para que no se repitan en el servidor.
                    $fichero_subido = $dir_subida.basename($nombre.".".$extension);
                    move_uploaded_file($files['SmnMediosTrabajadores']['tmp_name']['foto'],$fichero_subido); //sube el archivo desde el temporal para el servidor.
                    $modelArchivo=new SmnArchivos;
                    $modelArchivo->archivo=basename($nombre.".".$extension);
                    $modelArchivo->id_tabla=$model->id_persona;
                    $modelArchivo->tipo_tabla="smn_personas";
                    $modelArchivo->tipo_creador=$tipoUsuario;
                    $modelArchivo->nombre=$files['SmnMediosTrabajadores']['name']['foto'];
                    $modelArchivo->save();
                }
            }else{
                $transaction->rollback();
                return false;
            }

            $transaction->commit();
            return $model; //Retorna el objeto de SmnMediosTrabajadores con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }

    public function getPerimetro(){
        $condicion=['id_medio'=>$this->id_medio,'id_persona'=>$this->id_persona];
        $perimetro=SmnMediosTrabajadoresPerimetro::find()->where($condicion)->all();
        return $perimetro;
    }

    public function getMostrarPerimetro(){
        $perimetro=$this->getPerimetro();
        $html="<ul class='list-group'>";
        foreach ($perimetro as $perim){
            $html.="<li class='list-group-item f7 pad5'>".$perim->idEstado->nom_estado."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function getListPerimetro(){
        $perimetro=$this->getPerimetro();
        $listPerimetro=ArrayHelper::map($perimetro,'id_estado','id_estado');
        return $listPerimetro;
    }

    public function getFoto(){
        $condicion=['id_tabla'=>$this->id_persona,'tipo_tabla'=>'smn_personas'];
        $busqueda=SmnArchivos::find()->where($condicion)->all();
        if(count($busqueda) > 0){
            return $busqueda[0];
        }else{
            return false;
        }
    }


    public function cambiarFormatoFecha($fecha){
        if(strpos($fecha,"/")){ //Si está en formato dd/mm/yyyy lo convierte a yyyy-mm-dd
            $fecha=explode("/",$fecha);
            $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
        }else{ //Si está en formato yyyy-mm-dd lo convierte a dd/mm/yyyy
            $fecha=explode("-",$fecha);
            $fecha=$fecha[2]."/".$fecha[1]."/".$fecha[0];
        }
        return $fecha;
    }


    public function obtenerExtension($archivo){
        $partes=explode(".",$archivo);
        $extension=$partes[count($partes)-1];
        return $extension;
    }



    public function getEstatus(){
        if($this->status){
            return "<span class='label label-success'>Activo</span>";
        }else{
            return "<span class='label label-danger'>Inactivo</span>";
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoTrabajador(){
        return $this->hasOne(MciDescripcion::className(), ['id_descripcion' => 'id_tipo_trabajador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona(){
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado(){
        return $this->hasOne(MciEstado::className(), ['id_estado' => 'id_estado']);
    }

    public function getIdMedio(){
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }
}
