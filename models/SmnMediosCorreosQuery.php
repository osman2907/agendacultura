<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SmnMediosCorreos]].
 *
 * @see SmnMediosCorreos
 */
class SmnMediosCorreosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SmnMediosCorreos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SmnMediosCorreos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
