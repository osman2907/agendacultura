<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.mci_municipio".
 *
 * @property integer $id_estado
 * @property string $nom_municipio
 * @property integer $id_municipio
 */
class MciMunicipio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_municipio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'nom_municipio'], 'required'],
            [['id_estado'], 'integer'],
            [['nom_municipio'], 'string', 'max' => 50],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => MciEstado::className(), 'targetAttribute' => ['id_estado' => 'id_estado']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estado' => 'Id Estado',
            'nom_municipio' => 'Nom Municipio',
            'id_municipio' => 'Id Municipio',
        ];
    }

    public function listMunicipios($idEstado=NULL){
        $condicion=!is_null($idEstado)?['id_estado'=>$idEstado]:['id_estado'=>null];
        $municipios=MciMunicipio::find()->where($condicion)->all();
        $listMunicipios=ArrayHelper::map($municipios,'id_municipio','nom_municipio');
        return $listMunicipios;
    }
}
