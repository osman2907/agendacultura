<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_medios_telefonos".
 *
 * @property integer $id_telefono
 * @property integer $id_medio
 *
 * @property SIAMSiamMedios $idMedio
 * @property SIMONSmnMedios $idMedio0
 */
class SmnMediosTelefonos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_medios_telefonos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_telefono', 'id_medio'], 'required'],
            [['id_telefono', 'id_medio'], 'integer'],
            [['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id_medio']],
            /*[['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_telefono' => 'Teléfono',
            'id_medio' => 'Id Medio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio0()
    {
        return $this->hasOne(SiamMedios::className(), ['id' => 'id_medio']);
    }

    /**
     * @inheritdoc
     * @return SmnMediosTelefonosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SmnMediosTelefonosQuery(get_called_class());
    }
    
    public function getIdTelefono()
    {
        return $this->hasOne(SmnTelefonos::className(), ['id' => 'id_telefono']);
    }
}
