<?php

namespace app\models;


use Yii;
use app\models\SmnSolicitantes;
use app\models\SmnUser;
/**
 * This is the model class for table "SIMON.smn_usuarios_solicitantes".
 *
 * @property integer $id_usuario_solicitante
 * @property integer $id_solicitante
 * @property integer $id_usuario
 * @property boolean $estado
 *
 * @property SIMONSmnSolicitantes $idSolicitante
 * @property SIMONUser $idUsuario
 */
class SmnUsuariosSolicitantes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_usuarios_solicitantes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitante', 'id_usuario'], 'integer'],
            [['estado'], 'boolean'],
            [['id_solicitante'], 'exist', 'skipOnError' => true, 'targetClass' => SmnSolicitantes::className(), 'targetAttribute' => ['id_solicitante' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => SmnUser::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_usuario_solicitante' => 'Id Usuario Solicitante',
            'id_solicitante' => 'Id Solicitante',
            'id_usuario' => 'Id Usuario',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitante()
    {
        return $this->hasOne(SmnSolicitantes::className(), ['id' => 'id_solicitante']);
    }

    public function getIdUsuarioSolicitante()
    {
        return $this->hasOne(SmnUsuariosSolicitantes::className(), ['id_usuario' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getIdUsuario()
    {
        return $this->hasOne(SIMONUser::className(), ['id' => 'id_usuario']);
    }*/
}
