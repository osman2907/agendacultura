<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

use app\models\SmnDireccion;
use app\models\SmnOirEstados;


class SmnOir extends \yii\db\ActiveRecord
{
    public $zona_influencia;
    public $id_estado;
    public $id_ciudad;
    public $id_municipio;
    public $id_parroquia;
    public $direccion;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_oir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion','id_estado','id_municipio','id_parroquia','direccion','id_direccion'], 'required'],
            [['descripcion', 'created_by', 'updated_by'], 'string'],
            [['status'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['id_direccion'], 'integer'],
            [['id_direccion'], 'exist', 'skipOnError' => true, 'targetClass' => SmnDireccion::className(), 'targetAttribute' => ['id_direccion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'status' => 'Estatus',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'id_direccion' => 'Dirección',
            'id_estado'=>'Estado',
            'id_ciudad'=>'Ciudad',
            'id_municipio'=>'Municipio',
            'id_parroquia'=>'Parroquia',
            'direccion'=>'Dirección'
        ];
    }


    public function registrar($post){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $modelDireccion=new SmnDireccion;
            $post['SmnDireccion']=$post['SmnOir'];
            $modelDireccion->load($post);

            if(!$modelDireccion->save()){
                throw new ErrorException('Error registrando en la tabla smn_direccion');
            }
            $post['SmnOir']['id_direccion']=$modelDireccion->id;
            $post['SmnOir']['status']=TRUE;

            $model=new SmnOir;
            $model->load($post);
            if(!$model->save()){
                throw new ErrorException('Error registrando en la tabla smn_oir');
            }

            if(isset($post['SmnOir']['zona_influencia'])){
                $zonas=$post['SmnOir']['zona_influencia'];
                foreach ($zonas as $zona){
                    $modelOirEstados=new SmnOirEstados;
                    $modelOirEstados->id_oir=$model->id;
                    $modelOirEstados->id_estado=$zona;
                    if(!$modelOirEstados->save()){
                        throw new ErrorException('Error registrando en la tabla smn_oir_estados');
                    }
                }
            }
            
            $transaction->commit();
            return $model; //Retorna el objeto de OIR con los datos almacenados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function modificar($post,$model){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            $modelDireccion=SmnDireccion::find()->where(['id'=>$model->id_direccion])->one();
            $post['SmnDireccion']=$post['SmnOir'];
            $modelDireccion->load($post);

            if(!$modelDireccion->save()){
                throw new ErrorException('Error modificando en la tabla smn_direccion');
            }
            $post['SmnOir']['id_direccion']=$modelDireccion->id;

            $model->load($post);
            if(!$model->save()){
                throw new ErrorException('Error modificando en la tabla smn_oir');
            }

            $tablaOirEst=SmnOirEstados::tableName();
            \Yii::$app->db->createCommand()->delete($tablaOirEst,['id_oir' => $model->id])->execute();

            if(isset($post['SmnOir']['zona_influencia'])){
                $zonas=$post['SmnOir']['zona_influencia'];
                foreach ($zonas as $zona){
                    $modelOirEstados=new SmnOirEstados;
                    $modelOirEstados->id_oir=$model->id;
                    $modelOirEstados->id_estado=$zona;
                    if(!$modelOirEstados->save()){
                        throw new ErrorException('Error registrando en la tabla smn_oir_estados');
                    }
                }
            }
            
            $transaction->commit();
            return $model; //Retorna el objeto de OIR con los datos modificados.
        }catch(ErrorException $e){
            $transaction->rollback();
            return false;
        }
    }


    public function getZonaInfluencia(){
        $condicion=['id_oir'=>$this->id];
        $zonaInfluencia=SmnOirEstados::find()->where($condicion)->all();
        return $zonaInfluencia;
    }


    public function getMostrarZonaInfluencia(){
        $zonaInfluencia=$this->getZonaInfluencia();
        $html="<ul class='list-group'>";
        foreach ($zonaInfluencia as $zona){
            $html.="<li class='list-group-item f7 pad5'>".$zona->idEstado->nom_estado."</li>";
        }
        $html.="</ul>";
        return $html;
    }

    public function mostrarDireccion(){
        $html="
            <b>Estado: </b>".$this->idDireccion->estado->nom_estado."&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Ciudad: </b>".$this->idDireccion->ciudad->nom_ciudad."&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Municipio: </b>".$this->idDireccion->municipio->nom_municipio."&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Parroquia: </b>".$this->idDireccion->parroquia->nom_parroquia."&nbsp;&nbsp;&nbsp;&nbsp;<br>
            <b>Dirección: </b>".$this->idDireccion->direccion;
        return $html;
    }

    public function setZonasInfluencia(){
        $zonas=SmnOirEstados::find()->where(['id_oir'=>$this->id])->all();
        $this->zona_influencia=ArrayHelper::map($zonas,'id_estado','id_estado');
    }


    public function listOirs($idEstado = null){
        if(is_null($idEstado)){
            $condicion=['status'=>TRUE];
        }else{
            $condicion=['status'=>TRUE,"id IN(SELECT id_oir FROM smn_oir_estados WHERE id_estado=$idEstado)"];
        }
        $oirs=SmnOir::find()->where($condicion)->orderBy(['descripcion'=>SORT_ASC])->all();
        $listOirs=ArrayHelper::map($oirs,'id','descripcion');
        return $listOirs;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDireccion()
    {
        return $this->hasOne(SmnDireccion::className(), ['id' => 'id_direccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnOirEstados()
    {
        return $this->hasMany(SIMONSmnOirEstados::className(), ['id_oir' => 'id']);
    }

    public function behaviors(){
        return [
            'timestamp'=>[
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

            'blameable'=>[
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value'=>Yii::$app->user->username
            ],
        ];
    }

}
