<?php
namespace app\models;
use Yii;
use app\models\SmnPersonas;

class Correos{
	public static function enviarCorreo($asunto,$cuerpo,$destinatarios,$cc=NULL,$cco=NULL){
        $envio = Yii::$app->mail->compose()
             ->setFrom([Yii::$app->params['email']['fromAddress'] => Yii::$app->params['email']['fromName']])
             ->setTo($destinatarios)
             ->setSubject($asunto)
             ->setHtmlBody($cuerpo)
             ->send();
                
            return $envio;
	}
}