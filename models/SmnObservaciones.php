<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_observaciones".
 *
 * @property integer $id
 * @property string $observacion
 * @property integer $id_tabla
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $tipo_tabla
 * @property string $tipo_creador
 */
class SmnObservaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_observaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observacion'], 'required'],
            [['observacion', 'created_by', 'updated_by', 'tipo_tabla', 'tipo_creador'], 'string'],
            [['id_tabla'], 'integer'],
            [['created_at', 'updated_at','observacion','tipo_creador'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'observacion' => 'Observación',
            'id_tabla' => 'Id Tabla',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'tipo_tabla' => 'Tipo Tabla',
            'tipo_creador' => 'Tipo Creador',
        ];
    }
}
