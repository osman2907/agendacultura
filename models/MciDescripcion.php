<?php

namespace app\models;

use Yii;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "SIMON.mci_descripcion".
 *
 * @property integer $id_padre
 * @property string $nom_descripcion
 * @property integer $cant_hijos
 * @property string $username
 * @property string $fecha_registro
 * @property boolean $status_registro
 * @property integer $id_descripcion
 *
 * @property SIMONSistacEventos[] $sIMONSistacEventos
 * @property SIMONSistacEventos[] $sIMONSistacEventos0
 * @property SIMONSistacEventos[] $sIMONSistacEventos1
 * @property SIMONSistacEventos[] $sIMONSistacEventos2
 * @property SIMONSistacEventosAcreditaciones[] $sIMONSistacEventosAcreditaciones
 * @property SIMONSistacEventos[] $idEventos
 * @property SIMONSistacEventosMedios[] $sIMONSistacEventosMedios
 * @property SIMONSistacEventosMedios[] $sIMONSistacEventosMedios0
 * @property SIMONSmnConvocatoriaPropagacion[] $sIMONSmnConvocatoriaPropagacions
 * @property SIMONSmnConvocatorias[] $idConvocatorias
 * @property SIMONSmnConvocatorias[] $sIMONSmnConvocatorias
 * @property SIMONSmnConvocatorias[] $sIMONSmnConvocatorias0
 * @property SIMONSmnConvocatorias[] $sIMONSmnConvocatorias1
 * @property SIMONSmnConvocatorias[] $sIMONSmnConvocatorias2
 * @property SIMONSmnEntrevistas[] $sIMONSmnEntrevistas
 * @property SIMONSmnEntrevistas[] $sIMONSmnEntrevistas0
 * @property SIMONSmnEntrevistas[] $sIMONSmnEntrevistas1
 * @property SIMONSmnEntrevistas[] $sIMONSmnEntrevistas2
 * @property SIMONSmnEntrevistas[] $sIMONSmnEntrevistas3
 * @property SIMONSmnEquipos[] $sIMONSmnEquipos
 * @property SIMONSmnEquipos[] $sIMONSmnEquipos0
 * @property SIMONSmnEstructuraGobierno[] $sIMONSmnEstructuraGobiernos
 * @property SIMONSmnFormatoReporte[] $sIMONSmnFormatoReportes
 * @property SIMONSmnMedios[] $sIMONSmnMedios
 * @property SIMONSmnMedios[] $sIMONSmnMedios0
 * @property SIMONSmnMediosActividades[] $sIMONSmnMediosActividades
 * @property SIMONSmnMediosActividades[] $sIMONSmnMediosActividades0
 * @property SIMONSmnMediosActividades[] $sIMONSmnMediosActividades1
 * @property SIMONSmnMediosActividades[] $sIMONSmnMediosActividades2
 * @property SIMONSmnMediosConvocatorias[] $sIMONSmnMediosConvocatorias
 * @property SIMONSmnMediosTrabajadores[] $sIMONSmnMediosTrabajadores
 * @property SIMONSmnPautaAudiovisual[] $sIMONSmnPautaAudiovisuals
 * @property SIMONSmnPautaImpresa[] $sIMONSmnPautaImpresas
 * @property SIMONSmnPautaImpresa[] $sIMONSmnPautaImpresas0
 * @property SIMONSmnPautaPublicitaria[] $sIMONSmnPautaPublicitarias
 * @property SIMONSmnSugerenciasEspeciales[] $sIMONSmnSugerenciasEspeciales
 */
class MciDescripcion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.mci_descripcion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_padre', 'cant_hijos'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['status_registro'], 'boolean'],
            [['nom_descripcion'], 'string', 'max' => 500 ],
            [['username'], 'string', 'max' => 150 ],
            [['fecha_registro', 'nom_descripcion', 'username'], 'required'],
            [['status_registro'],'default','value'=>true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_padre' => 'Id Padre',
            'nom_descripcion' => 'Descripcion',
            'cant_hijos' => 'Cant Hijos',
            'username' => 'Username',
            'fecha_registro' => 'Fecha Registro',
            'status_registro' => 'Status Registro',
            'id_descripcion' => 'Id Descripcion',
        ];
    }

    public function listSectores(){
        $sectores=MciDescripcion::find()->where(['id_padre'=>'2'])->all();
        $listSectores=ArrayHelper::map($sectores,'id_descripcion','nom_descripcion');
        return $listSectores;
    }

    public function listTiposMediosSolicitados($idPadre){
        $tiposMediosSolicitados=MciDescripcion::find()->where(['id_padre'=>$idPadre])->all();
        $listTiposMediosSolicitados=ArrayHelper::map($tiposMediosSolicitados,'id_descripcion','nom_descripcion');
        return $listTiposMediosSolicitados;
    }

    public function listOpciones($idPadre){
        $opciones=MciDescripcion::find()->where(['id_padre'=>$idPadre])->orderBy(['nom_descripcion'=>SORT_ASC])->all();
        $listOpciones=ArrayHelper::map($opciones,'id_descripcion','nom_descripcion');
        return $listOpciones;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventos()
    {
        return $this->hasMany(SIMONSistacEventos::className(), ['id_des_tipo_evento' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventos0()
    {
        return $this->hasMany(SIMONSistacEventos::className(), ['id_des_nivel_convocatoria' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventos1()
    {
        return $this->hasMany(SIMONSistacEventos::className(), ['id_des_estatus_evento' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventos2()
    {
        return $this->hasMany(SIMONSistacEventos::className(), ['id_des_tipo_transmision' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventosAcreditaciones()
    {
        return $this->hasMany(SIMONSistacEventosAcreditaciones::className(), ['id_des_acreditacion' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEventos()
    {
        return $this->hasMany(SIMONSistacEventos::className(), ['id_evento' => 'id_evento'])->viaTable('sistac_eventos_acreditaciones', ['id_des_acreditacion' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventosMedios()
    {
        return $this->hasMany(SIMONSistacEventosMedios::className(), ['id_des_estatus_relacion' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSistacEventosMedios0()
    {
        return $this->hasMany(SIMONSistacEventosMedios::className(), ['id_des_tipo_transmision' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnConvocatoriaPropagacions()
    {
        return $this->hasMany(SIMONSmnConvocatoriaPropagacion::className(), ['id_propagacion' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatorias()
    {
        return $this->hasMany(SIMONSmnConvocatorias::className(), ['id' => 'id_convocatoria'])->viaTable('smn_convocatoria_propagacion', ['id_propagacion' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnConvocatorias()
    {
        return $this->hasMany(SIMONSmnConvocatorias::className(), ['id_prioridad' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnConvocatorias0()
    {
        return $this->hasMany(SIMONSmnConvocatorias::className(), ['id_sector' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnConvocatorias1()
    {
        return $this->hasMany(SIMONSmnConvocatorias::className(), ['id_estatus' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnConvocatorias2()
    {
        return $this->hasMany(SIMONSmnConvocatorias::className(), ['id_senal' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEntrevistas()
    {
        return $this->hasMany(SIMONSmnEntrevistas::className(), ['id_sector' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEntrevistas0()
    {
        return $this->hasMany(SIMONSmnEntrevistas::className(), ['id_prioridad' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEntrevistas1()
    {
        return $this->hasMany(SIMONSmnEntrevistas::className(), ['id_estatus' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEntrevistas2()
    {
        return $this->hasMany(SIMONSmnEntrevistas::className(), ['id_tipo_entrevista' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEntrevistas3()
    {
        return $this->hasMany(SIMONSmnEntrevistas::className(), ['tipo_senal' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEquipos()
    {
        return $this->hasMany(SIMONSmnEquipos::className(), ['id_tipo' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEquipos0()
    {
        return $this->hasMany(SIMONSmnEquipos::className(), ['id_estado' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnEstructuraGobiernos()
    {
        return $this->hasMany(SIMONSmnEstructuraGobierno::className(), ['tipo' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnFormatoReportes()
    {
        return $this->hasMany(SIMONSmnFormatoReporte::className(), ['tipo_formato' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMedios()
    {
        return $this->hasMany(SIMONSmnMedios::className(), ['tendencia' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMedios0()
    {
        return $this->hasMany(SIMONSmnMedios::className(), ['asignado' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosActividades()
    {
        return $this->hasMany(SIMONSmnMediosActividades::className(), ['id_prioridad' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosActividades0()
    {
        return $this->hasMany(SIMONSmnMediosActividades::className(), ['id_sector' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosActividades1()
    {
        return $this->hasMany(SIMONSmnMediosActividades::className(), ['id_estatus' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosActividades2()
    {
        return $this->hasMany(SIMONSmnMediosActividades::className(), ['id_senal' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosConvocatorias()
    {
        return $this->hasMany(SIMONSmnMediosConvocatorias::className(), ['id_senal_asignada' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnMediosTrabajadores()
    {
        return $this->hasMany(SIMONSmnMediosTrabajadores::className(), ['id_tipo_trabajador' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnPautaAudiovisuals()
    {
        return $this->hasMany(SIMONSmnPautaAudiovisual::className(), ['id_acceso' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnPautaImpresas()
    {
        return $this->hasMany(SIMONSmnPautaImpresa::className(), ['id_tipo_formato' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnPautaImpresas0()
    {
        return $this->hasMany(SIMONSmnPautaImpresa::className(), ['id_tipo_color' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnPautaPublicitarias()
    {
        return $this->hasMany(SIMONSmnPautaPublicitaria::className(), ['id_tipo_medio' => 'id_descripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSIMONSmnSugerenciasEspeciales()
    {
        return $this->hasMany(SIMONSmnSugerenciasEspeciales::className(), ['id_sector' => 'id_descripcion']);
    }
}
