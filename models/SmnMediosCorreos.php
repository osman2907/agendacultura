<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_medios_correos".
 *
 * @property integer $id_medio
 * @property integer $id_correo
 *
 * @property SIAMSiamMedios $idMedio
 * @property SIMONSmnCorreos $idCorreo
 */
class SmnMediosCorreos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_medios_correos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_medio', 'id_correo'], 'required'],
            [['id_medio', 'id_correo'], 'integer'],
            [['id_medio'], 'exist', 'skipOnError' => true, 'targetClass' => SiamMedios::className(), 'targetAttribute' => ['id_medio' => 'id_medio']],
            [['id_correo'], 'exist', 'skipOnError' => true, 'targetClass' => SmnCorreos::className(), 'targetAttribute' => ['id_correo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_medio' => 'Id Medio',
            'id_correo' => 'Correo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedio()
    {
        return $this->hasOne(SiamMedios::className(), ['id_medio' => 'id_medio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCorreo()
    {
        return $this->hasOne(SmnCorreos::className(), ['id' => 'id_correo']);
    }

    /**
     * @inheritdoc
     * @return SmnMediosCorreosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SmnMediosCorreosQuery(get_called_class());
    }
}
