<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_convocatorias_personas".
 *
 * @property integer $id_convocatoria
 * @property integer $id_persona
 *
 * @property SIMONSmnConvocatorias $idConvocatoria
 * @property SIMONSmnPersonas $idPersona
 */
class SmnConvocatoriasPersonas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_convocatorias_personas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_convocatoria', 'id_persona'], 'required'],
            [['id_convocatoria', 'id_persona'], 'integer'],
            [['id_convocatoria'], 'exist', 'skipOnError' => true, 'targetClass' => SmnConvocatorias::className(), 'targetAttribute' => ['id_convocatoria' => 'id']],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => SmnPersonas::className(), 'targetAttribute' => ['id_persona' => 'id']],
        ];
    }

    public function dataConvocatorias($idPers){

        $ContactConvocatorias=SmnConvocatoriasPersonas::find()->where(['id_persona'=>$idPers])->all();
        return $ContactConvocatorias;

    }

    public function dataCountConvocatorias($idCount) {

        $CountConvocatorias=SmnConvocatoriasPersonas::find()->where(['id_persona'=>$idCount])->count();
        return $CountConvocatorias;

    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convocatoria' => 'Id Convocatoria',
            'id_persona' => 'Id Persona',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria()
    {
        return $this->hasOne(SmnConvocatorias::className(), ['id' => 'id_convocatoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(SmnPersonas::className(), ['id' => 'id_persona']);
    }
}
