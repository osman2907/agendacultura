<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SIMON.smn_usuarios_oir".
 *
 * @property integer $id_usuario_oir
 * @property integer $id_oir
 * @property integer $id_usuario
 * @property boolean $estado
 *
 * @property SIMONSmnOir $idOir
 * @property SIMONUser $idUsuario
 */
class SmnUsuariosOir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SIMON.smn_usuarios_oir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_oir', 'id_usuario'], 'required'],
            [['id_oir', 'id_usuario'], 'integer'],
            [['estado'], 'boolean'],
            [['id_oir'], 'exist', 'skipOnError' => true, 'targetClass' => SmnOir::className(), 'targetAttribute' => ['id_oir' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => SmnUser::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_usuario_oir' => 'Id Usuario Oir',
            'id_oir' => 'Id Oir',
            'id_usuario' => 'Id Usuario',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOir()
    {
        return $this->hasOne(SmnOir::className(), ['id' => 'id_oir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(SIMONUser::className(), ['id' => 'id_usuario']);
    }
}
