<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MediosMedios]].
 *
 * @see MediosMedios
 */
class MediosMediosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return MediosMedios[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MediosMedios|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
