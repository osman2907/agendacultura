<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "public.mci_continente".
 *
 * @property integer $id_continente
 * @property string $nom_continente
 */
class MciContinente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.mci_continente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_continente'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_continente' => 'Id Continente',
            'nom_continente' => 'Nom Continente',
        ];
    }
}
