<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css=[
        'css/site.css',
        'js/dashboard/css/sb-admin.css',
        'js/dashboard/css/plugins/morris.css',
        'js/dashboard/font-awesome/css/font-awesome.min.css',
        'css/jquery.timepicker.min.css',
        'css/chosen/chosen2.min.css'
    ];
    public $js=[
        'js/bootbox-4.4.0/bootbox.js',
        'js/dashboard/js/bootstrap.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\SimonAsset',
    ];
}
