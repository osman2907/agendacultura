<?php
namespace app\assets;
use yii\web\AssetBundle;
use yii\web\View;

class SimonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css=[
        //aquí van los css.
        //'css/site.css',
    ];
    public $js=[
        'js/jquery-3.0.0.min.js',
        'js/librerias.js',
        'js/multiselect.min.js',
        'js/jquery.maskedinput.min.js',
        'js/validarUsuarioMippci.js',
        'js/timePicker/jquery.timepicker.min.js',
        'js/chosen/chosen2.jquery.min.js',
        'js/bootstrap-filestyle.min.js',
        'js/ajaxupload.js'
    ];
    public $jsOptions=[
        'position' => View::POS_HEAD // too high
        //'position' => View::POS_READY // in the html disappear the jquery.jrac.js declaration
        //'position' => View::POS_LOAD // disappear the jquery.jrac.js
        //'position' => View::POS_END // appear in the bottom of my page, but jquery is more down again
    ];
}