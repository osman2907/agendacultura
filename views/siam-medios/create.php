<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\MediosMedios */

$this->title = 'Create Medios';
$this->params['breadcrumbs'][] = ['label' => 'Medios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
