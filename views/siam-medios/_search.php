<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediosMediosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="medios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_medio') ?>

    <?= $form->field($model, 'nom_concesionario') ?>

    <?= $form->field($model, 'id_estado') ?>

    <?= $form->field($model, 'identificacion') ?>

    <?= $form->field($model, 'id_des_tendencia') ?>

    <?php // echo $form->field($model, 'id_des_estatus') ?>

    <?php // echo $form->field($model, 'id_des_tipo_medio') ?>

    <?php // echo $form->field($model, 'id_des_tipo_ambito') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'id_ciudad') ?>

    <?php // echo $form->field($model, 'id_municipio') ?>

    <?php // echo $form->field($model, 'id_parroquia') ?>

    <?php // echo $form->field($model, 'direccion') ?>

    <?php // echo $form->field($model, 'in_estacion')->checkbox() ?>

    <?php // echo $form->field($model, 'id_emisora') ?>

    <?php // echo $form->field($model, 'in_activo')->checkbox() ?>

    <?php // echo $form->field($model, 'ced_rif') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'id_junta_directiva') ?>

    <?php // echo $form->field($model, 'nro_concesion') ?>

    <?php // echo $form->field($model, 'fecha_inicio_concesion') ?>

    <?php // echo $form->field($model, 'nro_habilitacion') ?>

    <?php // echo $form->field($model, 'fecha_inicio_habilitacion') ?>

    <?php // echo $form->field($model, 'id_telefonos') ?>

    <?php // echo $form->field($model, 'id_correos') ?>

    <?php // echo $form->field($model, 'id_des_problemas_tecnicos') ?>

    <?php // echo $form->field($model, 'id_des_interferencia') ?>

    <?php // echo $form->field($model, 'url_twitter') ?>

    <?php // echo $form->field($model, 'url_facebook') ?>

    <?php // echo $form->field($model, 'url_instagram') ?>

    <?php // echo $form->field($model, 'url_youtube') ?>

    <?php // echo $form->field($model, 'otra_red_social') ?>

    <?php // echo $form->field($model, 'datos_tecnicos') ?>

    <?php // echo $form->field($model, 'datos_programacion') ?>

    <?php // echo $form->field($model, 'pagina_web') ?>

    <?php // echo $form->field($model, 'blog') ?>

    <?php // echo $form->field($model, 'id_des_estatus_legal') ?>

    <?php // echo $form->field($model, 'fecha_inicio_transmision') ?>

    <?php // echo $form->field($model, 'id_des_origen_financiamiento') ?>

    <?php // echo $form->field($model, 'id_des_redes_comunitarias') ?>

    <?php // echo $form->field($model, 'otras_redes_comunitarias') ?>

    <?php // echo $form->field($model, 'fecha_verificacion_tendencia') ?>

    <?php // echo $form->field($model, 'tipo_programacion') ?>

    <?php // echo $form->field($model, 'actualizado')->checkbox() ?>

    <?php // echo $form->field($model, 'internacional')->checkbox() ?>

    <?php // echo $form->field($model, 'autorizado_conatel')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
