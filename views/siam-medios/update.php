<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SmnPersonas;

?>

<script type="text/javascript">
    $(document).ready(function(){
        //elimina los corros de las personas
        $(document).on("click",".eliminar-correo",function(){
            $(this).parent().parent().parent().remove();
        });
        
        //Agrega cuantos correos la persona desee cuando se esta modificando a la persona
        $("#agregar-correo").click(function(){
            campoCorreo=$("#correo").parent().parent().clone();
            $(campoCorreo).children().children('.correos').removeAttr('id');
            $(campoCorreo).removeClass('has-error');
            $(campoCorreo).children('.help-block').html('');
            $(campoCorreo).children().children('.correos').val('');
            //console.log($(campoCorreo).children().children('.correos'));
            $("#html-correos").append(campoCorreo);
            $(campoCorreo).children('.input-group').children('span').children().removeClass('btn-success');
            $(campoCorreo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoCorreo).children('.input-group').children('span').children().addClass('eliminar-correo');
            $(campoCorreo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoCorreo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });
        
        //Valida los correos cuando desenfoca la caja de texto si existe o tiene buena sintaxis
        $(document).on("blur",".correos",function(){
            data={'correo':$(this).val()};
            validacion=consultarPHP('/personas/validar-correos',data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                $(this).parent().parent().children('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().parent().children('.help-block').html('');
            }
        });    

        //Lista los estados y las ciudades cuando se registra una persona
        $(document).on("change","#id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades(data);
            }else{
                $("#id_ciudad").html('');
                $("#id_municipio").html('');
                $("#id_parroquia").html('');
            }
        });

        //Lista los municipios y parroquias cuando se registra una persona    
        $(document).on("change","#id_municipio",function(){
            idMunicipio=$(this).val().trim();
            if(idMunicipio != ''){
                data={id:idMunicipio},
                parroquias=consultarPHP('/smn-personas/listparroquias',data,'html',false);
                $("#id_parroquia").html(parroquias);
            }else{
                $("#id_parroquia").html('');
            }
        });

        //Funcion para las lista de los municipios y ciudades
        listarMunicipiosCiudades=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#id_ciudad").html(ciudades);
        }
        
        //Abre la ventana emergente para ver si la persona esta registrada para asociar el contacto al medio y si no existe muestra el formulario
        $(document).on("click","#persona-contacto",function(){
            idPersona=$("#id_medio").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-personas/buscar-persona-medio',data,'html',false);
            bootbox.dialog({
                size:'large',
                title: "Agregar Persona",
                message: respuesta
            });

        });

        //Valida que esten todos los datos tipeados y guarda la persona
        $(document).on("click","#boton-registrar-contacto",function(){
            data=$("#contacto-medio").serialize();
            cedula=$("#cedula-contacto").val();
            retorno=consultarPHP('/smn-personas/validar-contacto',data,'html',false);

            if(retorno != ''){
                bootbox.alert(retorno);
                return false;
            }

            errores=ejecutarValidacionItems();
            if(errores > 0){
                bootbox.alert("Por favor validar los teléfonos y correos electrónicos");
                return false;
            }

            data=$("#contacto-medio").serialize();
            retorno=consultarPHP('/smn-personas/crear-contacto-medio',data,'json',false);

            id_persona = $(this).attr("data-id_persona"); 
            id_medio = $("#id_medio").html(); 
            /*data={id_persona:id_persona, id_medio:id_medio};
            envio=consultarPHP('/siam-medios/asociar-contacto',data,'html',false);*/
            data={cedula:cedula};
            envio=consultarPHP('/smn-personas/mostrar-persona',data,'html',false);
            $("#listado-personas").html(envio);
        });

        //Valida que la cedula este escrita correctamente y hace el proceso de busqueda
        $(document).on("click","#enviarContacto",function(){
            cedula=$("#cedula-contacto").val();
            str = cedula;
            n = str.length;

            if(n == ''){
                bootbox.alert("Por favor ingrese su Cédula");
                return false;
            }

            if(n < 6 || n >= 9){
                bootbox.alert("La cantidad de carácteres debe ser mayor a seis(6) y menor a ocho(8)");
                return false;
            }

            data={cedula:cedula};
            envio=consultarPHP('/smn-personas/validar-cedula',data,'html',false);
            
            if(envio != 0){
                data={cedula:cedula};
                envio=consultarPHP('/smn-personas/mostrar-persona',data,'html',false);
                $("#listado-personas").html(envio);     
            }else{
                data={cedula:cedula};
                envio=consultarPHP('/smn-personas/crear-contacto-medio',data,'html',false);
                $("#listado-personas").html(envio);
            }
        });

        //Asocia la persona al medio cuando ya esta registrada 
        $(document).on("click","#asociar-contacto",function(){
            id_persona = $(this).attr("data-id_persona");
            id_medio = $("#id_medio").html();
            data={id_persona:id_persona, id_medio:id_medio};

            envio=consultarPHP('/siam-medios/validar-contacto',data,'html',false);
            if(envio){
                bootbox.alert("Este contacto ya está asociado al medio");
                return false;
            }

            envio=consultarPHP('/siam-medios/asociar-contacto',data,'html',false);
            $("#datos-medios").append(envio);
            $(".bootbox").remove();
            $(".modal-backdrop").remove();
            $(".mensaje-tabla").remove();
        });

        $(document).on("click","#nuevo-correo-medio",function(){
            id_medio = $("#id_medio").html()
            data={id_medio:id_medio};
            respuesta=consultarPHP('/smn-correos/agregar-correos-medios',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });

        //Registra los telefonos de los medios
        $(document).on("click","#nuevo-telefono-medio",function(){
            id_medio = $("#id_medio").html()
            data={id_medio:id_medio};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-medios',data,'html',false);
            bootbox.dialog({
                title: "Agregar Teléfonos",
                message: respuesta
            });
        });

        //Elimina los contactos del medio ya existentes
        $(document).on("click",".remover-contacto",function(){
            fila=$(this).parents('tr');
            id_medio = $("#id_medio").html(); 
            id_persona = $(this).attr("data-id_persona");
            
            bootbox.confirm("¿Seguro que desea remover este contacto?",function(respuesta){
                if(respuesta){
                    fila.remove();
                    if($("#datos-medios tr").length == 0){
                        $("#datos-medios").append("<tr id='fila-mensaje'><td colspan='3' class='mensaje-tabla'>No hay personas registradas</td></tr>");
                    }

                    data={id_persona:id_persona, id_medio:id_medio};
                    consultarPHP('/siam-medios/remover-contacto',data,'html',false);
                }
            });
        });

        //Agrega los campos para registrar cuantos correos y/o telefonos se desee
        $(document).on("click",".agregar-item",function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        //elimina los campos que ya habian sido creados de correos y/o telefonos 
        $(document).on("click",".eliminar-item",function(e){
            $(this).parent().parent().parent().remove();
        });

        //Valida que los campos de correo y/o telefonos para guardar la informacion y no esten vacios
        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().parent().find('.help-block').html('');
            }
        });


        //Lista los correos asociados a la persona cuando se esta actualizando los datos
        if ($("#listado-correos").length > 0) {
            idMedio=$("#id_medio").html();
            listarCorreos(idMedio);
        }

        //Funcion para listar los correos
        function listarCorreos(idMedio){
            data={id_medio:idMedio};
            respuesta=consultarPHP('/smn-correos/listar-correos-medios',data,'html',false);
            $("#listado-correos").html(respuesta);
        }

        //
        $(document).on("submit","#formularioTelefonosMedios",function(){
            data=$(this).serialize();
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-medios',data,'html',false);
            if(respuesta == "error"){
                bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
            }else{
                bootbox.alert('Telefono registrado con éxito');
            }
            idMedio=$('#id_medio').html();
            listarTelefonos(idMedio);
    
                return false;
        });
        
        //Lista los telefonos asociados a la persona cuando se esta actualizando los datos
        if($("#listado-telefonos").length > 0){
            idMedio=$("#id_medio").html();
            listarTelefonos(idMedio);
        }

        //Funcion para listar los telefonos
        function listarTelefonos(idMedio){
            data={id_medio:idMedio};
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos-medios',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        $(document).on("submit","#contacto-medio",function(){
            alert("prueba");
            return false;
        });

        //Agrega correos cuando se esta actualizando los datos de la persona
        $(document).on("click",".eliminar-correo",function(){
            idMedio=$("#id_medio").html();
            idCorreo=$(this).attr("data-id");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_medio:idMedio};
                    respuesta=consultarPHP('/smn-correos/eliminar-correo-medio',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(idMedio);    
                }
            });
        });

        //elimina los telefono de las personas cuando se esta actualizando los datos de la persona
        $(document).on("click",".eliminar-telefono",function(){
            idMedio=$("#id_medio").html();
            idTelefono=$(this).attr("data-id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_medio:idMedio};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-telefono-medio',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Telefono eliminado con éxito');
                    }
                        listarTelefonos(idMedio);
                }
            });
        });

        //funcion que valida los items (correos y telefonos)
        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

    });
</script>

<?php
/* @var $this yii\web\View */
/* @var $model app\models\MediosMedios */

$this->title = 'Modificar Medio: ' . $model->identificacion;
$this->params['breadcrumbs'][] = ['label' => 'Medios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->identificacion, 'url' => ['view', 'id' => $model->id_medio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
    <h1><?= Html::encode($this->title) ?></h1>

<div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#datos-" aria-controls="datos-medios" role="tab" data-toggle="tab">DATOS DE MEDIO</a></li>
    <li role="presentation"><a href="#datos-contacto" aria-controls="datos-contacto" role="tab" data-toggle="tab">DATOS DE CONTACTO</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="datos-">
                <h1><center>Datos del Medio: <?= $model->identificacion; ?></center></h1>
            <table class="table table-bordered" id="tabla-contactos">
                <tr class="active">
                    <th width="33%">Nro. Del Medio</th>
                    <th width="33%">Nombre del Medio</th>
                    <th width="33%">Direccion</th>
                </tr>

                <tr>
                    <td id='id_medio'><?php echo $model->id_medio;?></td>
                    <td><?php echo isset($model->identificacion)?$model->identificacion:'';?></td>
                    <td><?php echo isset($model->direccion)?$model->direccion:'';?></td>
                </tr>

                <tr class="active">
                    <th>Estado</th>
                    <th>Ciudad</th>
                    <th>Municipio</th>
                </tr>

                <tr>
                    <td><?php echo isset($model->idEstado->nom_estado)?$model->idEstado->nom_estado:'';?></td>
                    <td><?php echo isset($model->idCiudad->nom_ciudad)?$model->idCiudad->nom_ciudad:'';?></td>
                    <td><?php echo isset($model->idMunicipio->nom_municipio)?$model->idMunicipio->nom_municipio:'';?></td>
                </tr>


            </table>
            <table class="table table-bordered" >    
                <tr class="active" >
                    <td width="50%"><b>Teléfonos</b></td>
                    <td width="50%"><b>Correos</b></td>
                </tr>
                <tr>
                    <td id="listado-telefonos" width="50%">
   
                    </td>
                    <td width="50%" id="listado-correos">

                    </td>
                </tr>
            </table>
	</div>
	<div role="tabpanel" class="tab-pane" id="datos-contacto">
        <?= "<center>"."<br>".Html::submitButton('Nueva Persona Contacto', ['class' => 'btn btn-success ','id' => 'persona-contacto'])."</center>"; ?>
        <h1><center>Persona contacto:</center></h1>
        <table id="datos-medios" class="table table-bordered">
            <tr>
                <th>Cédula</th>
                <th>Nombre y Apellido</th>
                <th width="10%">Acciones</th>
            </tr>

            <?php 
                if(count($model->smnMediosContactos) == 0){
                    ?>
                    <tr id="fila-mensaje">
                        <td colspan="3" class="mensaje-tabla">No hay personas registradas</td>
                    </tr>
                <?php }else{
                    $contactos=$model->smnMediosContactos;
                    foreach ($contactos as $filaContacto) {
                        ?>
                        <tr>
                            <td><?php echo $filaContacto->idPersona->cedula ?></td>
                            <td><?php echo $filaContacto->idPersona->nombre." ".$filaContacto->idPersona->apellido ?></td>
                            <td><?= Html::Button('Remover', ['class' => 'btn btn-xs btn-danger remover-contacto ','id' => 'eliminar-persona','data-id_persona'=>$filaContacto->idPersona->id])?>
                            
                        </tr>
                        <?php
                   }
                }
            ?>
        </table>        
    </div>
</div>

<?php
/*echo "<pre>";
print_r($contactos);
echo "</pre>";*/
?>