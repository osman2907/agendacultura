<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediosMediosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Medios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siam-medios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_medio',
            'identificacion',
            'direccion',
            //pg_send_query('pgsql:host=192.168.1.12;dbname=MCI', 'SELECT * FROM "SIMON".smn_convocatorias WHERE medios_solicitados = "135;";'); 
          /*[
                 'attribute' => 'direccion',
                 'value' => 'direccion.direccion'
            ],*/
            [
            'class' => 'yii\grid\ActionColumn', 
            'template' => '{view}{update}',
            ]
        ],
    ]); 
    
?>
<?php Pjax::end(); ?></div>
