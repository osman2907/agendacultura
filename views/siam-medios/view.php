<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MediosMedios */
?>
<script type="text/javascript">
    $(document).ready(function(){
        //Funcion para listar los correos
        function listarCorreos(idMedio){
            data={id_medio:idMedio};
            respuesta=consultarPHP('/smn-correos/mostrar-correos-medios',data,'html',false);
            $("#correos-vista").html(respuesta);
        }

        if($("#telefonos-vista").length > 0){
            idMedio=$("#id_medio").html();
            listarTelefonos(idMedio);
        }

        if($("#correos-vista").length > 0){
            idMedio=$("#id_medio").html();
            listarCorreos(idMedio);
        }

        //Funcion para listar los telefonos
        function listarTelefonos(idMedio){
            data={id_medio:idMedio};
            respuesta=consultarPHP('/smn-telefonos/mostrar-telefonos-medios',data,'html',false);
            $("#telefonos-vista").html(respuesta);
        }

    });
</script>
<?php 
$this->title = $model->identificacion;
$this->params['breadcrumbs'][] = ['label' => 'Medios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="medios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_medio], ['class' => 'btn btn-primary']) ?>
    </p>


    <table class="table table-bordered">
        <tr class="active">
            <th width="33%">Nro. Del Medio</th>
            <th width="33%">Nombre del Medio</th>
            <th width="33%">Direccion</th>
        </tr>

        <tr>
            <td id="id_medio"><?php echo $model->id_medio;?></td>
            <td><?php echo isset($model->identificacion)?$model->identificacion:'';?></td>
            <td><?php echo isset($model->direccion)?$model->direccion:'';?></td>
        </tr>

        <tr class="active">
            <th>Estado</th>
            <th>Ciudad</th>
            <th>Municipio</th>
        </tr>
        <tr>
            <td><?php echo isset($model->idEstado->nom_estado)?$model->idEstado->nom_estado:''?></td>
            <td><?php echo isset($model->idCiudad->nom_ciudad)?$model->idCiudad->nom_ciudad:'';?></td>
            <td><?php echo isset($model->idMunicipio->nom_municipio)?$model->idMunicipio->nom_municipio:'';?></td>
        </tr>
        <tr class="active">
            <td><b>Teléfonos</b></td>
            <td><b>Correos</b></td>
        </tr>
        <tr>
            <td id="telefonos-vista">  
            </td>

            <td id="correos-vista">
            </td>
        </tr>
    </table>
</div>