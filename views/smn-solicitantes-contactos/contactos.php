<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($modelPersonas, 'id_solicitante')->dropDownList($listContactos,['multiple'=>'multiple', 'id' => 'SelectMultiple']); ?>

<div id="DataPersona"></div>