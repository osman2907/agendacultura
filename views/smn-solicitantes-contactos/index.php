<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<script type="text/javascript">
        jQuery(document).ready(function($) {

        	$('select#smnsolicitantescontactos-id_solicitante').change(function() {
				var idSolicitante = $(this).val();
				dataSolicitante={idSolicitante:idSolicitante};
				Solicitantes=consultarPHP('/smn-solicitantes-contactos/contactos',dataSolicitante,'html',false);
				$('#Contactos').html(Solicitantes);
			});

        	$(document).on('change','select#SelectMultiple',function (){
				var idPersona = $(this).val();
				dataContacto={idPersona:idPersona};
				dataPersona=consultarPHP('/smn-solicitantes-contactos/datacontactos',dataContacto,'html',false);
				$('#DataPersona').html(dataPersona);

				listarCorreos(idPersona);
				listarTelefonos(idPersona);

			});

            $(".agregar-item").click(function(){
                campoNuevo=$(this).parent().parent().parent().clone();
                $(campoNuevo).children().children('.correos').removeAttr('id');
                $(campoNuevo).removeClass('has-error');
                $(campoNuevo).removeClass('has-success');
                $(campoNuevo).children('.help-block').html('');
                $(campoNuevo).children().children("input").val('');
                listadoItems=$(this).parents(".listado-items");
                listadoItems.append(campoNuevo);
                $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
                $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
                $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
                $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
                $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
                });

            $(document).on("click",".eliminar-item",function(){
                $(this).parent().parent().parent().remove();
            });

            $(document).on("blur",".item-validar",function(){
                data={'valor':$(this).val()};
                accion=$(this).data('accion-validar');
                validacion=consultarPHP(accion,data,'html',false);
                if(validacion){
                    $(this).parent().parent().addClass('has-error');
                    //$(this).parent().parent().children('.help-block').html(validacion);
                    $(this).parent().parent().find('.help-block').html(validacion);
                }else{
                    $(this).parent().parent().removeClass('has-error');
                    //$(this).parent().parent().children('.help-block').html('');
                    $(this).parent().parent().find('.help-block').html('');
                }
            });

            $(document).on("submit","#formularioPersonas",function(){
                errores=ejecutarValidacionItems();
                if(errores > 0){
                    return false;
                }
            });

            $(document).on("submit","#formularioCorreos",function(){
                errores=ejecutarValidacionItems();
                if(errores > 0){
                    return false;
                }

                data=$(this).serialize();
                respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
                bootbox.hideAll();
                if(respuesta == "error"){
                    bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                }else{
                    bootbox.alert('Correo electrónico registrado con éxito');
                }
                idPersona=$("#smnpersonas-id").val();
                listarCorreos(idPersona);

                return false;
            });


            $(document).on("click","#agregar-correo",function(){
                idPersona=$("#smnpersonas-id").val();
                data={id_persona:idPersona};
                respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
                bootbox.dialog({
                    title: "Agregar Correos",
                    message: respuesta
                });
            });

            $(document).on("click",".eliminar-correo",function(){
                idPersona=$("#smnpersonas-id").val();
                idCorreo=$(this).data("id_correo");
                bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                    if(respuesta){
                        data={id_correo:idCorreo, id_persona:idPersona};
                        respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                        if(respuesta == "error"){
                            bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                        }else{
                            bootbox.alert('Correo electrónico eliminado con éxito');
                        }
                        listarCorreos(idPersona);
                    }
                });
            });

            idPersona=$("#smnpersonas-id").val();

            function listarCorreos(idPersona){
                data={id_persona:idPersona};
                respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
                $("#listado-correos").html(respuesta);
            }

            $(document).on("submit","#formularioTelefonos",function(){
                errores=ejecutarValidacionItems();
                if(errores > 0){
                    return false;
                }

                data=$(this).serialize();
                respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
                if(respuesta == "error"){
                    bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
                }else{
                    bootbox.alert('Telefono registrado con éxito');
                }
                idPersona=$('#smnpersonas-id').val();
                listarTelefonos(idPersona);
        
                    return false;
            });

             $(document).on("click","#agregar-telefono",function(){
                idPersona=$("#smnpersonas-id").val();
                data={id_persona:idPersona};
                respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
                bootbox.dialog({
                    title: "Agregar Telefono",
                    message: respuesta
                });
            });

            idPersona=$("#smnpersonas-id").val();

            function listarTelefonos(idPersona){
                data={id_persona:idPersona};
                respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
                $("#listado-telefonos").html(respuesta);
            }

            $(document).on("click",".eliminar-telefono",function(){
                idPersona=$("#smnpersonas-id").val();
                idTelefono=$(this).data("id_telefono");
                bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                    if(respuesta){
                        data={id_telefono:idTelefono, id_persona:idPersona};
                        respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                        if(respuesta == "error"){
                            bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                        }else{
                            bootbox.alert('Telefono eliminado con éxito');
                        }
                            listarTelefonos(idPersona);
                    }
                });
            });
            
            ejecutarValidacionItems=function(){
                cantidadItems=$(".item-validar").length;
                for(i=0;i<cantidadItems;i++){
                    elementoHtml=$(".item-validar")[i];
                    $(elementoHtml).focus();
                    $(elementoHtml).blur();
                }
                errores=$(".has-error").length;
                return errores;
            }

        });
    </script>

<?= $form->field($modelSolicitantes, 'id_solicitante')->dropDownList($listSolicitantes,['prompt'=>'seleccione']) ?>

<div id="Contactos"></div>
