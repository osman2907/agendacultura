<?php

use Yii;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
use yii\helpers\Url;

?>

<div class="FormPersona">
	<div class="smn-solicitantes-form">

        <div id="Data"></div>

		<?php $form = ActiveForm::begin(
                    [
                    'options' => ['id' => $dataContacto->formName()],
                    'action'=>['smn-solicitantes-contactos/modificar_persona'],
                    ]
               );
        ?>
        <?= $form->field($dataContacto, 'id')->hiddenInput() ?>
		<div class="panel-body">
    	<div class="panel2">
        	<div class="row">
        		<div class="col-md-4"> 
                	<?= $form->field($dataContacto, 'nombre')->textInput() ?>
            	</div>
            	<div class="col-md-4"> 
                	<?= $form->field($dataContacto, 'apellido')->textInput() ?>
            	</div>
            	<div class="col-md-4">
            		<?= $form->field($dataContacto, 'cedula',['enableAjaxValidation' => true])->textInput()?>
            	</div>
            	<div class="col-md-4">
            		<?= $form->field($dataContacto, 'activo')->checkbox() ?>
            	</div>
            	<div class="col-md-4">
            		<?= $form->field($dataContacto, 'created_by')->textInput(['readonly' => true]) ?>
            	</div>
            	<div class="col-md-4">
            		<?= $form->field($dataContacto, 'created_at')->textInput(['readonly' => true]) ?>
            	</div>
                <div class="col-md-4">
                    <h1>Número de convocatorias:    <?php echo $countConvocatoria; ?></h1>
                    <?php
                        foreach ($convocatoriaPersona as $value) {
                            if ( $value === end($convocatoriaPersona) ):
                                echo '<a href="'.$value->id_convocatoria.'">'.$value->id_convocatoria.'</a>';
                            else:
                                echo '<a href="'.$value->id_convocatoria.'">'.$value->id_convocatoria.'</a> - ';
                            endif;
                        }
                     ?>
                </div>
                 <div class="col-md-4">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Correos Electrónicos</legend>
                        <div id="listado-correos">
                            
                        </div>
                    </fieldset>
                </div>

                <div class="col-md-4">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Teléfonos</legend>
                        <div id="listado-telefonos">
                           
                        </div>
                    </fieldset>
                </div>
			</div>


            <div class="row" style="margin-top:30px;">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= Html::submitButton($modelDataPersonas->isNewRecord ? 'Actualizar' : 'Update', ['class' => $modelDataPersonas->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'SubmitAjax']) ?>
                    </div>
                </div>
            </div>
		</div>
        <?php ActiveForm::end(); ?>
	</div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $('form').on('submit', function (e) {

                e.preventDefault();

                var form = $(this);
                
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (data) {
                         $('#Data').html(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $('#Data').html("error!!!");
                    }
                });

            });

        });
    </script>
</div>