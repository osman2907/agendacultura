<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
use yii\helpers\Url;
?>

<?php if (Yii::$app->session->getFlash('exitoso')): ?>

    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-check"></i>Guardado!</h4>
        <?= Yii::$app->session->getFlash('exitoso') ?>
    </div>

<?php else: ?>

    <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-check"></i>error!</h4>
        <?= Yii::$app->session->getFlash('error') ?>
    </div>

<?php endif; ?>