
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSugerenciasEspeciales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smn-sugerencias-especiales-form">

    <?php $form = ActiveForm::begin(); ?>
    
      <div class="panel-body">
       <div class="panel2">
            <div class="row"> 
                 <div class="col-md-4"> 
                     <?= $form->field($model, 'tema')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'acciones')->textarea(['rows' => 6]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Fecha de registro ...'],
                    'pluginOptions' => [
                        'autoclose'=>true
                    ]
                ]);?>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4"> 
                    <?= $form->field($model, 'id_sector')->dropDownList($listSectores,['prompt'=>'seleccione']) ?>
                </div>
                <div class="col-md-4"> 
                    <?= $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Fecha de registro ...'],
                    'pluginOptions' => [
                        'autoclose'=>true
                    ]
                ]);?>
                </div>
                <div class="col-md-4"> 
                    <?= $form->field($model, 'recomendaciones')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <?= $form->field($model, 'responsables')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
      </div>  

    <?php // $form->field($model, 'estatus')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Nueva' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
