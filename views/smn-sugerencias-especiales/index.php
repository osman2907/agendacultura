<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnSugerenciasEspecialesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tendencias Informativas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-sugerencias-especiales-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Tendencia Informativa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'pager'=> [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Ultima',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'fecha_inicio',
            'fecha_fin',
            [
                'attribute'=>'tema',
                'visible'=>true,
            ],
            'acciones:ntext',
            [
                'header' => 'Sector',
                'attribute' => 'nom_descripcion',
                'value' => 'idSector.nom_descripcion'
            ],
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'recomendaciones:ntext',
            // 'responsables:ntext',
             'estatus:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
