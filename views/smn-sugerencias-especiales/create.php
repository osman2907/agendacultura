<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnSugerenciasEspeciales */

$this->title = 'Tendencia Informativa';
$this->params['breadcrumbs'][] = ['label' => 'Tendencias informativas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-sugerencias-especiales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listSectores' => $listSectores
    ]) ?>

</div>
