<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSugerenciasEspeciales */

$this->title = 'Tendencias informativas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tendencias informativas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smn-sugerencias-especiales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listSectores' => $listSectores
    ]) ?>

</div>
