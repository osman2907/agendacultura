<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesCorreo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smn-solicitantes-correo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_solicitante')->textInput() ?>

    <?= $form->field($model, 'id_correo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
