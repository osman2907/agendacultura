<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesCorreo */

$this->title = 'Update Smn Solicitantes Correo: ' . $model->id_solicitante;
$this->params['breadcrumbs'][] = ['label' => 'Smn Solicitantes Correos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_solicitante, 'url' => ['view', 'id_solicitante' => $model->id_solicitante, 'id_correo' => $model->id_correo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smn-solicitantes-correo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
