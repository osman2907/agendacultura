<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesCorreo */

$this->title = $model->id_solicitante;
$this->params['breadcrumbs'][] = ['label' => 'Smn Solicitantes Correos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-correo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_solicitante' => $model->id_solicitante, 'id_correo' => $model->id_correo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_solicitante' => $model->id_solicitante, 'id_correo' => $model->id_correo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_solicitante',
            'id_correo',
        ],
    ]) ?>

</div>
