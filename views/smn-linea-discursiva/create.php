<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnLineaDiscursiva */

$this->title = 'Nueva Linea Discursiva';
$this->params['breadcrumbs'][] = ['label' => 'Linea Discursivas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-linea-discursiva-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
