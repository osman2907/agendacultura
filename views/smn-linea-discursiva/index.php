<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnLineaDiscursivaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lineas Discursivas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-linea-discursiva-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Linea Discursiva', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'pager'=> [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Ultima',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'descripcion:ntext',
            //'created_at',
            //'created_by',
            //'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
