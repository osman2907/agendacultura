<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnOir */

$this->title = 'Registrar Oficinas de Información Regional';
$this->params['breadcrumbs'][] = ['label' => 'Oficinas de Información Regional', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-oir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $listMunicipios=[];
    $listParroquias=[];
    $listCiudades=[];
    echo $this->render('_form', compact('model','listEstados','listMunicipios','listParroquias','listCiudades'));
   	?>

</div>
