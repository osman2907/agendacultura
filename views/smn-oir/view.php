<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnOir */

$this->title = "Consultar Oficinas de Información Regional";
$this->params['breadcrumbs'][] = ['label' => 'Oficinas de Información Regional', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-oir-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'descripcion',
            [
                'attribute'=>'status',
                'value'=>$model->status?'Activa':'Inactiva'
            ],
            [
                'attribute'=>'id_direccion',
                'format'=>'raw',
                'value'=>$model->mostrarDireccion()
            ],
            [
                'attribute'=>'zona_influencia',
                'format'=>'raw',
                'value'=>$model->getMostrarZonaInfluencia()
            ]
        ],
    ]) ?>

</div>
