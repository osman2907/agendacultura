<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\MciEstado;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnOirSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oficinas de Información Regional';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-oir-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Oficinas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'descripcion',
            [
                'attribute'=>'zona_influencia',
                'format'=>'html',
                'filter'=>MciEstado::listEstados(),
                'value'=>'mostrarZonaInfluencia'
            ],
            [
                'attribute'=>'status',
                'format'=>'html',
                'value'=>function($modelo){
                    return $modelo->status?'Activa':'Inactiva';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}&nbsp;&nbsp;{update}'
            ],
        ],
    ]); ?>
</div>
