<script type="text/javascript">
    $(document).ready(function(){

        $(document).on("change","#smnoir-id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades(data);
            }else{
                $("#smnoir-id_ciudad").html('');
                $("#smnoir-id_municipio").html('');
                $("#smnoir-id_parroquia").html('');
            }
        });

        $(document).on("change","#smnoir-id_municipio",function(){
            idMunicipio=$(this).val().trim();
            if(idMunicipio != ''){
                data={id:idMunicipio},
                parroquias=consultarPHP('/smn-personas/listparroquias',data,'html',false);
                $("#smnoir-id_parroquia").html(parroquias);
            }else{
                $("#smnoir-id_parroquia").html('');
            }
        });

        listarMunicipiosCiudades=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#smnoir-id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#smnoir-id_ciudad").html(ciudades);
        }

        $.noConflict();
        $(".chosen-select").chosen();
    });
</script>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnOir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smn-oir-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-4">
            <?= $form->field($model, 'descripcion')->textInput() ?>
        </div>

        <div class="col-xs-4">
            <?= $form->field($model, 'zona_influencia')->dropDownList($listEstados,['class'=>'chosen-select','multiple'=>'multiple','data-placeholder'=>'Seleccione las zonas de influencia']) ?>
        </div>

        <div class="col-xs-4">
            <?php
            if(!$model->isNewRecord){
                echo $form->field($model, 'status')->dropDownList(['0'=>'INACTIVA','1'=>'ACTIVA']);
            }
            ?>      
        </div>
    </div>

    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Dirección</legend>

        <div class="row">
            <div class="col-xs-3">
                <?= $form->field($model, 'id_estado')->dropDownList($listEstados,['prompt'=>'SELECCIONE']) ?>
            </div>

            <div class="col-xs-3">
                <?= $form->field($model, 'id_ciudad')->dropDownList($listCiudades,['prompt'=>'SELECCIONE']) ?>
            </div>

            <div class="col-xs-3">
                <?= $form->field($model, 'id_municipio')->dropDownList($listMunicipios,['prompt'=>'SELECCIONE']) ?>
            </div>

            <div class="col-xs-3">
                <?= $form->field($model, 'id_parroquia')->dropDownList($listParroquias,['prompt'=>'SELECCIONE']) ?>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12">
                <?= $form->field($model, 'direccion')->textArea() ?>
            </div>
        </div>
    </fieldset>


    <div class="row">
        <div class="col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
