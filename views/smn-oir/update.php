<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnOir */

$this->title = 'Modificar Oficinas de Información Regional';
$this->params['breadcrumbs'][] = ['label' => 'Oficinas de Información Regional', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-oir-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', compact('model','listEstados','listCiudades','listMunicipios','listParroquias')) ?>

</div>
