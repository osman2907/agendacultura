<?php error_reporting(E_ALL ^ E_NOTICE);
/* @var $this yii\web\View */
$dbconn = pg_connect("host=192.168.1.12 dbname=MCI user=postgres password=postgres");
//$db = new yii\db\Connection();
//require_once("../../php/connection.php");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>

		
		<style type="text/css">
#container {
    height: 400px; 
    min-width: 310px; 
    max-width: 1000px;
    margin: 0 auto;
}
		</style>

<script type="text/javascript">
 $(function () {
    Highcharts.chart('container', {
        chart: {
            type: 'line'//,
            /*options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }*/
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        plotOptions: {
            line:{
                dataLabels:{
                    enabled: true
                },
                //enableMouseTracking: false
            }
        },
        xAxis: {
            categories: [<?php echo implode(",",$horasGrafico) ?>]
        },
        yAxis: {
            title: {
                text: null
            }
        },
        series: [{
            name: 'CANTIDAD CONVOCATORIAS',
            data: [<?php echo implode(",",$horasGrafico) ?>]
        }]
    });
});

</script>
	</head>
	<body>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>

<div id="container" style="height: 400px"></div>
	</body>
        </html>
