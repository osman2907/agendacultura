<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesTelefonos */

$this->title = 'Create Smn Solicitantes Telefonos';
$this->params['breadcrumbs'][] = ['label' => 'Smn Solicitantes Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-telefonos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
