<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesTelefonos */

$this->title = $model->id_solicitante;
$this->params['breadcrumbs'][] = ['label' => 'Smn Solicitantes Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-telefonos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_solicitante' => $model->id_solicitante, 'id_telefono' => $model->id_telefono], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_solicitante' => $model->id_solicitante, 'id_telefono' => $model->id_telefono], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_solicitante',
            'id_telefono',
        ],
    ]) ?>

</div>
