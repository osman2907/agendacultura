<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantesTelefonos */

$this->title = 'Update Smn Solicitantes Telefonos: ' . $model->id_solicitante;
$this->params['breadcrumbs'][] = ['label' => 'Smn Solicitantes Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_solicitante, 'url' => ['view', 'id_solicitante' => $model->id_solicitante, 'id_telefono' => $model->id_telefono]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smn-solicitantes-telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
