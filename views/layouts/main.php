<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\controllers\SimonController;
use app\models\SmnUser;
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\components\GhostNav;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#page-wrapper").css('min-height',$("body").height());

            <?php if(!Yii::$app->user->isGuest){?>
                notificaciones=function(){
                    respuesta=consultarPHP('/smn-convocatorias/notificaciones',{},'html',false);
                    $(".notificaciones").html(respuesta);
                }
                setInterval(notificaciones,5000);


                avisar=function(){
                    cantidad=$(".notificaciones .vacio").length;
                    
                    if(cantidad == 0){
                        if($("#campana").hasClass('letra-roja')){
                            $("#campana").removeClass('letra-roja');
                        }else{
                            $("#campana").addClass('letra-roja');
                        }
                    }else{
                        $("#campana").removeClass('letra-roja');
                    }
                }
                setInterval(avisar,700);
            <?php } ?>
        });
    </script>
</head>
<body>
<?php $this->beginBody() ?>

<?php
$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
if(isset($usuario)){
    $tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
}
?>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="titulo" class="navbar-brand" href="<?php echo Yii::$app->homeUrl?>"><?php echo Yii::$app->name?></a>
        </div>
        <!-- Top Menu Items -->

        <?php if(!Yii::$app->user->isGuest){?>
            <ul class="nav navbar-right top-nav">
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong></h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left"><img class="media-object" src="http://placehold.it/50x50" alt=""></span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong></h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left"><img class="media-object" src="http://placehold.it/50x50" alt=""></span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong></h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>-->
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i id="campana" class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown notificaciones">
                        <li class="vacio"><a href="#">No existen notificaciones pendientes.</a></li>
                        <!--<li><a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a></li>
                        <li class="divider"></li>
                        <li><a href="#">View All</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo Yii::$app->user->username?> <b class="caret"></b></a>
                    <ul class="dropdown-menu ancho-200">
                        <!--<li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>-->
                        <?php if(!$usuario->superadmin){ ?>
                            <li>
                                <a href="<?php echo Yii::$app->homeUrl."site/cambiar-contrasena"?>"><i class="fa fa-fw fa-gear"></i> Cambiar Contraseña</a>
                            </li>
                        <?php } ?>
                        <li class="divider"></li>
                        <li>
                            <a id="cerrar-sesion" href="<?php echo Yii::$app->homeUrl."user-management/auth/logout"?>"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                </li>
            </ul>
        <?php }?>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <?php if(!Yii::$app->user->isGuest){?>
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#simon">
                            <i class="fa fa-fw fa-calendar"></i> AGENDA <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="simon" class="collapse">
                            <?php if(User::canRoute("/smn-convocatorias/index")){?>
                                <li><a href="<?php echo Yii::$app->homeUrl."smn-convocatorias/index"?>"><i class="fa fa-fw fa-clock-o"></i> Convocatorias </a></li>
                            <?php } ?>

                            <?php //if($tipoUsuario == 'MEDIOS'){?>
                                <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-medios-trabajadores/index"?>"><i class="fa fa-fw fa-users"></i> Administrar Personal </a></li>-->
                            <?php //} ?>
                            
                            <?php //if(User::canRoute("/siam-medios/index")){?>
                                <!--<li><a href="<?php echo Yii::$app->homeUrl."siam-medios/index"?>"><i class="fa fa-fw fa-microphone"></i> Medios </a></li>-->
                            <?php //} ?>

                            <?php if(User::canRoute("/smn-solicitantes/index")){?>
                                <li><a href="<?php echo Yii::$app->homeUrl."smn-solicitantes/index"?>"><i class="fa fa-fw fa-institution"></i> Entes Adscritos </a></li>
                            <?php } ?>

                            <?php if(User::canRoute("/smn-solicitantes-contactos/index")){?>
                                <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-solicitantes-contactos/index"?>"><i class="fa fa-fw fa-users"></i> Contactos por Solicitantes </a></li>-->
                            <?php } ?>

                            <?php //if(User::canRoute("/smn-linea-discursiva/index")){?>
                                <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-linea-discursiva/index"?>"><i class="fa fa-fw fa-file-text-o"></i> Líneas Discursivas </a></li>-->
                            <?php //} ?>

                            <?php //if(User::canRoute("/smn-sugerencias-especiales/index")){?>
                                <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-sugerencias-especiales/index"?>"><i class="fa fa-fw fa-file-text-o"></i> Tendencias Informativas </a></li>-->
                            <?php //} ?>
                        </ul>
                    </li>

                    <?php if(User::canRoute("/smn-usuarios-medios/index") || User::canRoute("/smn-usuarios-solicitantes/index") || User::canRoute("/smn-usuarios-mippci/index")){?>
                        <li>
                            <a href="#" data-toggle="collapse" data-target="#usuarios">
                                <i class="fa fa-fw fa-users"></i> USUARIOS <i class="fa fa-fw fa-caret-down"></i>
                            </a>
                            <ul id="usuarios" class="collapse">
                                <?php if(User::canRoute("/smn-usuarios-medios/index")){?>
                                    <li><a href="<?php echo Yii::$app->homeUrl."smn-usuarios-medios/index"?>"><i class="fa fa-fw fa-user"></i> Comunicaciones </a></li>
                                <?php } ?>

                                <?php if(User::canRoute("/smn-usuarios-solicitantes/index")){?>
                                    <li><a href="<?php echo Yii::$app->homeUrl."smn-usuarios-solicitantes/index"?>"><i class="fa fa-fw fa-user"></i> Entes Adscritos </a></li>
                                <?php } ?>

                                <?php //if(User::canRoute("/smn-usuarios-mippci/index")){?>
                                    <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-usuarios-mippci/index"?>"><i class="fa fa-fw fa-user"></i> Mippci </a></li>-->
                                <?php //} ?>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if($tipoUsuario == 'MIPPCI' && (User::canRoute("/mci-descripcion/index") || User::canRoute("/smn-personas/index") || User::canRoute("/smn-medios-trabajadores/index"))){?>
                        <li>
                            <a href="#" data-toggle="collapse" data-target="#gestion-datos"><i class="fa fa-fw fa-cog"></i> GESTIÓN DE DATOS <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="gestion-datos" class="collapse">
                                <?php if(User::canRoute("/mci-descripcion/index")){?>
                                    <li><a href="<?php echo Yii::$app->homeUrl."mci-descripcion/index"?>"> <i class="fa fa-fw fa-bookmark-o"></i> Descripciones </a></li>
                                <?php } ?>

                                <?php if(User::canRoute("/smn-personas/index")){?>
                                    <li><a href="<?php echo Yii::$app->homeUrl."smn-personas/index"?>"><i class="fa fa-fw fa-users"></i> Personas </a></li>
                                <?php } ?>
                                
                                <?php if(User::canRoute("/smn-medios-trabajadores/index")){?>
                                    <li><a href="<?php echo Yii::$app->homeUrl."smn-medios-trabajadores/index"?>"><i class="fa fa-fw fa-users"></i> Autoridades</a></li>
                                <?php } ?>

                                <?php //if(User::canRoute("/smn-oir/index")){?>
                                    <!--<li><a href="<?php echo Yii::$app->homeUrl."smn-oir/index"?>"><i class="fa fa-fw fa-users"></i> Oficinas de Información Regional</a></li>-->
                                <?php //} ?>
                            </ul>
                        </li>
                    <?php } ?>


                    <?php if(false){ ?>
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#reportes"><i class="fa fa-fw fa-files-o"></i> REPORTES <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="reportes" class="collapse">
                            <li><a href="<?php echo Yii::$app->homeUrl."smn-reportes/pdf"?>">Reporte1</a></li>
                        </ul>
                    </li>
                    <?php } ?>

                    <?php if(false){ ?>
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#estadisticas"><i class="fa fa-fw fa-bar-chart"></i> ESTADÍSTICAS <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="estadisticas" class="collapse">
                            <li><a href="<?php echo Yii::$app->homeUrl."smn-estadisticas/index"?>">Estadísticas1</a></li>
                        </ul>
                    </li>
                    <?php } ?>


                    <?php if(Yii::$app->user->identity->superadmin){?>
                        <li>
                            <a href="#" data-toggle="collapse" data-target="#permisologia"><i class="fa fa-fw fa-lock"></i> PERMISOLOGÍA <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="permisologia" class="collapse">
                                <li><a href="<?php echo Yii::$app->homeUrl."user-management/user/index"?>"><i class="fa fa-fw fa-users"></i> Usuarios </a></li>
                                <li><a href="<?php echo Yii::$app->homeUrl."user-management/role/index"?>"><i class="fa fa-fw fa-bookmark-o"></i> Roles </a></li>
                                <li><a href="<?php echo Yii::$app->homeUrl."user-management/permission/index"?>"><i class="fa fa-fw fa-tag"></i> Permisos </a></li>
                                <li><a href="<?php echo Yii::$app->homeUrl."user-management/auth-item-group/index"?>"><i class="fa fa-fw fa-tags"></i> Grupos de Permisos </a></li>
                                <li><a href="<?php echo Yii::$app->homeUrl."user-management/user-visit-log/index"?>"><i class="fa fa-fw fa-file-text-o"></i> Historial de Visitas </a></li>
                            </ul>
                        </li>
                    <?php } ?>
                <?php }?>

                <?php if(Yii::$app->user->isGuest){?>
                    <li><a href="<?php echo Yii::$app->homeUrl."user-management/auth/login"?>"><i class="fa fa-fw fa-user"></i> INICIAR SESIÓN</a></li>
                <?php }?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        <?php if(!Yii::$app->user->isGuest){?>
            <div class="mensaje-conectado">
                <b>CONECTADO COMO USUARIO:
                <?php
                $origen=$modelUser->getOrigen($usuario->id);
                if($origen){
                    echo "$tipoUsuario($origen)";
                }else{
                    echo "$tipoUsuario";
                }
                ?>
                </b>
            </div>
        <?php } ?>
        <div class="container-fluid container-principal">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?php
            $alertas=Yii::$app->session->getAllFlashes();
            foreach ($alertas as $tipo => $mensaje){
                if($tipo == 'success' || $tipo == 'warning' || $tipo == 'danger' || $tipo == 'info') {
                    ?>
                    <div class="alert alert-<?php echo $tipo?> alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?php echo $mensaje ?></strong>
                    </div>
                    <?php
                }else{
                    ?>
                    <div class="alert alert-info alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?php echo $mensaje ?></strong>
                    </div>
                    <?php
                }
            }
            ?>
            <?= $content ?>

        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
