<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
$form = ActiveForm::begin(['id'=>'formularioCorreosMippci','enableClientValidation'=>true,'enableAjaxValidation'=>true, 'action'=>['smn-correos/agregar-correos-mippci']]);
echo $form->field($model, "id_persona")->hiddenInput( array('value'=>$idPersona))->label(false);
?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'correo')->textInput(['class'=>'item-validar-mippci','data-accion-validar-mippci'=>'/smn-correos/validar-correos'])?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo Html::submitButton('Registrar',array('id'=>'registrar-correo-mippci','class'=>'item-validar btn btn-success'));
        ?>
    </div>
</div>
<?php
ActiveForm::end();
?>