<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
?>
<script type="text/javascript">
    $(document).on("submit", "#formularioCorreos", function () {
        data=$(this).serialize();
        respuesta=consultarPHP('/smn-correos/agregar-correos-medios',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
        idMedio=$("#id_medio").html();
        listarCorreos(idMedio);

    //Lista los correos asociados a la persona cuando se esta actualizando los datos
        if ($("#listado-correos").length > 0) {
            idMedio=$("#id_medio").html();
            listarCorreos(idMedio);
        }

        //Funcion para listar los correos
        function listarCorreos(idMedio){
            data={id_medio:idMedio};
            respuesta=consultarPHP('/smn-correos/listar-correos-medios',data,'html',false);
            $("#listado-correos").html(respuesta);
        }
        return false;
	});

</script>
<?php
$form = ActiveForm::begin(['id'=>'formularioCorreos','enableClientValidation'=>true,'enableAjaxValidation'=>true, 'action'=>['smn-correos/agregar-correos-medios']]);
?>

<input type="hidden" name="id_medio" value="<?= $idMedio ?>">
  
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'correo')->textInput(['class'=>'item-validar Smncorreo','data-accion-validar'=>'/smn-correos/validar-correos'])?>
    </div>
</div>
<div id="prueba">
    
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo Html::submitButton('Registrar',array('id'=>'registrar-correo-medio','class'=>'item-validar btn btn-success','data-accion-validar'=>'/smn-correos/validar-correos'));
        ?>
    </div>
</div>
<?php
ActiveForm::end();
?>