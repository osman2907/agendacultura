<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;


$form = ActiveForm::begin(['id'=>'formularioCorreos','enableClientValidation'=>true,'enableAjaxValidation'=>true]);

echo $form->field($modelSolicitante, 'id')->hiddenInput()->label(false);
?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'correo')->textInput(['class'=>'item-validar','data-accion-validar'=>'/smn-correos/validar-correos'])?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php
		echo Html::submitButton('Registrar',array('id'=>'registrar-correo','class'=>'btn btn-success'));
		?>
	</div>
</div>
<?php
ActiveForm::end();
?>