<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;

echo "<center>";
echo Html::button('Agregar Correo Electrónico',array('id'=>'nuevo-correo-medio','class'=>'btn btn-xs btn-success'));
echo "</center><br>";
if(count($mediosCorreos) > 0){
	echo "<table>";
		foreach ($mediosCorreos as $mediosCorreos){
			echo "<tr>";
				echo "<td>".$mediosCorreos->idCorreo->correo."</td>";
				echo "
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='button' value='Remover' class='btn btn-xs btn-danger margen1 eliminar-correo' data-id='".$mediosCorreos->id_correo."'>
					</td>";
			echo "</tr>";
		}
	echo "</table>";
}else{
	echo "No existen teléfonos registrados";
}
?>