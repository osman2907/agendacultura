<fieldset class="scheduler-border">    
    <legend class="scheduler-border">Correos Electrónicos</legend>
    <div class="listado-items">
        <div class="form-group field-correo required">
            <div class="input-group">
                <input type="text" id="correo" readonly="" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                </span>
            </div>
            <div class="help-block"></div>
        </div>
    </div>
</fieldset>