<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;

echo "<fieldset class='scheduler-border'>";
	echo "<legend class='scheduler-border'>Correos Electrónicos</legend>";
	echo "<left>";
	echo Html::button('Agregar Correo Electrónico',array('id'=>'agregar-correo','class'=>'btn btn-xs btn-success'));
	echo "</left><br>";
	if(count($personasCorreos) > 0){
		echo "<table align=left>";
			foreach ($personasCorreos as $personaCorreo){
				echo "<tr>";
					echo "<td class='margen-remover'>".$personaCorreo->correo->correo."</td>";
					echo "<td class='margen-remover'>&nbsp;&nbsp;&nbsp;<input type='button' class='btn btn-xs btn-danger eliminar-correo' data-id_correo='".$personaCorreo->id_correo."' value='Remover'></input></td>";
				echo "</tr>";
			}
		echo "</table>";
	}else{
		echo "No existen correos registrados";
	}
echo "</fieldset>";
?>