<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;

echo "<center>";
echo Html::button('Agregar Correo Electrónico',array('id'=>'agregar-correo','class'=>'btn btn-xs btn-success'));
echo "</center><br>";
if(count($solicitantesCorreos) > 0){
	echo "<table>";
		foreach ($solicitantesCorreos as $solicitanteCorreo){
			echo "<tr>";
				echo "<td>".$solicitanteCorreo->idCorreo->correo."</td>";
				echo "
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='button' value='Remover' class='btn btn-xs btn-danger margen1 eliminar-correo' data-id='".$solicitanteCorreo->id_correo."'>
					</td>";
			echo "</tr>";
		}
	echo "</table>";
}else{
	echo "No existen teléfonos registrados";
}
?>