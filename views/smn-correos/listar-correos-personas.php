<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;

echo "<center>";
echo Html::button('Agregar Correo Electrónico',array('id'=>'agregar-correo','class'=>'btn btn-xs btn-success'));
echo "</center><br>";
if(count($PersonasCorreos) > 0){
	echo "<table>";
		foreach ($PersonasCorreos as $PersonasCorreo){
			echo "<tr>";
				echo "<td>".$PersonasCorreo->idCorreo->correo."</td>";
				echo "
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='button' value='Remover' class='btn btn-xs btn-danger margen1' data-id='".$PersonasCorreo->id_correo."'>
					</td>";
			echo "</tr>";
		}
	echo "</table>";
}else{
	echo "No existen correos registrados";
}
?>