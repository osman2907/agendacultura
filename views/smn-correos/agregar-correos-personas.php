<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
$form = ActiveForm::begin(['id'=>'formularioCorreosPersonas','enableClientValidation'=>true,'enableAjaxValidation'=>true, 'action'=>['smn-correos/agregar-correos-personas']]);
echo $form->field($model, "id_persona")->hiddenInput( array('value'=>$idPersona))->label(false);
?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'correo')->textInput(['class'=>'item-validar-personas','data-accion-validar-mippci'=>'/smn-correos/validar-correos'])?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo Html::submitButton('Registrar',array('id'=>'registrar-correo-persona','class'=>'item-validar btn btn-success'));
        ?>
    </div>
</div>
<?php
ActiveForm::end();
?>