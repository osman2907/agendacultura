<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnMediosTrabajadores */

$this->title = 'Registrar Autoridades';
$this->params['breadcrumbs'][] = ['label' => 'Autoridades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-medios-trabajadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('Consultar Otra Cédula', ['create'], ['class' => 'btn btn-primary']) ?>

    <br><br>

    <?= $this->render('_form',compact('model','modelPersona','listMediosConvocatorias','listCargos','listEstados')) ?>

</div>
