<script type="text/javascript">
    $(document).ready(function(){
        //En el filtro de cédula sólo permite números.
        $("input[name=\"SmnMediosTrabajadoresSearch[cedula]\"]").addClass('solo-numero');
    });
</script>

<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\controllers\SimonController;
use app\models\SmnUser;
use app\models\MciDescripcion;
use app\models\MciEstado;
use app\models\SiamMedios;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnMediosTrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autoridades';
$this->params['breadcrumbs'][] = $this->title;

$usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado


$columnas1[]=['class' => 'yii\grid\SerialColumn'];
switch ($tipoUsuario){
    case 'MIPPCI':
        $columnas1[]=[
            'attribute'=>'id_medio',
            'value'=>'idMedio.identificacion',
            'filter'=>SiamMedios::listMediosConvocatorias()
        ];
    break;
}
$columnas2=[
    [
        'attribute'=>'cedula',
        'value'=>'idPersona.cedula'
    ],
    [
        'attribute'=>'nombre',
        'value'=>function($objeto,$clave,$index,$widget){
            return strtoupper($objeto->idPersona->nombre);
        }
    ],
    [
        'attribute'=>'apellido',
        'value'=>function($objeto,$clave,$index,$widget){
            return strtoupper($objeto->idPersona->apellido);
        }
    ],
    [
        'attribute'=>'id_tipo_trabajador',
        'label'=>'Cargo',
        'value'=>'idTipoTrabajador.nom_descripcion',
        'filter'=>MciDescripcion::listOpciones(Yii::$app->params['listas']['cargosMediosTrabajadores']),
    ],
    [
        'attribute'=>'perimetro',
        'format'=>'html',
        'filter'=>MciEstado::listEstados(),
        'value'=>'mostrarPerimetro'
    ],
    [
        'attribute'=>'status',
        'format'=>'html',
        'value'=>'estatus',
        'filter'=>array("1"=>"Activo","0"=>"Inactivo"),
    ],

    [
        'class' => 'yii\grid\ActionColumn',
        'template'=>'{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}'
    ],
];

$columnas=array_merge($columnas1,$columnas2);

?>
<div class="smn-medios-trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Autoridades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columnas
    ]); ?>
</div>