<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnMediosTrabajadores */

$this->title = "Consultar Autoridades";
$this->params['breadcrumbs'][] = ['label' => 'Autoridades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-medios-trabajadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id_medio' => $model->id_medio, 'id_persona' => $model->id_persona], ['class' => 'btn btn-primary']) ?>
    </p>

    <table class="table table-bordered">
        <tr>
            <th class="active" width="33%">Cédula</th>
            <th class="active" width="33%">Nombres</th>
            <th class="active" width="34%">Apellido</th>
        </tr>

        <tr>
            <td><?php echo $model->idPersona->cedula?></td>
            <td><?php echo strtoupper($model->idPersona->nombre)?></td>
            <td><?php echo strtoupper($model->idPersona->apellido)?></td>
        </tr>

        <tr>
            <th class="active">Entidad</th>
            <th class="active">Fecha Nacimiento</th>
            <th class="active">Cargo</th>
        </tr>

        <tr>
            <td><?php echo $model->idMedio->nom_concesionario?></td>
            <td><?php echo date("d/m/Y",strtotime($model->fecha_nacimiento))?></td>
            <td><?php echo strtoupper($model->idTipoTrabajador->nom_descripcion)?></td>
        </tr>

        <tr>
            <th class="active">Asignado al Estado</th>
            <th class="active">Perímetro de Cobertura</th>
            <th class="active">Pasaporte</th>
        </tr>

        <tr>
            <td><?php echo $model->idEstado->nom_estado?></td>
            <td>
                <ul>
                    <?php
                    $perimetro=$model->getPerimetro();
                    foreach ($perimetro as $per) {
                        echo "<li>".$per->idEstado->nom_estado."</li>";
                    }
                    ?>
                </ul>
            </td>

            <td>
                <b>Número:</b> <?php echo $model->pasaporte ?><br>
                <b>Fecha Emisión:</b> 
                <?php
                if(!empty($model->fecha_emision)){
                    echo date("d/m/Y",strtotime($model->fecha_emision));
                }
                ?>
                <br>
                <b>Fecha Vencimiento:</b> 
                <?php
                if(!empty($model->fecha_vencimiento)){
                    echo date("d/m/Y",strtotime($model->fecha_vencimiento));
                }
                ?>
                <br>
            </td>
        </tr>

        <tr>
            <th class="active">Correos Electrónicos</th>
            <th class="active">Teléfonos</th>
            <th class="active">Foto</th>
        </tr>

        <tr>
            <td>
                <ul>
                <?php
                foreach ($model->idPersona->smnPersonasCorreos as $smnPersonaCorreo) {
                    echo "<li>".$smnPersonaCorreo->correo->correo."</li>";
                }
                ?>
                </ul>
            </td>

            <td>
                <ul>
                <?php
                foreach ($model->idPersona->smnPersonasTelefonos as $smnPersonaTelefono) {
                    echo "<li>".$smnPersonaTelefono->telefono->telefono."</li>";
                }
                ?>
                </ul>
            </td>

            <td align="center">
                <?php
                if($model->getFoto()){
                    $foto=$model->getFoto()->archivo;
                    $foto='@web/'.Yii::$app->params['medios']['rutaFotosTrabajadores'].$foto;
                    echo Html::img($foto,array('width'=>'50%','class'=>'img-thumbnail'));
                }
                ?>
            </td>
        </tr>

    </table>

</div>

<?php
/*echo "<pre>";
print_r($model->idPersona);
echo "</pre>";*/
?>
