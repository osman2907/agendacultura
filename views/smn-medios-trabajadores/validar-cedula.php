<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Registrar Autoridades';
$this->params['breadcrumbs'][] = ['label' => 'Autoridades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="alert alert-info f13">
		Por favor ingrese el número de cédula de la autoridad a registrar para validar si existe en la base de datos.
    </div>
    <?php 
    $form = ActiveForm::begin(); 
    $form->enableClientValidation=false;
    $form->enableAjaxValidation=true;
	?>
    	<div class="row">
    		<div class="col-xs-3">
    			<?= $form->field($modelPersona, 'cedula')->textInput(['class'=>'solo-numero form-control']) ?>	
    		</div>

    		<div class="col-xs-3 margen-etiqueta">
    			<?= Html::submitButton('Validar', ['class' =>'btn btn-success']); ?>		
    		</div>
    	</div>

    <?php ActiveForm::end(); ?>

</div>

