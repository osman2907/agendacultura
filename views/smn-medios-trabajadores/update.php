<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnMediosTrabajadores */

$this->title = 'Modificar Autoridades';
$this->params['breadcrumbs'][] = ['label' => 'Autoridades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="smn-medios-trabajadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model','modelPersona','listMediosConvocatorias','listCargos','listEstados','listPerimetro')) ?>

</div>