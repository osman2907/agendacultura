<script type="text/javascript">
    $(document).ready(function($){
        $(".chosen-select").chosen();

        //funcion para agregar telefonos a las personas
        $(document).on("click","#agregar-telefono",function(){
            idPersona= $("#smnpersonas-id").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Telefono",
                message: respuesta
            });
        });

        $(document).on("submit","#formularioTelefonos",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
            }else{
                bootbox.alert('Telefono registrado con éxito');
            }
            idPersona= $("#smnpersonas-id").val();
            listarTelefonos(idPersona);

            return false;
        });

        //Funcion papara eliminar los telefonos de las personas
        $(document).on("click",".eliminar-telefono",function(){
            idPersona=$("#smnpersonas-id").val();
            idTelefono=$(this).data("id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Telefono eliminado con éxito');
                    }
                        listarTelefonos(idPersona);
                }
            });
        });




        //funcion para agregar correos a las personas
        $(document).on("click","#agregar-correo",function(){
            idPersona=$("#smnpersonas-id").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });

        $(document).on("submit","#formularioCorreos",function () {
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }
            
            data=$(this).serialize();
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
            idPersona=$("#smnpersonas-id").val();
            listarCorreos(idPersona);
            
            return false;
        });

        //Funcion papara eliminar los correos de las personas
        $(document).on("click",".eliminar-correo",function(){
            idPersona= $("#smnpersonas-id").val();
            idCorreo=$(this).data("id_correo");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(idPersona);
                }
            });
        });


        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                //$(this).parent().parent().children('.help-block').html(validacion);
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                //$(this).parent().parent().children('.help-block').html('');
                $(this).parent().parent().find('.help-block').html('');
            }
        });


        $(document).on("click",".agregar-item",function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('input').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        $(document).on("click",".eliminar-item",function(e){
            $(this).parent().parent().parent().remove();
        });

        $(document).on("submit","#formularioTrabajadores",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }
        });

        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

        function listarTelefonos(envio){
            data={id_persona:envio},
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        function listarCorreos(envio){
            data={id_persona:envio};
            respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
            $("#listado-correos").html(respuesta);
        }

        idPersona=$("#smnpersonas-id").val().trim();
        if(idPersona != ''){
            listarTelefonos(idPersona);
            listarCorreos(idPersona);
        }


        $.noConflict();
        $('.archivo').filestyle({  //Le mejora el estilo a los input file.
            buttonText : ' Examinar',
            buttonName : 'btn-primary'
        });
    });
</script>

<style type="text/css">
    #ui-datepicker-div{
        z-index: 2 !important;
    }
</style>



<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\SmnMediosTrabajadores */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(['id'=>'formularioTrabajadores',"options" => ["enctype" => "multipart/form-data"]]); ?>

<?php
echo $form->field($modelPersona,'id')->hiddenInput()->label(false);

$readonly=empty($modelPersona->id)?false:'readonly';

if(!$model->isNewRecord){
    $readonly=false;
}
?>
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model,'cedula')->textInput(['value'=>$modelPersona->cedula,'class'=>'solo-numero form-control','readonly'=>'readonly']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model,'nombre')->textInput(['value'=>$modelPersona->nombre,'readonly'=>$readonly]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model,'apellido')->textInput(['value'=>$modelPersona->apellido,'readonly'=>$readonly]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'id_medio')->dropDownList($listMediosConvocatorias,['prompt'=>'SELECCIONE','class'=>'chosen-select']) ?>
        </div>

        <?php 
        if(!$model->isNewRecord){
            $columnas=3;
            ?>
            <div class="col-md-1">
                <?php
                if($model->getFoto()){
                    $foto=$model->getFoto()->archivo;
                    $foto='@web/'.Yii::$app->params['medios']['rutaFotosTrabajadores'].$foto;
                    echo Html::img($foto,array('width'=>'100%','class'=>'img-thumbnail'));
                }
                ?>
                <br>&nbsp;
            </div>
        <?php }else{
            $columnas=4;
        }?>

        <div class="col-md-<?php echo $columnas?>">
            <?= $form->field($model, 'foto')->fileInput(['class'=>'archivo']) ?>
        </div>

        <div class="col-md-4">
            <?php
            $anio=date("Y");
            echo $form->field($model, 'fecha_nacimiento')->widget(DatePicker::classname(),[
                'language' => 'es',
                'dateFormat' => 'dd/MM/yyyy',
                'clientOptions'=>[
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange'=> "1900:$anio"
                ]
            ])->textInput();
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'id_tipo_trabajador')->dropDownList($listCargos,['prompt'=>'SELECCIONE','class'=>'chosen-select']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'id_estado')->dropDownList($listEstados,['prompt'=>'SELECCIONE','class'=>'chosen-select']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'perimetro')->dropDownList($listEstados,['class'=>'chosen-select','multiple'=>'multiple','data-placeholder'=>'Seleccione los estados de cobertura']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Pasaporte</legend>
                <div class="col-md-4">
                    <?= $form->field($model, 'pasaporte')->textInput(['class'=>'solo-numero form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?php
                    $anio=date("Y")+20;
                    echo $form->field($model, 'fecha_emision')->widget(DatePicker::classname(),[
                        'language' => 'es',
                        'dateFormat' => 'dd/MM/yyyy',
                        'clientOptions'=>[
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange'=> "1900:$anio"
                        ]
                    ])->textInput();
                    ?>
                </div>

                <div class="col-md-4">
                    <?php
                    $anio=date("Y")+20;
                    echo $form->field($model, 'fecha_vencimiento')->widget(DatePicker::classname(),[
                        'language' => 'es',
                        'dateFormat' => 'dd/MM/yyyy',
                        'clientOptions'=>[
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange'=> "1900:$anio"
                        ]
                    ])->textInput();
                    ?>
                </div>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div id="listado-correos">
                <?php if(empty($modelPersona->id)){ ?>
                        <div class="listado-items">
                            <div class="form-group field-correo required">
                                <div class="input-group">
                                    <input type="text" id="correo" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                    </span>
                                </div>
                                <div class="help-block"></div>
                            </div>
                        </div>
                <?php } ?>
                </div>
            </fieldset>
        </div>

        <div class="col-md-4">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Teléfonos</legend>
                <div id="listado-telefonos">
                    <?php if(empty($modelPersona->id)){ ?>
                        <div class="listado-items">
                            <div class="form-group field-telefono required">
                                <div class="input-group">
                                    <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" data-accion-validar="/smn-telefonos/validar-telefonos">
                                    <span class="input-group-btn">
                                        <button type="button" class="agregar-item btn btn-success"><i class="glyphicon glyphicon-plus"></i></button>
                                    </span>
                                </div>
                                <div class="help-block"></div>
                            </div> 
                        </div>
                    <?php } ?>
                </div>
            </fieldset>
        </div>

        <?php if(!$model->isNewRecord){ ?>
            <div class="col-md-4">
                <label class="control-label" for="smnMediosTrabajadores-estatus">Estatus</label>
                <div class="[ form-group ]">
                    <input type="checkbox" name="SmnMediosTrabajadores[status]" id="smnmediostrabajadores-status" autocomplete="off" class="roles" value="1" <?php echo ($model->status)?'checked':''; ?>/>
                    <div class="[ btn-group ]">
                        <label for="smnmediostrabajadores-status" class="[ btn btn-primary ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span></span>
                        </label>
                        <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                            Activo
                        </label>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
