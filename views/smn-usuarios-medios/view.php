<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = 'Consultar Usuarios Medios';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Medios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<br>

<table class="table table-bordered">
	<tr class="active">
		<th width="33%">CÉDULA</th>
		<th width="33%">NOMBRE</th>
		<th width="33%">APELLIDO</th>
	</tr>

	<tr>
		<td><?php echo isset($model->idPersona->cedula)?$model->idPersona->cedula:'';?></td>
		<td><?php echo isset($model->idPersona->nombre)?$model->idPersona->nombre:'';?></td>
		<td><?php echo isset($model->idPersona->apellido)?$model->idPersona->apellido:'';?></td>
	</tr>

	<tr class="active">
		<th>USUARIO</th>
		<th>MEDIO</th>
		<th>ROLES</th>
	</tr>

	<tr>
		<td><?php echo $model->username ?></td>
		<td><?php echo isset($model->usuarioMedio->idMedio->identificacion)?$model->usuarioMedio->idMedio->identificacion:'';?></td>
		<td>
			<?php
			$roles=$model->authAssignments;
			foreach ($roles as $rol){
				echo "<span class='label label-primary'>$rol->item_name</span>&nbsp;&nbsp;";
			}
			?>
		</td>
	</tr>
</table>
<br>
<center>
<?= Html::a('Bandeja de Usuarios', ['index'], ['class' => 'btn btn-danger']) ?>
</center>


<?php
/*echo "<pre>";
print_r($model->usuarioMedio->idMedio->identificacion);
echo "</pre>";*/
?>


