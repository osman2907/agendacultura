<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = 'Registrar Usuarios Comunicaciones';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Comunicaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form',compact('model','listMedios','listRoles')); ?>

