<script type="text/javascript">
	$(document).ready(function(){
		$("#smnuser-username").attr('readonly','readonly');
		idPersona= $("#smnuser-id_persona").val();
		data={id_persona:idPersona};
	    consulta=consultarPHP('/smn-personas/buscar-persona2',data,'json',false);
	    $("#smnuser-id_persona").val(consulta.id);
    	$("#smnuser-cedula").val(consulta.cedula);
    	$("#smnuser-nombre").val(consulta.nombre);
    	$("#smnuser-apellido").val(consulta.apellido);
		
		if ($("#listado-telefonos").length > 0) {
        	    listarTelefonos(idPersona);
  		}
    	
        if ($("#listado-correos").length > 0) {
	        listarCorreos(idPersona);
  		}

  		//Funcion para listar los telefonos
        function listarTelefonos(envio){
            data={id_persona:envio},
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }
  		
  		//Funcion para listar los correos
        function listarCorreos(envio){
            data={id_persona:envio};
            respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
            $("#listado-correos").html(respuesta);
        }
	});
</script>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = 'Modificar Usuarios Medios';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Medios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cedula, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-usuarios-medios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form',compact('model','telefonos','correos','listRoles','listMedios'));
    ?>

</div>
