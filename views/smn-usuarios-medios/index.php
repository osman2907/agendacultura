<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\models\rbacDB\Role;
//use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\Pjax;
use webvimark\extensions\GridBulkActions\GridBulkActions;
use webvimark\extensions\GridPageSize\GridPageSize;
use app\models\SmnUser;
use webvimark\modules\UserManagement;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Usuarios Comunicaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'idPersona.cedula',
                'idPersona.nombre',
                'idPersona.apellido',
                [
                    'attribute'=>'usuarioMedio.idMedio.identificacion',
                    'label'=>'Entidad'
                ],
                ['class' => 'webvimark\components\StatusColumn',
                    'attribute' => 'status',
                    'label'=> 'Estatus',
                    'optionsArray' => [
                        [SmnUser::STATUS_ACTIVE, UserManagementModule::t('back', 'Activo'), 'success'],
                        [SmnUser::STATUS_INACTIVE, UserManagementModule::t('back', 'Inactivo'), 'warning'],
                    ],
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} &nbsp; {update} &nbsp; {delete} &nbsp; {regenerar}',
                    'buttons'=>[
                        'regenerar' => function ($url, $model){
                            return Html::a('<span class="glyphicon glyphicon-lock"></span>','#', ['title' => Yii::t('yii', 'Regenerar contraseña'), 'id' => 'RegenerarClave', 'data-userid' => $model->id, 'data-user' => $model->username, 'data-idpersona' => $model->id_persona
                            ]);                                
                        }
                    ]          
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>