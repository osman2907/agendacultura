<script type="text/javascript">
	$(document).ready(function(){
		//funcion para buscar a la persona tanto en el Ldap como en la BD interna
		$(document).on("click","#verificar",function(){
			limpiar();
			cedula=$("#cedula").val().trim();
			if(cedula == ''){
				bootbox.alert("Por favor ingrese la cédula del usuario a registrar");
				return false;
			}


            data = {cedula: cedula},
            usuarioMippci = consultarPHP('/smn-usuarios-mippci/validar-usuario-mippci', data, 'html', false);
            if (usuarioMippci) {
                bootbox.alert("La persona que desea registrar ya es un usuario mippci.");
                return false;
            }

			data={cedula:cedula},
            usuarioMedio=consultarPHP('/smn-personas/validar-usuario-medio',data,'html',false);

            if(usuarioMedio){
            	bootbox.alert("La persona que desea registrar ya es un usuario medio.");
            	return false;
            }

            data = {cedula: cedula},
            usuarioSolicitante = consultarPHP('/smn-personas/validar-usuario-solicitante', data, 'html', false);
            if (usuarioSolicitante) {
                bootbox.alert("La persona que desea registrar ya es un usuario solicitante.");
                return false;
            }

			data={cedula:cedula},
            respuesta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);

            envio={id:respuesta.id};

            if(respuesta.id){
                bootbox.alert("La persona que desea registrar ya existe, complete los datos solicitados");
            	$("#smnuser-id_persona").val(respuesta.id);
            	$("#smnuser-cedula").val(respuesta.cedula);
            	$("#smnuser-nombre").val(respuesta.nombre);
            	$("#smnuser-apellido").val(respuesta.apellido);

                data={id_persona:respuesta.id},
                respuesta=consultarPHP('/smn-usuarios-solicitantes/buscar-usuario',data,'json',false);

                if(respuesta){
                    $("#smnuser-username").val(respuesta.usuario);
                    $("#smnuser-username").attr('readonly','readonly');

                    if($("#smnuser-username").val().trim() == ''){
                        $("#smnuser-username").removeAttr('readonly');    
                    }
                }else{
                    $("#smnuser-username").removeAttr('readonly');
                }

	        
	            if ($("#listado-telefonos").length > 0) {
                    listarTelefonos(envio);
                }

                if ($("#listado-correos").length > 0) {
                    listarCorreos(envio);
                }

            }else{
            	bootbox.alert("El número de cédula no existe por favor ingrese el nombre y apellido de la persona",function(){
            		$("#smnuser-cedula").val(cedula);
	            	$("#smnuser-nombre").attr('readonly',false);
	            	$("#smnuser-apellido").attr('readonly',false);
	            	$("#telefono").attr('readonly',false);
	            	$("#correo").attr('readonly',false);
            	});
            }
		});

  	//Funcion para listar los telefonos
    listarTelefonos=function(envio){
        data={id_persona:envio},
        respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
        $("#listado-telefonos").html(respuesta);
    }

  		
  	//Funcion para listar los correos
    listarCorreos=function(envio){
        data={id_persona:envio};
        respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
        $("#listado-correos").html(respuesta);
    }


    function nuevosTelefonos(){
        data={};
        respuesta=consultarPHP('/smn-telefonos/nuevos-telefonos',data,'html',false);
        $("#listado-telefonos").html(respuesta);
    }

    function nuevosCorreos(){
        data={};
        respuesta=consultarPHP('/smn-correos/nuevos-correos',data,'html',false);
        $("#listado-correos").html(respuesta);
    }


        //funcion para agregar correos a las personas
        $(document).on("click","#agregar-correo",function(){
            idPersona= $("#smnuser-id_persona").val();
            data={id_persona:idPersona};
        });
        //funcion para agregar telefonos a las personas
		/*$(document).on("click","#agregar-telefono",function(){
            idPersona=$("#smnuser-cedula").val();
            data={cedula:idPersona};
    		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
            data={id_persona:consulta.id};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Telefono",
                message: respuesta
            });
    	});*/

        //funcion para agregar telefonos a las personas
        $(document).on("click","#agregar-telefono",function(){
            idPersona= $("#smnuser-id_persona").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Telefono",
                message: respuesta
            });
        });

	//funcion para agregar correos a las personas
    	$(document).on("click","#agregar-correo",function(){
            idPersona=$("#smnuser-cedula").val();
            data={cedula:idPersona};
    		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
            data={id_persona:consulta.id};
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });

        //Funcion papara eliminar los telefonos de las personas
        $(document).on("click",".eliminar-telefono",function(){
            idPersona=$("#smnuser-id_persona").val();
            idTelefono=$(this).data("id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Telefono eliminado con éxito');
                    }
                        listarTelefonos(idPersona);
                }
            });
        });

        //Funcion papara eliminar los correos de las personas
        $(document).on("click",".eliminar-correo",function(){
            idPersona= $("#smnuser-id_persona").val();
            idCorreo=$(this).data("id_correo");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(idPersona);
                }
            });
        });

        //funcion para limpiar la vista de los usuarios medios
        limpiar=function(){
            telefono =$("#los-telefonos").html();
            $("#smnuser-id_persona").val('');
            $("#smnuser-cedula").val('');
            $("#smnuser-nombre").val('');
            $("#smnuser-apellido").val('');
            $("#smnuser-nombre").attr('readonly','readonly');
            $("#smnuser-apellido").attr('readonly','readonly');
            $("#smnuser-username").val('');
            $("#smnuser-username").attr('readonly',false);
            $("#telefono").val('');
            $("#telefono").attr('readonly','readonly');
            $("#correo").val('');
            $("#correo").attr('readonly','readonly');
            $("#listado-telefonos").val('');
            nuevosTelefonos();
            nuevosCorreos();
        }
    });

    $(document).on("click","#limpiar",function(){
        limpiar();
    });

    $(document).on("click",".agregar-item",function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

    $(document).on("click",".eliminar-item",function(){
        $(this).parent().parent().parent().remove();
    });

    $(document).on("blur",".item-validar",function(){
        data={'valor':$(this).val()};
        accion=$(this).data('accion-validar');
        validacion=consultarPHP(accion,data,'html',false);
        if(validacion){
            $(this).parent().parent().addClass('has-error');
            //$(this).parent().parent().children('.help-block').html(validacion);
            $(this).parent().parent().find('.help-block').html(validacion);
        }else{
            $(this).parent().parent().removeClass('has-error');
            //$(this).parent().parent().children('.help-block').html('');
            $(this).parent().parent().find('.help-block').html('');
        }
    });

    /*$(document).on("submit","#formularioTelefonos",function(){
        //data=$(this).serialize();
        Persona= $("#smnuser-id_persona").val();
        idPersona=$("#smnuser-cedula").val();
        data={cedula:idPersona};
        consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
        bootbox.hideAll();
        if(respuesta == "error"){
            bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
        }else{
            bootbox.alert('Telefono registrado con éxito');
        }
        listartelefonos(Persona);
        return false;
    });*/

    $(document).on("submit","#formularioTelefonos",function(){
        errores=ejecutarValidacionItems();
        if(errores > 0){
            return false;
        }

        data=$(this).serialize();
        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
        bootbox.hideAll();
        if(respuesta == "error"){
            bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
        }else{
            bootbox.alert('Telefono registrado con éxito');
        }
        idPersona= $("#smnuser-id_persona").val();
        listarTelefonos(idPersona);

        return false;
    });

    $(document).on("submit", "#formularioCorreos", function () {
        data=$(this).serialize();
        Persona= $("#smnuser-id_persona").val();
        idPersona=$("#smnuser-cedula").val();
        respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
            listarCorreos(Persona);
        return false;
    });

    ejecutarValidacionItems=function(){
        cantidadItems=$(".item-validar").length;
        for(i=0;i<cantidadItems;i++){
            elementoHtml=$(".item-validar")[i];
            $(elementoHtml).focus();
            $(elementoHtml).blur();
        }
        errores=$(".has-error").length;
        return errores;
    }

    $(document).on("submit","#formularioPersonas",function(){
        errores=ejecutarValidacionItems();
        if(errores > 0){
            return false;
        }

        Persona= $("#listado-medios").val();
        if (Persona = ''){
            bootbox.alert("Debe seleccionar un Medio.");
            return false;
        }
    });
</script>

<?php
use yii\helpers\Html;
use yii\base\Model;
use yii\widgets\ActiveForm;
?>

<style type="text/css">
</style>

<div class="simonsmn-personas-form">

    <?php $form = ActiveForm::begin(['id'=>'formularioPersonas','enableClientValidation'=>false,'enableAjaxValidation'=>true]); ?>
    
    	<?= $form->field($model, 'id')->hiddenInput()->label(false);?>

    	<?= $form->field($model, 'id_persona')->hiddenInput()->label(false);?>
    	
        <?php if($model->isNewRecord){?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">Por favor verifique el número de cédula para registrar un usuario nuevo.</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <?= Html::label('Ingrese la Cédula:', 'cedula', ['class' => '']) ?>
                <?= Html::input('text', 'cedula', '', ['id'=>'cedula','class'=>'form-control solo-numero','label'=>'Cédula']); ?>
            </div>
            <div class="col-md-2 margen-etiqueta">
                <?= Html::button('Verificar', ['id'=> 'verificar','class' => 'btn btn-primary']) ?>
                <?= Html::button('Limpiar', ['id' => 'limpiar','class' => 'btn btn-warning']) ?>
            </div>
        </div>
        <?php } ?>
        <br>
	    <div class="row">
	        <div class="col-md-3">
	            <?= $form->field($model,'cedula')->textInput(['readonly'=>'readonly'])?>
	        </div>
	        <div class="col-md-3">
	            <?= $form->field($model,'nombre')->textInput(['readonly'=>'readonly'])?>
	        </div>
	        <div class="col-md-3">
	            <?= $form->field($model,'apellido')->textInput(['readonly'=>'readonly'])?>
	        </div>
	        <div class="col-md-3">
	            <?= $form->field($model,'username')->textInput(['class'=>'form-control minuscula'])?>
	        </div>
	    </div>

        <div class="row">
            <div class="col-md-3">
	            <?= $form->field($model,'id_medio')->dropDownList($listMedios,['class'=>'form-control','id'=>'listado-medios','prompt'=>'Seleccione'])?>
	        </div>
            
        <div class="col-md-4" id="listado-telefonos">
	    		<?php if($model->isNewRecord){?>
		            <div id="los-telefonos">
		                <fieldset class="scheduler-border">    
		                    <legend class="scheduler-border">Teléfonos</legend>
		                    <div class="listado-items">
		                        <div class="form-group field-telefono required">
		                            <div class="input-group">
		                                <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" readonly="" data-accion-validar="/smn-telefonos/validar-telefonos">
		                                <span class="input-group-btn">
		                                    <button type="button" class="agregar-item btn btn-success"><i class="glyphicon glyphicon-plus"></i></button>
		                                </span>
		                            </div>
		                            <div class="help-block"></div>
		                        </div> 
		                    </div>
		                </fieldset>
		            </div>
       			<?php }?>
       			<?php if(!$model->isNewRecord){?>
		            <div>
		                <fieldset class="scheduler-border">
		                    <legend class="scheduler-border">Teléfonos</legend>
		                    <div id="listado-telefonos">
		                       
		                    </div>
		                </fieldset>
		            </div>
		        <?php }?>
	    	</div>
	    	<div class="col-md-5" id="listado-correos">
	    		<?php if($model->isNewRecord){?>
		            <div id="los-correos">
		                <fieldset class="scheduler-border">    
		                    <legend class="scheduler-border">Correos Electrónicos</legend>
		                    <div class="listado-items">
		                        <div class="form-group field-correo required">
		                            <div class="input-group">
		                                <input type="text" id="correo" readonly="" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
		                                <span class="input-group-btn">
		                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
		                                </span>
		                            </div>
		                            <div class="help-block"></div>
		                        </div>
		                    </div>
		                </fieldset>
		            </div>
	            <?php }?>
	            <?php if(!$model->isNewRecord){?>
            		<div>
	                <fieldset class="scheduler-border">
	                    <legend class="scheduler-border">Correos Electrónicos</legend>
	                    <div id="listado-correos">
	                        
	                    </div>
	                </fieldset>
                <?php }?>
                         </div>
	    	</div>
	    </div>
	    <fieldset class="scheduler-border">
            <legend class="scheduler-border">Asignación de Roles</legend>
            <?php 
                $array = array(); 
                $roles=$model->authAssignments;
                foreach ($roles as $rol){
                    array_push($array, $rol->item_name);
                }
            ?>
            <?php
            foreach ($listRoles as $rol){
            	?>
            	<div class="col-md-3">
	            	<div class="[ form-group ]">
			            <input type="checkbox" name="roles[<?php echo $rol ?>]" id="<?php echo $rol ?>" autocomplete="off" class="roles" <?php if(in_array($rol, $array)) echo 'checked'; ?> />
			            <div class="[ btn-group ]">
			                <label for="<?php echo $rol ?>" class="[ btn btn-primary ]">
			                    <span class="[ glyphicon glyphicon-ok ]"></span>
			                    <span> </span>
			                </label>
			                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
			                    <?php echo $rol ?>
			                </label>
			            </div>
			        </div>
			    </div>
            	<?php
            }
            ?>
        </fieldset>

        <div class="row">
	        <div class="col-md-4">
	            <div class="form-group">
	            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'registrar']) ?>
	            </div>
	        </div> 
	    </div>
    <?php ActiveForm::end(); ?>
</div>