<script type="text/javascript">
	$(document).ready(function(){
	    /*$(document).on("submit","#formularioTelefonos",function(){
	        data=$(this).serialize();
	        Persona= $("#smnuser-id_persona").val();
	        idPersona=$("#smnuser-cedula").val();
	        data={cedula:idPersona};
	        consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
	        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
	        bootbox.hideAll();
	        if(respuesta == "error"){
	            bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
	        }else{
	            bootbox.alert('Telefono registrado con éxito');
	        }
	        listartelefonos(Persona);
	    
	        return false;
	    });*/
	});
</script>    
<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;


$form = ActiveForm::begin(['id'=>'formularioTelefonos','enableClientValidation'=>true,'enableAjaxValidation'=>true]);

echo $form->field($modelPersona, 'id')->hiddenInput()->label(false);
?>
<div class="row">
    <div class="col-md-12"> 
        <?= $form->field($model, 'telefono')->textInput(['class'=>'item-validar2','data-accion-validar'=>'/smn-telefonos/validar-telefonos'])?>
    </div>
    <input type="hidden" name="usuario_medio">

</div>

<div class="row">
	<div class="col-md-12">
		<?php
		echo Html::submitButton('Registrar',array('id'=>'registrar-telefono','class'=>'btn btn-success'));
		?>
	</div>
</div>
<?php
ActiveForm::end();
?>