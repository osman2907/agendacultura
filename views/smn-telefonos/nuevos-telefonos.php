<fieldset class="scheduler-border">    
    <legend class="scheduler-border">Teléfonos</legend>
    <div class="listado-items">
        <div class="form-group field-telefono required">
            <div class="input-group">
                <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" readonly="" data-accion-validar="/smn-telefonos/validar-telefonos">
                <span class="input-group-btn">
                    <button type="button" class="agregar-item btn btn-success"><i class="glyphicon glyphicon-plus"></i></button>
                </span>
            </div>
            <div class="help-block"></div>
        </div>
    </div>
</fieldset>