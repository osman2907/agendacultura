<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
?>
<script type="text/javascript">
	$(document).on("submit", "#formularioTelefonosMedios", function () {
        data=$(this).serialize();
        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-medios',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
        idMedio=$("#id_medio").html();
        return false;

    });

</script>

<?php 

$form = ActiveForm::begin(['id'=>'formularioTelefonosMedios','enableClientValidation'=>true,'enableAjaxValidation'=>true]);

echo $form->field($model, 'id')->hiddenInput()->label(false);

echo $form->field($modelMedios, 'id_medio')->hiddenInput()->label(false);?>
<div class="row">
    <div class="col-md-12"> 
        <?= $form->field($modelMedios, 'id_telefonos')->textInput(['class'=>'item-validar','data-accion-validar'=>'/smn-telefonos/validar-telefonos'])?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php
		echo Html::submitButton('Registrar',array('id'=>'registrar-telefono-medio','class'=>'btn btn-success'));
		?>
	</div>
</div>
<?php
ActiveForm::end();
?>