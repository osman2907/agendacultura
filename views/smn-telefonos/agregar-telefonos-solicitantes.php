<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;


$form = ActiveForm::begin(['id'=>'formularioTelefonos','enableClientValidation'=>true,'enableAjaxValidation'=>true]);

echo $form->field($modelSolicitante, 'id')->hiddenInput()->label(false);
?>
<div class="row">
    <div class="col-md-12"> 
        <?= $form->field($model, 'telefono')->textInput(['class'=>'item-validar','data-accion-validar'=>'/smn-telefonos/validar-telefonos'])?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php
		echo Html::submitButton('Registrar',array('id'=>'registrar-telefono','class'=>'btn btn-success'));
		?>
	</div>
</div>
<?php
ActiveForm::end();
?>