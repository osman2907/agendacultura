<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;


echo "<fieldset class='scheduler-border'>";
	echo "<legend class='scheduler-border'>Teléfonos</legend>";
	
	echo "<left>";
	echo Html::button('Agregar Telefono',array('id'=>'agregar-telefono','class'=>'btn btn-xs btn-success'));
	echo "</left><br>";

	if(count($personasTelefonos) > 0){
		echo "<table align=left>";
			foreach ($personasTelefonos as $personaTelefono){
				echo "<tr>";
					echo "<td class='text-right margen-remover'>".$personaTelefono->telefono->telefono."</td>";
					echo "<td class='margen-remover'>&nbsp;<input type='button' class='btn btn-xs btn-danger eliminar-telefono' data-id_telefono='".$personaTelefono->id_telefono."' value='Remover'></input></td>";
				echo "</tr>";
			}
		echo "</table>";
	}else{
		echo "No existen teléfonos registrados";
	}
echo "</fieldset>";
?>