<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
$form = ActiveForm::begin(['id'=>'formularioTelefonosMedios','enableClientValidation'=>true,'enableAjaxValidation'=>true, 'action'=>['smn-telefonos/agregar-telefonos-mippci']]);
echo $form->field($model, "id_persona")->hiddenInput( array('value'=>$idPersona))->label(false);
?>
<div class="row">
    <div class="col-md-12"> 
        <?= $form->field($model, 'telefono')->textInput(['class'=>'item-validar-mippci', 'data-accion-validar-mippci'=>'/smn-telefonos/validar-telefonos'])?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php 
                    echo Html::submitButton('Registrar',array('id'=>'registrar-telefonos-mippci','class'=>'item-validar btn btn-success'));
                ?>
	</div>
</div>
<?php
ActiveForm::end();
?>