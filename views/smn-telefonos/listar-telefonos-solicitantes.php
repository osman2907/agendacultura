<?php
use yii\helpers\Html;
use yii\base\Model;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;

echo "<center>";
echo Html::button('Agregar Telefono',array('id'=>'agregar-telefono','class'=>'btn btn-xs btn-success'));
echo "</center><br>";
if(count($solicitantesTelefonos) > 0){
	echo "<table>";
		foreach ($solicitantesTelefonos as $solicitanteTelefono){
			echo "<tr>";
				echo "<td>".$solicitanteTelefono->idTelefono->telefono."</td>";
				echo "<td>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='button' value='Remover' class='btn btn-xs btn-danger margen1 eliminar-telefono' data-id_telefono='".$solicitanteTelefono->id_telefono."'>
					</td>";
			echo "</tr>";
		}
	echo "</table>";
}else{
	echo "No existen teléfonos registrados";
}
?>