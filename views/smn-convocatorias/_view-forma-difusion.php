<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 negrita">
			<?php if(count($model->smnConvocatoriaPropagacions) > 0){ ?>
				Las Formas de Difusión seleccionadas para esta convocatoria son las siguientes:
			<?php }else{ ?>
				<div class="letra-roja">No se han seleccionado formas de Difusión para esta convocatoria.</div>
			<?php } ?>
		</div>
	</div>
	<br>
	<div>
		<div class="col-md-6">
			<ul class='list-group'>
			<?php
			$formasDifusion=$model->smnConvocatoriaPropagacions;
			foreach ($formasDifusion as $formaDifusion){
				echo "<li class='list-group-item'>".$formaDifusion->idPropagacion->nom_descripcion."</li>";
			}
			?>
			</ul>
		</div>
	</div>
</div>