<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php 
$form = ActiveForm::begin(['id'=>'formulario-observaciones']);
$form->enableClientValidation=false;
$form->enableAjaxValidation=false;
//echo  Html::hiddenInput("accion","registrar",['id'=>'accion']);
?>
	<div class="row">
		<div class="col-md-12">
			<?= $form->field($model, 'observacion')->textarea(['rows' => '3']) ?>
		</div>

		<div class="col-md-12">
			<?= Html::submitButton('Registrar',['class'=>'btn btn-success']) ?>
		</div>
	</div>
<?php ActiveForm::end(); ?>