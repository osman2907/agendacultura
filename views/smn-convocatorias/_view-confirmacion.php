<div class="container-fluid">
	<div>
		<div class="col-md-12">
			<table class="table table-bordered">
				<tr>
					<th class="active">Señal</th>
					<td><?php echo $model->idSenal->nom_descripcion?></td>
				</tr>

				<tr>
					<th class="active">Nivel de Vocería</th>
					<td><?php echo $model->idPrioridad->nom_descripcion ?></td>
				</tr>

				<tr>
					<th class="active">Sector</th>
					<td><?php echo $model->idSector->nom_descripcion ?></td>
				</tr>

				<tr>
					<th class="active">Estatus</th>
					<td>
						<?php echo $model->getEstatusLetra()?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>