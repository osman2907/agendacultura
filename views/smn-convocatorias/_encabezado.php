<?php
use app\controllers\SimonController;
use app\models\SmnUser;

$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
?>


<?php if($tipoUsuario == 'MIPPCI' || $tipoUsuario == 'ENTIDAD SOLICITANTE'){?>
	<tr class='fila-encabezado'>
		<td></td>
		<td colspan="7" class="encabezado-rojo">DATOS GENERALES</td>
		<td colspan="3" class="encabezado-verde">DIFUSIÓN</td>
		<td class="encabezado-amarillo">CLASIFICACIÓN EDITORIAL</td>
		<!--<td class="encabezado-cobertura">COBERTURA</td>-->
	</tr>
<?php } ?>


<?php if($tipoUsuario == 'MEDIOS'){?>
	<tr class='fila-encabezado'>
		<td></td>
		<td colspan="8" class="encabezado-rojo">DATOS GENERALES</td>
		<td colspan="4" class="encabezado-verde">DIFUSIÓN</td>
	</tr>
<?php } ?>