<table class="table table-bordered">
    <tr>
        <th class="active">Descripción</th>
        <td ><?php echo $model->descripcion ?></td>
    </tr>

    <tr>
        <th class="active" width="33%">Fecha</th>
        <td><?php echo $model->fecha ?></td>
    </tr>

    <tr>
        <th class="active">Dirección</th>
        <td>
            <div class="row">
                <div class="col-md-3">
                    <b>País:</b> <?php echo $model->idDireccion->idPais->nom_pais; ?>
                </div>

                <?php if($model->idDireccion->id_pais == Yii::$app->params['constantes']['idVenezuela']){ ?>
                    <div class="col-md-3">
                        <b>Estado:</b> <?php echo $model->idDireccion->estado->nom_estado; ?>
                    </div>

                    <div class="col-md-3">
                        <b>Ciudad:</b> <?php echo $model->idDireccion->ciudad->nom_ciudad; ?>
                    </div>

                    <div class="col-md-3">
                        <b>Municipio:</b> <?php echo $model->idDireccion->municipio->nom_municipio; ?>
                    </div>
                <?php } ?>
            </div>

            <div class="row">
                <div class="col-md-4 margen-top-10">
                    <b>Dirección:</b> <?php echo $model->idDireccion->direccion; ?>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <th class="active">Fecha de Confirmación</th>
        <td></td>
    </tr>

    <tr>
        <th class="active">Convocante</th>
        <td><?php echo $model->idConvocante->nombre ?></td>
    </tr>

    <tr>
        <th class="active">Vocero</th>
        <td><?php echo $model->vocero ?></td>
    </tr>

    <tr>
        <th class="active">Personas de Contacto</th>
        <td>
            <?php
            echo "<ul class='list-group'>";
                $convocatoriasPersonas=$model->smnConvocatoriasPersonas;
                foreach ($convocatoriasPersonas as $convocatoriaPersona){
                    $cedula=$convocatoriaPersona->idPersona->cedula;
                    $nombre=$convocatoriaPersona->idPersona->nombre;
                    $apellido=$convocatoriaPersona->idPersona->apellido;
                    echo "<li class='list-group-item'>";
                        echo  "$cedula - $nombre $apellido";
                    echo "</li>";
                }
            echo "</ul>";
            ?>
        </td>
    </tr>

     <tr>
        <th class="active">Observaciones</th>
        <td colspan="2">
            <ul class="list-group">
                <?php
                $observaciones=$model->idObservaciones;
                foreach ($observaciones as $observacion){
                    ?>
                    <li class="list-group-item">
                        <b><?php echo $observacion->tipo_creador ?></b><br>
                        <?php echo $observacion->observacion; ?>
                    </li>
                    <?php   
                }
                ?>
            </ul>
        </td>
    </tr>
</table>