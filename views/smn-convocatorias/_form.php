<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\controllers\SimonController;
use app\models\SmnUser;

$usuario=SimonController::getUsuario(Yii::$app->user->id); //Usuario Conectado
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado
?>

<script type="text/javascript">
    $(document).ready(function(){
        var solicitanteSeleccionado=$("#smnconvocatorias-id_convocante").val();
        
        altoTexto=$("#smnconvocatorias-id_estado").height();
        $("#horas-criticas").height(altoTexto);

        $(document).on("change","#smnconvocatorias-id_pais",function(){
            idPais=$(this).val();
            if(idPais == 1){
                $("#venezuela").show();
            }else{
                $("#venezuela").hide();
            }
        });

        $(document).on("change","#smnconvocatorias-id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades(data);
            }else{
                $("#smnconvocatorias-id_ciudad").html('');
                $("#smnconvocatorias-id_municipio").html('');
            }
        });

        $(document).on("click","#smnconvocatorias-id_convocante",function(){
            ultimaSeleccion=$("#smnconvocatorias-id_convocante option:selected");
        });

        $(document).on("change","#smnconvocatorias-id_convocante",function(){
            idConvocante=$(this).val();
            idConvocatoria=$("#smnconvocatorias-id").val();
            
            if(idConvocatoria != ''){
                validarCambioConvocante();
                return false;
            }

            actualizarContactos();
        });

        //Cuando cambiamos el convocante en el action update preguntamos si realmente lo quiere cambiar.
        //Ya que al cambiarlo se borra automáticamente las personas previamente seleccionadas.
        validarCambioConvocante=function(){
            mensaje="¿Seguro que desea cambiar el convocante? Si su respuesta es afirmativa se borrarán las personas de contacto seleccionadas actualmente.";
            bootbox.confirm(mensaje,function(respuesta){
                if(respuesta){
                    actualizarContactos();
                    eliminarContactos();
                    solicitanteSeleccionado=$("#smnconvocatorias-id_convocante").val();
                }else{
                    $("#smnconvocatorias-id_convocante").val(solicitanteSeleccionado);
                    $("#smnconvocatorias-id_convocante").trigger('chosen:updated');
                }
            });
        }

        //actualiza las personas de contacto cuando se cambia el ente convocante.
        actualizarContactos=function(){
            idConvocante=$("#smnconvocatorias-id_convocante").val();

            if(idConvocante.trim() == ''){
                $("#personas").html('<a class="letra-roja negrita">Por favor seleccione el Convocante para habilitar esta opción.</a>');
                return false;
            }

            data={id_convocante:idConvocante};
            resultado=consultarPHP('/smn-convocatorias/personas-convocatorias-anteriores',data,'html',false);
            $("#personas").html(resultado);
        }


        eliminarContactos=function(){
            idConvocatoria=$("#smnconvocatorias-id").val();
            data={id_convocatoria:idConvocatoria};
            resultado=consultarPHP('/smn-convocatorias/eliminar-contactos',data,'html',false);
        }


        //Se utiliza desde _formRegistro.php cuando se va a registrar o asociar un contacto nuevo.
        $(document).on("click","#contacto-nuevo",function(){
            data={accion:'cedula'};
            resultado=consultarPHP('/smn-convocatorias/contacto-nuevo',data,'html',false);

            bootbox.dialog({
                size: 'large',
                title: 'Contacto Nuevo',
                message: resultado
            });
        });

        //Se utiliza desde contacto-nuevo-cedula.php cuando se va a buscar un contacto nuevo.
        $(document).on("click","#contacto-buscar",function(){
            cedula=$("#contacto-cedula").val();

            if(cedula == ''){
                bootbox.alert("Por favor ingrese el número de cédula del contacto a registrar");
                return false;
            }

            if(cedula.length < 6){
                bootbox.alert("El número de cédula debe contener al menos 6 dígitos");
                return false;
            }

            //Acción que busca si la persona existe en la BD.
            data={cedula:cedula,accion:'validar_cedula'};
            persona=consultarPHP('/smn-convocatorias/contacto-nuevo',data,'json',false);

            if(!persona.id){
                bootbox.confirm("La persona que desea agregar no se encuentra registrada ¿Desea registrarla?",function(result){
                    if(result){
                        bootbox.hideAll();
                        data={'cedula':cedula};
                        formularioContacto=consultarPHP('/smn-personas/crear-contacto-medio',data,'html',false);
                        bootbox.dialog({
                            size: 'large',
                            title: 'Registrar Contacto',
                            message: formularioContacto
                        });
                    }else{
                        $("#contacto-cedula").val('');
                    }
                });
                return false;
            }
            
            idConvocatoria=$("#smnconvocatorias-id").val();

            if(idConvocatoria.trim() == ''){
                //ejecuta esta validación en la acción create
                optionPersona=$("#smn-convocatorias-id_persona").children("option[value='"+persona.id+"']");
            }else{
                //ejecuta esta validación en la acción update
                optionPersona=$(".li-personas[data-id='"+persona.id+"']");//.children("option[value='"+persona.id+"']");
            }

            if(optionPersona.length > 0){
                bootbox.alert("La persona que desea agregar ya se encuentra en la lista");
                $("#contacto-cedula").val('');
                return false;
            }

            bootbox.confirm("La persona que desea agregar ya se encuentra registrada ¿Desea asociarla a esta convocatoria?",function(result){
                if(result){
                    if(idConvocatoria.trim() == ''){
                        insertarPersonaSelect(persona.id,persona.cedula,persona.apellido,persona.nombre);
                    }else{
                        data={id_convocatoria:idConvocatoria, id_persona:persona.id};
                        guardarPersonaContacto=consultarPHP('/smn-convocatorias/guardar-persona-contacto',data,'html',false);
                        if(guardarPersonaContacto){
                            insertarLiPersona(persona.id,persona.cedula,persona.apellido,persona.nombre);
                        }else{
                            alert("Error registrando contacto. Intente de nuevo.");
                        }
                    }
                    bootbox.hideAll();
                }else{
                    $("#contacto-cedula").val('');
                }
            });
        });

        $(document).on("click",".remover-persona",function(){
            idConvocatoria=$("#smnconvocatorias-id").val();
            idConvocante=$("#smnconvocatorias-id_convocante").val();
            idPersona=$(this).parents(".li-personas").data('id');
            fila=$(this).parents(".li-personas");
            cantidadPersonas=$(".li-personas").length;
            if(cantidadPersonas <= 1){
                bootbox.alert("No es posible remover, la Convocatoria debe tener al menos un contacto asociado");
                return false;
            }

            data={id_convocatoria:idConvocatoria, id_persona:idPersona};
            bootbox.confirm("¿Seguro que desea remover este contacto?",function(resultado){
                if(resultado){
                    removerPersonaContacto=consultarPHP('/smn-convocatorias/remover-persona-contacto',data,'html',false);
                    if(removerPersonaContacto){
                        data={id_convocatoria:idConvocatoria, id_convocante:idConvocante};
                        contactosActualizados=consultarPHP('/smn-convocatorias/actualizar-personas-convocantes',data,'html',false);
                        $("#smn-convocatorias-id_persona").html(contactosActualizados);
                        $("#smn-convocatorias-id_persona").trigger('chosen:updated');
                        fila.remove();
                    }else{
                        alert("Error eliminando contacto");
                    }
                }
            });
        });


        $(document).on("click","#boton-registrar-contacto",function(){
            data=$("#contacto-medio").serialize();
            retorno=consultarPHP('/smn-personas/validar-contacto',data,'html',false);

            if(retorno != ''){
                bootbox.alert(retorno);
                return false;
            }

            errores=ejecutarValidacionItems2();//Revisar este método y el blur en la línea 188.
            if(errores > 0){
                bootbox.alert("Por favor validar los teléfonos y correos electrónicos");
                return false;
            }

            bootbox.confirm("¿Seguro que desea registrar esta persona?",function(result){
                if(result){
                    data=$("#contacto-medio").serialize();
                    persona=consultarPHP('/smn-personas/crear-contacto-medio',data,'json',false);
                    bootbox.hideAll();
                    idConvocatoria=$("#smnconvocatorias-id").val();
                    if(idConvocatoria.trim() == ''){
                        insertarPersonaSelect(persona.id,persona.cedula,persona.apellido,persona.nombre);
                    }else{
                        data={id_convocatoria:idConvocatoria, id_persona:persona.id};
                        guardarPersonaContacto=consultarPHP('/smn-convocatorias/guardar-persona-contacto',data,'html',false);
                        if(guardarPersonaContacto){
                            insertarLiPersona(persona.id,persona.cedula,persona.apellido,persona.nombre);
                        }else{
                            alert("Error registrando contacto. Intente de nuevo.");
                        }
                    }
                    bootbox.alert("Contacto registrado con éxito");
                }
            });
        });

        $(document).on("click",".eliminar-archivo",function(){
            idConvArch=$(this).data('id');
            elementoHtml=$(this);
            bootbox.confirm("¿Seguro que desea eliminar el archivo seleccionado?",function(respuesta){
                if(respuesta){
                    data={id_convocatoria_archivo:idConvArch};
                    eliminarArchivo=consultarPHP('/smn-convocatorias/eliminar-archivo',data,'html',false);
                    bootbox.alert("Archivo eliminado con éxito");
                    elementoHtml.parents('.list-group-item').remove();
                }
            });
        });

        //Se utiliza desde _formRegistro.php cuando se va a registrar una observación nueva.
        $(document).on("click","#observacion-nueva",function(){
            data={accion:'formulario'};
            resultado=consultarPHP('/smn-convocatorias/observacion-nueva',data,'html',false);

            bootbox.dialog({
                size: 'large',
                title: 'Observación Nueva',
                message: resultado
            });
        });


        $(document).on("click",".remover-observacion",function(){
            idObservacion=$(this).parents(".li-observaciones").data('id');
            fila=$(this).parents(".li-observaciones");
            data={id_observacion:idObservacion};
            bootbox.confirm("¿Seguro que desea remover esta observación?",function(resultado){
                if(resultado){
                    removerObservacion=consultarPHP('/smn-convocatorias/remover-observacion',data,'html',false);
                    if(removerObservacion){
                        fila.remove();
                    }else{
                        alert("Error eliminando observación");
                    }
                }
            });
        });


        var files;
        $('input[type=file]').on('change', prepareUpload);
        // Grab the files and set them to our variable
        function prepareUpload(event){
            files = event.target.files;
        }

        $(document).on("click",".subir",function(event){
            
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            archivo=$(".archivo").val();
            if(archivo.trim() == ""){
                bootbox.alert("Por favor seleccione el archivo a subir");
                return false;
            }

            extensionesValidas=['jpg','JPG','png','PNG','pdf','PDF'];
            textoExtensiones=extensionesValidas.join(", ");
            if(!validarExtensiones(archivo,extensionesValidas)){
                bootbox.alert("Por favor seleccione un archivo con extensión válida ("+textoExtensiones+")");
                return false;
            }

            var data = new FormData();
            $.each(files, function(key, value){
                data.append(key, value);
            });

            idConvocatoria=$("#smnconvocatorias-id").val();
            data.append('SmnConvocatorias[id_convocatoria]',idConvocatoria);

            $.ajax({
                url: '/smn-convocatorias/subir-archivo',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR){
                    insertarArchivo(data);
                    bootbox.alert("Archivo cargado con éxito");
                    /*if(typeof data.error === 'undefined'){
                        // Success so call function to process the form
                        submitForm(event, data);
                    }else{
                        // Handle errors here
                        console.log('ERRORS: ' + data.error);
                    }*/
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // Handle errors here
                    //console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        });

        
        $(document).on("click",".eliminar-item",function(e){
            $(this).parent().parent().parent().remove();
        });

        $(document).on("change","#id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades2(data);
            }else{
                $("#id_ciudad").html('');
                $("#id_municipio").html('');
                $("#id_parroquia").html('');
            }
        });

        $(document).on("change","#id_municipio",function(){
            idMunicipio=$(this).val().trim();
            if(idMunicipio != ''){
                data={id:idMunicipio},
                parroquias=consultarPHP('/smn-personas/listparroquias',data,'html',false);
                $("#id_parroquia").html(parroquias);
            }else{
                $("#id_parroquia").html('');
            }
        });


        //Método utilizado para validar por ajax los correos y telefonos en el modal /smn-personas/crear-contacto-medio
        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().parent().find('.help-block').html('');
            }
        });

        //Método para capturar el evento submit. Para poder validar antes de enviar.
        $(document).on("submit","#formularioConvocatorias",function(){
            idConvocatoria=$("#smnconvocatorias-id").val();
            formulario=$(this);

            if(idConvocatoria.trim() == ''){ //Acción cuando estamos registrando...
                valPersonas=$("#smn-convocatorias-id_persona").val();

                if(typeof valPersonas == "undefined"){
                    //Sucede cuando no se ha seleccionado el convocante y no existe el select múltiple
                    bootbox.alert("Por favor seleccione el convocante");
                    return false;
                }else{
                    if(valPersonas.length == 0){
                        bootbox.alert("Por favor seleccione al menos una persona de contacto");
                        return false;
                    }
                }

                data=$("#formularioConvocatorias").serialize();
                validacion=consultarPHP('/smn-convocatorias/validar-formulario',data,'html',false);

                if(validacion){
                    bootbox.alert(validacion);
                    return false;
                }

                    
                inputFileBlancos();
                validacion=validarArchivos(".archivo");
                if(!validacion){
                    return false; //detiene el envío cuando se encuentra un error en la validación de los archivos.
                }
                bootbox.confirm("¿Seguro que desea registrar la convocatoria?",function(respuesta){
                    if(respuesta){
                        formulario.submit();
                    }
                });
            }else{ //Acción cuando estamos modificando...
                data=$("#formularioConvocatorias").serialize();
                validacion=consultarPHP('/smn-convocatorias/validar-formulario',data,'html',false);

                if(validacion){
                    bootbox.alert(validacion);
                    return false;
                }

                cantidadPersonas=$(".li-personas").length;
                if(cantidadPersonas < 1){
                    bootbox.alert("Por favor seleccionar al menos una persona de contacto");
                    return false;
                }

                bootbox.confirm("¿Seguro que desea modificar la convocatoria?",function(respuesta){
                    if(respuesta){
                        formulario.submit();
                    }
                });
            }
            return false;
        });

        $(document).on("submit","#formulario-observaciones",function(){
            observacion=$("#smnobservaciones-observacion").val().trim();
            if(observacion == ''){
                bootbox.alert("Por favor ingrese la observación");
                return false;
            }
            data={accion:'registrar',observacion:$("#smnobservaciones-observacion").val(),
                id_convocatoria:$("#smnconvocatorias-id").val()};
            registro=consultarPHP('/smn-convocatorias/observacion-nueva',data,'json',false);
            insertarLiObservacion(registro);
            bootbox.hideAll();
            return false;
        });


        //Método utilizado para el modal la dirección de la convocatoria /smn-convocatorias/_formRegistro.php
        listarMunicipiosCiudades=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#smnconvocatorias-id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#smnconvocatorias-id_ciudad").html(ciudades);
        }


        //Método utilizado para el modal /smn-personas/crear-contacto-medio
        listarMunicipiosCiudades2=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#id_ciudad").html(ciudades);
        }

        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

        ejecutarValidacionItems2=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error .contacto").length;
            return errores;
        }

        insertarPersonaSelect=function(id,cedula,apellido,nombre){
            opcion="<option value="+id+" selected='selected'>"+cedula+" - "+apellido+" "+nombre+"</option>";
            $("#smn-convocatorias-id_persona").append(opcion);
            $("#smn-convocatorias-id_persona").trigger("chosen:updated");
        }

        //Método para insertar personas, funciona en la acción update...
        insertarLiPersona=function(id,cedula,apellido,nombre){
            liPersona="<li class='list-group-item li-personas' data-id='"+id+"'>";
            liPersona+=cedula+" - "+nombre+" "+apellido;
            liPersona+="<span class='pull-right'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-persona'></span></span>";
            liPersona+="</li>";
            $("#lista-personas").append(liPersona);
        }

        insertarLiObservacion=function(observacion){
            liObservacion="<li class='list-group-item li-observaciones' data-id='"+observacion.id_observacion+"'>";
                liObservacion+="<span class='pull-right'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-observacion'></span></span>";
                liObservacion+="<b>"+observacion.tipo_creador+"</b><br>";
                liObservacion+=observacion.observacion;
            liObservacion+="</li>";
            $("#lista-observaciones").append(liObservacion);
        }

        insertarArchivo=function(archivo){
            ruta=archivo.ruta+archivo.archivo+"."+archivo.extension;
            liArchivo="<li class='list-group-item'>";
                liArchivo+=archivo.archivo+"."+archivo.extension;
                liArchivo+="<span class='pull-right'>";
                    liArchivo+="<a href='../"+ruta+"' target='_blank' class='nd black'>";
                        liArchivo+="<span class='glyphicon glyphicon-download f15 cursor'></span>";
                    liArchivo+="</a>&nbsp;";
                    liArchivo+="<span class='glyphicon glyphicon-remove-circle f15 cursor black eliminar-archivo' data-id='"+archivo.id_convocatoria_archivo+"'></span>";
                liArchivo+="</span>";
            liArchivo+="</li>";
            $("#lista-archivos").append(liArchivo);
            $("#filestyle-0").val(''); //Limpia el input real
            $("#filestyle-0").siblings('.bootstrap-filestyle').children('input').val('');//limpia el input que aparece en pantalla que muestra sólo el nombre del archivo sin ruta.
        }

        valoresInputFile=function(clase){
            for(i=0; i<$(clase).length; i++){
                valor=$($(clase)[i]).val();
                $($(clase)[i]).siblings('.bootstrap-filestyle').children('input').val(valor);
            }
        };

        validarArchivos=function(clase){
            archivos=new Array();
            extensionesValidas=['jpg','JPG','png','PNG','pdf','PDF'];
            textoExtensiones=extensionesValidas.join(", ");

            for(i=0; i<$(clase).length; i++){
                valor=$($(clase)[i]).val();
                
                if(valor.trim() != ''){
                    if(!validarExtensiones(valor,extensionesValidas)){
                        bootbox.alert("Por favor seleccione archivos con extensiones válidas ("+textoExtensiones+")");
                        $($(clase)[i]).siblings('.bootstrap-filestyle').children('input').css("background","#F5A9A9");
                        return false;
                    }
                }

                if($(clase).length > 1){
                    if(valor.trim()== ''){
                        bootbox.alert("Por favor seleccionar todos los archivos");
                        $($(clase)[i]).siblings('.bootstrap-filestyle').children('input').css("background","#F5A9A9");
                        return false;
                    }
                }

                if(archivos.indexOf(valor) == -1){
                    archivos.push(valor);
                }else{
                    $($(clase)[i]).siblings('.bootstrap-filestyle').children('input').css("background","#F5A9A9");
                    bootbox.alert("Está intentando subir archivos repetidos.");
                    return false;
                }
            }
            return true;
        }

        validarExtensiones=function(archivo,extensionesValidas){
            var partes;
            partes=archivo.split(".");
            indice=(partes.length)-1;
            extension=partes[indice];
            busquedaExt=extensionesValidas.indexOf(extension);
            if(busquedaExt == -1){
                return false;
            }
            return true;
        }

        inputFileBlancos=function(){
            $('.bootstrap-filestyle').children('input').css("background","white");
        }

        $(document).on("click","#horas-criticas",function(){
            
            fecha=$("#smnconvocatorias-fecha").val();
            idEstado=$("#smnconvocatorias-id_estado").val();
            hora=$("#smnconvocatorias-hora").val();

            if(fecha.trim() == ''){
                bootbox.alert("Por favor seleccione la fecha de convocatoria");
                return false;
            }

            if(hora == ''){
                bootbox.alert("Por favor seleccione la hora de convocatoria");
                return false;
            }

            data={fecha:fecha,hora:hora,id_estado:idEstado};
            respuesta=consultarPHP('/smn-convocatorias/horas-criticas',data,'html',false);

            bootbox.dialog({
                size: 'large',
                title: 'Convocatorias',
                message: respuesta
            });
        });
              
        
        $.noConflict();
        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            //minTime: '10',
            //maxTime: '6:00pm',
            //defaultTime: '00',
            startTime: '00:00',
            dynamic: true,
            dropdown: true,
            scrollbar: false
        });


        $(document).on("click",".agregar-item",function(){
            $(".archivo").filestyle('destroy');
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('input').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');

            $(".archivo").filestyle({ //Le mejora el estilo a los input file cuando se agrega uno nuevo dinamicamente.
                buttonText : ' Examinar',
                buttonName : 'btn-primary'
            });

            valoresInputFile('.archivo');
            
            $("span[class='buttonText']:contains('Examinar')").parent().css('border-radius','0px');//quita el borde redondo al examinar.
        });


        $('.archivo').filestyle({  //Le mejora el estilo a los input file.
            buttonText : ' Examinar',
            buttonName : 'btn-primary'
        });

            
        $("span[class='buttonText']:contains('Examinar')").parent().css('border-radius','0px');//quita el borde redondo al examinar.
    });
</script>

<style type="text/css">
    .form-group input[type="checkbox"][class="roles"]::before{
        background:#000000;
    }
</style>

<div class="smn-convocatorias-form">

    <?php
    $form = ActiveForm::begin(['id'=>'formularioConvocatorias',"options" => ["enctype" => "multipart/form-data"]]); 
    $form->enableClientValidation=false;
    $form->enableAjaxValidation=false;
    
    echo $form->field($model,'id')->hiddenInput()->label(false);

    ?>
        <div class="alert alert-info alert-dismissible">
            <strong>Los campos con <a class="requerido">*</a> son requeridos.</strong>
        </div>

        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
            <?php if(Yii::$app->controller->action->id == 'update' && $tipoUsuario != 'ENTE ADSCRITO'){?>
                <li><a data-toggle="tab" href="#forma-difusion">FORMA DE DIFUSIÓN</a></li>
            <?php } ?>
        </ul>

        <div class="tab-content">
            <div id="datos-registro" class="tab-pane fade in active">
                <br>
                <?php
                if(!isset($personas)){
                    $personas="";
                }
                echo $this->render('_formRegistro',compact('model','form','listestructuragobierno','listPaises','listEstados','listCiudades','listMunicipios','listSolicitantes','listTendencias','listTiposMediosSolicitados','personas','listCoconvocantes')); ?>
            </div>

            <?php if(Yii::$app->controller->action->id == 'update' && $tipoUsuario != 'ENTE ADSCRITO'){?>
                <div id="forma-difusion" class="tab-pane fade">
                    <?php echo $this->render('_formDifusion',compact('model','form','listTiposDifusion')); ?>
                </div>
            <?php } ?>
        </div>

        <div class="form-group">
            <br>
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>