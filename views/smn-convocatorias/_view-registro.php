<?php
use app\controllers\SimonController;
use app\models\SmnUser;

$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id);
?>

<table class="table table-bordered">
    <tr>
        <th colspan="3" class="active">Descripción</th>
    </tr>
    
    <tr>
        <td colspan="3"><?php echo $model->descripcion ?></td>
    </tr>

    <?php if($tipoUsuario == 'MIPPCI'){ ?>
        <tr>
            <th class="active" width="33%">Fecha</th>
            <th class="active" width="33%">Hora</th>
            <th class="active" width="33%">Revolución</th>
        </tr>

        <tr>
            <td><?php echo $model->fecha ?></td>
            <td><?php echo date("h:i a",strtotime($model->hora)) ?></td>
            <td><?php echo isset($model->idEstructuraGobierno->nombre)?$model->idEstructuraGobierno->nombre:''; ?></td>
        </tr>
    <?php }else{ ?>
        <tr>
            <th class="active" width="33%">Fecha</th>
            <th class="active" width="33%">Hora</th>
            <th class="active" width="33%"></th>
        </tr>

        <tr>
            <td><?php echo $model->fecha ?></td>
            <td><?php echo date("h:i a",strtotime($model->hora)) ?></td>
            <td></td>
        </tr>
    <?php } ?>

    <tr>
        <th colspan="3" class="active">Dirección</th>
    </tr>

    <tr>
        <td colspan="3">
            <div class="row">
                <div class="col-md-3">
                    <b>País:</b> <?php echo $model->idDireccion->idPais->nom_pais; ?>
                </div>

                <?php if($model->idDireccion->id_pais == Yii::$app->params['constantes']['idVenezuela']){ ?>
                    <div class="col-md-3">
                        <b>Estado:</b> <?php echo $model->idDireccion->estado->nom_estado; ?>
                    </div>

                    <div class="col-md-3">
                        <b>Ciudad:</b> <?php echo $model->idDireccion->ciudad->nom_ciudad; ?>
                    </div>

                    <div class="col-md-3">
                        <b>Municipio:</b> <?php echo $model->idDireccion->municipio->nom_municipio; ?>
                    </div>
                <?php } ?>
            </div>

            <div class="row">
                <div class="col-md-4 margen-top-10">
                    <b>Dirección:</b> <?php echo $model->idDireccion->direccion; ?>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <th class="active">Convocante</th>
        <th class="active">Coconvocante</th>
        <th class="active">Vocero</th>
    </tr>

    <tr>
        <td><?php echo $model->idConvocante->nombre ?></td>
        <td><?php echo $model->idCoconvocante->nombre ?></td>
        <td><?php echo $model->vocero ?></td>
    </tr>

    <tr>
        <th class="active">Cargo del Vocero</th>
        <th class="active">Señal Solicitada</th>
        <th class="active">Estatus</th>
    </tr>

    <tr>
        <td><?php echo $model->vocero_rol ?></td>
        <td><?php echo $model->senal_solicitada?"<div class='letra-roja negrita'>En Vivo</div>":''; ?></td>
        <td><?php echo $model->getEstatusLetra() ?></td>
    </tr>

    <tr>
        <th class="active">Personas de Contacto</th>
        <th class="active">Medios Solicitados</th>
        <th class="active">Archivos</th>
    </tr>

    <tr>
        <td>
            <?php
            echo "<ul class='list-group'>";
                $convocatoriasPersonas=$model->smnConvocatoriasPersonas;
                foreach ($convocatoriasPersonas as $convocatoriaPersona){
                    $cedula=$convocatoriaPersona->idPersona->cedula;
                    $nombre=$convocatoriaPersona->idPersona->nombre;
                    $apellido=$convocatoriaPersona->idPersona->apellido;
                    echo "<li class='list-group-item'>";
                        echo  "$cedula - $nombre $apellido";
                    echo "</li>";
                }
            echo "</ul>";
            ?>
        </td>
        <td>
            <?php
            $medios=explode(";",$model->medios_solicitados);
            echo "<ul class='list-group'>";
                foreach ($listTiposMediosSolicitados as $key=>$tipoMedioSolicitado){
                    if(in_array($key,$medios)){
                        echo "<li class='list-group-item'>";
                            echo "$tipoMedioSolicitado";
                        echo "</li>";
                    }
                }
            echo "</ul>";
            ?>
        </td>

        <td>
            <ul class="list-group">
                <?php
                $archivos=$model->convocatoriasArchivos;
                foreach($archivos as $archivo){
                    $ruta=Yii::$app->params['convocatorias']['rutaArchivos'].$archivo->archivo;
                    ?>
                    <li class="list-group-item">
                        <?php echo $archivo->archivo; ?>
                        <span class="pull-right">
                            <a href="../<?php echo $ruta?>" target="_blank" class="nd black">
                                <span class="glyphicon glyphicon-download f15"></span>
                            </a>
                        </span>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </td>
    </tr>

    <tr>
        <th colspan="3" class="active">Observaciones</th>
    </tr>

     <tr>
        <td colspan="3">
            <ul class="list-group">
                <?php
                $observaciones=$model->idObservaciones;
                foreach ($observaciones as $observacion){
                    ?>
                    <li class="list-group-item">
                        <b><?php echo $observacion->tipo_creador ?></b><br>
                        <?php echo $observacion->observacion; ?>
                    </li>
                    <?php   
                }
                ?>
            </ul>
        </td>
    </tr>
</table>