<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = "Confirmar Asistencia Convocatoria Nro. ".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Confirmar Asistencia';
?>

<script type="text/javascript">
    $(document).ready(function(){

        $.noConflict();

        $(document).on("click","#btn-enviar",function(){
            asistencia=$("#smnmediosconvocatorias-asistencia").val();
            if(asistencia == 1){
                if($(".li-personas").length == 0){
                    bootbox.alert("Debe seleccionar al menos una persona que asista a la convocatoria");
                    return false;
                }
            }else{
                if($("#smnmediosconvocatorias-observaciones").val().trim() == ''){
                    bootbox.alert("Debe ingresar el motivo de la ausencia");
                    return false;
                }
            }

            $formulario=$("#convocarMedios");

            bootbox.confirm("¿Seguro que desea realizar esta acción?",function(respuesta){
                if(respuesta){
                    $formulario.submit();                    
                }
            });

            return false;
        });

        $(".chosen-select").chosen();

        $(document).on("click",".remover-trabajador",function(){
            trabajador=$(this).parents('li');
            idConvocatoria=$(trabajador).data('id_convocatoria');
            idPersona=$(trabajador).data('id_persona');
            idMedio=$(trabajador).data('id_medio');
            bootbox.confirm("¿Seguro que desea remover este trabajador?",function(respuesta){
                if(respuesta){
                    data={id_convocatoria:idConvocatoria, id_persona:idPersona,id_medio:idMedio};
                    removerTrabajador=consultarPHP('/smn-convocatorias/remover-convocatoria-trabajador',data,'html',false);
                    if(removerTrabajador){
                        data={id_convocatoria:idConvocatoria, id_medio:idMedio};
                        trabajadoresActualizados=consultarPHP('/smn-convocatorias/actualizar-trabajadores',data,'html',false);
                        $("#smn-medios-trabajadores-id_persona").html(trabajadoresActualizados);
                        $("#smn-medios-trabajadores-id_persona").trigger('chosen:updated');
                        trabajador.remove();
                    }else{
                        alert("Error eliminando contacto");
                    }
                }
            });
        });

        $("#smn-medios-trabajadores-id_persona").change(function(){
            idPersona=$("#smn-medios-trabajadores-id_persona").val()[0];
            idTipoTrabajador=$("#smn-medios-trabajadores-id_persona option[value="+idPersona+"]").data('id_tipo_trabajador');
            idConvocatoria=$("#smnconvocatorias-id").val();
            idMedio=$("#smnconvocatorias-id_medio").val();

            data={id_convocatoria:idConvocatoria, id_persona:idPersona, id_tipo_trabajador:idTipoTrabajador, id_medio:idMedio};
            guardarConvocatoriaTrabajador=consultarPHP('/smn-convocatorias/guardar-convocatoria-trabajador',data,'html',false);
            if(guardarConvocatoriaTrabajador){
                //eliminar la opción seleccionada...
                $("#smn-medios-trabajadores-id_persona option[value="+idPersona+"]").remove();
                $("#smn-medios-trabajadores-id_persona").trigger('chosen:updated');
                insertarLiTrabajador(idConvocatoria,idPersona,idMedio);
            }else{
                alert("Error registrando trabajador. Intente de nuevo.");
            }
        });


        $(document).on("change","#smnmediosconvocatorias-asistencia",function(){
            validarOpcionales();
        });


        insertarLiTrabajador=function(idConvocatoria,idPersona,idMedio){
            data={id_convocatoria:idConvocatoria, id_persona:idPersona, id_medio:idMedio};
            trabajador=consultarPHP('/smn-convocatorias/consultar-convocatoria-trabajador',data,'html',false);
            
            liTrabajador="<li class='list-group-item li-personas' data-id_convocatoria='"+idConvocatoria+"' data-id_persona='"+idPersona+"' data-id_medio='"+idMedio+"'>";
                liTrabajador+=trabajador;
            liTrabajador+="<span class='pull-right remover-personal-asistente'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-trabajador'></span></span>";
            liTrabajador+="</li>";
            
            $("#lista-trabajadores").prepend(liTrabajador);
        }


        validarOpcionales=function(){
            valor=$("#smnmediosconvocatorias-asistencia").val();
            if(valor == "1"){
                $(".opcionales").show();
            }else{
                $(".opcionales").hide();
            }
        }

        validarOpcionales();

    });
</script>

<div class="smn-convocatorias-convocar">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    /*echo "<pre>";
    print_r($modelMedConv);
    echo "</pre>";*/
    $form = ActiveForm::begin(['id'=>'convocarMedios',"options" => ["enctype" => "multipart/form-data"]]); 
    $form->enableClientValidation=false;
    $form->enableAjaxValidation=true;
    
    echo $form->field($model,'id')->hiddenInput()->label(false);
    echo $form->field($model,'id_medio')->hiddenInput()->label(false);
    echo $form->field($modelMedConv,'id_convocatoria')->hiddenInput()->label(false);
    echo $form->field($modelMedConv,'id_medio')->hiddenInput()->label(false);
    ?>

    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#datos-asistencia">DATOS DE ASISTENCIA</a></li>
        <li><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
        <!--<li><a data-toggle="tab" href="#datos-confirmacion">DATOS DE CONFIRMACIÓN</a></li>
        <li><a data-toggle="tab" href="#forma-difusion">FORMA DE DIFUSIÓN</a></li>-->
    </ul>

    <div class="tab-content">
    	<div id="datos-asistencia" class="tab-pane fade in active">
    		<br>
    		<?php echo $this->render('_form-confirmar-medios',compact('form','model','modelMedConv','listSenalAsignada','mediosTrabajadores','trabajadoresElegidos')) ?>
    	</div>
        
        <div id="datos-registro" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-registro-medios',compact('model','listTiposMediosSolicitados')); ?>
        </div>

        <!--<div id="datos-confirmacion" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-confirmacion',compact('model')); ?>
        </div>

        <div id="forma-difusion" class="tab-pane fade">
        	<br>
            <?php echo $this->render('_view-forma-difusion',compact('model')); ?>
        </div>-->
    </div>

    <div class="form-group">
        <?= Html::submitButton('Enviar',['class'=>'btn btn-success','id'=>'btn-enviar']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>