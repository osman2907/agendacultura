<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($modelMedConv, 'asistencia')->dropDownList(['1'=>'Si','0'=>'No'],[])->label("¿Asistirá a la convocatoria?") ?>
                <div id="mensaje-asistencia" class="negrita letra-roja">
                    En caso de no asistir debe especificar la razón.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="opcionales">
                <div class="col-md-12">
                    <?= $form->field($modelMedConv, 'id_senal')->dropDownList($listSenalAsignada,['prompt'=>'SELECCIONE']) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($modelMedConv, 'observaciones')->textarea(['rows'=>'4']) ?>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="opcionales">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Personal Asistente</legend>
                
                <select id="smn-medios-trabajadores-id_persona" data-placeholder="Seleccione el personal asistente" class="chosen-select form-control" multiple="multiple">
                    <?php
                    foreach ($mediosTrabajadores as $medioTrabajador){
                        $idPersona=$medioTrabajador->idPersona->id;
                        $datosTrabajador=$medioTrabajador->datosTrabajador;
                        $idTipoTrabajador=$medioTrabajador->id_tipo_trabajador;
                        if(!in_array($idPersona,$trabajadoresElegidos)){
                            echo "<option value='$idPersona' data-id_tipo_trabajador='$idTipoTrabajador'>$datosTrabajador</option>";
                        }
                    }
                    ?>
                </select>
                <br><br>
                <div>
                    <ul id="lista-trabajadores">
                        <?php
                        $trabajadores=$model->smnConvocatoriasTrabajadores;
                        $idConvocatoria=$model->id;
                        $idMedio=$model->id_medio;
                        foreach($trabajadores as $convocatoriaTrabajador){
                            $idPersona=$convocatoriaTrabajador->id_trabajador;
                            echo "<li class='list-group-item li-personas' data-id_convocatoria='$idConvocatoria' data-id_persona='$idPersona' data-id_medio='$idMedio'>";
                                echo \Yii::$app->view->render('_consultar-convocatoria-trabajador',compact('convocatoriaTrabajador'));
                                echo "<span class='pull-right remover-personal-asistente'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-trabajador'></span></span>";
                            echo "</li>";
                        }
                        ?>
                    </ul>

                    <?php
                    /*echo "<pre>";
                    print_r($trabajadores);
                    echo "</pre>";*/
                    ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
