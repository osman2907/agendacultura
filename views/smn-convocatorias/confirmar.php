<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click","#btn-confirmar",function(){
            formulario=$("#confirmarConvocatorias");
            bootbox.confirm("¿Seguro que desea realizar esta acción?",function(respuesta){
                if(respuesta){
                    formulario.submit();
                }
            });
            return false;
        });
    });
</script>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = "Confirmar Convocatoria - Nro: ".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Confirmar';
?>
<div class="smn-convocatorias-confirmar">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $form = ActiveForm::begin(['id'=>'confirmarConvocatorias',"options" => ["enctype" => "multipart/form-data"]]); 
    $form->enableClientValidation=false;
    $form->enableAjaxValidation=true;
    
    echo $form->field($model,'id')->hiddenInput()->label(false);
    ?>

    <div class="alert alert-info alert-dismissible">
        <strong>Los campos con <a class="requerido">*</a> son requeridos.</strong>
    </div>

    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#datos-confirmacion">DATOS DE CONFIRMACIÓN</a></li>
        <li><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
        <li><a data-toggle="tab" href="#forma-difusion">FORMA DE DIFUSIÓN</a></li>
    </ul>

    <div class="tab-content">
    	<div id="datos-confirmacion" class="tab-pane fade in active">
    		<br>
    		<?= $this->render('_form-confirmar',compact('form','model','listSenal','listPrioridad','listSector','listEstatus','listestructuragobierno')) ?>
    	</div>
        
        <div id="datos-registro" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-registro',compact('model','listTiposMediosSolicitados')); ?>
        </div>

        <div id="forma-difusion" class="tab-pane fade">
        	<br>
            <?php echo $this->render('_view-forma-difusion',compact('model')); ?>
        </div>
    </div>

    <div class="form-group">
        <br>
        <?= Html::submitButton('Confirmar',['class'=>'btn btn-success','id'=>'btn-confirmar']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>