<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatoriasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smn-convocatorias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'id_prioridad') ?>

    <?= $form->field($model, 'id_sector') ?>

    <?php // echo $form->field($model, 'id_direccion') ?>

    <?php // echo $form->field($model, 'id_convocante') ?>

    <?php // echo $form->field($model, 'id_linea_discursiva') ?>

    <?php // echo $form->field($model, 'vocero') ?>

    <?php // echo $form->field($model, 'vocero_rol') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'id_estatus') ?>

    <?php // echo $form->field($model, 'hora') ?>

    <?php // echo $form->field($model, 'id_senal') ?>

    <?php // echo $form->field($model, 'medios_solicitados') ?>

    <?php // echo $form->field($model, 'id_sugerencia') ?>

    <?php // echo $form->field($model, 'id_coconvocante') ?>

    <?php // echo $form->field($model, 'senal_solicitada') ?>

    <?php // echo $form->field($model, 'id_estructura_gobierno') ?>

    <?php // echo $form->field($model, 'id_des_tendencia') ?>

    <?php // echo $form->field($model, 'agenda_comunicacional')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
