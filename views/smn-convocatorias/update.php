<script type="text/javascript">
	$(document).ready(function($){
		//$.noConflict();
		$(".chosen-select").chosen();

		$("#smn-convocatorias-id_persona").change(function(){
			bootbox.confirm("¿Seguro desea asignar esta persona a la convocatoria?",function(result){
				if(result){
					valor=$("#smn-convocatorias-id_persona").val()[0];
					persona=$("#smn-convocatorias-id_persona option[value="+valor+"]").html();
					idConvocatoria=$("#smnconvocatorias-id").val();

					data={id_convocatoria:idConvocatoria, id_persona:valor};
                    guardarPersonaContacto=consultarPHP('/smn-convocatorias/guardar-persona-contacto',data,'html',false);
                    if(guardarPersonaContacto){
                    	//eliminar la opción seleccionada...
						$("#smn-convocatorias-id_persona option[value="+valor+"]").remove();
						$("#smn-convocatorias-id_persona").trigger('chosen:updated');
						insertarLiPersonaUpdate(valor,persona);
                    }else{
                        alert("Error registrando contacto. Intente de nuevo.");
                    }
				}
			});
		});

		insertarLiPersonaUpdate=function(id,persona){
            liPersona="<li class='list-group-item li-personas' data-id='"+id+"'>";
            liPersona+=persona;
            liPersona+="<span class='pull-right'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-persona'></span></span>";
            liPersona+="</li>";
            $("#lista-personas").append(liPersona);
        }
	});
</script>

<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = 'Modificar Convocatorias';
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="smn-convocatorias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model','listestructuragobierno','listPaises','listEstados','listCiudades','listMunicipios','listSolicitantes','listTendencias','listTiposMediosSolicitados','listTiposDifusion','personas','listCoconvocantes')); ?>

</div>
