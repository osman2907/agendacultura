<script type="text/javascript">
 	$(function (){
	    Highcharts.chart('container', {
	        chart: {
	            type: 'line'//,
	            /*options3d: {
	                enabled: true,
	                alpha: 10,
	                beta: 25,
	                depth: 70
	            }*/
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	        plotOptions: {
	            line:{
	                dataLabels:{
	                    enabled: true
	                },
	                //enableMouseTracking: false
	            }
	        },
	        xAxis: {
	            categories: [
	            	<?php
	            	foreach ($horasGrafico as $horaGraf){
	            		echo "'$horaGraf'";?>,<?php
	            	}
	            	?>
	            ]
	        },
	        yAxis: {
	            title: {
	                text: null
	            }
	        },
	        series: [{
	            name: 'CANTIDAD CONVOCATORIAS',
	            data: [
	            	<?php
	            	foreach ($cantidadGrafico as $cantidadGraf){
	            		echo $cantidadGraf;?>,<?php
	            	}
	            	?>
	            ]
	        }]
	    });
	});
</script>

<h4 class="centrar negrita">Cantidad de Convocatorias para el <?php echo date("d/m/Y",strtotime($fecha)) ?> <?php echo empty($modelEstado)?'en todo el Territorio Nacional':'en el estado '.$modelEstado->nom_estado; ?></h4>

<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered">
			<tr>
				<th class="active" width="40%">Hora</th>
				<td><?php echo $hora ?></td>
			</tr>
			<tr>
				<th class="active">Cantidad de Convocatorias</th>
				<td><?php echo count($modelConvocatorias) ?></td>
			</tr>
		</table>
		<?php if(count($horasGrafico) > 0){?>
			<div id="container" style="width: 90%; margin: auto;"></div><br>
			<div class="negrita">
				En esta gráfica se muestran el número de convocatorias confirmadas según su hora asignada. Recuerde que cuenta con la opción de seleccionar un estado en particular, de no ser así vera el total de convocatorias confirmadas a nivel nacional.
			</div>
		<?php }else{ ?>
			<div class="alert alert-warning negrita centrar">No hay convocatorias registradas con los parámetros seleccionados</div>
		<?php } ?>
	</div>
</div>