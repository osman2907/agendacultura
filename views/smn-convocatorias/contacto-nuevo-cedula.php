<?php
use yii\helpers\Html;
?>

<div class="row">
	<div class="col-md-1 margen-top">
		<b>Cédula:</b>
	</div>
	
	<div class="col-md-3">
		<?php echo Html::input("text","contacto_cedula","",['id'=>'contacto-cedula','class'=>'form-control solo-numero']); ?>
	</div>

	<div class="col-md-1">
		<b><?php echo Html::button("Buscar",['id'=>'contacto-buscar','class'=>'btn btn-primary']); ?></b>
	</div>
</div>