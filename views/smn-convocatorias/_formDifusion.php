<h4>Seleccione las Formas de Difusión necesarias:</h4>
<div class="container-fluid">
<?php
$formasDifusion=$model->smnConvocatoriaPropagacions;
$tiposDifusion=[];
foreach ($formasDifusion as $formaDifusion){
	$tiposDifusion[]=$formaDifusion->id_propagacion;
}

foreach($listTiposDifusion as $key => $value){
    ?>
    <div class="row">
    	<div class="col-xs-6">
		    <div class="[ form-group ]">
		        <input type="checkbox" name="SmnConvocatorias[formas_difusion][<?php echo $key?>]" id="smnconvocatorias-formas_difusion_<?php echo $key?>" autocomplete="off" class="roles" value="<?php echo $key?>" <?php echo (in_array($key,$tiposDifusion))?'checked':'' ?>/>
		        <div class="[ btn-group ] ancho-total">
		            <label for="smnconvocatorias-formas_difusion_<?php echo $key?>" class="[ btn btn-primary ]">
		                <span class="[ glyphicon glyphicon-ok ]"></span>
		                <span></span>
		            </label>
		            <label for="fancy-checkbox-primary" class="[ btn btn-default active ]" style="width:80%">
		                <?php echo $value ?>
		            </label>
		        </div>
		    </div>
	    </div>
	</div>
    <?php
}

/*echo "<pre>";
print_r($model->smnConvocatoriaPropagacions);
echo "</pre>";*/
?>
</div>