<?php
use yii\helpers\Html;

$cedula=$convocatoriaTrabajador->idTrabajador->cedula;
$nombreCompleto=strtoupper($convocatoriaTrabajador->idTrabajador->nombreCompleto);
$tipoTrabajador=strtoupper($convocatoriaTrabajador->idTipoTrabajador->nom_descripcion);
?>

<table>
	<tr>
		<td><?php echo Html::img('@web/images/usuario.jpg',array('class'=>'icono-personal-asistente'))?></td>
		<td class="celda-personal-asistente">
			<b>Cédula: </b><?php echo $cedula; ?><br>
			<b>Nombre: </b><?php echo $nombreCompleto ?><br>
			<b>Rol: </b><?php echo $tipoTrabajador ?>
		</td>
	</tr>
</table>