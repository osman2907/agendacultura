<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = 'Crear Convocatoria';
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-convocatorias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model','listestructuragobierno','listPaises','listEstados','listCiudades','listMunicipios','listSolicitantes','listTendencias','listTiposMediosSolicitados','listCoconvocantes')) ?>
    
</div>
