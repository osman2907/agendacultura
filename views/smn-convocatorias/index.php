<script>
    $(document).ready(function(){

        //bootbox.classes.add('ancho-modal');
        //Función que coloca secciones en la parte superior de la grilla.
        encabezado=function(){
            cabecera=$("#tabla-convocatorias thead");
            data={};
            respuesta=consultarPHP('/smn-convocatorias/encabezado',{},'html',false);
            cabecera.prepend(respuesta);
        };
        encabezado(); //Llamado de la función encabezado.


        $(document).on("click",".accion",function(){
            $(this).parents('.btn-group').removeClass('open');
            accion=$(this).data('accion');
            id=$(this).data('id');
            
            if(accion == 'consultar'){
                titulo="Consultar Convocatoria";
                respuesta=consultarPHP('/smn-convocatorias/view?id='+id,{},'html',false);
            }

            if(accion == 'modificar'){
                titulo="Modificar Convocatoria";
                respuesta=consultarPHP('/smn-convocatorias/update?id='+id,{},'html',false);
            }

            bootbox.dialog({
                title: titulo,
                message: respuesta,
                clasName: 'ancho-modal'
            });
            $(".modal-dialog").css("width","80%");

            return false;
        });


        //Método para borrar los datos de los filtros de fechas
        $(document).on("click","#limpiar-fechas",function(){
            $("#smnconvocatoriassearch-fecha_desde").val('');
            $("#smnconvocatoriassearch-fecha_hasta").val('');
        });


        //Este método frena el envío del formulario...
        $(document).on("submit","#formularioFiltros",function(){
            fechaDesde=$("#smnconvocatoriassearch-fecha_desde").val().trim();
            fechaHasta=$("#smnconvocatoriassearch-fecha_hasta").val().trim();

            if(fechaDesde != "" || fechaHasta != ""){
                if(fechaDesde == ""){
                    bootbox.alert("Usted seleccionó la <b>Fecha Hasta</b>, le falta seleccionar la <b>Fecha Desde</b>");
                    return false;
                }

                if(fechaHasta == ""){
                    bootbox.alert("Usted seleccionó la <b>Fecha Desde</b>, le falta seleccionar la <b>Fecha Hasta</b>");
                    return false;
                }

                fechaDesde=fechaDesde.split("/");
                fechaDesde=fechaDesde[2]+"-"+fechaDesde[1]+"-"+fechaDesde[0];
                fechaHasta=fechaHasta.split("/");
                fechaHasta=fechaHasta[2]+"-"+fechaHasta[1]+"-"+fechaHasta[0];

                if(fechaDesde > fechaHasta){
                    bootbox.alert("La <b>Fecha Desde</b> debe ser menor o igual a la <b>Fecha Hasta</b>");
                    return false;
                }
            }

        });


        <?php
        if(isset($_GET['id'])){
            ?>
            idConvocatoria=<?php echo $_GET['id'] ?>;
            elementoConsultar=$("a[data-id="+idConvocatoria+"][data-accion=consultar]");
            elementoConsultar.trigger('click');
            <?php
        }
        ?>

        $.noConflict();
        if(!$('#filter-form').hasClass('oculto')){
            $(".chosen-select").chosen();
        }

        $(document).on("click","#btn-filtros",function(){
            $("#filter-form").fadeIn('fast');
            $("#btn-filtros").fadeOut('fast');
            $(".chosen-select").chosen();
        });


        $(document).on("click","#btn-filtros-ocultar",function(){
            $("#filter-form").fadeOut('fast');
            $("#btn-filtros").fadeIn('fast');
        });


        $(document).on("click",".convocado",function(){
            idMedio=$(this).children('.id-medio').html();
            idConvocatoria=$(this).children('.id-convocatoria').html();
            data={id_medio:idMedio, id_convocatoria:idConvocatoria};
            respuesta=consultarPHP('/smn-convocatorias/medio-convocado',data,'html',false);

            bootbox.dialog({
                title: 'Medios Convocados',
                message: respuesta,
                clasName: 'ancho-modal'
            });
            $(".modal-dialog").css("width","800px");
        });
    });
</script>


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\jui\DatePicker;
use yii\grid\GridView;
//use yii\widgets\Pjax;
use app\controllers\SimonController;
use app\models\SmnUser;
use webvimark\modules\UserManagement\models\User;


$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id); //Tipo de Usuario Conectado

$this->title = 'Convocatorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-convocatorias-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if(isset($_GET['SmnConvocatoriasSearch']['id'])){
        $mostrarBtn="oculto";
        $mostrarForm="";
    }else{
        $mostrarBtn="";
        $mostrarForm="oculto";
    }
    ?>

    <p>
        <?php
        if(User::canRoute("/smn-convocatorias/create")){
            echo Html::a('Nueva Convocatoria', ['create'], ['class' => 'btn btn-success']);
        }
        echo "&nbsp;&nbsp;";
        echo Html::button('Filtros de Búsqueda',['id'=>'btn-filtros','class'=>"btn btn-info $mostrarBtn"]);
        ?>
    </p>

    <div id="filter-form" class="panel panel-default <?php echo $mostrarForm ?>">
        <div class="panel-heading">
            <div class="row">
                <div class='col-xs-9'><h3 class="panel-title">Filtros de Búsqueda</h3></div>
                <div class='col-xs-3 derecha'><?php echo Html::button('Ocultar Filtros',['id'=>'btn-filtros-ocultar','class'=>'btn btn-xs btn-danger']); ?></div>
            </div>
        </div>
        <div class="panel-body">
            <?php 
            $form = ActiveForm::begin(['id'=>'formularioFiltros','method'=>'get','action'=>'index']); 
            $form->enableClientValidation=false;
            $form->enableAjaxValidation=false;
            ?>
                <div class="row">
                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id')->textInput(['class'=>'solo-numero form-control'])->label("Nro. Convocatoria");?>  
                    </div>

                    <?php if($tipoUsuario != 'COMUNICACIONES'){ ?>
                        <div class="col-xs-3">
                            <?php echo $form->field($searchModel,'id_medio')->dropDownList($listMediosConvocatorias,['prompt'=>'SELECCIONE','class'=>'chosen-select'])
                                ->label("Medio Convocado");?>
                        </div>
                    <?php } ?>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_medio_solicitado')->dropDownList($listTiposMediosSolicitados,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Medio Solicitado");?>  
                    </div>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_propagacion')->dropDownList($listTiposDifusion,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Propagación");?>  
                    </div>
                
                    <?php if($tipoUsuario != 'ENTE ADSCRITO'){ ?>
                        <div class="col-xs-3">
                            <?php echo $form->field($searchModel,'id_convocante')->dropDownList($listSolicitantes,['prompt'=>'SELECCIONE','class'=>'chosen-select','readonly'=>'readonly'])->label("Convocante");?>  
                        </div>
                    <?php } ?>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_estatus')->dropDownList($listEstatus,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Estatus");?>  
                    </div>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_estado')->dropDownList($listEstados,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Estado");?>  
                    </div>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_prioridad')->dropDownList($listPrioridad,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Nivel de Vocería");?>  
                    </div>
                
                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'id_sector')->dropDownList($listSector,['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Sector");?>
                    </div>

                    <div class="col-xs-3">
                        <?php echo $form->field($searchModel,'locacion')->dropDownList(['1'=>'NACIONAL','2'=>'INTERNACIONAL'],['prompt'=>'SELECCIONE','class'=>'chosen-select'])->label("Locación de la Convocatoria");?>
                    </div>

                    <div class="col-xs-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border2">Fecha de la Convocatoria</legend>
                            <div class="col-xs-4">
                                <?php 
                                $anio=date("Y");
                                echo $form->field($searchModel, 'fecha_desde')->widget(DatePicker::classname(),[
                                    'language' => 'es',
                                    'dateFormat' => 'dd/MM/yyyy',
                                    'clientOptions'=>[
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange'=> "1900:$anio"
                                    ]
                                ])->textInput(['readonly'=>'readonly']);
                                ?>
                            </div>

                            <div class="col-xs-4">
                                <?php 
                                $anio=date("Y");
                                echo $form->field($searchModel, 'fecha_hasta')->widget(DatePicker::classname(),[
                                    'language' => 'es',
                                    'dateFormat' => 'dd/MM/yyyy',
                                    'clientOptions'=>[
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange'=> "1900:$anio"
                                    ]
                                ])->textInput(['readonly'=>'readonly']);
                                ?>
                            </div>

                            <div class="col-xs-4 margen-etiqueta">
                                <?= Html::button('Limpiar Fechas', ['id'=>'limpiar-fechas','class'=>'btn btn-warning']) ?>
                            </div>
                        </fieldset>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12 derecha">
                        <?= Html::button('Limpiar', ['type'=>'reset','class'=>'btn btn-warning']) ?>
                        <?= Html::submitButton('Buscar', ['class'=>'btn btn-success']) ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php //Pjax::begin(); ?>

    <?php
    if($tipoUsuario == 'MIPPCI' || $tipoUsuario == 'ENTE ADSCRITO' || $tipoUsuario == 'COMUNICACIONES'){
        $columnas=[
            //DATOS GENERALES***************************************
            [
                'label'=>'Acciones',
                'format'=>'raw',
                'value'=>function($objeto,$clave,$index,$widget){
                    //return Html::a('My Action', ['my-action', 'id'=>2]);
                    //return Html::url('smnConvocatorias/botonesConvocatorias');
                    return $objeto->botonesConvocatorias;
                    
                }
            ],

            [
                'attribute'=>'id',
                'label'=>'Nro. Conv'
            ],
            [
                'attribute'=>'idConvocante.nombre_abreviado',
                'label'=>'Solt'
            ],
            [
                'attribute'=>'senal_solicitada',
                'label'=>'Señal',
                'value'=>function($objeto,$clave,$index,$widget){
                    return ($objeto->senal_solicitada)?'En Vivo':'';
                },
            ],
            [
                'attribute'=>'id_estado',
                'label'=>'Estado',
                'format'=>'html',
                'value'=>function($objeto,$clave,$index,$widget){
                    if($objeto->idDireccion->id_pais == Yii::$app->params['constantes']['idVenezuela']){
                        return $objeto->idDireccion->estado->nom_estado;
                    }else{
                        $retorno="<b><u>INTERNACIONAL</u></b><br>".$objeto->idDireccion->idPais->nom_pais;
                        return $retorno;
                    }
                },
            ],
            [
                'attribute'=>'fecha',
                'label'=>'Fecha',
                'value'=>function($objeto,$clave,$index,$widget){
                    return date("d/m/Y",strtotime($objeto->fecha));
                }
            ],
            'descripcion:ntext',
            'vocero',
            //FIN DATOS GENERALES***********************************

            //DIFUSIÓN**********************************************
            [
                'attribute'=>'estatus',
                'format'=>'html',
                'label'=>'Estatus'
            ],
            [
                'label'=>'Entidades',
                'format'=>'html',
                'value'=>'mediosConvocados'
            ],
            [
                'label'=>'Autoridades Asistentes',
                'format'=>'html',
                'value'=>'mediosAsistentes'
            ],
            //FIN DIFUSIÓN*******************************************

            //CLASIFICACIÓN EDITORIAL********************************
            [
                'label'=>'Propagación',
                'format'=>'html',
                'value'=>'propagacion'
            ],
            //FIN CLASIFICACIÓN EDITORIAL****************************


            //CLASIFICACIÓN COBERTURA********************************
            /*[
                'label'=>'Cobertura',
                'format'=>'html',
                'value'=>'cobertura'
            ],*/
            //FIN CLASIFICACIÓN COBERTURA****************************


            [
                'label'=>'Observaciones',
                'format'=>'html',
                'value'=>'obtenerObservaciones'
            ]
        ];
    }

    /*if($tipoUsuario == 'COMUNICACIONES'){
        $columnas=[
             //DATOS GENERALES***************************************
            [
                'label'=>'Acciones',
                'format'=>'raw',
                'value'=>function($objeto,$clave,$index,$widget){
                    return $objeto->botonesConvocatorias;
                }
            ],

            [
                'attribute'=>'id',
                'label'=>'Nro. Conv'
            ],
            [
                'attribute'=>'fecha',
                'label'=>'Fecha',
                'format'=>'html',
                'value'=>function($objeto,$clave,$index,$widget){
                    $mostrar=date("d/m/Y",strtotime($objeto->fecha))."<br>";
                    $mostrar.="<b>".date("h:i a",strtotime($objeto->hora))."</b><br>";
                    $mostrar.=$objeto->idDireccion->estado->nom_estado;
                    return $mostrar;
                }
            ],
            'descripcion:ntext',
            [
                'attribute'=>'idDireccion.direccionCompleta',
                'label'=>'Dirección'
            ],
            'vocero',
            'vocero_rol',
            [
                'attribute'=>'idEstatus.nom_descripcion',
                'label'=>'Estatus'
            ],
            [
                'attribute'=>'idConvocante.nombreCompleto',
                'label'=>'Solicitante'
            ],
            //FIN DATOS GENERALES***********************************


            //DATOS DIFUSIÓN***************************************
            [
                'label'=>'Contactos',
                'format'=>'html',
                'value'=>'contactos'
            ],
            [
                'label'=>'Medios Convocados',
                'format'=>'html',
                'value'=>'mediosConvocados'
            ],
            [
                'label'=>'Medios Asistentes',
                'format'=>'html',
                'value'=>'mediosAsistentes'
            ],
            [
                'attribute'=>'senal_solicitada',
                'label'=>'Señal',
                'value'=>function($objeto,$clave,$index,$widget){
                    return ($objeto->senal_solicitada)?'En Vivo':'';
                },
            ],
            //FIN DATOS DIFUSIÓN***************************************

            [
                'label'=>'Propagación',
                'format'=>'html',
                'value'=>'propagacion'
            ],
        ];
    }*/
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo GridView::widget([
                'pager'=> [
                    'firstPageLabel' => 'Primera',
                    'lastPageLabel' => 'Ultima',
                ],
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions'=>['id'=>'tabla-convocatorias','class'=>'table table-striped table-bordered','style'=>'font-size:9pt;'],
                'columns' => $columnas
            ]); ?>
        </div>
    </div>
<?php //Pjax::end(); ?></div>
