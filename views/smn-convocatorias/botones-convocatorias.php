<?php
use webvimark\modules\UserManagement\models\User;
use app\controllers\SimonController;
use app\models\SmnUser;
use app\models\SmnMediosConvocatorias;

$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id);


$modificar=true;//Bandera para saber si la entidad solicitante conectada registró la convocatoria...
if($tipoUsuario == 'ENTE ADSCRITO'){
    $idConvocante=$usuario->idUsuarioSolicitante->id_solicitante;
    if($idConvocante != $model->id_convocante){
        $modificar=false;
    }
}
?>

<div class="btn-group">
    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Acciones <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <?php if(User::canRoute("/smn-convocatorias/view")){ ?>
            <li><a href="#" class="accion" data-accion="consultar" data-id="<?php echo $model->id ?>"><i class="fa fa-fw fa-search"></i>&nbsp;Consultar</a></li>
        <?php } ?>
        
        <?php if(User::canRoute("/smn-convocatorias/update") && $modificar){?>
            <li><a href="<?php echo Yii::$app->homeUrl."smn-convocatorias/update?id=".$model->id?>"><i class="fa fa-fw fa-pencil"></i>&nbsp;Modificar</a></li>
        <?php } ?>
        
        <li role="separator" class="divider"></li><!--Separador-->
        
        <?php if($tipoUsuario == 'COMUNICACIONES'){ //Sólo para usuarios MIPPCI?>
            <?php if($model->id_estatus == Yii::$app->params['convocatorias']['idPorConfirmar'] && User::canRoute("/smn-convocatorias/confirmar")){ ?>
            	<li><a href="<?php echo Yii::$app->homeUrl."smn-convocatorias/confirmar?id=".$model->id?>"><i class="fa fa-fw fa-check"></i>&nbsp;Confirmar</a></li>
            <?php } ?>

            <?php if($model->id_estatus == Yii::$app->params['convocatorias']['idConfirmadas'] && User::canRoute("/smn-convocatorias/convocar")){ ?>
                <li><a href="<?php echo Yii::$app->homeUrl."smn-convocatorias/convocar?id=".$model->id?>"><i class="fa fa-fw fa-microphone"></i>&nbsp;Convocar Autoridades</a></li>
            <?php } ?>
        <?php } ?>

        <?php if(User::canRoute("/smn-convocatorias/confirmar-medios")  && $tipoUsuario == 'COMUNICACIONES'){ ?>
            <?php
            $usuario=SimonController::getUsuario(Yii::$app->user->id);
            $idMedio=$usuario->usuarioMedio->id_medio;

            $modelMedConv=SmnMediosConvocatorias::find()->where(['id_convocatoria'=>$model->id,'id_medio'=>$idMedio])->all();

            if(count($modelMedConv) > 0){
                ?>
                <?php if($model->id_estatus == Yii::$app->params['convocatorias']['idConfirmadas']){ ?>
                    <li><a href="<?php echo Yii::$app->homeUrl."smn-convocatorias/confirmar-medios?id=".$model->id?>"><i class="fa fa-fw fa-users"></i>&nbsp;Confirmar Asistencia</a></li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </ul>
</div>