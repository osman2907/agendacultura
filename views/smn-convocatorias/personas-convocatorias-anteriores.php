<script type="text/javascript">
	$(document).ready(function(){
		$(".chosen-select").chosen();
	});
</script>

<?php
use yii\helpers\Html;

if(count($personas)==0){
	echo "<a class='letra-roja negrita'>Este convocante no posee personas contactos. Debe registrarlas presionando el boton Contacto Nuevo.</a><br>";
}


//Convertir datos de contactos a mayúsculas.
foreach ($personas as $key => $value){
	$personas[$key]=strtoupper($value);
}

echo "El siguiente listado muestra las personas que ya se encuentran asociadas al convocante";

echo Html::dropDownList('SmnConvocatorias[id_persona]', null,$personas,['id'=>'smn-convocatorias-id_persona','data-placeholder'=>'Seleccione las personas de contacto','class'=>'chosen-select form-control','multiple'=>'multiple']);

echo Html::button('Contacto Nuevo',['id'=>'contacto-nuevo','class'=>'btn btn-sm btn-primary margen-top']);
?>