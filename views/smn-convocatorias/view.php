<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = "Consultar Convocatoria - Nro: ".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-convocatorias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])*/ ?>
    </p>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
        <li><a data-toggle="tab" href="#datos-medio">DATOS AUTORIDADES</a></li>
        <li><a data-toggle="tab" href="#forma-difusion">FORMA DE DIFUSIÓN</a></li>
    </ul>

    <div class="tab-content">
        <div id="datos-registro" class="tab-pane fade in active">
            <br>
            <?php echo $this->render('_view-registro',compact('model','listTiposMediosSolicitados')); ?>
        </div>
        <div id="datos-medio" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-medios-convocados',compact('model')); ?>
        </div>
        <div id="forma-difusion" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-forma-difusion',compact('model')); ?>
        </div>
    </div>
</div>
