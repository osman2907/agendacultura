<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<?php
use yii\helpers\Html;
use app\controllers\SimonController;
use app\models\SmnUser;

$usuario=SimonController::getUsuario(Yii::$app->user->id);
$modelUser=new SmnUser;
$tipoUsuario=$modelUser->getTipoUsuario($usuario->id);
?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'descripcion')->textarea(['rows' => 3]) ?>
    </div>
</div>

<div class="row">
    
    <?php  if($tipoUsuario == 'MIPPCI'){?>
        <div class="col-md-4">
            <?php echo $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(),[
                'language' => 'es',
                'dateFormat' => 'dd/MM/yyyy',
            ])->textInput(['readonly'=>'readonly']);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'hora')->textInput(['class'=>'form-control timepicker']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'id_estructura_gobierno')->dropDownList($listestructuragobierno,['prompt'=>'SELECCIONE'],['enableAjaxValidation'=>true]) ?>
        </div>
    <?php }else{ ?>

         <div class="col-md-6">
            <?php echo $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(),[
                'language' => 'es',
                'dateFormat' => 'dd/MM/yyyy',
            ])->textInput();
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'hora')->textInput(['class'=>'form-control timepicker']) ?>
        </div>

    <?php } ?>
</div>
<fieldset class="scheduler-border">
    <legend class="scheduler-border">Dirección</legend>
    
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'id_pais')->dropDownList($listPaises,['prompt'=>'SELECCIONE']) ?>
        </div>

        <div id="venezuela">
            <div class="col-md-2">
                <?= $form->field($model, 'id_estado')->dropDownList($listEstados,['prompt'=>'SELECCIONE']) ?>
            </div>
            <div class="col-md-1 div-horas-criticas">
                <?= Html::button('Horas Críticas', ['id'=>'horas-criticas','class' =>'btn btn-info']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'id_ciudad')->dropDownList($listCiudades) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'id_municipio')->dropDownList($listMunicipios) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'direccion')->textArea() ?>
        </div>
    </div>
</fieldset>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'id_convocante')->dropDownList($listSolicitantes,['prompt'=>'SELECCIONE']) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'id_coconvocante')->dropDownList($listCoconvocantes,['prompt'=>'SELECCIONE']) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'vocero')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'vocero_rol')->textInput() ?>
    </div>

    <div class="col-md-2">
        <label class="control-label" for="smnconvocatorias-senal_solicitada">Señal Solicitada</label>
        <div class="[ form-group ]">
            <input type="checkbox" name="SmnConvocatorias[senal_solicitada]" id="smnconvocatorias-senal_solicitada" autocomplete="off" class="roles" value="219" <?php echo ($model->senal_solicitada)?'checked':''; ?>/>
            <div class="[ btn-group ]">
                <label for="smnconvocatorias-senal_solicitada" class="[ btn btn-primary ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span></span>
                </label>
                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                    En Vivo
                </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Personas de Contacto</legend>
            <?php if($model->isNewRecord){?>
                <div id="personas">
                    <a class="letra-roja negrita">Por favor seleccione el Convocante para habilitar esta opción.</a>
                </div>
            <?php
            }else{
                echo "<div id='personas'>";
                    echo "<b>La siguiente lista muestra las personas asociadas al ente convocante, si desea asignar alguno(a) a la convocatoria puede seleccionarlo(a)</b>";
                    echo Html::dropDownList('SmnConvocatorias[id_persona]', null,$personas,['id'=>'smn-convocatorias-id_persona','data-placeholder'=>'Seleccione las personas de contacto','class'=>'chosen-select form-control','multiple'=>'multiple']);
                    echo "<br><br>";
                    echo "<ul class='list-group' id='lista-personas'>";
                        $convocatoriasPersonas=$model->smnConvocatoriasPersonas;
                        foreach ($convocatoriasPersonas as $convocatoriaPersona){
                            $idPersona=$convocatoriaPersona->idPersona->id;
                            $cedula=$convocatoriaPersona->idPersona->cedula;
                            $nombre=strtoupper($convocatoriaPersona->idPersona->nombre);
                            $apellido=strtoupper($convocatoriaPersona->idPersona->apellido);
                            echo "<li class='list-group-item li-personas' data-id='$idPersona'>";
                                echo  "$cedula - $nombre $apellido";
                                echo "<span class='pull-right'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-persona'></span></span>";
                            echo "</li>";
                        }
                    echo "</ul>";

                    echo Html::button('Contacto Nuevo',['id'=>'contacto-nuevo','class'=>'btn btn-sm btn-primary margen-bottom']);
                echo "</div>";
            }?>
        </fieldset>
    </div>

    <div class="col-md-4">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Medios Solicitados</legend>
            <?php
            $medios=explode(";",$model->medios_solicitados);
            foreach($listTiposMediosSolicitados as $key => $value){
                ?>
                <div class="[ form-group ]">
                    <input type="checkbox" name="SmnConvocatorias[medios_solicitados][<?php echo $key?>]" id="smnconvocatorias-medios_solicitados_<?php echo $key?>" autocomplete="off" class="roles" value="<?php echo $key?>" <?php echo (in_array($key,$medios))?'checked':'' ?>/>
                    <div class="[ btn-group ] ancho-total">
                        <label for="smnconvocatorias-medios_solicitados_<?php echo $key?>" class="[ btn btn-primary ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span></span>
                        </label>
                        <label for="fancy-checkbox-primary" class="[ btn btn-default active ]" style="width:80%">
                            <?php echo $value ?>
                        </label>
                    </div>
                </div>
                <?php
            }
            ?>
        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Archivos</legend>
            <?php if($model->isNewRecord){ ?>
                <div class="listado-items"><!--No eliminar la clase listado-items se utiliza en la JS-->
                    <div class="form-group field-archivo required">
                        <div class="input-group">
                            <input type="file" class="form-control archivo" name="SmnConvocatorias[archivo][]">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                            </span>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>
            <?php }else{?>
                <div class="form-group field-archivo required">
                    <div class="negrita">Si desea agregar un archivo nuevo presione examinar y luego el botón subir.</div>
                    <br>
                    <div class="input-group">
                        <input type="file" class="form-control archivo" name="SmnConvocatorias[archivo][]">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success subir">Subir</button>
                        </span>
                    </div>
                    <div class="help-block"></div>
                </div>

                <ul id="lista-archivos" class="list-group">
                    <?php
                    $archivos=$model->convocatoriasArchivos;
                    foreach($archivos as $archivo){
                        $ruta=Yii::$app->params['convocatorias']['rutaArchivos'].$archivo->archivo;
                        ?>
                        <li class="list-group-item">
                            <?php echo $archivo->archivo; ?>
                            <span class="pull-right">
                                <a href="../<?php echo $ruta?>" target="_blank" class="nd black">
                                    <span class="glyphicon glyphicon-download f15 cursor" ></span>
                                </a>
                                <span class="glyphicon glyphicon-remove-circle f15 cursor black eliminar-archivo" data-id="<?php echo $archivo->id; ?>"></span>
                            </span>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            <?php }?>
        </fieldset>
    </div>

    <div class="col-md-6">
        <?php
        if($model->isNewRecord){
            echo $form->field($model, 'observaciones')->textArea();
        }else{
            ?>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Observaciones</legend>
                <?php
                echo Html::button('Observación Nueva',['id'=>'observacion-nueva','class'=>'btn btn-sm btn-primary margen-bottom']);
                echo "<ul class='list-group' id='lista-observaciones'>";
                    $convocatoriasObservaciones=$model->idObservaciones;
                    foreach ($convocatoriasObservaciones as $convocatoriaObservacion){
                        $idObservacion=$convocatoriaObservacion->id;
                        $observacion=$convocatoriaObservacion->observacion;
                        $tipoCreador=$convocatoriaObservacion->tipo_creador;
                        echo "<li class='list-group-item li-observaciones' data-id='$idObservacion'>";
                            echo "<span class='pull-right'><span class='glyphicon glyphicon-remove-circle f15 cursor remover-observacion'></span></span>";
                            echo "<b>$tipoCreador</b><br>";
                            echo "$observacion";
                        echo "</li>";
                    }
                echo "</ul>";
                ?>
            </fieldset>
            <?php
        }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <!--<?= $form->field($model, 'agenda_comunicacional')->checkbox() ?>-->
    </div>
</div>

<?php
/*echo "<pre>";
print_r($personas);
echo "</pre>";*/
?>
