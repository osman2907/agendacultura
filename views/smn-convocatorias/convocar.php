<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmnConvocatorias */

$this->title = "Convocar Autoridades";
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Convocar Medios';
?>

<script type="text/javascript">
    $(document).ready(function($){
        $(".chosen-select").chosen();

        $(document).on("click",".remover-convocatoria",function(){
            url=$(this).attr('href');
            bootbox.confirm("¿Seguro que desea remover esta convocatoria?",function(respuesta){
                if(respuesta){
                    document.location.href=url;
                }
            });
            return false;
        });
    });
</script>

<div class="smn-convocatorias-convocar">
    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= "Convocatoria Nro. ".$model->id?></h3>

    <?php
    $form = ActiveForm::begin(['id'=>'convocarMedios',"options" => ["enctype" => "multipart/form-data"]]); 
    $form->enableClientValidation=false;
    $form->enableAjaxValidation=true;
    
    echo $form->field($model,'id')->hiddenInput()->label(false);
    echo $form->field($modelMedConv,'id_convocatoria')->hiddenInput(['value'=>$model->id])->label(false);
    ?>

    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#datos-medios">DATOS DE AUTORIDADES</a></li>
        <li><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
        <li><a data-toggle="tab" href="#datos-confirmacion">DATOS DE CONFIRMACIÓN</a></li>
        <li><a data-toggle="tab" href="#forma-difusion">FORMA DE DIFUSIÓN</a></li>
    </ul>

    <div class="tab-content">
    	<div id="datos-medios" class="tab-pane fade in active">
    		<br>
    		<?= $this->render('_form-convocar',compact('form','model','modelMedConv','listMediosConvocatorias','listSenalAsignada','mediosConvocados')) ?>
    	</div>
        
        <div id="datos-registro" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-registro',compact('model','listTiposMediosSolicitados')); ?>
        </div>

        <div id="datos-confirmacion" class="tab-pane fade">
            <br>
            <?php echo $this->render('_view-confirmacion',compact('model')); ?>
        </div>

        <div id="forma-difusion" class="tab-pane fade">
        	<br>
            <?php echo $this->render('_view-forma-difusion',compact('model')); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>