<?php
use yii\helpers\Html;
use app\models\SmnConvocatoriasTrabajadores;
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 negrita">
			<?php if(count($model->smnMediosConvocatorias) > 0){ ?>
				Las autoridades convocadas para esta pauta son las siguientes:
			<?php }else{ ?>
				<div class="letra-roja">No se han convocado autoridades para esta pauta.</div>
			<?php } ?>
		</div>
	</div>
	<br>
	<div>
		<div class="col-md-12">
			<ul class='list-group'>
			<?php
			$mediosConvocatorias=$model->smnMediosConvocatorias;
			foreach ($mediosConvocatorias as $medioConvocatoria){
				echo "<li class='list-group-item'>";
					echo $medioConvocatoria->idMedio->nom_concesionario;
					$condicion=['id_convocatoria'=>$medioConvocatoria->id_convocatoria,'id_medio'=>$medioConvocatoria->id_medio];
			        $modelConvTrab=SmnConvocatoriasTrabajadores::find()->where($condicion)->all();

			        if(count($modelConvTrab) > 0){
			        	?>
			        	<br><br>
			        	<table class='table table-bordered'>
							<tr class='active'>
								<th class='foto-trabajador-mc'>Foto</th>
								<th>Nombre</th>
								<th>Cargo</th>
								<th>Contacto</th>
							</tr>

							<?php
							foreach ($modelConvTrab as $convocatoriaTrabajador){
								?>
								<tr>
									<td>
										<?php
										if($convocatoriaTrabajador->getFoto()){
						                    $foto=$convocatoriaTrabajador->getFoto()->archivo;
						                    $foto='@web/'.Yii::$app->params['medios']['rutaFotosTrabajadores'].$foto;
						                    echo Html::img($foto,array('width'=>'100%','class'=>'img-thumbnail'));
						                }else{
						                	$foto='@web/images/usuario.jpg';
						                	echo Html::img($foto,array('width'=>'100%','class'=>'img-thumbnail'));
						                }
						                ?>
									</td>
									<td><?php echo strtoupper($convocatoriaTrabajador->idTrabajador->nombreCompleto)?></td>
									<td><?php echo strtoupper($convocatoriaTrabajador->idTipoTrabajador->nom_descripcion) ?></td>
									<td>
										<?php
										$telefonos=$convocatoriaTrabajador->idTrabajador->smnPersonasTelefonos;
										foreach ($telefonos as $telefono){
											echo "<pre>";
											print_r($telefono->telefono->telefono);
											echo "</pre>";
										}
										?>
									</td>
								</tr>
								<?php
							}
							?>
						</table>
						<?php
			        }else{
			        	echo "<div class='letra-roja'>No se ha confirmado la asistencia de ninguna autoridad.</div>";
			        }
				echo "</li>";
			}
			?>
			</ul>
		</div>
	</div>
</div>