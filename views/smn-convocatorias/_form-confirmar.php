<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'id_senal')->dropDownList($listSenal,['prompt'=>'SELECCIONE']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'id_estructura_gobierno')->dropDownList($listestructuragobierno,['prompt'=>'SELECCIONE']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'id_prioridad')->dropDownList($listPrioridad,['prompt'=>'SELECCIONE']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'id_sector')->dropDownList($listSector,['prompt'=>'SELECCIONE']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'id_estatus')->dropDownList($listEstatus,['prompt'=>'SELECCIONE']) ?>
    </div>
</div>