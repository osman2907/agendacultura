<?php
use yii\helpers\Html;
?>

<h4><b>DETALLE DE CONVOCATORIA</b></h4>

<table class="table table-bordered">
	<tr>
		<th class="active" width="30%">Número de Convocatoria:</th>
		<td><?php echo $modelMedConv->idConvocatoria->id ?></td>
	</tr>

	<tr>
		<th class="active">Descripción:</th>
		<td><?php echo $modelMedConv->idConvocatoria->descripcion ?></td>
	</tr>

	<tr>
		<th class="active">Fecha:</th>
		<td><?php echo date("d/m/Y",strtotime($modelMedConv->idConvocatoria->fecha)) ?></td>
	</tr>

	<tr>
		<th class="active">Dirección:</th>
		<td><?php echo $modelMedConv->idConvocatoria->idDireccion->direccion ?></td>
	</tr>

	<tr>
		<th class="active">Señal del Medio:</th>
		<td><?php echo $modelMedConv->idSenal->nom_descripcion ?></td>
	</tr>

	<tr>
		<th class="active">Confirmada el:</th>
		<td><?php echo date("d/m/Y h:i:s a",strtotime($modelMedConv->fecha_confirmado)) ?></td>
	</tr>
</table>



<h4><b>DETALLE DEL MEDIO</b></h4>

<table class="table table-bordered">
	<tr>
		<th class="active" width="30%">Nombre:</th>
		<td><?php echo $modelMedConv->idMedio->identificacion ?></td>
	</tr>

	<tr>
		<th class="active">Dirección:</th>
		<td><?php echo $modelMedConv->idMedio->direccion ?></td>
	</tr>
</table>



<h4><b>DETALLE DE AUTORIDADES QUE ASISTIRÁN A LA PAUTA</b></h4>

<table class="table table-bordered">
	<tr class="active">
		<td class="centrar" colspan="4"><b>AUTORIDADES</b></td>
	</tr>

	<tr class="active">
		<th class="foto-trabajador-mc">Foto</th>
		<th>Nombre</th>
		<th>Cargo</th>
		<th>Contacto</th>
	</tr>

	<?php
	foreach ($modelConvTrab as $convocatoriaTrabajador){
		?>
		<tr>
			<td>
				<?php
				if($convocatoriaTrabajador->getFoto()){
                    $foto=$convocatoriaTrabajador->getFoto()->archivo;
                    $foto='@web/'.Yii::$app->params['medios']['rutaFotosTrabajadores'].$foto;
                    echo Html::img($foto,array('width'=>'100%','class'=>'img-thumbnail'));
                }else{
                	$foto='@web/images/usuario.jpg';
                	echo Html::img($foto,array('width'=>'100%','class'=>'img-thumbnail'));
                }
                ?>			
			</td>
			<td><?php echo strtoupper($convocatoriaTrabajador->idTrabajador->nombreCompleto)?></td>
			<td><?php echo strtoupper($convocatoriaTrabajador->idTipoTrabajador->nom_descripcion) ?></td>
			<td>
				<?php
				$telefonos=$convocatoriaTrabajador->idTrabajador->smnPersonasTelefonos;
				foreach ($telefonos as $telefono){
					echo "<pre>";
					print_r($telefono->telefono->telefono);
					echo "</pre>";
				}
				?>
			</td>
		</tr>
		<?php
	}
	?>
</table>
