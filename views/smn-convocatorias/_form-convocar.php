<?php
use yii\helpers\Html;
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 negrita">
			Por favor seleccione uno por uno los medios a convocar:
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($modelMedConv, 'id_medio')->dropDownList($listMediosConvocatorias,['prompt'=>'SELECCIONE','class'=>'chosen-select alto-chosen']) ?>
		</div>

		<div class="col-md-3">
			<?= $form->field($modelMedConv, 'id_senal_asignada')->dropDownList($listSenalAsignada,['prompt'=>'A CRITERIO DEL CANAL']) ?>
		</div>

		<div class="col-md-3" style="padding-top: 25px">
			<?= Html::submitButton('Convocar',['class'=>'btn btn-success']) ?>
		</div>
	</div>

	<?php if(count($mediosConvocados) > 0){?>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<tr>
						<th class="active">Medio</th>
						<th class="active">Señal Asignada</th>
						<th class="active">Acciones</th>
					</tr>

					<?php
					foreach ($mediosConvocados as $medioConvocado){
						?>
						<tr>
							<td>
								<?php echo $medioConvocado->idMedio->nom_concesionario; ?>
							</td>
							<td><?php echo (empty($medioConvocado->id_senal_asignada))?'A CRITERIO DEL CANAL':$medioConvocado->idSenalAsignada->nom_descripcion ?></td>
							<td>
								<?php echo Html::a('Remover Convocatoria',
										["/smn-convocatorias/remover-convocatoria","idConvocatoria"=>$medioConvocado->id_convocatoria,"idMedio"=>$medioConvocado->id_medio],
										['class'=>'btn btn-danger btn-sm remover-convocatoria']);
								?>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
			</div>
		</div>
	<?php } ?>
	
</div>

<?php
/*echo "<pre>";
print_r($mediosConvocados);
echo "</pre>";*/
?>