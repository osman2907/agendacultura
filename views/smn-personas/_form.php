<?php

use yii\helpers\Html;
use yii\base\Model;
use yii\widgets\ActiveForm;
?>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("change","#id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades(data);
            }else{
                $("#id_ciudad").html('');
                $("#id_municipio").html('');
                $("#id_parroquia").html('');
            }
        });

        $(document).on("change","#id_municipio",function(){
            idMunicipio=$(this).val().trim();
            if(idMunicipio != ''){
                data={id:idMunicipio},
                parroquias=consultarPHP('/smn-personas/listparroquias',data,'html',false);
                $("#id_parroquia").html(parroquias);
            }else{
                $("#id_parroquia").html('');
            }
        });

        $(".agregar-item").click(function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        $(document).on("click",".eliminar-item",function(){
            $(this).parent().parent().parent().remove();
        });

        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                //$(this).parent().parent().children('.help-block').html(validacion);
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                //$(this).parent().parent().children('.help-block').html('');
                $(this).parent().parent().find('.help-block').html('');
            }
        });

        $(document).on("submit","#formularioPersonas",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }
        });

        $(document).on("submit","#formularioCorreos",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
            idPersona=$("#smnpersonas-id").val();
            listarCorreos(idPersona);

            return false;
        });


        $(document).on("click","#agregar-correo",function(){
            idPersona=$("#smnpersonas-id").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });

        $(document).on("click",".eliminar-correo",function(){
            idPersona=$("#smnpersonas-id").val();
            idCorreo=$(this).data("id_correo");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(idPersona);
                }
            });
        });

        if ($("#listado-correos").length > 0){
            idPersona=$("#smnpersonas-id").val();
            listarCorreos(idPersona);
        }

        function listarCorreos(idPersona){
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
            $("#listado-correos").html(respuesta);
        }

        $(document).on("submit","#formularioTelefonos",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            if(respuesta == "error"){
                bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
            }else{
                bootbox.alert('Telefono registrado con éxito');
            }
            idPersona=$('#smnpersonas-id').val();
            listarTelefonos(idPersona);
    
                return false;
        });

         $(document).on("click","#agregar-telefono",function(){
            idPersona=$("#smnpersonas-id").val();
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Telefono",
                message: respuesta
            });
        });

        if($("#listado-telefonos").length > 0){
            idPersona=$("#smnpersonas-id").val();
            listarTelefonos(idPersona);
        }

        function listarTelefonos(idPersona){
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        $(document).on("click",".eliminar-telefono",function(){
            idPersona=$("#smnpersonas-id").val();
            idTelefono=$(this).data("id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Telefono eliminado con éxito');
                    }
                        listarTelefonos(idPersona);
                }
            });
        });

        /*$(document).on("click","#registrar",function(){
            consultarPHP('/smn-personas/update2','','html',false);
        });*/

        listarMunicipiosCiudades=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#id_ciudad").html(ciudades);
        }

        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

    });
</script>


<div class="simonsmn-personas-form">

    <?php $form = ActiveForm::begin(['id'=>'formularioPersonas','enableClientValidation'=>false,'enableAjaxValidation'=>true]); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false);?>
    <div class="row">
        <div class="col-md-4">
            <?php
            $readonly=($model->isNewRecord)?false:'readonly';
            echo $form->field($model, 'cedula')->textInput(['class'=>'solo-numero form-control','maxlength'=>8,'readonly'=>$readonly]);
            ?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($model, 'nombre')->textInput()?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($model, 'apellido')->textInput()?>
        </div>   
    </div>

    <div class="row">    
  
        <div class="col-md-4"> 
           <?= $form->field($direccion, 'id_estado')->dropDownList($listEstados,['id'=>'id_estado','prompt'=>'Seleccione']) ?>
        </div> 
        <div class="col-md-4"> 
          <?= $form->field($direccion, 'id_ciudad')->dropDownList($listCiudades,['prompt'=>'Selseccione','id'=>'id_ciudad']) ?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($direccion, 'id_municipio')->dropDownList($listMunicipios,['prompt'=>'Seleccione','id'=>'id_municipio']); ?>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-4"> 
           <?= $form->field($direccion, 'id_parroquia')->dropDownList($listParroquias,['id'=>'id_parroquia', 'prompt'=>'Seleccione']) ?>
        </div>

        <div class="col-md-8"> 
            <?= $form->field($direccion, 'direccion')->textInput() ?> 
        </div> 
    </div>

    <div class="row">
        <?php if($model->isNewRecord){?>
            <div class="col-md-4">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Correos Electrónicos</legend>
                    <div class="listado-items">
                        <div class="form-group field-correo required">
                            <div class="input-group">
                                <input type="text" id="correo" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Teléfonos</legend>
                    <div class="listado-items">
                        <div class="form-group field-telefono required">
                            <div class="input-group">
                                <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" data-accion-validar="/smn-telefonos/validar-telefonos">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div> 
                    </div>
                </fieldset>
            </div>
        <?php }?>

        <?php if(!$model->isNewRecord){?>
            <div class="col-md-4">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Correos Electrónicos</legend>
                    <div id="listado-correos">
                        
                    </div>
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Teléfonos</legend>
                    <div id="listado-telefonos">
                       
                    </div>
                </fieldset>
            </div>
        <?php }?>

        <div class="col-md-4">
            <?= $form->field($model, 'activo')->hiddenInput()->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'registrar']) ?>
            </div>
        </div> 
    </div>
    
    <?php ActiveForm::end(); ?>
</div>
