<?php 
use yii\helpers\Html;
use yii\base\Model;
use yii\widgets\ActiveForm;
?>

<div class="personas-form">
    <?php $form = ActiveForm::begin(['id'=>'contacto-medio','class'=> 'item-validar','enableClientValidation'=>false,'enableAjaxValidation'=>true]); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false);?>

    <!--En este campo se coloca el id del solicitante cuando se está modificando, si está registrando queda vacío-->
    <?= Html::input('hidden','id_solicitante',$idSolicitante,['id'=>'smnsolicitantes-id']);?>

    <div class="row">
        <div class="col-md-4"> 
            <?= $form->field($model, 'cedula')->textInput(['class' => 'solo-numero form-control', 'readonly'=>'readonly'])?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($model, 'nombre')->textInput()?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($model, 'apellido')->textInput()?>
        </div>   
    </div>

    <div class="row">    
  
        <div class="col-md-4"> 
           <?= $form->field($direccion, 'id_estado')->dropDownList($listEstados,['id'=>'id_estado','prompt'=>'Seleccione']) ?>
        </div> 
        <div class="col-md-4"> 
          <?= $form->field($direccion, 'id_ciudad')->dropDownList($listCiudades,['prompt'=>'Seleccione','id'=>'id_ciudad']) ?>
        </div>
        <div class="col-md-4"> 
            <?= $form->field($direccion, 'id_municipio')->dropDownList($listMunicipios,['prompt'=>'Seleccione','id'=>'id_municipio']); ?>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-4"> 
           <?= $form->field($direccion, 'id_parroquia')->dropDownList($listParroquias,['id'=>'id_parroquia', 'prompt'=>'Seleccione']) ?>
        </div>

        <div class="col-md-8"> 
            <?= $form->field($direccion, 'direccion')->textInput() ?> 
        </div> 
    </div>


    <div class="row">
        <?php if($model->isNewRecord){?>
            <div class="col-md-4">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Correos Electrónicos</legend>
                    <div class="listado-items">
                        <div class="form-group field-correo required">
                            <div class="input-group contacto">
                                <input type="text" id="correo" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Teléfonos</legend>
                    <div class="listado-items">
                        <div class="form-group field-telefono required">
                            <div class="input-group contacto">
                                <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" data-accion-validar="/smn-telefonos/validar-telefonos">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div> 
                    </div>
                </fieldset>
            </div>
        <?php }?>


        <?php if(!$model->isNewRecord){?>
            <div class="col-md-4">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Correos Electrónicos</legend>
                    <div id="listado-correos">
                        
                    </div>
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Teléfonos</legend>
                    <div id="listado-telefonos">
                       
                    </div>
                </fieldset>
            </div>
        <?php }?>

        <div class="col-md-4">
            <?= $form->field($model, 'activo')->hiddenInput()->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
            <?= Html::Button($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'boton-registrar-contacto']) ?>
            </div>
        </div> 
    </div>
    
    <?php ActiveForm::end(); ?>
</div>