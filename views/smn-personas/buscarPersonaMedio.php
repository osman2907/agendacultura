<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SmnPersonas;
?>
<div class="row">
    <div class="col-md-1" style="padding-top:5px;"> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cédula</b>
    </div>
    <div class="col-md-4"> 
		<?= Html::textInput('cedula','', [ 'class' => 'form-control solo-numero', 'id' => 'cedula-contacto' ]); ?>
    </div>
    <div class="col-md-4"> 
		<?= Html::button('Buscar', ['class' => 'btn btn-success','id' => 'enviarContacto']) ?>
    </div>   
</div>
<div id="listado-personas">

</div>
