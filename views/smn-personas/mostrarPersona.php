<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SmnPersonas;
?>

<center>
<br>
    <table class="table table-bordered">
        <tr class="active">
        	<td><b>Cédula</b></td>
            <td><b>Nombre</b></td>
            <td><b>Apellido</b></td>
            <td><b>Acciones</b></td>
        </tr>
            <td><?= $model->cedula."&nbsp;"; ?></td>
            <td><?= $model->nombre."&nbsp;"; ?></td>
            <td><?=$model->apellido."&nbsp;"; ?></td>
            <td>
            	<?= Html::submitbutton('Asociar', ['class' => 'btn btn-xs btn-info','id' => 'asociar-contacto','data-id_persona'=>$model->id]) ?>
            </td>
        </tr>
    </table>
</center>

