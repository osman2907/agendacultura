<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = "Consultar Personas";
$this->params['breadcrumbs'][] = ['label' => 'Personas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simonsmn-personas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table class="table table-bordered">
        <tr class="active">
            <th width="33%">Cédula</th>
            <th width="33%">Nombre</th>
            <th width="33%">Apellido</th>
        </tr>

        <tr>
            <td><?php echo isset($model->cedula)?$model->cedula:''?></td>
            <td><?php echo isset($model->nombre)?$model->nombre:''?></td>
            <td><?php echo isset($model->apellido)?$model->apellido:''?></td>
        </tr>

        <tr class="active">
            <th>Estado</th>
            <th>Ciudad</th>
            <th>Municipio</th>
        </tr>
        
        <tr>
            <td><?php echo isset($model->direccion->estado->nom_estado)?$model->direccion->estado->nom_estado:''?></td>
            <td><?php echo isset($model->direccion->ciudad->nom_ciudad)?$model->direccion->ciudad->nom_ciudad:''?></td>
            <td><?php echo isset($model->direccion->municipio->nom_municipio)?$model->direccion->municipio->nom_municipio:''?></td>
        </tr>

        <tr class="active">
            <th>Parroquia</th>
            <th colspan="2">Dirección</th>
        </tr>

        <tr>
            <td><?php echo isset($model->direccion->parroquia->nom_parroquia)?$model->direccion->parroquia->nom_parroquia:''?></td>
            <td colspan="2"><?php echo isset($model->direccion->direccion)?$model->direccion->direccion:''?></td>
        </tr>

        <tr class="active">
            <th>Correos Electrónicos</th>
            <th>Teléfonos</th>
            <th></th>
        </tr>

        <tr>
            <td>
                <ul>
                <?php
                foreach ($model->smnPersonasCorreos as $smnPersonaCorreo) {
                    echo "<li>".$smnPersonaCorreo->correo->correo."</li>";
                }
                ?>
                </ul>
            </td>
            <td>
                <ul>
                <?php
                foreach ($model->smnPersonasTelefonos as $smnPersonaTelefono) {
                    echo "<li>".$smnPersonaTelefono->telefono->telefono."</li>";
                }
                ?>
                </ul>
            </td>
            <td></td>
        </tr>
    </table>
</div>
