<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = 'Nueva Persona';
$this->params['breadcrumbs'][] = ['label' => 'Personas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simonsmn-personas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model','telefonos','correos','direccion','listEstados',
                'listMunicipios','listParroquias','listCiudades')); ?>

</div>

