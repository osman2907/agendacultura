<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
?>

<div class="row">
    <div class="col-md-1" style="padding-top:5px;"> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cédula</b>
    </div>
    <div class="col-md-2"> 
		<?= Html::textInput('cedula','', [ 'class' => 'form-control solo-numero', 'id' => 'cedula-contacto' ]); ?>
    </div>
    <div class="col-md-4"> 
		<?= Html::button('Buscar', ['class' => 'btn btn-success','id' => 'buscar-persona']) ?>
    </div>   
</div>

<br>

<table id="tabla-contactos" class="table table-bordered">
	<tr class="active">
		<th>Cédula</th>
		<th>Nombre y Apellido</th>
		<th width="10%">Acciones</th>
	</tr>

	<?php if($model->isNewRecord){?>
		<tr id="fila-mensaje">
			<td colspan="3" class="mensaje-tabla">No hay personas registradas</td>
		</tr>
	<?php }?>

	<?php
	if(!$model->isNewRecord){
		$solicitantesContactos=$model->smnSolicitantesContactos;
    	if(count($solicitantesContactos) == 0){
    		?>
    		<tr id="fila-mensaje">
				<td colspan="3" class="mensaje-tabla">No hay personas registradas</td>
			</tr>
    		<?php
    	}else{
            foreach ($solicitantesContactos as $solicitanteContacto){
                $persona=$solicitanteContacto->idPersona;
                ?>
                <tr>
                    <td><?php echo strtoupper($persona->cedula) ?></td>
                    <td><?php echo strtoupper($persona->nombreCompleto) ?></td>
                    <td><input type="button" value="Remover" class="btn btn-xs btn-danger remover-contacto" data-id="<?php echo $persona->id?>" data-cedula="<?php echo $persona->cedula?>"></td>
                </tr>
                <?php
            }
    	}	
	}
	?>
</table>

<?php
/*echo "<pre>";
print_r($model->smnSolicitantesContactos);
echo "</pre>";*/
?>