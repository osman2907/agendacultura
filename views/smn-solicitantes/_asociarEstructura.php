<?php
use yii\helpers\Html;
?>

<tr class="table">
	<td><?php echo $solicitanteestructuragobierno->idEstructuraGobierno->nombre ?></td>
	<td><?php echo empty($solicitanteestructuragobierno->id_vicepresidencia)?'N/A':$solicitanteestructuragobierno->idVicepresidencia->nombre ?></td>
	<td>
		<?= Html::button('Remover',['class'=>'btn btn-xs btn-danger remover-estructura','data-id_estructura_gobierno'=>$solicitanteestructuragobierno->id_estructura_gobierno,'data-id_solicitante'=>$solicitanteestructuragobierno->id_solicitante])?>
	</td>
</tr>