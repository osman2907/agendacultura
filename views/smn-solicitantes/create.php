<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantes */

$this->title = 'Registrar Entes Adscritos';
$this->params['breadcrumbs'][] = ['label' => 'Entes Adscritos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--?= $this->render('_form', [
        'model' => $model,
    ]) ?-->
     <?= $this->render('_form',compact('model','telefonos','correos','direccion','solicitanteestructuragobierno','listestructuragobierno')); ?>


</div>
