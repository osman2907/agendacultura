<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
?>

<script type="text/javascript">
    $(document).ready(function($){

        $(document).on("change","#id_estado",function(){
            idEstado=$(this).val().trim();
            if(idEstado != ''){
                data={id:idEstado},
                listarMunicipiosCiudades(data);
            }else{
                $("#id_ciudad").html('');
                $("#id_municipio").html('');
                $("#id_parroquia").html('');
            }
        });

        $(document).on("change","#id_municipio",function(){
            idMunicipio=$(this).val().trim();
            if(idMunicipio != ''){
                data={id:idMunicipio},
                parroquias=consultarPHP('/smn-personas/listparroquias',data,'html',false);
                $("#id_parroquia").html(parroquias);
            }else{
                $("#id_parroquia").html('');
            }
        });


        $(".agregar-item2").click(function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items2");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item2');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        $(document).on("click",".eliminar-item2",function(){
            $(this).parent().parent().parent().remove();
        });

        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().parent().find('.help-block').html('');
            }
        });

        $(document).on("blur",".item-validar2",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().parent().find('.help-block').html('');
            }
        });

        $("#enviar-formulario").click(function(evento){
            evento.preventDefault();
            errores=ejecutarValidacionItems2();

            if(errores > 0){
                bootbox.alert("Por favor ingrese los datos del formulario correctamente. El mismo le mostrará las alertas");
                return false;
            }

            if($(".remover-contacto").length == 0){
                bootbox.confirm("¿Seguro que desea registrar este solicitante sin contactos?",function(respuesta){
                    if(respuesta){
                        $("#formularioSolicitantes").submit();
                    }
                });
                return false;
            }

            $("#formularioSolicitantes").submit();
        });

        $(document).on("click","#buscar-persona",function(){
            cedulaContacto=$("#cedula-contacto").val();
            if(cedulaContacto == ''){
                bootbox.alert("Por favor ingrese el número de cédula");
                return false;
            }

            if(buscarRegistrados(cedulaContacto)){
                bootbox.alert("La persona que está intentando agregar ya se encuentra registrada");
                return false;
            }

            data={'cedula':cedulaContacto};
            validacion=consultarPHP('/smn-personas/validar-cedula',data,'html',false);

            if(validacion == 'error'){
                bootbox.alert("Ha ocurrido un error. Por favor intente nuevamente");
                return false;
            }

            if(validacion == 1){
               insertarFilaContactos(cedulaContacto);
            }else{
                bootbox.confirm("La cédula ingresada no se encuentra registrada. ¿Desea registrar un nuevo contacto?",function(respuesta){
                    if(respuesta){
                        idSolicitante=$("#smnsolicitantes-id").val();
                        data={'cedula':cedulaContacto,'id_solicitante':idSolicitante};
                        formularioContacto=consultarPHP('/smn-personas/crear-contacto-medio',data,'html',false);
                        
                        bootbox.dialog({
                            size:'large',
                            title:'Registrar Contacto',
                            message: formularioContacto,
                        });
                    }
                });
            }
        });

        $(document).on("click",".remover-contacto",function(){
            fila=$(this).parents('tr');
            idPersona=$(this).data('id');
            idSolicitante=$("#smnsolicitantes-id").val();
            
            bootbox.confirm("¿Seguro que desea remover este contacto?",function(respuesta){
                if(respuesta){
                    fila.remove();
                    if($("#tabla-contactos tr").length == 1){
                        $("#tabla-contactos").append("<tr id='fila-mensaje'><td colspan='3' class='mensaje-tabla'>No hay personas registradas</td></tr>");
                    }

                    data={'id_persona':idPersona, 'id_solicitante':idSolicitante};
                    consultarPHP('/smn-solicitantes/remover-contacto',data,'html',false);
                }
            });
        });

        $(document).on("click",".agregar-item",function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        $(document).on("click",".eliminar-item",function(e){
            $(this).parent().parent().parent().remove();
        });


        $(document).on("click","#boton-registrar-contacto",function(){
            data=$("#contacto-medio").serialize();
            retorno=consultarPHP('/smn-personas/validar-contacto',data,'html',false);

            if(retorno != ''){
                bootbox.alert(retorno);
                return false;
            }

            errores=ejecutarValidacionItems();
            if(errores > 0){
                bootbox.alert("Por favor validar los teléfonos y correos electrónicos");
                return false;
            }

            data=$("#contacto-medio").serialize();
            retorno=consultarPHP('/smn-personas/crear-contacto-medio',data,'json',false);
            bootbox.hideAll();
            bootbox.alert("Contacto registrado con éxito");
            insertarFilaContactos(retorno.cedula);
        });


        
        if ($("#listado-correos").length > 0){
            idSolicitante=$("#smnsolicitantes-id").val();
            listarCorreos(idSolicitante);
        }

        function listarCorreos(idSolicitante){
            data={id_solicitante:idSolicitante};
            respuesta=consultarPHP('/smn-correos/listar-correos-solicitantes',data,'html',false);
            $("#listado-correos").html(respuesta);
        }

        $(document).on("submit","#formularioCorreos",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            respuesta=consultarPHP('/smn-correos/agregar-correos-solicitantes',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
            idSolicitante=$("#smnsolicitantes-id").val();
            listarCorreos(idSolicitante);

            return false;
        });

        $(document).on("click","#agregar-correo",function(){
            idSolicitante=$("#smnsolicitantes-id").val();
            data={id_solicitante:idSolicitante};
            respuesta=consultarPHP('/smn-correos/agregar-correos-solicitantes',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });

        if($("#listado-telefonos").length > 0){
            idSolicitante=$("#smnsolicitantes-id").val();
            listarTelefonos(idSolicitante);
        }

        function listarTelefonos(idSolicitante){
            data={id_solicitante:idSolicitante};
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos-solicitantes',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        $(document).on("submit","#formularioTelefonos",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-solicitantes',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.hideAll('Ha ocurrido un error. por favor intente de nuevo');
            }else{
                bootbox.alert('Telefono registrado con éxito');
            }
            idSolicitante=$('#smnsolicitantes-id').val();
            listarTelefonos(idSolicitante);
            return false;
        });

         $(document).on("click","#agregar-telefono",function(){
            idSolicitante=$("#smnsolicitantes-id").val();
            data={id_solicitante:idSolicitante};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-solicitantes',data,'html',false);
            bootbox.dialog({
                title: "Agregar Telefono",
                message: respuesta
            });
        });

        $(document).on("click",".eliminar-correo",function(){
            idSolicitante=$("#smnsolicitantes-id").val();
            idCorreo=$(this).data("id");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_solicitante:idSolicitante};
                    respuesta=consultarPHP('/smn-correos/eliminar-solicitantes-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(idSolicitante);
                }
            });
        });


        $(document).on("click",".eliminar-telefono",function(){
            idSolicitante=$("#smnsolicitantes-id").val();
            idTelefono=$(this).data("id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este número telefónico?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_solicitante:idSolicitante};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-solicitantes-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Número telefónico eliminado con éxito');
                    }
                    listarTelefonos(idSolicitante);
                }
            });
        });


        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

        ejecutarValidacionItems2=function(){
            $("input").focus();
            $("input").blur();
            cantidadItems=$(".item-validar2").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar2")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

        buscarRegistrados=function(cedula){
            retorno=false;
            cantidad=$(".remover-contacto").length;
            for(i=0; i<cantidad; i++){
                elementoContacto=$(".remover-contacto")[i];
                if($(elementoContacto).data('cedula') == cedula){
                    retorno=true;
                }
            }
            return retorno;
        }

        insertarFilaContactos=function(cedulaContacto){
            solicitanteId=$("#smnsolicitantes-id").val();
            data={'cedula':cedulaContacto,'solicitante_id':solicitanteId};
            filaContacto=consultarPHP('/smn-solicitantes/agregar-contacto',data,'html',false);
            $("#fila-mensaje").remove();
            $("#tabla-contactos").append(filaContacto);
            $("#cedula-contacto").val('');
            $("#cedula-contacto").focus();
        }

        listarMunicipiosCiudades=function(data){
            municipios=consultarPHP('/smn-personas/listmunicipios',data,'html',false);
            $("#id_municipio").html(municipios);
            ciudades=consultarPHP('/smn-personas/listciudades',data,'html',false);
            $("#id_ciudad").html(ciudades);
        }

    });
</script>
<div class="smn-solicitantes-form">

    <?php $form = ActiveForm::begin(['id'=>'formularioSolicitantes']); ?>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#datos-registro">DATOS DE REGISTRO</a></li>
            <li><a data-toggle="tab" href="#datos-contactos">DATOS DE CONTACTOS</a></li>
        </ul>

        <div class="tab-content">
            <div id="datos-registro" class="tab-pane fade in active">
                <br>
                <?= $this->render('_formRegistro',compact('model','form','direccion','correos','telefonos','listestructuragobierno','solicitanteestructuragobierno','listVicepresidencias')); ?>
            </div>
            <div id="datos-contactos" class="tab-pane fade">
                <br>
                <?= $this->render('_formContactos',compact('model')); ?>
            </div>
        </div>

        

        <!-- Jquery Multi Select -->
        <br><br>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'enviar-formulario']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
