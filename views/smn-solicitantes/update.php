<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantes */

$this->title = 'Modificar Entes Adscritos';
$this->params['breadcrumbs'][] = ['label' => 'Entes Adscritos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="smn-solicitantes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form',compact('model','telefonos','correos','direccion','listestructuragobierno','listVicepresidencias'));?>

</div>
