<?php
use yii\helpers\Html;
?>

<div class="vicepresidencia" data-idEg="<?= $estructuraGobierno->id?>">
	<?= Html::label($estructuraGobierno->nombre)?>
	<?= Html::dropDownList("SmnSolicitanteEstructuraGobierno2[$estructuraGobierno->id][id_vicepresidencia]",'',$listVicepresidencias,['class'=>'form-control estructuras-gobiernos','prompt'=>'SELECCIONE']);?>
</div>