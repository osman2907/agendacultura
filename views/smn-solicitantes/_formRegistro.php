<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
?>

<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

<div class="row"> 
    <div class="col-md-4"> 
        <?= $form->field($model, 'nombre')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'nombre_abreviado')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($direccion, 'direccion')->textInput() ?>
    </div> 
</div>
<div class="row">

    <?php if($model->isNewRecord){?>
        <div class="col-md-4">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div class="listado-items2">
                    <div class="form-group field-correo required">
                        <div class="input-group">
                            <input type="text" id="correo" class="form-control item-validar2" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success agregar-item2"><i class="glyphicon glyphicon-plus"></i></button>
                            </span>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="col-md-4">
            <fieldset class="scheduler-border">    
                <legend class="scheduler-border">Teléfonos</legend>
                <div class="listado-items2">
                    <div class="form-group field-telefono required">
                        <div class="input-group">
                            <input type="text" id="telefono" class="form-control item-validar2 tlf solo-numero" name="SmnTelefonos[telefono][]" data-accion-validar="/smn-telefonos/validar-telefonos">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success agregar-item2"><i class="glyphicon glyphicon-plus"></i></button>
                            </span>
                        </div>
                        <div class="help-block"></div>
                    </div> 
                </div>
            </fieldset>
        </div>
    <?php }?>

    <?php if(!$model->isNewRecord){?>
        <div class="col-md-4">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div id="listado-correos">
                    
                </div>
            </fieldset>
        </div>

        <div class="col-md-4">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Teléfonos</legend>
                <div id="listado-telefonos">
                   
                </div>
            </fieldset>
        </div>
    <?php }?>

</div> 
<div class="row">
    <?php if(!$model->isNewRecord){?>
        <div class="col-md-4">
            <?= $form->field($model, 'activo')->checkbox(); ?>
        </div>
    <?php } ?>
</div>

