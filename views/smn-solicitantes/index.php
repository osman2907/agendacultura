<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnSolicitantesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entes Adscritos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Entes Adscritos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'pager'=> [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Ultima',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'nombre_abreviado',
           [
                 'attribute' => 'direccion',
                 'value' => 'idDireccion.direccion'
            ],
            //'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'tipo:boolean',
             'activo:boolean',
            // 'vicepresidencia:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
