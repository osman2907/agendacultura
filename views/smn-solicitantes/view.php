<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmnSolicitantes */

$this->title = "Consultar Entes Adscritos";
$this->params['breadcrumbs'][] = ['label' => 'Entes Adscritos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-solicitantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /*Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])*/ ?>
    </p>

    <table class="table table-bordered">
        <tr class="active">
            <th width="33%">NOMBRE</th>
            <th width="33%">NOMBRE ABREVIADO</th>
            <th width="34%">DIRECCIÓN</th>
        </tr>

        <tr>
            <td><?php echo strtoupper($model->nombre) ?></td>
            <td><?php echo strtoupper($model->nombre_abreviado) ?></td>
            <td><?php echo $model->idDireccion->direccion ?></td>
        </tr>

        <tr class="active">
            <th>CORREOS ELECTRÓNICOS</th>
            <th>TELÉFONOS</th>
            <th>ESTATUS</th>
        </tr>

        <tr>
            <td>
                <ul>
                    <?php
                    foreach ($model->smnSolicitantesCorreos as $solicitanteCorreo){
                        echo "<li>".$solicitanteCorreo->idCorreo->correo."</li>";
                    }
                    ?>
                </ul>
            </td>
            <td>
                <ul>
                    <?php
                    foreach ($model->smnSolicitantesTelefonos as $solicitanteTelefono){
                        echo "<li>".$solicitanteTelefono->idTelefono->telefono."</li>";
                    }
                    ?>
                </ul>
            </td>
            <td><?php echo $model->activo?"<span class='letra-verde negrita'>ACTIVO</span>":"<span class='letra-roja negrita'>INACTIVO</span>"; ?></td>
        </tr>
    </table>

    <?php
    $estructurasGobiernos=$model->smnSolicitanteEstructuraGobiernos;
    if(count($estructurasGobiernos) > 0){
        ?>
        <br>
        <table class="table table-bordered">
            <tr class="active">
                <th>ESTRUCTURA DE GOBIERNO</th>
                <th>VICEPRESIDENCIA</th>
            </tr>

            <?php
            foreach ($estructurasGobiernos as $estructuraGobierno){
                ?>
                <tr>
                    <td><?php echo $estructuraGobierno->idEstructuraGobierno->nombre ?></td>
                    <td>
                        <?php
                        if(!empty($estructuraGobierno->id_vicepresidencia)){
                            echo $estructuraGobierno->idVicepresidencia->nombre;
                        }else{
                            echo "N/A";
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    ?>



    <?php
    $solicitantesContactos=$model->smnSolicitantesContactos;
    if(count($solicitantesContactos) > 0){
        ?>
        <br>
        <table class="table table-bordered">
            <tr class="active">
                <th class="centrar" colspan="3">CONTACTOS</th>
            </tr>

            <tr class="active">
                <th>NOMBRE Y APELLIDO</th>
                <th>TELÉFONOS</th>
                <th>CORREOS ELECTRÓNICOS</th>
            </tr>

            <?php
            foreach ($solicitantesContactos as $solicitanteContacto){
                $persona=$solicitanteContacto->idPersona;
                ?>
                <tr>
                    <td><?php echo strtoupper($persona->nombreCompleto) ?></td>
                    <td>
                        <ul>
                            <?php
                            foreach ($persona->smnPersonasTelefonos as $personaTelefono){
                                echo "<li>".$personaTelefono->telefono->telefono."</li>";
                            }
                            ?>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <?php
                            foreach ($persona->smnPersonasCorreos as $personaCorreo){
                                echo "<li>".$personaCorreo->correo->correo."</li>";
                            }
                            ?>
                        </ul>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    ?>

    <?php
    /*echo "<pre>";
    print_r($model->smnSolicitantesContactos);
    echo "</pre>";*/
    ?>

</div>
