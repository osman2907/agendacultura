<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\Model;
?>


    	<div class="row">
	        <div class="col-md-2">
	        	<?= Html::label('Ingrese la Cédula:', 'cedula', ['class' => '']) ?>
	            <?= Html::input('text', 'cedula', '', ['id'=>'cedula','class'=>'form-control solo-numero','label'=>'Cédula']); ?>
	        </div>
	        <div class="col-md-2 margen-etiqueta">
	        	<?= Html::button('Verificar', ['id'=>'verificar','class' => 'btn btn-primary']) ?>
	        	<?= Html::button('Limpiar', ['class' => 'btn btn-warning']) ?>
	        </div>
	</div>

	<br>
<div class="row">     
    <div class="col-md-4">
        <?= $form->field($model, 'cedula')->textInput(['readonly'=>'readonly']) ?>
    </div> 
    <div class="col-md-4">
        <?= $form->field($model, 'nombre')->textInput(['readonly'=>'readonly']) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'apellido')->textInput(['readonly'=>'readonly']) ?>
    </div>
    <div class="col-md-4"> 
        <?= $form->field($model, 'username')->textInput(['class'=>'form-control minuscula']) ?>
    </div>
 
    <div class="col-md-4">
        <?= $form->field($model, 'id_solicitante')->dropDownList($listSolicitantes,['prompt'=>'seleccione']) ?>
    </div> 
</div>
<div class="row">

        <div class="col-md-4" id="listado-correos">
            <?php if($model->isNewRecord){?>
            <div id="los-correos">
            <fieldset class="scheduler-border">    
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div class="listado-items">
                    <div class="form-group field-correo required">
                        <div class="input-group">
                            <input type="text" id="correo" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                            </span>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>
            </fieldset>
           </div>
    <?php }?> 
        <?php if(!$model->isNewRecord){?>
        <div>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div id="listado-correos">
                    
                </div>
            </fieldset>
        </div>
    <?php }?>
    </div> 

        <div class="col-md-4" id="listado-telefonos">
            <?php if($model->isNewRecord){?>
            <div id="los-telefonos">
            <fieldset class="scheduler-border">    
                <legend class="scheduler-border">Teléfonos</legend>
                <div class="listado-items">
                    <div class="form-group field-telefono required">
                        <div class="input-group">
                            <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" data-accion-validar="/smn-telefonos/validar-telefonos">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                            </span>
                        </div>
                        <div class="help-block"></div>
                    </div> 
                </div>
            </fieldset>
            </div>
     <?php }?> 

    <?php if(!$model->isNewRecord){?>
        <div>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Teléfonos</legend>
                <div id="listado-telefonos">
                   
                </div>
            </fieldset>
        </div>
    <?php }?>
     </div>
</div>
	<fieldset class="scheduler-border">
            <legend class="scheduler-border">Asignación de Roles</legend>
            <?php
            foreach ($listRoles as $rol){
            	?>
            	<div class="col-md-3">
	            	<div class="[ form-group ]">
			            <input type="checkbox" name="roles[<?php echo $rol ?>]" id="<?php echo $rol ?>" autocomplete="off" class="roles" />
			            <div class="[ btn-group ]">
			                <label for="<?php echo $rol ?>" class="[ btn btn-primary ]">
			                    <span class="[ glyphicon glyphicon-ok ]"></span>
			                    <span> </span>
			                </label>
			                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
			                    <?php echo $rol ?>
			                </label>
			            </div>
			        </div>
			    </div>
            	<?php
            }
            ?>
        </fieldset>

