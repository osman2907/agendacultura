<script type="text/javascript">
    $(document).ready(function(){
        
        <?php if(!$model->isNewRecord){ ?>
        if ($("#listado-correos").length > 0)
            listarCorreosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
        
        if($("#listado-telefonos").length > 0)
            listarTelefonosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
        <?php } ?>
            
        $(document).on("click","#verificar",function(){
            limpiar();
            cedula=$("#cedula").val().trim();
            if(cedula == ''){
                    bootbox.alert("Por favor ingrese la cédula del usuario a registrar");
                    return false;
            }

            data = {cedula: cedula},
            usuarioMippci = consultarPHP('/smn-usuarios-mippci/validar-usuario-mippci', data, 'html', false);
            if (usuarioMippci){
                bootbox.alert("La persona que desea registrar ya es un usuario mippci.");
                return false;
            }
            
            data = {cedula: cedula},
            usuarioMedio = consultarPHP('/smn-personas/validar-usuario-medio', data, 'html', false);
            if (usuarioMedio) {
                bootbox.alert("La persona que desea registrar ya es un usuario medio.");
                return false;
            }

            data = {cedula: cedula},
            usuarioSolicitante = consultarPHP('/smn-personas/validar-usuario-solicitante', data, 'html', false);
            if (usuarioSolicitante){
                bootbox.alert("La persona que desea registrar ya es un usuario solicitante.");
                return false;
            }
            

            data={cedula:cedula},
            respuesta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
                
            envio={id:respuesta.id},
            consulta=consultarPHP('/smn-telefonos/buscar-telefono',envio,'json',false);

            if(respuesta.id){
                bootbox.alert("La persona que desea registrar ya existe, complete los datos solicitados");
                $("#smnuser-id_persona").val(respuesta.id);
                $("#smnuser-cedula").val(respuesta.cedula);
                $("#smnuser-nombre").val(respuesta.nombre);
                $("#smnuser-apellido").val(respuesta.apellido);

                data={id_persona:respuesta.id},
                respuesta=consultarPHP('/smn-usuarios-solicitantes/buscar-usuario',data,'json',false);

                if(respuesta){
                    $("#smnuser-username").val(respuesta.usuario);
                    $("#smnuser-username").attr('readonly','readonly');

                    if($("#smnuser-username").val().trim() == ''){
                        $("#smnuser-username").removeAttr('readonly');    
                    }
                }else{
                    $("#smnuser-username").removeAttr('readonly');
                }

                if ($("#listado-telefonos").length > 0) {
                    listarTelefonos(envio);
                }

                if ($("#listado-correos").length > 0) {
                    listarCorreos(envio);
                }

            }else{
                bootbox.alert("El número de cédula no existe por favor ingrese el nombre y apellido de la persona",function(){
                        $("#smnuser-cedula").val(cedula);
                        $("#smnuser-nombre").attr('readonly',false);
                        $("#smnuser-apellido").attr('readonly',false);
                        nuevosTelefonos();
                        nuevosCorreos();
                        $("#correo").removeAttr('readonly');
                        $("#telefono").removeAttr('readonly');
                });
            }

        });
          
        $(document).on("submit", "#formularioCorreos", function (){
            errores=ejecutarValidacionItems2();
            if(errores > 0){
                return false;
            }
            
        data=$(this).serialize();
        respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Correo electrónico registrado con éxito');
            }
             idPersona=$("#smnpersonas-id").val();
            listarCorreos(idPersona);
            
            return false;
        });


    	//funcion para agregar correos a las personas
    	$(document).on("click","#agregar-correo",function(){
            idPersona=$("#smnuser-cedula").val();
            data={cedula:idPersona};
    		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
            data={id_persona:consulta.id};
            respuesta=consultarPHP('/smn-correos/agregar-correos',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correos",
                message: respuesta
            });
        });
        
        //Funcion papara eliminar los correos de las personas
        $(document).on("click",".eliminar-correo",function(){
            idPersona=$("#smnuser-cedula").val();
            data={cedula:idPersona};
    		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
            data={id_persona:consulta.id};
            idCorreo=$(this).data("id_correo");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_persona:consulta.id};
                    respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreos(consulta.id);
                }
            });
        });
 
        
        function listarCorreos(envio){
            data={id_persona:envio};
            respuesta=consultarPHP('/smn-correos/listar-correos',data,'html',false);
            $("#listado-correos").html(respuesta);
        }

    $(document).on("submit", "#formularioTelefonos", function () {
        errores=ejecutarValidacionItems2();
            if(errores > 0){
                return false;
            }
        
        data=$(this).serialize();
        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
            bootbox.hideAll();
            if(respuesta == "error"){
                bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
            }else{
                bootbox.alert('Teléfono registrado con éxito');
            }
            idPersona=$('#smnpersonas-id').val();
            listarTelefonos(idPersona);
        return false;
    });

    $(document).on("click","#agregar-telefono",function(){
        idPersona=$("#smnuser-cedula").val();
        data={cedula:idPersona};
		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
        data={id_persona:consulta.id};
        respuesta=consultarPHP('/smn-telefonos/agregar-telefonos',data,'html',false);
        bootbox.dialog({
            title: "Agregar Telefono",
            message: respuesta
        });
    });

        
        function listarTelefonos(envio){
            data={id_persona:envio};
            respuesta=consultarPHP('/smn-telefonos/listar-telefonos',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        function nuevosTelefonos(){
            data={};
            respuesta=consultarPHP('/smn-telefonos/nuevos-telefonos',data,'html',false);
            $("#listado-telefonos").html(respuesta);
        }

        function nuevosCorreos(){
            data={};
            respuesta=consultarPHP('/smn-correos/nuevos-correos',data,'html',false);
            $("#listado-correos").html(respuesta);
        }
        

        //Funcion papara eliminar los telefonos de las personas
    	$(document).on("click",".eliminar-telefono",function(){
            idPersona=$("#smnuser-cedula").val();
            data={cedula:idPersona};
    		consulta=consultarPHP('/smn-personas/buscar-persona',data,'json',false);
            idTelefono=$(this).data("id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Telefono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_persona:consulta.id};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Telefono eliminado con éxito');
                    }
                    listarTelefonos(consulta.id);
                }
            });
        });
    
       //funcion para limpiar la vista de los usuarios medios
		limpiar=function(){
            telefono =$("#los-telefonos").html();
			$("#smnuser-id_persona").val('');
			$("#smnuser-cedula").val('');
        	$("#smnuser-nombre").val('');
        	$("#smnuser-apellido").val('');
        	$("#smnuser-nombre").attr('readonly','readonly');
        	$("#smnuser-apellido").attr('readonly','readonly');
        	$("#smnuser-username").val('');
            $("#smnuser-username").attr('readonly',false);
            $("#telefono").val('');
            $("#telefono").attr('readonly','readonly');
            $("#correo").val('');
            $("#correo").attr('readonly','readonly');
            $("#listado-telefonos").val('');
            nuevosTelefonos();
            nuevosCorreos();
		}
    	
        $(document).on("click","#limpiar",function(){
    		limpiar();
        });

	    $(document).on("click",".agregar-item",function(){
            campoNuevo=$(this).parent().parent().parent().clone();
            $(campoNuevo).children().children('.correos').removeAttr('id');
            $(campoNuevo).removeClass('has-error');
            $(campoNuevo).removeClass('has-success');
            $(campoNuevo).children('.help-block').html('');
            $(campoNuevo).children().children("input").val('');
            listadoItems=$(this).parents(".listado-items");
            listadoItems.append(campoNuevo);
            $(campoNuevo).children('.input-group').children('span').children().removeClass('agregar-item');
            $(campoNuevo).children('.input-group').children('span').children().removeClass('btn-success');
            $(campoNuevo).children('.input-group').children('span').children().addClass('btn-danger');
            $(campoNuevo).children('.input-group').children('span').children().addClass('eliminar-item');
            $(campoNuevo).children('.input-group').children('span').children().children().removeClass('glyphicon-plus');
            $(campoNuevo).children('.input-group').children('span').children().children().addClass('glyphicon-minus');
        });

        $(document).on("click",".eliminar-item",function(){
            $(this).parent().parent().parent().remove();
        });

        $(document).on("blur",".item-validar",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                //$(this).parent().parent().children('.help-block').html(validacion);
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                //$(this).parent().parent().children('.help-block').html('');
                $(this).parent().parent().find('.help-block').html('');
            }
        });


        $(document).on("blur",".item-validar2",function(){
            data={'valor':$(this).val()};
            accion=$(this).data('accion-validar');
            validacion=consultarPHP(accion,data,'html',false);
            if(validacion){
                $(this).parent().parent().addClass('has-error');
                //$(this).parent().parent().children('.help-block').html(validacion);
                $(this).parent().parent().find('.help-block').html(validacion);
            }else{
                $(this).parent().parent().removeClass('has-error');
                //$(this).parent().parent().children('.help-block').html('');
                $(this).parent().parent().find('.help-block').html('');
            }
        });
    
        ejecutarValidacionItems=function(){
            cantidadItems=$(".item-validar").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }

        ejecutarValidacionItems2=function(){
            cantidadItems=$(".item-validar2").length;
            for(i=0;i<cantidadItems;i++){
                elementoHtml=$(".item-validar2")[i];
                $(elementoHtml).focus();
                $(elementoHtml).blur();
            }
            errores=$(".has-error").length;
            return errores;
        }


        $(document).on("submit","#formularioUsuarios",function(){
            errores=ejecutarValidacionItems();
            if(errores > 0){
                return false;
            }

            data=$(this).serialize();
            consulta=consultarPHP('/smn-usuarios-solicitantes/validar-formulario',data,'html',false);

            if(consulta){
                bootbox.alert(consulta);
                return false;
            }
        });

 });   
</script>

<?php
use yii\helpers\Html;
use yii\base\Model;
use yii\widgets\ActiveForm;
?>
<style type="text/css">
</style>
<div class="smn-user-form">
    
    <?php $form = ActiveForm::begin(['id'=>'formularioUsuarios','enableClientValidation'=>false,'enableAjaxValidation'=>true]); ?>
    
    	<?= $form->field($model, 'id')->hiddenInput()->label(false);?>

    	<?= $form->field($model, 'id_persona')->hiddenInput()->label(false);?>
    
    	<div class="row">
             <?php if($model->isNewRecord){?>
			<div class="col-md-12">
	            <div class="alert alert-info">Por favor verifique el número de cédula para registrar un usuario nuevo.</div>
	        </div>
            <?php }?>
    	</div>

    
<div class="row">
    <?php if($model->isNewRecord){?>
	        <div class="col-md-2">
	        	<?= Html::label('Ingrese la Cédula:', 'cedula', ['class' => '']) ?>
	            <?= Html::input('text', 'cedula', '', ['id'=>'cedula','class'=>'form-control solo-numero','label'=>'Cédula','maxlength'=>8]); ?>
	        </div>
	        <div class="col-md-4 margen-etiqueta">
	        	<?= Html::button('Verificar', ['id'=>'verificar','class' => 'btn btn-primary']) ?>
	        	<?= Html::button('Limpiar', ['id'=>'limpiar','class' => 'btn btn-warning']) ?>
	        </div>
    <?php }?>
</div>

<br>
   
<?php if($model->isNewRecord){?>
    <div class="row">     
        <div class="col-md-4"> 
            <?= $form->field($model, 'username')->textInput(['class'=>'form-control minuscula','readonly'=>'readonly']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cedula')->textInput(['readonly'=>'readonly']) ?>
        </div> 
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['readonly'=>'readonly']) ?>
        </div>
    </div>

    <div class="row">     
        <div class="col-md-4">
            <?= $form->field($model, 'apellido')->textInput(['readonly'=>'readonly']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'id_solicitante')->dropDownList($listSolicitantes,['prompt'=>'seleccione']) ?>
        </div>
    </div>
<?php }?>
      
    <div class="row">
         <?php if(!$model->isNewRecord){?>
        <div class="col-md-2">
            <?= $form->field($model, 'username')->textInput(['class' => 'form-control minuscula', 'readonly' => 'readonly']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cedula')->textInput(['value'=>$model->idPersona->cedula, 'readonly' => 'readonly']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'nombre')->textInput(['value'=>$model->idPersona->nombre]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'apellido')->textInput(['value'=>$model->idPersona->apellido]) ?>
        </div>
       
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4">
                    <?php $model->id_solicitante= $model->idUsuarioSolicitante->idSolicitante->id;?>
                    <?= $form->field($model, 'id_solicitante')->dropDownList($listSolicitantes, []);?>
                </div>
            </div>
            
        </div>
    </div>
        <br> 
    <div class="row">
        <div class="col-md-4">
            <div class="[ form-group ]">
                <input type="checkbox" name="SmnUser[status]" id="<?php echo $model->status ?>" autocomplete="off" class="roles"  <?php if($model->status) echo 'checked'; ?>/>
                <div class="[ btn-group ]">
                    <label for="<?php echo $model->status ?>" class="[ btn btn-primary ]">
                        <span class="[ glyphicon glyphicon-ok ]"></span>
                        <span> </span>
                    </label>
                    <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                        <?php echo 'Activo'; ?>
                    </label>
                </div>
            </div>
        </div>
    </div>
        <br>    
        <?php }?>
     
  <?php if($model->isNewRecord){?>
    <div class="row">
        <div class="col-md-4">
            <div id="listado-telefonos">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Teléfonos</legend>
                    <div class="listado-items">
                        <div class="form-group field-telefono required">
                            <div class="input-group">
                                <input type="text" id="telefono" class="form-control item-validar tlf solo-numero" name="SmnTelefonos[telefono][]" readonly="" data-accion-validar="/smn-telefonos/validar-telefonos">
                                <span class="input-group-btn">
                                    <button type="button" class="agregar-item btn btn-success"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div> 
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="col-md-4" id="listado-correos">
            <div id="los-correos">
                <fieldset class="scheduler-border">    
                    <legend class="scheduler-border">Correos Electrónicos</legend>
                    <div class="listado-items">
                        <div class="form-group field-correo required">
                            <div class="input-group">
                                <input type="text" id="correo" readonly="" class="form-control item-validar" name="SmnCorreos[correo][]" data-accion-validar="/smn-correos/validar-correos">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success agregar-item"><i class="glyphicon glyphicon-plus"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
   </div>
        
 <?php }else{?>
    <table class="table table-bordered" >    
        <tr class="active" >
            <td width="50%"><b>Teléfonos</b></td>
            <td width="50%"><b>Correos</b></td>
        </tr>
        <tr>
            <td id="listado-telefonos" width="50%">

            </td>
            <td width="50%" id="listado-correos">

            </td>
        </tr>
    </table>
 <?php }?>
   
<?php if($model->isNewRecord){?> 
    <div class="row">
        <div class="col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Asignación de Roles</legend>
                <?php
                foreach ($listRoles as $rol){
                	?>
                	<div class="col-md-3">
    	            	<div class="[ form-group ]">
    			            <input type="checkbox" name="roles[<?php echo $rol ?>]" id="<?php echo $rol ?>" autocomplete="off" class="roles" />
    			            <div class="[ btn-group ]">
    			                <label for="<?php echo $rol ?>" class="[ btn btn-primary ]">
    			                    <span class="[ glyphicon glyphicon-ok ]"></span>
    			                    <span> </span>
    			                </label>
    			                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
    			                    <?php echo $rol ?>
    			                </label>
    			            </div>
    			        </div>
    			    </div>
                	<?php }?>
            </fieldset>
        </div>
    </div>
<?php }?> 
    
<?php if(!$model->isNewRecord){?>  
  
    <?php
    $array = array();
    $roles=$model->authAssignments;
    foreach ($roles as $rol){
        array_push($array, $rol->item_name);
    }
    ?>

    <div class="row">
        <div class="col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Asignación de Roles</legend>
                <?php foreach ($listRoles as $rol){ ?>
                    <div class="col-md-3">
                        <div class="[ form-group ]">
                            <input type="checkbox" name="roles[<?php echo $rol ?>]" id="<?php echo $rol ?>" autocomplete="off" class="roles" <?php if(in_array($rol, $array)) echo 'checked'; ?>/>
                            <div class="[ btn-group ]">
                                <label for="<?php echo $rol ?>" class="[ btn btn-primary ]">
                                    <span class="[ glyphicon glyphicon-ok ]"></span>
                                    <span> </span>
                                </label>
                                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                                    <?php echo $rol ?>
                                </label>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </fieldset>
        </div>
    </div>
<?php }?>
    
    <div class="form-group">
        <div class="col-md-4">
            <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div> 
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
/*echo "<pre>";
print_r(Yii::$app->user->ldapServer[0]);
echo "<pre>";*/
?>