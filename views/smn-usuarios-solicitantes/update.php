<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SIMONSmnPersonas */

$this->title = 'Modificar Usuarios Entes Adscritos';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Entes Adscritos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form',compact('model','telefonos','correos','direccion','personas','solicitantes','listSolicitantes','listRoles'));
    ?>

</div>