<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmnUser */

$this->title = 'Registrar Usuarios Entes Adscritos';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Solicitantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--?= $this->render('_form', [
        'model' => $model,
    ]) ?-->
    <?= $this->render('_form',compact('model','telefonos','correos','direccion','personas','solicitantes','listSolicitantes','listRoles','smnUsuariosSolicitantes')); ?>


</div>
