<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\models\rbacDB\Role;
//use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\Pjax;
use webvimark\extensions\GridBulkActions\GridBulkActions;
use webvimark\extensions\GridPageSize\GridPageSize;
use app\models\SmnUser;
use webvimark\modules\UserManagement;
use app\models\User;
//use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmnUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios Entes Adscritos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Registrar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                'username',
                'idPersona.cedula',
                'idPersona.nombre',
                'idPersona.apellido',
            [
                'attribute'=>'idUsuarioSolicitante.idSolicitante.nombre',
                'label'=>'Ente Adscrito'
            ],
              ['class' => 'webvimark\components\StatusColumn',
               'attribute' => 'status',
               'label'=> 'Estatus',
               'optionsArray' => [
                [SmnUser::STATUS_ACTIVE, UserManagementModule::t('back', 'Activo'), 'success'],
                [SmnUser::STATUS_INACTIVE, UserManagementModule::t('back', 'Inactivo'), 'warning'],
                ],
               ],
            ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{regenerar}',
                          'template'=>'{view} &nbsp; {update} &nbsp; {delete} &nbsp; {regenerar}',
                            'buttons'=>[
                              'regenerar' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-lock"></span>', /* $url */ '#', [
                                        'title' => Yii::t('yii', 'Regenerar contraseña'), 'id' => 'RegenerarClave', 'data-userid' => $model->id, 'data-user' => $model->username, 'data-idpersona' => $model->id_persona
                                ]);                                
                                }
                          ]                            
                            ],

            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>