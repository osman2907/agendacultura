<?php
use app\models\SmnUser;
use app\models\Correos;
use app\models\SmnPersonas;

?>

<?php if($clave=='noAplica'){?>
    <table width='750' height='158'>
        <tr><th style="text-aign:right;">REGISTRO DE USUARIO</th>
        </tr>
            <th style='font-size: 13px' style="text-align:left;">Sr(a):<?php echo ' '.$modeloUser->idPersona->nombre; echo ' '.$modeloUser->idPersona->apellido ?> El Ministerio del Poder Popular para la Comunicación e Información le da la Bienvenida. Su registro al Sistema se ha completado satisfactoriamente. Sus datos de acceso Son:</th>
                <br>
                <tr style='font-size: 12px'>
                  <td>
                      <b>Usuario:<?php echo ' ' .$modeloUser->username ?></b><br>
                        <p style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;color: #999999;'><a href='http://simon.mippci.gob.ve/'>simon.mippci.gob.ve</a>
                        </p>
                  </td>
                </tr>
            <th style='font-size:10px; text-align:left; color:#FF0000'>Su contraseña es la misma que se le asignó al momento de su registro</th>
    </table>
<?php }else{ ?>
    <table width='750' height='158'>
        <tr><th style="text-aign:right;">REGISTRO DE USUARIO</th>
        </tr>
            <th style='font-size: 13px' style="text-align:left;">Sr(a):<?php echo ' '.$modeloUser->idPersona->nombre; echo ' '.$modeloUser->idPersona->apellido ?> El Ministerio del Poder Popular para la Comunicación e Información le da la Bienvenida. Su registro al Sistema se ha completado satisfactoriamente. Sus datos de acceso Son:</th>
                <tr style='font-size: 12px'>
                  <td>
                      <b>Usuario:<?php echo ' ' .$modeloUser->username ?></b><br>
                      <b>Contraseña:<?php echo ' '.$clave ?></b><br>
                        <p style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;color: #999999;'><a href='http://simon.mippci.gob.ve/'>simon.mippci.gob.ve</a>
                        </p>
                  </td>
                </tr>                        
    </table>
<?php } ?>