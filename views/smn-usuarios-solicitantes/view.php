<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\SmnUser;
use webvimark\modules\UserManagement;
use webvimark\modules\UserManagement\UserManagementModule;
use app\models\SmnCorreos;
use app\models\SmnTelefonos;
use app\models\SmnPersonas;
use app\models\SmnSolicitantes;
use app\models\SmnPersonasCorreos;
use app\models\SmnPersonasTelefonos;

/* @var $this yii\web\View */
/* @var $model app\models\SmnUser */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Solicitantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

<table class="table table-bordered">
        <tr class="active">
            <th width="33%">CÉDULA</th>
            <th width="33%">NOMBRE</th>
            <th width="33%">APELLIDO</th>
           
        </tr>

        <tr>    
                <td><?php echo isset($model->idPersona->cedula)?$model->idPersona->cedula:''?></td>
		<td><?php echo isset($model->idPersona->nombre)?$model->idPersona->nombre:''?></td>
		<td><?php echo isset($model->idPersona->apellido)?$model->idPersona->apellido:''?></td>
                
        </tr>
	<tr class="active">
		<th>USUARIO</th>
		<th>SOLICITANTE</th>
		<th>ROLES</th>
	</tr>

	<tr>
		<td><?php echo $model->username ?></td>
		<td><?php echo strtoupper($model->idUsuarioSolicitante->idSolicitante->nombre) ?></td>
		<td>
			<?php
			$roles=$model->authAssignments;
			foreach ($roles as $rol){
				echo "<span class='label label-primary'>$rol->item_name</span>&nbsp;&nbsp;";
			}
			?>
		</td>
	</tr>
        <tr class="active">
            <th>Correos Electrónicos</th>
            <th>Teléfonos</th>
            <th></th>
        </tr>

        <tr>
            <td>
                <ul>
                <?php
                 foreach ($model->smnUsuarioCorreos as $smnUsuarioCorreo) {
                    echo "<li>".$smnUsuarioCorreo->correo->correo."</li>";
                }
                ?>
                </ul>
            </td>
            <td>
                <ul>
                <?php
                foreach ($model->smnUsuarioTelefonos as $smnUsuarioTelefono) {
                    echo "<li>".$smnUsuarioTelefono->telefono->telefono."</li>";
                }
                ?>
                </ul>
            </td>
            <td></td>
        </tr>
    </table>
    <br>
<center>
<?= Html::a('Bandeja de Usuarios', ['index'], ['class' => 'btn btn-danger']) ?>
</center>
</div>
