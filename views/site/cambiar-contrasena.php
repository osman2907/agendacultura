<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#btn-cambiar",function(){
			contrasenaActual=$("#contrasena-actual").val();
			contrasenaNueva=$("#contrasena-nueva").val();
			confirmeContrasena=$("#confirme-contrasena").val();

			if(contrasenaActual == '' || contrasenaNueva == '' || confirmeContrasena == ''){
				bootbox.alert("Por favor debe ingresar datos a todos los campos");
				return false;
			}

			data=$("#formulario").serialize();
			resultado=consultarPHP('/site/validar-contrasena',data,'html',false);

			if(!resultado){
				bootbox.alert("La contraseña actual es incorrecta");
				return false;
			}

			if(contrasenaNueva != confirmeContrasena){
				bootbox.alert("La contraseña nueva y la confirmación no coinciden");
				return false;
			}

			if(contrasenaNueva == contrasenaActual){
				bootbox.alert("La nueva contraseña debe ser distinta a la actual");
				return false;
			}

			formulario=$("#formulario");
			bootbox.confirm("¿Seguro que desea modificar la contraseña?",function(respuesta){
				if(respuesta){
					formulario.submit();
				}
			});

			return false;
		});
	});
</script>


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Cambiar Contraseña';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<form id="formulario" method="post">
	<?= Html::input('hidden', 'usuario[username]',$modelUsuario->username, ['class'=>'','id'=>'username']) ?>
	<br>
	<div class="row">
		<div class="col-md-4">
			<?= Html::label('Contraseña Actual', 'contrasena_actual', ['class' => 'required']) ?><br>
			<?= Html::input('password', 'usuario[contrasena_actual]','', ['class' => 'form-control','id'=>'contrasena-actual']) ?>
		</div>

		<div class="col-md-4">
			<?= Html::label('Contraseña Nueva', 'contrasena_nueva', ['class' => 'required']) ?><br>
			<?= Html::input('password', 'usuario[contrasena_nueva]','', ['class' => 'form-control','id'=>'contrasena-nueva']) ?>
		</div>

		<div class="col-md-4">
			<?= Html::label('Confirme Contraseña', 'confirme_contrasena', ['class' => 'required']) ?><br>
			<?= Html::input('password', 'usuario[confirme_contrasena]','', ['class' => 'form-control','id'=>'confirme-contrasena']) ?>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<?= Html::submitButton('Cambiar Contraseña',['class'=>'btn btn-success','id'=>'btn-cambiar']) ?>
		</div>
	</div>
</form>
