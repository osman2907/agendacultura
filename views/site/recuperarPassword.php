<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RecuperarPassword */

use yii\helpers\Html;
use yii\base\Model;
//use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;

$this->title = 'Recuperar Contraseña';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
</style>
<div class="site-recuperarPassword">
    <h1><?= Html::encode($this->title) ?></h1>

     <?php $form = ActiveForm::begin(['id' => 'recuperar-password']); ?>
            <div class="row">
              <div class="col-md-4"> 
                    <?= $form->field($model, 'email') ?>
                     </div>
                     <div class="col-md-4"> 
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
                    </div>
                
                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'recuperar-password-button']) ?>
                    </div>
            </div>
                <?php ActiveForm::end(); ?>


</div>
