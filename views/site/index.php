<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="jumbotron">
        <h2><?php echo $this->title; ?></h2>
        <br>
        <?php echo Html::img('@web/images/mision_cultura.jpg',array('width'=>'300'))?>
        <br><br>
        <?php
        if(Yii::$app->user->isGuest){
            echo Html::a("Iniciar Sesion",['/user-management/auth/login'],['class'=>'btn btn-lg btn-success']);
        }
        ?>
        
 <br><br>
<?php //if(Yii::$app->user->isGuest){
//        echo Html::a("Recuperar Contraseña",['/site/recuperarpassword'], ["class" => "btn btn-primary"]); 
//}?>
    </div>
</div>
