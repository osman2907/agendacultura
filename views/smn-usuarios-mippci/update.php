<?php

use yii\helpers\Html;
use yii\base\Model;
use yii\widgets\ActiveForm;
//use yii\helpers\Url;

$this->title = 'Actualizar usuario Mippci';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios MIPPCI', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="text/javascript">
    $(document).ready(function(){
        if ($("#listado-correos").length > 0)
            listarCorreosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
        
        if($("#listado-telefonos").length > 0)
            listarTelefonosMippci(document.getElementsByName("SmnUser[id_persona]")[0].value);
        
         $(document).on("click",".eliminar-telefono",function(){
            idPersona=document.getElementsByName("SmnUser[id_persona]")[0].value;
            idTelefono=$(this).attr("data-id_telefono");
            bootbox.confirm('¿Seguro que desea eliminar este Teléfono?',function(respuesta){
                if(respuesta){
                    data={id_telefono:idTelefono, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-telefonos/eliminar-personas-telefonos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Teléfono eliminado con éxito');
                    }
                    listarTelefonosMippci(idPersona);
                }
            });
        });
        
        $(document).on("click",".eliminar-correo",function(){
            idPersona=document.getElementsByName("SmnUser[id_persona]")[0].value;
            idCorreo=$(this).attr("data-id_correo");
            bootbox.confirm('¿Seguro que desea eliminar este correo electrónico?',function(respuesta){
                if(respuesta){
                    data={id_correo:idCorreo, id_persona:idPersona};
                    respuesta=consultarPHP('/smn-correos/eliminar-personas-correos',data,'html',false);
                    if(respuesta == "error"){
                        bootbox.alert('Ha ocurrido un error. Por favor intente de nuevo');
                    }else{
                        bootbox.alert('Correo electrónico eliminado con éxito');
                    }
                    listarCorreosMippci(idPersona);    
                }
            });
        });

        $(document).on("click","#agregar-telefono",function(){
            idPersona=document.getElementsByName("SmnUser[id_persona]")[0].value;
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-telefonos/agregar-telefonos-mippci',data,'html',false);
            bootbox.dialog({
                title: "Agregar Teléfonos",
                message: respuesta
            });
        });
        
        $(document).on("click","#agregar-correo",function(){
            idPersona=document.getElementsByName("SmnUser[id_persona]")[0].value;
            data={id_persona:idPersona};
            respuesta=consultarPHP('/smn-correos/agregar-correos-mippci',data,'html',false);
            bootbox.dialog({
                title: "Agregar Correo",
                message: respuesta
            });
        });

        $(document).on("click","#registrar",function(){
            selectOir=$("#oir_mippci").siblings("div").children('label').children('span').css('display');
            if(selectOir == 'none'){
                if($("#smnuser-id_oir").val() != ''){
                    bootbox.alert("Por favor deje vacío el campo Oficina de Información Regional ya que sólo aplica para usuarios OIR_MIPPCI");
                    return false;
                }
            }
        });
    });
</script> <?php //Url::remember();?>
<div class="medios-create">
    <h1><?= Html::encode($this->title) ?></h1>

<div class="simonsmn-personas-form">
    
<?php $form = ActiveForm::begin(['id' => 'formularioPersonas', 'enableClientValidation' => false, 'enableAjaxValidation' => true]); ?>
<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'id_persona')->hiddenInput()->label(false); ?>

    <div id="formularioUsuarioMippciUpdate">
    <div class="row">
        <div class="col-md-4">
<?= $form->field($model, 'username')->textInput(['class' => 'form-control minuscula', 'readonly' => 'readonly']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
<?= $form->field($model, 'cedula')->textInput(['value'=>$model->idPersona->cedula, 'readonly' => 'readonly']) ?>
        </div>
        <div class="col-md-4">
<?= $form->field($model, 'nombre')->textInput(['value'=>$model->idPersona->nombre]) ?>
        </div>
        <div class="col-md-4">
<?= $form->field($model, 'apellido')->textInput(['value'=>$model->idPersona->apellido]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Teléfonos</legend>
                <div id="listado-telefonos"></div>
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Correos Electrónicos</legend>
                <div id="listado-correos"></div>
            </fieldset>
        </div>
    </div>


    <div class="row">
        <div class="col-md-2">
            <label class="control-label">Estatus</label>
            <div class="[ form-group ]">
                <input type="checkbox" name="SmnUser[status]" id="<?php echo $model->status ?>" autocomplete="off" class="roles" <?php if($model->status) echo 'checked'; ?>/>
                <div class="[ btn-group ]">
                    <label for="<?php echo $model->status ?>" class="[ btn btn-primary ]">
                        <span class="[ glyphicon glyphicon-ok ]"></span>
                        <span> </span>
                    </label>
                    <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                        <?php echo 'Activo'; ?>
                    </label>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?= $form->field($model,'id_oir')->dropDownList($listOirs,['prompt'=>'SELECCIONE']); ?>
        </div>

        <div class="col-md-6" style="padding-top: 10px;">
            <div class="alert alert-info">El campo <b>Oficina de Informacion Regional</b> debemos seleccionarlo sólo cuando estemos registrando un trabajador de una OIR. En caso contrario lo dejamos vacío.</div>
        </div>
    </div>
        
    <?php
        $array = array(); 
        $roles=$model->authAssignments;
        foreach ($roles as $rol){
            array_push($array, $rol->item_name);
        }
    ?>

    <div class="row">
        <div class="col-md-6">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Asignación de Roles</legend>
                <?php
                foreach ($listRoles as $rol) {
                    ?>
                    <div class="col-md-12">
                        <div class="[ form-group ]">
                            <input type="checkbox" name="roles[<?php echo $rol ?>]" id="<?php echo $rol ?>" autocomplete="off" class="roles" <?php if(in_array($rol, $array)) echo 'checked'; ?>/>
                            <div class="[ btn-group ]">
                                <label for="<?php echo $rol ?>" class="[ btn btn-primary ]">
                                    <span class="[ glyphicon glyphicon-ok ]"></span>
                                    <span> </span>
                                </label>
                                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                                    <?php echo $rol ?>
                                </label>
                            </div>
                        </div><br>
                    </div>
                    <?php
                }
                ?>
            </fieldset>
        </div>

        <div class="col-md-6" style="padding-top: 20px;">
            <div class="alert alert-info">
                <div class="centrar negrita">Leyenda de Roles</div><br>
                <div class="justificar">
                    <b>comunicacion_analista: </b>Rol utilizado para los Analistas de Comunicación de Gobierno. Los mismos se encargan de confirmar las convocatorias.<br><br>

                    <b>articulacion_analista: </b>Rol utilizado para los Analistas de Articulación con Medios. Los mismos se encargan de seleccionar los Medios de Comunicación siempre y cuando las convocatorias estén confirmadas.<br><br>

                    <b>oir_mippci: </b>Rol utilizado para los trabajadores de la Dirección de Línea de las OIR y los trabajadores de las OIR.
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'registrar']) ?>
            </div>
        </div> 
    </div>
    </div>
        
<?php ActiveForm::end(); ?>
</div>
</div>
