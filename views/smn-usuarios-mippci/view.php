<?php
use yii\helpers\Html;

$this->title = 'Detalles del usuario';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios MIPPCI', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="text/javascript">
    $(document).ready(function(){
        var id_persona = "<?php echo $model->idPersona->id; ?>";
        if ($("#listado-correos").length > 0)
            listarCorreosMippci(id_persona);
        
        if($("#listado-telefonos").length > 0)
            listarTelefonosMippci(id_persona);
        
        $(".eliminar-telefono").remove();
        $(".eliminar-correo").remove();
        $(".btn-xs").remove();
    });
</script>

<h1><?= Html::encode($this->title) ?></h1>

<br>

<table class="table table-bordered">
	<tr class="active">
		<th width="33%">CÉDULA</th>
		<th width="33%">NOMBRES</th>
		<th width="33%">APELLIDOS</th>
	</tr>

	<tr>
		<td><?php echo isset($model->idPersona->cedula)?$model->idPersona->cedula:'';?></td>
		<td><?php echo isset($model->idPersona->nombre)?$model->idPersona->nombre:'';?></td>
		<td><?php echo isset($model->idPersona->apellido)?$model->idPersona->apellido:'';?></td>
	</tr>

	<tr class="active">
		<th>USUARIO</th>
		<th>ROLES</th>
        <th>OIR</th>
	</tr>

	<tr>
		<td><?php echo $model->username ?></td>
		<td>
			<?php
			$roles=$model->authAssignments;
			foreach ($roles as $rol){
				echo "<span class='label label-primary'>$rol->item_name</span>&nbsp;&nbsp;";
			}
			?>
		</td>
        <td><?php echo isset($model->usuarioOir->id_oir)?$model->usuarioOir->idOir->descripcion:''; ?></td>
	</tr>
    <tr class="active">
        <td><b>Teléfonos</b></td>
        <td><b>Correos</b></td>
        <td><b>&nbsp;</b></td>
    </tr>

    <tr>
        <td id="listado-telefonos"></td>
        <td id="listado-correos"></td>
        <td>&nbsp;</td>
    </tr>

</table>
<br>
<center>
<?= Html::a('Volver', ['index'], ['class' => 'btn btn-danger']) ?>
</center>


<?php
/*echo "<pre>";
print_r($model->authAssignments);
echo "</pre>";*/
?>


