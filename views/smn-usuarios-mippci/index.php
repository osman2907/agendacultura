<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Usuarios Mippci';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smn-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Registrar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                //'email:email',
                ['class' => 'webvimark\components\StatusColumn',
                'attribute' => 'status',
                'label'=> 'Activo',
                ],
                'idPersona.nombre',
                'idPersona.apellido',
                'idPersona.cedula',
                [
                    'class' => 'yii\grid\ActionColumn', 
                    'template' => '{view}  {update}',
                ]
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>