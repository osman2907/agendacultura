<?php
use yii\helpers\Html;

$this->title = 'Crear usuario Mippci';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios MIPPCI', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medios-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form',compact('model','listRoles','listOirs')); ?>
</div>
