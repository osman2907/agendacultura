<?php
/*echo "<pre>";
print_r($model);
echo "</pre>";
exit();*/
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MciDescripcion */

$this->title = $model->nom_descripcion;
$this->params['breadcrumbs'][] = ['label' => 'Descripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mci-descripcion-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_descripcion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_descripcion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nom_descripcion',
            'username',
            'fecha_registro',
            'status_registro:boolean',
           // 'id_descripcion',
        ],
    ]) ?>

    <h2><?= Html::encode("HIJOS DE ".$this->title) ?></h2>

     <p>
        <?= Html::a('Nuevo Hijos', ['crearhijos?id='.$model->id_descripcion], ['class' => 'btn btn-success']) ?>
    </p>
     <?= GridView::widget([
    'pager'=> [
        'firstPageLabel' => 'Primera',
        'lastPageLabel' => 'Ultima',
    ],    
    'dataProvider' => $dataProvider,
    'filterModel'=>$searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'nom_descripcion',
        'username',
        [
            'attribute' => 'fecha_registro',
            'value' => 'fecha_registro',
            'format' => 'raw',
            'options' => ['style' => 'width: 20%;'],
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_registro',
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'id' => 'fecha_registro',
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ])
        ],
        // 'status_registro:boolean',
        // 'id_descripcion',

        [
            'class' => 'yii\grid\ActionColumn', 
            'template' => '{listarhijos}{update}',
            'buttons' => [
                'listarhijos' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, 
                        ['title' => Yii::t('app', 'Ver Hijos'),]
                    );
                }
            ],
            /*'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'listarhijos'){
                    $url ='index.php?r=mci-descripcion/listarhijos?idPadre='.$model->id_descripcion;
                    return $url;
                }
            }*/
        ]

 
    ]
]);


    ?>



</div>
