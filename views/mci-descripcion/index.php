<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Descripciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mci-descripcion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevas Descripciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    

<?= GridView::widget([
    'pager'=> [
        'firstPageLabel' => 'Primera',
        'lastPageLabel' => 'Ultima',
    ],
    'dataProvider' => $dataProvider,
    'filterModel'=>$searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'nom_descripcion',
        'username',
        //'fecha_registro',
        [
            'attribute' => 'fecha_registro',
            'value' => 'fecha_registro',
            'format' => 'raw',
            'options' => ['style' => 'width: 20%;'],
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_registro',
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'id' => 'fecha_registro',
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    //'startView' => 'year',
                ]
            ])
        ],

        [
            'class' => 'yii\grid\ActionColumn', 
            'template' => '{listarhijos}{update}',
            'buttons' => [
                'listarhijos' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, 
                        ['title' => Yii::t('app', 'Ver Hijos'),]
                    );
                }
            ],
        ]

 
    ]
]);


    ?>
<?php Pjax::end(); ?></div>
