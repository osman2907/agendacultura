<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MciDescripcionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mci-descripcion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_padre') ?>

    <?= $form->field($model, 'nom_descripcion') ?>

    <?= $form->field($model, 'cant_hijos') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'status_registro')->checkbox() ?>

    <?php // echo $form->field($model, 'id_descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
