<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MciDescripcion */

$this->title = 'Modifica Descripcion: ' . $model->id_descripcion;
$this->params['breadcrumbs'][] = ['label' => 'Descripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_descripcion, 'url' => ['view', 'id' => $model->id_descripcion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mci-descripcion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
