<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\MciDescripcion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mci-descripcion-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel-body">
    <div class="panel2">
        <div class="row"> 
            <?= $form->field($model, 'id_padre')->hiddenInput()->label(false) ?>
            <div class="col-md-4"> 
               <?= $form->field($model, 'nom_descripcion')->textInput(['maxlength' => true]) ?> 
            </div>
            <?= $form->field($model, 'cant_hijos')->hiddenInput()->label(false) ?>
            <div class="col-md-4"> 
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>  
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'fecha_registro')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Fecha de registro ...'],
                    'pluginOptions' => [
                        'autoclose'=>true
                    ]
                ]);?>
            </div>
        </div>

    <!--?= $form->field($model, 'status_registro')->checkbox() ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
