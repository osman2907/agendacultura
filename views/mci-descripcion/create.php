<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MciDescripcion */

$this->title = 'Nueva Descripción';
$this->params['breadcrumbs'][] = ['label' => 'Descripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mci-descripcion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
