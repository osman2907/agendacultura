--Cambios 2017-03-30
CREATE TABLE "SIMON".smn_convocatorias_archivos
(
   id_convocatoria_archivo serial, 
   id_convocatoria integer NOT NULL, 
   ruta character varying NOT NULL, 
   archivo character varying NOT NULL, 
   extension character varying NOT NULL, 
   PRIMARY KEY (id_convocatoria_archivo), 
   FOREIGN KEY (id_convocatoria) REFERENCES "SIMON".smn_convocatorias (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;


--Cambios 2017-07-26

--Quitar la relación con la tabla de medios en el esquema de SIMON
ALTER TABLE "SIMON".smn_medios_convocatorias
  DROP CONSTRAINT smn_medios_convocatorias_fkey1;

--Crear Relación con el esquema de SIAM.
ALTER TABLE "SIMON".smn_medios_convocatorias
  ADD FOREIGN KEY (id_medio) REFERENCES "SIAM".siam_medios (id_medio) ON UPDATE NO ACTION ON DELETE NO ACTION;



--Cambios 2017-07-31

--Quitar la relación con la tabla de medios en el esquema de SIMON
ALTER TABLE "SIMON".smn_medios_trabajadores
  DROP CONSTRAINT smn_medios_trabajadores_fkey1;

--Quitar relación entre perimetros de los trabajadores con los medios
ALTER TABLE "SIMON".smn_medios_trabajadores_perimetro
  DROP CONSTRAINT smn_medios_trabajadores_perimetro_fk;


--Cambios 2017-08-01
ALTER TABLE "SIMON".smn_convocatoria_trabajadores
  ADD COLUMN id_tipo_trabajador integer NOT NULL DEFAULT 1;
  
ALTER TABLE "SIMON".smn_convocatoria_trabajadores
  ADD FOREIGN KEY (id_tipo_trabajador) REFERENCES "SIMON".mci_descripcion (id_descripcion) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "SIMON".smn_convocatoria_trabajadores
  DROP CONSTRAINT smn_convocatoria_trabajadores_fkey3;
ALTER TABLE "SIMON".smn_convocatoria_trabajadores
  ADD FOREIGN KEY (id_medio) REFERENCES "SIAM".siam_medios (id_medio) ON UPDATE NO ACTION ON DELETE NO ACTION;





--Cambios 2017-11-15
CREATE TABLE "SIMON".smn_oir
(
  id serial NOT NULL,
  descripcion character varying NOT NULL,
  status boolean NOT NULL,
  created_by character varying,
  created_at timestamp without time zone,
  updated_by character varying,
  updated_at timestamp without time zone,
  CONSTRAINT smn_oir_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE "SIMON".smn_oir_estados
(
  id_oir_estado serial NOT NULL,
  id_oir integer,
  id_estado integer,
  CONSTRAINT smn_oir_estados_pkey PRIMARY KEY (id_oir_estado),
  CONSTRAINT smn_oir_estados_id_estado_fkey FOREIGN KEY (id_estado)
      REFERENCES mci_estado (id_estado) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT smn_oir_estados_id_oir_fkey FOREIGN KEY (id_oir)
      REFERENCES "SIMON".smn_oir (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);



--Cambios 2017-11-17
ALTER TABLE "SIMON".smn_oir
  ADD COLUMN id_direccion integer NOT NULL DEFAULT 1;
ALTER TABLE "SIMON".smn_oir
  ADD FOREIGN KEY (id_direccion) REFERENCES "SIMON".smn_direccion (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "SIMON".smn_oir
   ALTER COLUMN id_direccion DROP DEFAULT;




--Cambios 2017-11-29
CREATE TABLE "SIMON".smn_usuarios_oir
(
   id_usuario_oir serial, 
   id_oir integer NOT NULL, 
   id_usuario integer NOT NULL, 
   estado boolean NOT NULL DEFAULT TRUE, 
   PRIMARY KEY (id_usuario_oir), 
   FOREIGN KEY (id_oir) REFERENCES "SIMON".smn_oir (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   FOREIGN KEY (id_usuario) REFERENCES "SIMON"."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
COMMENT ON TABLE "SIMON".smn_usuarios_oir
  IS 'Esta tabla almacena los datos sólo de usuario OIR de los estados, no aplica para usuarios OIR que trabajan en la torre MIPPCI.';

